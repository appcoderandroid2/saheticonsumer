package com.iapps.sehatee.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.Login;
import com.iapps.sehatee.interfaces.OnCompleteHelper;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.interfaces.OnReviewHelper;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.models.UserModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Objects;

import static com.iapps.sehatee.utils.APIs.getAddRemoveFavouriteURL;

/**
 * Created by jpdbiet on 12,June,2020
 */
public class Constants {

    public static final String NO_DISCOUNT = "No Discount";
    public static final String FLAT = "Flat";
    public static final String PERCENTAGE = "Percent";

    public static final SimpleDateFormat sdf_yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_ddMMMyyyy = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_yyyyMMdd_hhmm_a = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_yyyyMMdd_hhmmss = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_ddMMMyyyyComma_hhmm_a = new SimpleDateFormat("dd MMM yyyy, hh:mm a", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_dd_MMM = new SimpleDateFormat("dd MMM", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_HHmmss = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    public static final SimpleDateFormat sdf_hhmm_a = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

    //Action Bar styles
    public static final int AB_MENU_WITH_NOTI = 0;
    public static final int AB_BACK_WITH_NOTI = 1;
    public static final int AB_BACK_WITHOUT_NOTI = 2;
    public static final int AB_BACK_WITH_NOTI_NO_BOTTOM_NAV = 3;
    // FRAGMENT NAMES
    public static final String HOME = "HOME";
    public static final String NOTIFICATIONS = "NOTIFICATIONS";
    public static final String ABOUT_US = "ABOUT_US";
    public static final String CONTACT_US = "CONTACT_US";
    public static final String FEEDBACK = "FEEDBACK";
    public static final String HELP = "HELP";
    public static final String TERMS_N_CONDITIONS = "TERMS_N_CONDITIONS";
    public static final String PRIVACY_POLICY = "PRIVACY_POLICY";
    public static final String MY_PROFILE = "MY_PROFILE";
    public static final String CHANGE_PASSWORD = "CHANGE_PASSWORD";
    public static final String OFFERS = "OFFERS";
    public static final String APPOINTMENTS = "APPOINTMENTS";
    public static final String APPOINTMENT_HISTORY = "APPOINTMENT_HISTORY";
    public static final String PRESCRIPTIONS = "PRESCRIPTIONS";
    public static final String MY_TESTS = "MY_TESTS";
    public static final String REPORTS = "REPORTS";
    public static final String REPORT_DETAILS = "REPORT_DETAILS";
    public static final String SEARCH_DOCTOR = "SEARCH_DOCTOR";
    public static final String SEARCH_NURSE = "SEARCH_NURSE";
    public static final String DOCTOR_DETAILS = "DOCTOR_DETAILS";
    public static final String NURSE_DETAILS = "NURSE_DETAILS";
    public static final String CONFIRMATION = "CONFIRMATION";
    public static final String HOME_DC = "HOME_DC";
    public static final String CUSTOMER_SERVICE = "CUSTOMER_SERVICE";
    public static final String SETTINGS = "MORE_DC";
    public static final String SEARCH_DC = "SEARCH_DC";
    public static final String ALL_CATEGORIES = "ALL_CATEGORIES";
    public static final String SEARCH_PACKAGES = "SEARCH_PACKAGES";
    public static final String SEARCH_TESTS = "SEARCH_TESTS";
    public static final String PACKAGE_DETAILS = "PACKAGE_DETAILS";
    public static final String TEST_DETAILS = "TEST_DETAILS";
    public static final String SETUP_APPO_DETAILS = "SETUP_APPO_DETAILS";
    public static final String SCHEDULE_APPOINTMENT = "SCHEDULE_APPOINTMENT";
    public static final String ALL_REVIEWS = "ALL_REVIEWS";
    public static final String APPOINTMENT_DETAILS = "APPOINTMENT_DETAILS";
    public static final String MY_FAVOURITES = "MY_FAVOURITES";

    // STATIC CONSTANTS

    private static final String TAG = "Constants";
    public static final String LOGIN = "LOGIN";

    public static final String ANDROID = "Android";
    public static final String OTP_NOTIFICATION = "OTP_NOTIFICATION";
    public static final String UPDATE_NOTIFICATION_COUNT = "UPDATE_NOTIFICATION_COUNT";
    public static final String DOCTOR_DETAILS_UPDATED = "DOCTOR_DETAILS_UPDATED";
    public static final String NURSE_DETAILS_UPDATED = "NURSE_DETAILS_UPDATED";
    public static final String APPOINTMENT_DETAILS_UPDATED = "APPOINTMENT_DETAILS_UPDATED";
    public static final String REPORT_DETAILS_UPDATED = "REPORT_DETAILS_UPDATED";
    public static final String APPOINTMENT_HISTORY_DETAILS_UPDATED = "APPOINTMENT_HISTORY_DETAILS_UPDATED";
    public static final String DC_QUERY_UPDATED = "DC_QUERY_UPDATED";

    public static final Pair<Integer, Integer> BANNER_ASPECT_RATIO = Pair.create(8, 3);
    public static final int BANNER_WIDTH = BANNER_ASPECT_RATIO.first;
    public static final int BANNER_HEIGHT = BANNER_ASPECT_RATIO.second;

    public static final String CUSTOMER = "Customer";
    public static final String PROVIDER = "Provider";

    public static final String SPECIALIST = "Specialist";
    public static final String CONSULTANT = "Consultant";
    public static final String HOSPITAL = "Hospital";
    public static final String INDIVIDUAL = "Individual";
    public static final String INSTITUTE = "Institute";
    public static final String ANY_DAY = "All";
    public static final String TOMORROW = "Tomorrow";
    public static final String TODAY = "Today";
    public static final String CLINIC = "Clinic";

    public static final String DOCTOR = "Doctor";
    public static final String NURSE = "Nurse";

    public static final String PRESCRIPTION = "Prescription";
    public static final String REPORT = "Report";

    public static final String PACKAGE = "Package";
    public static final String TEST = "Test";

    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final String OTHER = "Other";

    public static final String REQUESTED = "Requested";
    public static final String ACCEPTED = "Accepted";
    public static final String VISITED = "Visited";
    public static final String REJECTED = "Rejected";
    public static final String CANCELED = "Canceled";
    public static final String FINISHED = "Finished";

    public static final String NA = "NA";

    public static final String PROFILE_UPDATED = "PROFILE_UPDATED";


    //STATIC VARIABLES
    public static String CURRENT_FRAG = HOME;
    private static Prefs prefs;
    private static boolean appInBackground = false;

    public static Prefs getPrefs() {
        return prefs;
    }

    public static void setPrefs(Prefs prefs) {
        Constants.prefs = prefs;
    }

    public static boolean isAppInBackground() {
        return appInBackground;
    }

    public static void setAppInBackground(boolean appInBackground) {
        Constants.appInBackground = appInBackground;
    }

    // COMMON STATIC FUNCTIONS

    public static boolean isOnline(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;

        if (manager != null) networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static Dialog getProgressDialog(Context ctx, int msg) {
        Dialog dialog = new Dialog(ctx);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
            dialog.getWindow().setDimAmount(0.4f);
        }
        dialog.setContentView(R.layout.dialog_progress_layout);
        TextView msgTv = dialog.findViewById(R.id.msgTv);

        msgTv.setText(String.format(Locale.getDefault(), "%s ...", ctx.getString(msg)));

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    public static void showUserRestrictedDialog(Activity ctx) {
        AlertDialog dialog = new AlertDialog.Builder(ctx).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_single_action_btn, null, false);
        dialog.setView(view);

        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView msgTv = view.findViewById(R.id.msgTv);
        Button posBtn = view.findViewById(R.id.posBtn);

        titleTv.setText(ctx.getString(R.string.alert));
        msgTv.setText(ctx.getString(R.string.alert_access_denied_by_admin));
        posBtn.setText(ctx.getString(R.string.try_login_again));

        posBtn.setOnClickListener(v -> {
            getPrefs().clearSp();
            ctx.finish();
            ctx.finishAffinity();
            ctx.startActivity(new Intent(ctx, Login.class));
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.create();
        if (!ctx.isFinishing()) {
            dialog.show();
        }
    }

    public static void showUserRestrictedDialog(Fragment frag) {
        Context ctx = frag.getContext();
        AlertDialog dialog = new AlertDialog.Builder(ctx).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_single_action_btn, null, false);
        dialog.setView(view);

        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView msgTv = view.findViewById(R.id.msgTv);
        Button posBtn = view.findViewById(R.id.posBtn);

        titleTv.setText(frag.getString(R.string.alert));
        msgTv.setText(frag.getString(R.string.alert_access_denied_by_admin));
        posBtn.setText(frag.getString(R.string.try_login_again));

        posBtn.setOnClickListener(v -> {
            getPrefs().clearSp();
            frag.startActivity(new Intent(ctx, Login.class));
            if (frag.getActivity() != null) {
                frag.getActivity().finishAffinity();
            } else {
                System.exit(0);
            }
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.create();
        if (frag.getActivity() != null) {
            if (!frag.getActivity().isFinishing()) {
                dialog.show();
            }
        }
    }

    public static Dialog getDoubleBtnDialog(Context ctx, String title, String msg, String negBtnText, String posBtnText, OnDialogActionHelper helper) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_double_action_btn, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView msgTv = view.findViewById(R.id.msgTv);
        Button posBtn = view.findViewById(R.id.posBtn);
        Button negBtn = view.findViewById(R.id.negBtn);

        titleTv.setText(title);
        msgTv.setText(msg);
        negBtn.setText(negBtnText);
        posBtn.setText(posBtnText);

        posBtn.setOnClickListener((v) -> helper.onPositiveBtnClicked(dialog));
        negBtn.setOnClickListener((v) -> helper.onNegativeBtnClicked(dialog));

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        return dialog;
    }

    public static void updateUserPrefData(UserModel user) {

        getPrefs().setUserId(user.getUserId());
        getPrefs().setUserType(user.getUserType());
        getPrefs().setFullName(user.getFullName());
        getPrefs().setEmail(user.getEmail());
        getPrefs().setIsdCode(user.getIsdCode());
        getPrefs().setPhone(user.getPhone());
        getPrefs().setGender(user.getGender().toLowerCase().equals("male") ? Prefs.MALE : Prefs.FEMALE);
        getPrefs().setDOB(user.getDob());
        getPrefs().setRating(user.getRating());
        getPrefs().setTotalReview(user.getTotalReview());

        getPrefs().setAddress(user.getAddress());
        getPrefs().setCityName(user.getCityName());
        getPrefs().setCityArea(user.getAreaName());
        getPrefs().setPostalCode(user.getPostalCode());

        String address = user.getAddress().equals(NA) ? "" : user.getAddress();
        address += user.getAreaName().equals(NA) ? "" : ", " + user.getAreaName();
        address += user.getCityName().equals(NA) ? "" : ", " + user.getCityName();
        address += user.getPostalCode().equals(NA) ? "" : ", " + user.getPostalCode();
//        address += user.getCountryName().equals(NA) ? "" : ", " + user.getCountryName();
        address = address.isEmpty() ? NA : address;

        getPrefs().setFullAddress(address);
        getPrefs().setProfileImage(user.getProfileImage());
        getPrefs().setAboutMe(user.getAboutMe());

    }

    public static void showToast(Context ctx, int resId) {
        View layout = LayoutInflater.from(ctx).inflate(R.layout.layout_toast, null, false);

        TextView text = layout.findViewById(R.id.tv);
        text.setText(resId);

        Toast toast = new Toast(ctx);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 160);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public static void showToast(Context ctx, String msg) {
        View layout = LayoutInflater.from(ctx).inflate(R.layout.layout_toast, null, false);

        TextView text = layout.findViewById(R.id.tv);
        text.setText(msg);

        Toast toast = new Toast(ctx);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 160);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public static String getFormattedDate(String d) {
        String date = d;
        try {
            date = sdf_ddMMMyyyy.format(Objects.requireNonNull(sdf_yyyyMMdd.parse(d)));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "getFormattedDate: ", e);
        }
        return date;
    }

    public static String getFormattedDateNoYear(String d) {
        String date = d;
        try {
            date = sdf_dd_MMM.format(Objects.requireNonNull(sdf_yyyyMMdd.parse(d)));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "getFormattedDate: ", e);
        }
        return date;
    }

    public static String getFormattedTime(String format24Hr) {
        String time = format24Hr;
        try {
            time = sdf_hhmm_a.format(Objects.requireNonNull(sdf_HHmmss.parse(format24Hr)));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "getFormattedTime: ", e);
        }
        return time;
    }

    public static String getFormattedDateTime(String d) {
        String date = d;
        try {
            date = sdf_ddMMMyyyyComma_hhmm_a.format(Objects.requireNonNull(sdf_yyyyMMdd_hhmm_a.parse(d)));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "getFormattedDateTime: ", e);
            try {
                date = sdf_ddMMMyyyyComma_hhmm_a.format(Objects.requireNonNull(sdf_yyyyMMdd_hhmmss.parse(d)));
            } catch (ParseException ex) {
                ex.printStackTrace();
                Log.e(TAG, "getFormattedDateTime: ", e);
            }
        }
        return date;
    }

    public static String convertSecondsToHMmSs(long millis) {
        long seconds = millis / 1000;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        long d = (seconds / ((60 * 60) * 24)) % 24;
        return d > 0 ? String.format(Locale.ENGLISH, "%02dd %02dh %02dm %02ds", d, h, m, s)
                : h > 0 ? String.format(Locale.ENGLISH, "%02dh %02dm %02ds", h, m, s)
                : String.format(Locale.ENGLISH, "%02dm %02ds", m, s);
    }

    public static int getPxFromDp(Resources resources, int dp) {
        return (int) (dp * ((float) resources.getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public Dialog getSingleBtnDialog(Context ctx, String title, String msg, String posBtnText, OnDialogActionHelper helper) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_single_action_btn, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView msgTv = view.findViewById(R.id.msgTv);
        Button posBtn = view.findViewById(R.id.posBtn);

        titleTv.setText(title);
        msgTv.setText(msg);
        posBtn.setText(posBtnText);

        posBtn.setOnClickListener((v) -> helper.onPositiveBtnClicked(dialog));

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        return dialog;
    }

    public static Context setLocale(@NonNull Context ctx, @NonNull String language) {
        Log.e(TAG, "setLocale: langCode => " + language);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources res = ctx.getResources();
        Configuration config = res.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
        } else {
            config.locale = locale;
        }

        res.updateConfiguration(config, res.getDisplayMetrics());
        ctx = ctx.createConfigurationContext(config);

        if (getPrefs() != null) {
            getPrefs().setAppLanguageCode(language);
        }
        return ctx;
    }

    public static int getDiscountedPrice(int original, int discount, @NonNull String discountType) {
        switch (discountType) {
            case PERCENTAGE:
                return original - (original * discount / 100);
            case FLAT:
                return original - discount;
            default:
                return original;
        }
    }

    public static void addToFavourite(Fragment frag, boolean isFinishing, String id, String type, OnCompleteHelper helper) {
        if (isOnline(frag.getContext())) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", type);
                jsonObject.put("itemId", id);

                final Dialog dialog = getProgressDialog(frag.getContext(), R.string.please_wait);
                if (!isFinishing) {
                    dialog.show();
                }

                VolleyHelper.getInstance(frag.getContext()).addRequest(getAddRemoveFavouriteURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model1 = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model1.getStatus() == 1) {
                            helper.onSuccess();
                        } else {
                            helper.onFailure(model1.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        helper.onFailure(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(frag);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "addToFavourite: ", e);
                showToast(frag.getContext(), R.string.something_went_wrong);
            }
        } else {
            showToast(frag.getContext(), R.string.error_internet_unavilable);
        }
    }

//        "itemId": 1,
//            "type": "Package",
//            "rating": "3",
//            "review": "Good",
//            "reviewDateTime": "2020-07-29 16:23:45"

    public static void showAddReviewDialog(Context ctx, OnReviewHelper helper) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_rate_and_review, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        EditText reviewEt = view.findViewById(R.id.reviewEt);
        Button posBtn = view.findViewById(R.id.posBtn);
        Button negBtn = view.findViewById(R.id.negBtn);

        posBtn.setOnClickListener(v -> {
            float rating = ratingBar.getRating();
            String review = reviewEt.getText().toString().trim();

            if (rating == 0f) {
                helper.onNoRating(dialog);
                return;
            }
            if (review.length() == 0) {
                helper.onReviewEmpty(dialog);
                return;
            }

            helper.onSubmitClicked(dialog, rating, review);

        });

        negBtn.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
    }
}
