package com.iapps.sehatee.utils;

import static com.iapps.sehatee.utils.Constants.CUSTOMER;

/**
 * Created by jpdbiet on 12,June,2020
 */
public class APIs {
    private static final String DEV_URL = "http://intelligentappsolutionsdemo.com/current-project/website/Sehatee/api/";
    //    private static final String LIVE_URL = "";
    private static final String BASE_URL = DEV_URL;
//    private final static String BASE_URL = LIVE_URL;

    public static String getLoginURL() {
        return BASE_URL + "auth/login";
    }

    public static String getLogoutURL() {
        return BASE_URL + "auth/logout";
    }

    public static String getRegisterURL() {
        return BASE_URL + "auth/register";
    }

    public static String getForgotPasswordURL() {
        return BASE_URL + "forgotPassword";
    }

    public static String getResetPasswordURL() {
        return BASE_URL + "resetPassword";
    }

    public static String getChangePasswordURL() {
        return BASE_URL + "changePassword";
    }

    public static String getUpdateDeviceTokenURL() {
        return BASE_URL + "updateDeviceToken";
    } // deviceType= Android / Ios

    public static String getProfileDataURL() {
        return BASE_URL + "getProfile";
    }

    public static String getCityListURL() {
        return BASE_URL + "getCity?countryId=1";
    }

    public static String getCityAreaListURL(String cityId) {
        return BASE_URL + "getArea?cityId=" + cityId;
    }

    public static String getCheckIfSocialAccountExistsURL() {
        return BASE_URL + "auth/checkExistingUser";
    }

    public static String getSocialLoginURL() {
        return BASE_URL + "auth/socialLogin";
    }

    public static String getUploadProfilePicURL() {
        return BASE_URL + "uploadProfilePic";
    }

    public static String getSpecialityURL(String userType) {
        return BASE_URL + "getSpeciality?userType=" + userType;
    } // userType = Doctor/Nurse

    public static String getUpdateProfileURL() {
        return BASE_URL + "updateProfile";
    }

    public static String getDoctorListURL(int pageNum) {
        return BASE_URL + "getDoctorList?page=" + pageNum;
    }

    public static String getSubmitDoctorAppointmentURL() {
        return BASE_URL + "saveDoctorBookig";
    }

    public static String getAppointmentListURL(int pageNum, String appointmentId) {
        return BASE_URL + "getAppointmentList?page=" + pageNum + "&appointmentId=" + appointmentId;
    }

    public static String getSingleAppointmentDetailsURL(String appointmentId) {
        return BASE_URL + "getAppointmentDetails?appointmentId=" + appointmentId;
    }

    public static String getAllInsuranceProvidersURL() {
        return BASE_URL + "getInsurance";
    }

    public static String getNurseListURL(int pageNum) {
        return BASE_URL + "getNurseList?page=" + pageNum;
    }

    public static String getSubmitNurseBookingURL() {
        return BASE_URL + "saveNurseBooking";
    }

    public static String getUploadPrescriptionReportImageURL() {
        return BASE_URL + "uploadPrescriptionReportImage";
    }

    public static String getCancelAppointmentURL() {
        return BASE_URL + "cancelAppointment";
    }

    public static String getDeletePrescriptionReportURL() {
        return BASE_URL + "deletePrescriptionReportImage";
    }

    public static String getTestPackageListUrl(int pageNum) {
        return BASE_URL + "getTestPackageList?page=" + pageNum;
    }

    public static String getAddRemoveFavouriteURL() {
        return BASE_URL + "addRemoveFavourite";
    }

    public static String getCategoriesURL() {
        return BASE_URL + "getPackageCategory";
    }

    public static String getAboutUsURL() {
        return BASE_URL + "getAboutUs?userType=" + CUSTOMER;
    }

    public static String getPrivacyPolicyURL() {
        return BASE_URL + "getPrivacyPolicy?userType= " + CUSTOMER;
    }

    public static String getTermsConditionsURL() {
        return BASE_URL + "getTermsConditions?userType=" + CUSTOMER;
    }

    public static String getBookDcAppointmentURL() {
        return BASE_URL + "saveTestPackageAppointment";
    }

    public static String getReportsURL(String fromDate, String toDate, String docId, int pageNum) {
        return BASE_URL + "getReportAppointmentList?fromDate=" + fromDate + "&toDate=" + toDate + "&userId=" + docId + "&page=" + pageNum;
    }

    public static String getReportDetailsURL(String id) {
        return BASE_URL + "getReportAppointmentDetails?appointmentId=" + id;
    }

    public static String getHelpURL() {
        return BASE_URL + "getHelpList";
    }

    public static String getNotificationCountURL() {
        return BASE_URL + "getNotificationCount";
    }

    public static String getNotificationsURL(int pageNum) {
        return BASE_URL + "getNotificationList?page=" + pageNum;
    }

    public static String getContactUsURL() {
        return BASE_URL + "saveContactUs";
    }

    public static String getOfferPackagesTestsListURL(String catId, String type) {
        return BASE_URL + "getTestPackageOfferList?categoryId=" + catId + "&type=" + type;
    }

    public static String getOfferBannerURL() {
        return BASE_URL + "getOfferBanner";
    }

    public static String getDcHomeBannerURL() {
        return BASE_URL + "getDcHomeBanner";
    }

    public static String getAppointmentsForReportVisibilityURL(String reportId, String dcId, int pageNum) {
        return BASE_URL + "getAppointmentListWithDoctor?reportAppointmentId=" + reportId + "&dcId=" + dcId + "&page=" + pageNum;
    }

    public static String getAddAppointmentToShareReportsURL() {
        return BASE_URL + "changeReportVisibility";
    }

    public static String getRemoveAppointmentToShareReportsURL() {
        return BASE_URL + "deleteReportVisibility";
    }

    public static String getFavouriteListURL(String type) {
        return BASE_URL + "getFavouriteList?type=" + type;
    }

    public static String getReviewsURL(String itemId, String type, int pageNum) { // type= Customer / Doctor / Nurse / Test / Package
        return BASE_URL + "getReviewList?itemId=" + itemId + "&type=" + type + "&page=" + pageNum;
    }

    public static String getAddReviewURL() {
        return BASE_URL + "saveRating";
    }

    public static String getCancelReportAppointmentURL() {
        return BASE_URL + "cancelReportAppointment";
    }

    public static String getHomeBannerURL() {
        return BASE_URL + "getHomeBanner";
    }

    public static String getRefereeDoctorListUrl() {
        return BASE_URL + "getRefferedDoctorList";
    }

}
