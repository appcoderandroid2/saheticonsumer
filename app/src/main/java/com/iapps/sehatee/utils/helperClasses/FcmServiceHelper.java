package com.iapps.sehatee.utils.helperClasses;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.Home;
import com.iapps.sehatee.activities.Splash;
import com.iapps.sehatee.utils.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.iapps.sehatee.utils.APIs.getUpdateDeviceTokenURL;
import static com.iapps.sehatee.utils.Constants.ANDROID;
import static com.iapps.sehatee.utils.Constants.UPDATE_NOTIFICATION_COUNT;
import static com.iapps.sehatee.utils.Constants.isOnline;

/**
 * Created by jpdbiet on 24,June,2020
 */
public class FcmServiceHelper extends FirebaseMessagingService {

    private static final String TAG = "FcmServiceHelper";
    private Prefs prefs;
    private String GENERAL = "general";
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    private String NOTIFICATION_CHANNEL_ID = "Sahety Consumer App";

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = Prefs.getInstance(this);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String, String> msgData = remoteMessage.getData();
        Log.e(TAG, "onMessageReceived: " + msgData.toString());

        if (msgData.containsKey("notifyType")) {
            if (msgData.get("notifyType").equals(GENERAL)) {

            }
        }

        notifyUser(msgData);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(UPDATE_NOTIFICATION_COUNT));

    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        prefs.setFcmToken(s);
        updateFcmToken(s);
    }

    private void updateFcmToken(String s) {
        if (isOnline(this)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("deviceType", ANDROID);
                jsonObject.put("deviceToken", s);

                VolleyHelper.getInstance(this).addRequest(getUpdateDeviceTokenURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFailure(String error) {

                    }

                    @Override
                    public void onRestrictedByAdmin() {

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "updateFcmToken: ", e);
            }
        }
    }

    private void notifyUserOTP(Map<String, String> data) {
        mBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.drawable.logo_128);
        mBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_128));
        mBuilder.setColorized(true);
        mBuilder.setColor(Color.parseColor("#063d83"));
        mBuilder.setTicker(getResources().getString(R.string.app_name));
        mBuilder.setSmallIcon(R.drawable.logo_128, 1);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle());
        mBuilder.setContentTitle(data.get("title"))
                .setContentText(data.get("message"))
                .setAutoCancel(true)
                .setContentIntent(null);
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.app_name), importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify((int) System.currentTimeMillis() /* Request Code */, mBuilder.build());
    }

    private void notifyUser(Map<String, String> data) {

        Intent intent = null;
        if (prefs.isLoggedIn()) {
            intent = new Intent(this, Home.class);
        } else {
            intent = new Intent(this, Splash.class);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.drawable.logo_128);
        mBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_256));
        mBuilder.setColorized(true);
        mBuilder.setColor(ResourcesCompat.getColor(this.getResources(), R.color.colorAccent, getTheme()));
        mBuilder.setTicker(getResources().getString(R.string.app_name));
//        mBuilder.setSmallIcon(R.drawable.logo_128, 1);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle());
        mBuilder.setContentTitle(data.get("title"))
                .setContentText(data.get("message"))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.app_name), importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify((int) System.currentTimeMillis() /* Request Code */, mBuilder.build());
    }

}
