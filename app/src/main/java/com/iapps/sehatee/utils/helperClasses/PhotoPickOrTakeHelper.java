package com.iapps.sehatee.utils.helperClasses;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

/**
 * Created by jpdbiet on 25,June,2020
 */
public class PhotoPickOrTakeHelper {

    private static final String TAG = "PhotoPickOrTakeHelper";
    public static int RC_OPEN_CAMERA = 1111;
    private Fragment frag;
    private Context ctx;
    public static int RC_OPEN_EXPLORER = 2222;
    private static boolean isActivity = false;
    private static boolean enableCropper = false;
    private static Pair<Integer, Integer> cropperAspectRatio = Pair.create(1, 1);
    private static CropImageView.CropShape cropShape = CropImageView.CropShape.RECTANGLE;
    private static boolean enableMultiSelect = false;
    private static OnResultHelper helper;
    private static int portraitWidthThreshold = 720;
    private static int landscapeWidthThreshold = 1280;
    private Uri photoURI;
    private Activity act;
    private int RC_CAMERA_WRITE_STORAGE_PERMISSION = 1010;
    private String currentPhotoPath = "";
    private int RC_READ_EXTERNAL_STORAGE_PERMISSION = 2020;

    private PhotoPickOrTakeHelper(Activity activity, Fragment fragment) {
        act = activity;
        frag = fragment;
        ctx = isActivity ? act : frag.getContext();
    }

    public void openCamera() {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (isActivity) {
                act.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_CAMERA_WRITE_STORAGE_PERMISSION);
            } else {
                frag.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_CAMERA_WRITE_STORAGE_PERMISSION);
            }
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("createFile", e.getLocalizedMessage() + "");
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(ctx, ctx.getPackageName(), photoFile);
                Log.e("photoUri", photoURI + "");
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                ctx.grantUriPermission(ctx.getPackageName(), photoURI, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (isActivity) {
                    act.startActivityForResult(cameraIntent, RC_OPEN_CAMERA);
                } else {
                    frag.startActivityForResult(cameraIntent, RC_OPEN_CAMERA);
                }

            }
        }
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File((isActivity ? act : frag.getContext()).getFilesDir(), "");

//        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SUB_DIRECTORY_IMAGES);

        if (!storageDir.exists()) {
            if (storageDir.mkdirs()) {
                Log.e(TAG, "createImageFile: Directory created successfully => " + storageDir.getPath());
            } else {
                Log.e(TAG, "createImageFile: Could not make directory => " + storageDir.getPath());
            }
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void openExplorer() {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (isActivity) {
                act.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_READ_EXTERNAL_STORAGE_PERMISSION);
            } else {
                frag.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_READ_EXTERNAL_STORAGE_PERMISSION);
            }
            return;
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
//        if (enableMultiSelect) {
//            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true); // This is used for multi selection
//        }
        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/png", "image/jpg", "image/jpeg"});
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        if (isActivity) {
            act.startActivityForResult(intent, RC_OPEN_EXPLORER);
        } else {
            frag.startActivityForResult(intent, RC_OPEN_EXPLORER);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        if (requestCode == RC_READ_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openExplorer();
            } else {
                if ((isActivity ? act : frag.getActivity()).shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) { // this returns true if the user denied
                    helper.onPermissionDenied(RC_OPEN_CAMERA);
                } else {
                    helper.onNeverAskPermissionChecked(RC_OPEN_CAMERA);
                }
            }
        }
        if (requestCode == RC_CAMERA_WRITE_STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            } else {
                if ((isActivity ? act : frag.getActivity()).shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) { // this returns true if the user denied
                    helper.onPermissionDenied(RC_OPEN_EXPLORER);
                } else {
                    helper.onNeverAskPermissionChecked(RC_OPEN_EXPLORER);
                }
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data, OnResultHelper helper) {
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_OPEN_CAMERA) {

                if (enableCropper) {
                    openCropper(photoURI);
                } else {
                    helper.onSuccess(getResult(photoURI));
                }
            }
            if (requestCode == RC_OPEN_EXPLORER && data != null) {
                if (data.getData() != null) {
                    if (enableCropper) {
                        Log.e(TAG, "onActivityResult: explorer uri => " + data.getData().toString());
                        openCropper(data.getData());
                    } else {
                        helper.onSuccess(getResult(data.getData()));
                    }
                } else {
                    if (data.getClipData() != null) {
                        // TODO here multi selected data will be available.
                    }
                    Log.e(TAG, "onActivityResult: data.getData() = null");
                }
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e(TAG, "onActivityResult: Cropper Uri => " + resultUri.toString());
                helper.onSuccess(getResult(resultUri));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e(TAG, "onActivityResult: ", error);
                helper.onFailure();
            }
        }
    }

    public void openCropper(Uri uri) {

        try {
            File photoFile = createImageFile();
            Uri outputUri = FileProvider.getUriForFile(ctx, ctx.getPackageName(), photoFile);

            if (isActivity) {
                CropImage.activity(uri)
                        .setAllowFlipping(true)
                        .setAutoZoomEnabled(true)
                        .setAspectRatio(cropperAspectRatio.first, cropperAspectRatio.second)
                        .setAllowRotation(true)
                        .setOutputUri(outputUri)
                        .setCropShape(cropShape)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(act);
            } else {
                CropImage.activity(uri)
                        .setAllowFlipping(true)
                        .setAutoZoomEnabled(true)
                        .setAspectRatio(cropperAspectRatio.first, cropperAspectRatio.second)
                        .setAllowRotation(true)
                        .setOutputUri(outputUri)
                        .setCropShape(cropShape)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(ctx, frag);
            }

        } catch (IOException e) {
            helper.onFailure();
            e.printStackTrace();
            Log.e("createFile", e.getLocalizedMessage() + "");
        }
    }

    private Result getResult(Uri uri) {
        Result result = new Result();
        result.setUri(uri);
        try {

            Cursor cursor = ctx.getContentResolver().query(uri, null, null, null, null);
            Objects.requireNonNull(cursor).moveToFirst();

            int size = cursor.getInt(cursor.getColumnIndex(OpenableColumns.SIZE));
            String fileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
            Log.e(TAG, "getResult: fileName = " + fileName);
            String ext = fileName.substring(fileName.lastIndexOf("."));

            result.setImageExtension(ext);
            result.setImageName(fileName.substring(0, fileName.lastIndexOf(".")));

            InputStream is = ctx.getContentResolver().openInputStream(uri);
            File file = new File(ctx.getCacheDir().getAbsolutePath() + "/" + fileName);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[size];
            int len;
            while ((len = is.read(buf)) > 0) {
                fos.write(buf, 0, len);
            }
            is.close();
            fos.close();

            String filePath = file.getAbsolutePath();
            Log.e(TAG, "getResult: filePath => " + filePath);

            File newFile = new File(filePath);

            Bitmap bmp0 = getRotationFixed(filePath);

            int width = bmp0.getWidth();
            int height = bmp0.getHeight();

            if (width < height) {
                if (width > portraitWidthThreshold) {
                    width = portraitWidthThreshold;
                    height = (bmp0.getHeight() * portraitWidthThreshold) / bmp0.getWidth();
                }
            } else if (width == height) {
                width = height = portraitWidthThreshold;
            } else {
                if (width > landscapeWidthThreshold) {
                    width = landscapeWidthThreshold;
                    height = (bmp0.getHeight() * landscapeWidthThreshold) / bmp0.getWidth();
                }
            }

            Bitmap bmp = Bitmap.createScaledBitmap(bmp0, width, height, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (ext.equalsIgnoreCase(".png")) {
                bmp.compress(Bitmap.CompressFormat.PNG, 60, baos);
            }
            if (ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".jpeg")) {
                bmp.compress(Bitmap.CompressFormat.JPEG, 60, baos);
            }

            byte[] byteArrayImage = baos.toByteArray();
            result.setEncodedString(Base64.encodeToString(byteArrayImage, Base64.NO_WRAP));

            if (newFile.delete()) {
                Log.e(TAG, "getResult: file deleted successfully.");
            }
        } catch (IOException | NullPointerException e) {
            helper.onFailure();
            e.printStackTrace();
            Log.e(TAG, "getResult: ", e);
        }

        return result;
    }

    public interface OnResultHelper {
        void onSuccess(PhotoPickOrTakeHelper.Result result);

        void onFailure();

        void onPermissionDenied(int requestType);

        void onNeverAskPermissionChecked(int requestType);
    }

    private Bitmap getRotationFixed(String path) {
        Bitmap bmp = BitmapFactory.decodeFile(path);
        try {
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bmp = rotateImage(bmp, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    bmp = rotateImage(bmp, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    bmp = rotateImage(bmp, 270);
                    break;
                default:
                    bmp = rotateImage(bmp, 0);
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "getRotationFixed: ", e);
        }

        return bmp;
    }

    private Bitmap rotateImage(Bitmap source, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static class Builder {
        private Activity activity;
        private Fragment frag;

        public Builder(Activity act) {
            isActivity = true;
            activity = act;
        }

        public Builder(Fragment fragment) {
            isActivity = false;
            frag = fragment;
        }

        public Builder setPortraitWidth(int px) {
            portraitWidthThreshold = px;
            return this;
        }

        public Builder setLandscapeWidth(int px) {
            landscapeWidthThreshold = px;
            return this;
        }

        public Builder setEnableCropper(boolean bool) {
            enableCropper = bool;
            return this;
        }

        public Builder setEnableMultiSelect(boolean bool) {
            enableMultiSelect = bool;
            return this;
        }

        public Builder setCropperAspectRatio(int width, int height) {
            cropperAspectRatio = Pair.create(width, height);
            return this;
        }

        public Builder setCropShape(CropImageView.CropShape shape) {
            cropShape = shape;
            return this;
        }

        public PhotoPickOrTakeHelper build(OnResultHelper onResultHelper) {
            helper = onResultHelper;
            return new PhotoPickOrTakeHelper(activity, frag);
        }
    }

    public static class Result {
        private String imageName = "";
        private String imageExtension = "";
        private String encodedString = "";
        private Uri uri = null;

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageExtension() {
            return imageExtension;
        }

        public void setImageExtension(String imageExtension) {
            this.imageExtension = imageExtension;
        }

        public String getEncodedString() {
            return encodedString;
        }

        public void setEncodedString(String encodedString) {
            this.encodedString = encodedString;
        }

        public Uri getUri() {
            return uri;
        }

        public void setUri(Uri uri) {
            this.uri = uri;
        }
    }
}
