package com.iapps.sehatee.utils.helperClasses;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;

import java.io.File;
import java.io.Serializable;

/**
 * Created by jpdbiet on 08,July,2020
 */
public class DownloadHelper {

    private static final String TAG = "MyDownloadManager";
    private Context ctx;
    private Activity ACTIVITY;
    private Fragment FRAGMENT;
    private boolean IS_ACTIVITY;
    private String FILE_URL;
    private String SUB_FOLDER;
    private String FILE_NAME;
    private String FILE_EXTENSION;
    private boolean SHOW_AS_NOTIFICATION = true;
    private int RC_READ_EXTERNAL_STORAGE_PERMISSION = 1001;
    private OnDownloadProgressHelper helper;
    private DownloadedFile downloadedFile;

    private DownloadHelper(Activity activity, Fragment fragment, boolean isActivity, boolean showAsNotification, String fileUrl, String subFolderName, String fileName, String fileExtension, OnDownloadProgressHelper downloadProgressHelper) {
        ACTIVITY = activity;
        FRAGMENT = fragment;
        IS_ACTIVITY = isActivity;
        ctx = isActivity ? activity : fragment.getContext();
        SHOW_AS_NOTIFICATION = showAsNotification;
        FILE_URL = fileUrl;
        SUB_FOLDER = subFolderName;
        FILE_NAME = fileName;
        FILE_EXTENSION = fileExtension;
        helper = downloadProgressHelper;
        downloadedFile = new DownloadedFile();
    }

    public void start() {

        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (IS_ACTIVITY) {
                ACTIVITY.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_READ_EXTERNAL_STORAGE_PERMISSION);
            } else {
                FRAGMENT.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_READ_EXTERNAL_STORAGE_PERMISSION);
            }
            return;
        }

        downloadedFile.setFileName(FILE_NAME);
        downloadedFile.setFileUrl(FILE_URL);
        downloadedFile.setFileExtension(FILE_EXTENSION);

        DownloadManager dm = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
        String subPath = (SUB_FOLDER.startsWith("/") ? "" : "/") // check if there is a starting slash
                + SUB_FOLDER
                + (SUB_FOLDER.endsWith("/") ? "" : "/") // check if there is a ending slash
                + FILE_NAME
                + FILE_EXTENSION;

        Log.e(TAG, "startDownload: file path => " + Environment.DIRECTORY_DOWNLOADS + subPath);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(FILE_URL))
                .setTitle(FILE_NAME + FILE_EXTENSION)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, subPath);

        if (SHOW_AS_NOTIFICATION) {
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE | DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        } else {
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
        }

        long downloadId = dm.enqueue(request);
        helper.onDownloadStarted();
        downloadedFile.setDownloadId(downloadId);

        new Thread(new Runnable() {
            @Override
            public void run() {

                boolean downloading = true;
                while (downloading) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(downloadId);
                    Cursor cursor = dm.query(query);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    downloadedFile.setFileSizeInBytes(bytes_total);
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
//                        Uri fileUri = FileProvider.getUriForFile(ctx, ctx.getPackageName(), new File(directoryDownloads + subPath));
                        downloadedFile.setDownloadedFileUri(dm.getUriForDownloadedFile(downloadId));
                        downloadedFile.setMimeType(dm.getMimeTypeForDownloadedFile(downloadId));
                        Log.e(TAG, "run: downloaded file => " + downloadedFile.toString());
                        helper.onDownloadComplete(dm, downloadedFile);
                    }

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_FAILED) {
                        helper.onDownloadFailed();
                    }

                    final int dl_progress = (bytes_downloaded / bytes_total) * 100;
                    helper.onDownloadProgress(dl_progress);
                    cursor.close();
                }
            }

//            private String statusMessage(Cursor c) {
//                String msg = "???";
//
//                switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
//                    case DownloadManager.STATUS_FAILED:
//                        msg = "Download failed!";
//                        break;
//
//                    case DownloadManager.STATUS_PAUSED:
//                        msg = "Download paused!";
//                        break;
//
//                    case DownloadManager.STATUS_PENDING:
//                        msg = "Download pending!";
//                        break;
//
//                    case DownloadManager.STATUS_RUNNING:
//                        msg = "Download in progress!";
//                        break;
//
//                    case DownloadManager.STATUS_SUCCESSFUL:
//                        msg = "Download complete!";
//                        break;
//
//                    default:
//                        msg = "Download is nowhere in sight";
//                        break;
//                }
//
//                return (msg);
//            }
        }).start();

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_READ_EXTERNAL_STORAGE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            start();
        } else {
            if ((IS_ACTIVITY ? ACTIVITY : FRAGMENT.getActivity()).shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                helper.onPermissionDenied();
            } else {
                helper.onNeverAskPermissionChecked();
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

    }

    public interface OnDownloadProgressHelper {

        void onDownloadStarted();

        void onDownloadComplete(DownloadManager dm, DownloadedFile file);

        void onDownloadProgress(int percent);

        void onDownloadFailed();

        void onPermissionDenied();

        void onNeverAskPermissionChecked();

    }

    public static class Builder {

        private static String SUB_FOLDER;
        private static String FILE_NAME;
        private static String FILE_EXTENSION;
        private static String FILE_URL;
        private Activity ACTIVITY;
        private Fragment FRAGMENT;
        private boolean IS_ACTIVITY;
        private boolean SHOW_AS_NOTIFICATION = true;

        public Builder(Activity activity) {
            ACTIVITY = activity;
            IS_ACTIVITY = true;
            initVar();
        }

        public Builder(Fragment fragment) {
            FRAGMENT = fragment;
            IS_ACTIVITY = false;
            initVar();
        }

        private void initVar() {

            FILE_EXTENSION = ".jpg";

            boolean isImageType = FILE_EXTENSION.equals(".jpeg") || FILE_EXTENSION.equals(".jpg") || FILE_EXTENSION.equals(".png");
            SUB_FOLDER = (IS_ACTIVITY ? ACTIVITY : FRAGMENT.getContext()).getString(R.string.app_name) + File.separator
                    + (isImageType ? "Images" : "Documents") + File.separator
                    + (FILE_NAME = isImageType ? "IMG_" : "DOC_") + System.currentTimeMillis()
                    + FILE_EXTENSION;
        }

        public Builder setShowAsNotification(boolean bool) {
            this.SHOW_AS_NOTIFICATION = bool;
            return this;
        }

        public Builder setFileUrl(@NonNull String url) {
            FILE_URL = url;
            return this;
        }

        public Builder setSubFolderName(@NonNull String subFolderName) {
            SUB_FOLDER = subFolderName;
            return this;
        }

        public Builder setFileName(@NonNull String fileName) {
            FILE_NAME = fileName;
            return this;
        }

        public Builder setFileExtension(@NonNull String fileExtension) {
            FILE_EXTENSION = fileExtension;
            return this;
        }

        public DownloadHelper build(OnDownloadProgressHelper downloadProgressHelper) {
            return new DownloadHelper(ACTIVITY, FRAGMENT, IS_ACTIVITY, SHOW_AS_NOTIFICATION, FILE_URL, SUB_FOLDER, FILE_NAME, FILE_EXTENSION, downloadProgressHelper);
        }
    }

    public static class DownloadedFile implements Serializable {
        private long downloadId = 0;
        private String fileName = "";
        private String fileExtension = "";
        private String mimeType = "";
        private int fileSizeInBytes = 0;
        private String fileUrl = "";
        private Uri downloadedFileUri = Uri.parse("");

        public long getDownloadId() {
            return downloadId;
        }

        public void setDownloadId(long downloadId) {
            this.downloadId = downloadId;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFileExtension() {
            return fileExtension;
        }

        public void setFileExtension(String fileExtension) {
            this.fileExtension = fileExtension;
        }

        public String getMimeType() {
            return mimeType;
        }

        public void setMimeType(String mimeType) {
            this.mimeType = mimeType;
        }

        public int getFileSizeInBytes() {
            return fileSizeInBytes;
        }

        public void setFileSizeInBytes(int fileSizeInBytes) {
            this.fileSizeInBytes = fileSizeInBytes;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public void setFileUrl(String fileUrl) {
            this.fileUrl = fileUrl;
        }

        public Uri getDownloadedFileUri() {
            return downloadedFileUri;
        }

        public void setDownloadedFileUri(Uri downloadedFileUri) {
            this.downloadedFileUri = downloadedFileUri;
        }

        @NonNull
        @Override
        public String toString() {
            return "DownloadedFile{" +
                    "downloadId=" + downloadId +
                    ", fileName='" + fileName + '\'' +
                    ", fileExtension='" + fileExtension + '\'' +
                    ", mimeType='" + mimeType + '\'' +
                    ", fileSizeInBytes=" + fileSizeInBytes +
                    ", fileUrl='" + fileUrl + '\'' +
                    ", downloadedFileUri=" + downloadedFileUri +
                    '}';
        }
    }
}
