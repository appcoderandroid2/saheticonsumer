package com.iapps.sehatee.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.StringDef;

import com.iapps.sehatee.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 12,June,2020
 */
public class Prefs {


    public static final String INTRO_SHOWN = "INTRO_SHOWN";
    public static final String NORMAL = "Normal";
    public static final String GOOGLE = "Google";
    public static final String FACEBOOK = "Facebook";
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final String OTHER = "Other";
    private static final String LANGUAGE_CODE = "LANGUAGE_CODE";
    private final String IS_LOGGED_IN = "IS_LOGGED_IN";
    private final String FCM_TOKEN = "FCM_TOKEN";
    private final String AUTH_TOKEN = "AUTH_TOKEN";
    private final String LOGIN_PROVIDER = "LOGIN_PROVIDER";
    private final String USER_ID = "USER_ID";
    private final String USER_TYPE = "USER_TYPE";
    private final String FULL_NAME = "FULL_NAME";
    private final String EMAIL = "EMAIL";
    private final String ISD_CODE = "ISD_CODE";
    private final String PHONE = "PHONE";
    private final String DOB = "DOB";
    private final String GENDER = "GENDER";
    private final String FULL_ADDRESS = "FULL_ADDRESS";
    private final String ADDRESS = "ADDRESS";
    private final String CITY_NAME = "CITY_NAME";
    private final String CITY_AREA = "CITY_AREA";
    private final String POSTAL_CODE = "POSTAL_CODE";
    private final String SELECTED_CITY_NAME = "SELECTED_CITY_NAME";
    private final String SELECTED_CITY_ID = "SELECTED_CITY_ID";
    private final String SELECTED_CITY_AREA_NAME = "SELECTED_CITY_AREA_NAME";
    private final String SELECTED_CITY_AREA_ID = "SELECTED_CITY_AREA_ID";
    private final String ABOUT_ME = "ABOUT_ME";
    private final String PROFILE_IMAGE = "PROFILE_IMAGE";
    private final String CURRENCY = "CURRENCY";
    private final String CURRENCY_SYMBOL = "CURRENCY_SYMBOL";
    private final String TOTAL_REVIEW = "TOTAL_REVIEW";
    private final String RATING = "RATING";
    private SharedPreferences sp, fcmSp, langSp;
    private SharedPreferences.Editor ed, fcmEd, langEd;

    private static Prefs instance;

    private Prefs(Context ctx) {
        sp = ctx.getSharedPreferences(ctx.getString(R.string.app_name), Context.MODE_PRIVATE);
        fcmSp = ctx.getSharedPreferences(ctx.getString(R.string.app_name) + "_FCM", Context.MODE_PRIVATE);
        langSp = ctx.getSharedPreferences(ctx.getString(R.string.app_name) + "_LANG", Context.MODE_PRIVATE);
        ed = sp.edit();
        ed.apply();
        fcmEd = fcmSp.edit();
        fcmEd.apply();
        langEd = langSp.edit();
        langEd.apply();
    }

    @NonNull
    @Override
    public String toString() {
        return sp.getAll().toString();
    }

//    private static final String jobDetails = NA;

    public static synchronized Prefs getInstance(Context ctx) {
        if (instance == null) {
            instance = new Prefs(ctx);
        }
        return instance;
    }

    public String getAppLanguageCode() {
        return langSp.getString(LANGUAGE_CODE, "");
    }

    public void setAppLanguageCode(@NonNull String languageCode) {
        langEd.putString(LANGUAGE_CODE, languageCode).apply();
    }

    public String getFcmToken() {
        return fcmSp.getString(FCM_TOKEN, NA);
    }

    public boolean isLoggedIn() {
        return sp.getBoolean(IS_LOGGED_IN, false);
    }

    public void setLoggedIn(boolean bool) {
        ed.putBoolean(IS_LOGGED_IN, bool).apply();
    }

    public void setFcmToken(String token) {
        fcmEd.putString(FCM_TOKEN, token).apply();
    }

    public void clearSp() {
        ed.clear().apply();
    }

    public String getAuthToken() {
        return sp.getString(AUTH_TOKEN, NA);
    }

    public String getLoginProvider() {
        return sp.getString(LOGIN_PROVIDER, NORMAL);
    }

    public void setLoginProvider(@LoginProvider @NonNull String loginProvider) {
        ed.putString(LOGIN_PROVIDER, loginProvider).apply();
    }

    public void setAuthToken(String authToken) {
        ed.putString(AUTH_TOKEN, authToken).apply();
    }

    public boolean getIntroShown() {
        return sp.getBoolean(INTRO_SHOWN, false);
    }

    public void setIntroShown(boolean bool) {
        ed.putBoolean(INTRO_SHOWN, bool).apply();
    }

    public String getUserId() {
        return sp.getString(USER_ID, NA);
    }

    public void setUserId(@NonNull String userId) {
        ed.putString(USER_ID, userId).apply();
    }

    public String getUserType() {
        return sp.getString(USER_TYPE, CUSTOMER);
    }

    public void setUserType(@NonNull String userType) {
        ed.putString(USER_TYPE, userType).apply();
    }

    public String getFullName() {
        return sp.getString(FULL_NAME, NA);
    }

    public void setFullName(@NonNull String fullName) {
        ed.putString(FULL_NAME, fullName).apply();
    }

    public String getEmail() {
        return sp.getString(EMAIL, NA);
    }

    public void setEmail(@NonNull String email) {
        ed.putString(EMAIL, email).apply();
    }

    public String getIsdCode() {
        return sp.getString(ISD_CODE, "0");
    }

    public void setIsdCode(@NonNull String isdCode) {
        ed.putString(ISD_CODE, isdCode).apply();
    }

    public String getPhone() {
        return sp.getString(PHONE, NA);
    }

    public void setPhone(@NonNull String phone) {
        ed.putString(PHONE, phone).apply();
    }

    public String getDOB() {
        return sp.getString(DOB, NA);
    }

    public void setDOB(@NonNull String dob) {
        ed.putString(DOB, dob).apply();
    }

    public String getGender() {
        return sp.getString(GENDER, NA);
    }

    public void setGender(@Gender @NonNull String gender) {
        ed.putString(GENDER, gender).apply();
    }

    public String getAddress() {
        return sp.getString(ADDRESS, NA);
    }

    public void setAddress(@NonNull String address) {
        ed.putString(ADDRESS, address).apply();
    }

    public String getCity() {
        return sp.getString(CITY_NAME, NA);
    }

    public void setCityName(@NonNull String cityName) {
        ed.putString(CITY_NAME, cityName).apply();
    }

    public String getCityArea() {
        return sp.getString(CITY_AREA, NA);
    }

    public void setCityArea(@NonNull String cityArea) {
        ed.putString(CITY_AREA, cityArea).apply();
    }

    public String getPostalCode() {
        return sp.getString(POSTAL_CODE, NA);
    }

    public void setPostalCode(@NonNull String postalCode) {
        ed.putString(POSTAL_CODE, postalCode).apply();
    }

    public String getFullAddress() {
        return sp.getString(FULL_ADDRESS, NA);
    }

    public void setFullAddress(@NonNull String fullAddress) {
        ed.putString(FULL_ADDRESS, fullAddress).apply();
    }

    public String getSelectedCityName() {
        return sp.getString(SELECTED_CITY_NAME, NA);
    }

    public void setSelectedCityName(@NonNull String selectedCityName) {
        ed.putString(SELECTED_CITY_NAME, selectedCityName).apply();
    }

    public String getSelectedCityId() {
        return sp.getString(SELECTED_CITY_ID, NA);
    }

    public void setSelectedCityId(@NonNull String selectedCityId) {
        ed.putString(SELECTED_CITY_ID, selectedCityId).apply();
    }

    public String getSelectedCityAreaName() {
        return sp.getString(SELECTED_CITY_AREA_NAME, NA);
    }

    public void setSelectedCityAreaName(@NonNull String selectedCityAreaName) {
        ed.putString(SELECTED_CITY_AREA_NAME, selectedCityAreaName).apply();
    }

    public String getSelectedCityAreaId() {
        return sp.getString(SELECTED_CITY_AREA_ID, NA);
    }

    public void setSelectedCityAreaId(@NonNull String selectedCityAreaId) {
        ed.putString(SELECTED_CITY_AREA_ID, selectedCityAreaId).apply();
    }

    public String getAboutMe() {
        return sp.getString(ABOUT_ME, NA);
    }

    public void setAboutMe(@NonNull String aboutMe) {
        ed.putString(ABOUT_ME, aboutMe).apply();
    }

    public String getProfileImage() {
        return sp.getString(PROFILE_IMAGE, NA);
    }

    public void setProfileImage(@NonNull String profileImage) {
        ed.putString(PROFILE_IMAGE, profileImage).apply();
    }

    public String getCurrency() {
        return sp.getString(CURRENCY, "");
    }

    public void setCurrency(@NonNull String currency) {
        ed.putString(CURRENCY, currency).apply();
    }

    public String getCurrencySymbol() {
        return sp.getString(CURRENCY_SYMBOL, "");
    }

    public void setCurrencySymbol(@NonNull String currencySymbol) {
        ed.putString(CURRENCY_SYMBOL, currencySymbol).apply();
    }

    public float getRating() {
        return sp.getFloat(RATING, 0f);
    }

    public void setRating(float currencySymbol) {
        ed.putFloat(RATING, currencySymbol).apply();
    }

    public String getTotalReviews() {
        return sp.getString(TOTAL_REVIEW, "0");
    }

    public void setTotalReview(@NonNull String totalReview) {
        ed.putString(TOTAL_REVIEW, totalReview).apply();
    }


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({NORMAL, GOOGLE, FACEBOOK})
    public @interface LoginProvider {
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({MALE, FEMALE, OTHER})
    public @interface Gender {
    }
}
