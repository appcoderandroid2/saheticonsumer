package com.iapps.sehatee.utils.helperClasses;

import android.content.Context;
import android.util.Log;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.iapps.sehatee.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import static com.iapps.sehatee.utils.Constants.getPrefs;

/**
 * Created by jpdbiet on 27,April,2020
 */
public class VolleyHelper {
    public static final int POST = Request.Method.POST;
    public static final int GET = Request.Method.GET;
    public static final int PUT = Request.Method.PUT;
    public static final int DELETE = Request.Method.DELETE;
    private static final String TAG = "VolleyHelper";
    private static VolleyHelper instance;
    private Context ctx;
    private RequestQueue requestQueue;

    private VolleyHelper(Context ctx) {
        this.ctx = ctx;
        requestQueue = Volley.newRequestQueue(ctx);
    }

    public static synchronized VolleyHelper getInstance(Context ctx) {
        if (instance == null) {
            instance = new VolleyHelper(ctx);
        }
        return instance;
    }

    public void addRequest(@NonNull String url, @VHMethods int method, @Nullable String data, OnVolleyResponseListener helper) {
        requestQueue = Volley.newRequestQueue(ctx);
        requestQueue.getCache().clear();
        requestQueue.add(new StringRequest(method, url, response -> {
            Log.e(TAG, "URL  =>  " + url);
            Log.e(TAG, "JSON data  =>  " + data);
            Log.e(TAG, "Response  =>  " + response);

            try {
                SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                if (model.getStatus() == 1) {
                    helper.onSuccess(response);
                } else if (model.getStatus() == 0) {
                    if (model.getMsg().equals("New User")) {
                        helper.onSuccess(response);
                    } else if (model.getMsg().toLowerCase().contains("blocked") || model.getMsg().toLowerCase().contains("unauthorized")) {
                        Log.e(TAG, "addRequest: User is restricted by admin.");
                        helper.onRestrictedByAdmin();
                    } else {
                        helper.onFailure(model.getMsg());
                    }
                } else {
                    helper.onFailure(model.getMsg());
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                Log.d(TAG, "addRequest() returned: " + e);
                helper.onFailure(ctx.getString(R.string.something_went_wrong));
            }

        }, error -> {

            Log.e(TAG, "URL  =>  " + url);
            Log.e(TAG, "JSON data  =>  " + data);

            if (error.networkResponse != null) {
                if (error.networkResponse.data != null) {
                    try {
                        JSONObject response = new JSONObject(new String(error.networkResponse.data, StandardCharsets.UTF_8));

                        String msg = response.has("msg") ? response.getString("msg") : "";

                        if (response.has("message")) {
                            msg = response.getString("message");
                            if (msg.isEmpty()) {
                                if (response.toString().toLowerCase().contains("Unable to resolve host".toLowerCase()) || response.toString().toLowerCase().contains("Connection closed by peer".toLowerCase())) {
                                    msg = ctx.getString(R.string.could_not_connect_to_server);
                                }
                            }
                        }

                        if (response.has("verrors")) {
                            Iterator<String> iterator = response.getJSONObject("payload").getJSONObject("verrors").keys();
                            if (iterator.hasNext()) {
                                msg = response.getJSONObject("payload").getJSONObject("verrors").getString(iterator.next());
                            }
                        }

                        if (msg.toLowerCase().contains("blocked") || msg.toLowerCase().contains("unauthorized")) {
                            helper.onRestrictedByAdmin();
                        } else {
                            helper.onFailure(msg);
                        }

                        Log.e(TAG, "Error response => " + response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error JSONException => " + e.getLocalizedMessage());
                        helper.onFailure(error.getLocalizedMessage());
                    }
                } else {
                    Log.e(TAG, "Error networkResponse.data = null => " + error.getLocalizedMessage());
                    helper.onFailure(ctx.getString(R.string.could_not_connect_to_server));
                }
            } else {
                Log.e(TAG, "Error networkResponse = null => " + error.getLocalizedMessage());
                helper.onFailure(ctx.getString(R.string.could_not_connect_to_server));
            }
        }) {
            @Override
            public byte[] getBody() {
                return data != null ? data.getBytes() : "".getBytes();
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", getPrefs().getAuthToken());
                headers.put("Content-Type", "application/json");
                headers.put("countryId", "1");
                headers.put("Content-Language", Locale.getDefault().getLanguage());
                headers.put("Accept", "application/json");
//                Log.e(TAG, "Headers: used for api = " + url + ", Headers => " + headers.toString());
                return headers;
            }
        }).setShouldCache(false);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({POST, GET, PUT, DELETE})
    public @interface VHMethods {
    }

    public interface OnVolleyResponseListener {

        void onSuccess(String response);

        void onFailure(String error);

        void onRestrictedByAdmin();
    }

    private static class SimpleResponseModel implements Serializable {
        private int status = 0;
        private String msg = "NA";

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

}
