package com.iapps.sehatee.applicationClass;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.iapps.sehatee.utils.Constants;
import com.iapps.sehatee.utils.Prefs;

import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.setPrefs;

/**
 * Created by jpdbiet on 12,June,2020
 */
public class ApplicationClass extends Application implements LifecycleEventObserver, LifecycleObserver {
    private static final String TAG = "ApplicationClass";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(setLocale(base, Locale.getDefault().getLanguage()));
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        setPrefs(Prefs.getInstance(this));
        setLocale(this, getPrefs().getAppLanguageCode().equals("") ? Locale.getDefault().getLanguage() : getPrefs().getAppLanguageCode());

//        TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
//        String countryCodeValue = tm.getNetworkCountryIso();
//
//        Log.e(TAG, "onCreate: Country => " + countryCodeValue);
//        Log.e(TAG, "onCreate: Country => " + Locale.getDefault().getISO3Country());
    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
        switch (event) {
            case ON_CREATE:
                Log.e(TAG, "onStateChanged: APP IS CREATED");
                break;
            case ON_START:
                Log.e(TAG, "onStateChanged: APP IS STARTED");
                break;
            case ON_RESUME:
                Constants.setAppInBackground(false);
                Log.e(TAG, "onStateChanged: APP IS IN FOREGROUND");
                break;
            case ON_PAUSE:
                Constants.setAppInBackground(true);
                Log.e(TAG, "onStateChanged: APP IS IN BACKGROUND");
                break;
            case ON_STOP:
                Log.e(TAG, "onStateChanged: APP IS STOPPED");
                break;
            case ON_DESTROY:
                Log.e(TAG, "onStateChanged: APP IS BEING DESTROYED");
                break;
        }
    }
}