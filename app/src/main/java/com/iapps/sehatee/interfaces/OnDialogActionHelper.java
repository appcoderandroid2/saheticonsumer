package com.iapps.sehatee.interfaces;

import android.app.Dialog;

/**
 * Created by jpdbiet on 25,June,2020
 */
public interface OnDialogActionHelper {
    void onPositiveBtnClicked(Dialog d);

    default void onNegativeBtnClicked(Dialog d) {

    }

    default void onCancelledBtnClicked(Dialog d) {

    }

    default void onDismissedBtnClicked(Dialog d) {

    }
}
