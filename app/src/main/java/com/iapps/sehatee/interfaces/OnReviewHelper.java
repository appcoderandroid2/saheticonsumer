package com.iapps.sehatee.interfaces;

import android.app.Dialog;

/**
 * Created by jpdbiet on 28,July,2020
 */
public interface OnReviewHelper {

    void onSubmitClicked(Dialog d, float rate, String review);

    void onNoRating(Dialog d);

    void onReviewEmpty(Dialog d);

}
