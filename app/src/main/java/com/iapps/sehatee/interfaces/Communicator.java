package com.iapps.sehatee.interfaces;

import android.app.Dialog;

import androidx.fragment.app.Fragment;

/**
 * Created by jpdbiet on 12,June,2020
 */
public interface Communicator {

    void goBack();

    void goToHome();

    void goToAppointments();

    void goToDcHome();

    void goToReports();

    void recreateActivity();

    void openFragment(Fragment fragment, String pageTitle);

    void setMenuSelection(String fragName);

    Dialog getSingleBtnDialog(String title, String msg, String posBtnText, OnDialogActionHelper helper);

    Dialog getDoubleBtnDialog(String title, String msg, String negBtnText, String posBtnText, OnDialogActionHelper helper);

    void showSnackBar(int resId);

    void showSnackBar(String msg);

    void showToast(int resId);

    void showToast(String msg);

    void setActionBar(int type);

    void setPageTitle(int resId);

    void updateProfileData();

    void updateNotificationBadge(int count);

    boolean isActivityFinishing();

    void logout();

}
