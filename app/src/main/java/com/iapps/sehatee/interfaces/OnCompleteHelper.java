package com.iapps.sehatee.interfaces;

/**
 * Created by jpdbiet on 14,July,2020
 */
public interface OnCompleteHelper {

    void onSuccess();

    void onFailure(String error);

}
