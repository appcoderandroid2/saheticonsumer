package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.CityListRvAdapter;
import com.iapps.sehatee.models.CityModel;
import com.iapps.sehatee.models.ResponseModels.CityResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getCityListURL;
import static com.iapps.sehatee.utils.Constants.LOGIN;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class SelectCity extends AppCompatActivity implements CityListRvAdapter.OnItemClickedHelper {

    private static final String TAG = "SelectCity";
    private RecyclerView rv;
    private CityListRvAdapter adapter;
    private List<CityModel> list = new ArrayList<>();
    private TextView myLocationTv;
    private int RC_SELECT_AREA = 1010;
    private String from = LOGIN;
    private String cityName = "", areaName = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_city);

        from = getIntent().getStringExtra("from");
        cityName = getIntent().getStringExtra("cityName");
        areaName = getIntent().getStringExtra("areaName");

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());

        rv = findViewById(R.id.rv);
        myLocationTv = findViewById(R.id.myLocationTv);

        rv.setAdapter(adapter = new CityListRvAdapter(list, this));
        rv.setLayoutManager(new LinearLayoutManager(this));

        myLocationTv.setOnClickListener(v -> getMyLocation());

        getCities();
    }

    private void getCities() {
        if (isOnline(this)) {

            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                dialog.show();
            }
            VolleyHelper.getInstance(this).addRequest(getCityListURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    CityResponseModel model = new Gson().fromJson(response, CityResponseModel.class);
                    if (model.getStatus() == 1) {
                        if (model.getPayload().getCity().size() > 0) {
                            list.clear();
                            CityModel cityModel = new CityModel();
                            cityModel.setChecked(false);
                            cityModel.setCityName(getString(R.string.all_cities));
                            cityModel.setCityId("0");
                            list.add(cityModel);
                            list.addAll(model.getPayload().getCity());
                        }

                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setChecked(cityName.equals(list.get(i).getCityName()));
                        }

                        adapter.notifyDataSetChanged();
                    } else {
                        showToast(SelectCity.this, model.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onRestrictedByAdmin() {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(SelectCity.this);
                }
            });
        } else {
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void getMyLocation() {

    }

    private void proceed() {
        int index = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isChecked()) {
                index = i;
                break;
            }
        }
        if (index == 0) {
            setResult(RESULT_OK, new Intent()
                    .putExtra("cityName", list.get(index).getCityName())
                    .putExtra("areaName", getString(R.string.all_areas))
            );
            if (from.equals(LOGIN)) {
                cityName = list.get(index).getCityName();
                areaName = getString(R.string.all_areas);
                getPrefs().setSelectedCityName(cityName);
                getPrefs().setSelectedCityId("0");
                getPrefs().setSelectedCityAreaName(areaName);
                getPrefs().setSelectedCityAreaId("0");
                startActivity(new Intent(this, Home.class));
            }
            finish();
            Log.e(TAG, "onActivityResult: " + "City = " + cityName + ", Area = " + areaName);
        } else {
            startActivityForResult(new Intent(this, SelectArea.class).putExtra("model", list.get(index)), RC_SELECT_AREA);
        }

    }

    @Override
    public void onItemChecked(int pos) {
        if (pos > -1) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setChecked(false);
            }
            list.get(pos).setChecked(true);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    if (rv.isAnimating()) {
                        new Handler().postDelayed(this, 100);
                    } else {
                        adapter.notifyDataSetChanged();
                        proceed();
                    }
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        if (from.equals(LOGIN)) {
            startActivity(new Intent(this, Home.class));
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SELECT_AREA && resultCode == RESULT_OK && data != null) {

            String cityName = data.getStringExtra("cityName");
            String areaName = data.getStringExtra("areaName");

            Log.e(TAG, "onActivityResult: " + "City = " + cityName + ", Area = " + areaName);

            setResult(RESULT_OK, new Intent()
                    .putExtra("cityName", cityName)
                    .putExtra("areaName", areaName)
            );
            if (from.equals(LOGIN)) {
                getPrefs().setSelectedCityName(cityName);
                getPrefs().setSelectedCityId("0");
                getPrefs().setSelectedCityAreaName(areaName);
                getPrefs().setSelectedCityAreaId("0");
                startActivity(new Intent(this, Home.class));
            }
            finish();
        }
    }
}