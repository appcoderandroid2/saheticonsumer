package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.DcCategoriesFilterRvAdapter;
import com.iapps.sehatee.models.CategoryModel;
import com.iapps.sehatee.models.ResponseModels.DcPackTestCategoriesResponseModel;
import com.iapps.sehatee.utils.Constants;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getCategoriesURL;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DcCategoriesFilter extends AppCompatActivity implements DcCategoriesFilterRvAdapter.OnItemClickedHelper, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "DcCategoriesFilter";
    private RecyclerView rv;
    private DcCategoriesFilterRvAdapter adapter;
    private List<CategoryModel> list = new ArrayList<>();
    private LinearLayout noDataLL;
    private FloatingActionButton nextFab;
    private CheckBox selectAllCb;
    private String categoriesFromIntent = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dc_categories_filter);

        categoriesFromIntent = getIntent().getStringExtra("categories") == null ? "" : getIntent().getStringExtra("categories");

        findViewById(R.id.backIv).setOnClickListener((v) -> onBackPressed());

        noDataLL = findViewById(R.id.noDataLL);
        rv = findViewById(R.id.rv);
        nextFab = findViewById(R.id.nextFab);
        selectAllCb = findViewById(R.id.selectAllCb);

        rv.setAdapter(adapter = new DcCategoriesFilterRvAdapter(list, this));
        rv.setLayoutManager(new LinearLayoutManager(this));

        nextFab.setOnClickListener(v -> proceed());

        selectAllCb.setOnCheckedChangeListener(this);

        isAllSelected();

        getCategories();

    }

    private void getCategories() {
        if (isOnline(this)) {

            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                dialog.show();
            }

            VolleyHelper.getInstance(this).addRequest(getCategoriesURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    DcPackTestCategoriesResponseModel model = new Gson().fromJson(response, DcPackTestCategoriesResponseModel.class);
                    if (model.getStatus() == 1) {
                        list.clear();
                        list.addAll(model.getPayload().getCategories());

                        assert categoriesFromIntent != null;
                        List<String> strList = Arrays.asList(categoriesFromIntent.split(","));
                        Log.e(TAG, "onCreate: categories => " + strList);
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setChecked(strList.contains(list.get(i).getId()));
                        }
                        isAllSelected();
                        adapter.notifyDataSetChanged();
                    } else {
                        showToast(DcCategoriesFilter.this, model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showToast(DcCategoriesFilter.this, error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(DcCategoriesFilter.this);
                }
            });

        } else {
            updateUI();
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        selectAllCb.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
    }

    private void proceed() {
        StringBuilder categories = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isChecked()) {
                categories.append(list.get(i).getId());
                categories.append(",");
            }
        }
        if (categories.toString().contains(",")) {
            categories = new StringBuilder(categories.substring(0, categories.lastIndexOf(",")).trim());
        }
        if (categories.length() == 0) {
            Constants.showToast(this, getString(R.string.error_categories_not_selected));
            return;
        }

        setResult(RESULT_OK, new Intent().putExtra("categories", categories.toString()));
        finish();
    }

    @Override
    public void onFilterChecked(int pos, boolean isChecked) {
        if (pos > -1) {
            list.get(pos).setChecked(isChecked);
            isAllSelected();
        }
    }

    private void isAllSelected() {
        boolean isAll = false;
        for (int i = 0; i < list.size(); i++) {
            isAll = list.get(i).isChecked();
            if (!isAll) {
                break;
            }
        }
        selectAllCb.setOnCheckedChangeListener(null);
        selectAllCb.setChecked(isAll);
        selectAllCb.setOnCheckedChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setChecked(isChecked);
        }
        adapter.notifyDataSetChanged();
    }
}