package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.ForgotPasswordResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getForgotPasswordURL;
import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.OTP_NOTIFICATION;
import static com.iapps.sehatee.utils.Constants.convertSecondsToHMmSs;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class OTP extends AppCompatActivity {

    private static final String TAG = "OTP";
    private EditText otpEt;
    private Button submitBtn;
    private TextView didGetOtpTv, resendTv;
    private CountDownTimer timer;
    private String otpSent;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (otpEt != null) {
                otpEt.post(() -> {
                    if (intent != null) {
                        if (intent.getAction() != null) {
                            if (intent.getAction().equals(OTP_NOTIFICATION)) {
                                if (intent.getStringExtra("otp") != null) {
                                    String otp = intent.getStringExtra("otp") == null ? "" : intent.getStringExtra("otp");
                                    assert otp != null;
                                    int len = otp.length();
                                    if (otpEt.getText().toString().trim().length() > 0) {
                                        otpEt.setText(null);
                                    }
                                    for (int i = 0; i < len; i++) {
                                        final int j = i;
                                        new Handler().postDelayed(() -> otpEt.append(String.valueOf(otp.charAt(j))), 800);
                                    }
                                } else {
                                    Log.e(TAG, "onReceive: otp is null");
                                }
                            } else {
                                Log.e(TAG, "onReceive: action not " + OTP_NOTIFICATION);
                            }
                        } else {
                            Log.e(TAG, "onReceive: intent action is null");
                        }
                    } else {
                        Log.e(TAG, "onReceive: intent is null");
                    }
                });
            }
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);

        otpEt = findViewById(R.id.otpEt);
        submitBtn = findViewById(R.id.submitBtn);
        didGetOtpTv = findViewById(R.id.didGetOtpTv);
        resendTv = findViewById(R.id.resendTv);

        otpSent = getIntent().getStringExtra("otp");
        String userId = getIntent().getStringExtra("userId");

        otpEt.setText(otpSent);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                String otp = otpEt.getText().toString().trim();
                if (otp.length() < 6) {
                    showToast(OTP.this, R.string.error_invalid_otp);
                    v.setEnabled(true);
                    return;
                }
                if (otp.equals(otpSent)) {
                    startActivity(new Intent(OTP.this, ResetPassword.class)
                            .putExtra("userId", userId)
                            .putExtra("otp", otpSent)
                    );
                } else {
                    showToast(OTP.this, R.string.error_invalid_otp);
                    v.setEnabled(true);
                }
            }
        });

        otpEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                submitBtn.performClick();
                return true;
            }
            return false;
        });

        resendTv.setOnClickListener(v -> {
            if (resendTv.getText().toString().trim().equals(getString(R.string.resend_otp))) {
                sendOtp(getIntent().getStringExtra("email"));
            }
        });
    }

    private void sendOtp(String email) {

        if (isOnline(this)) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userType", CUSTOMER);
                jsonObject.put("email", email);

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(this).addRequest(getForgotPasswordURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        ForgotPasswordResponseModel model = new Gson().fromJson(response, ForgotPasswordResponseModel.class);
                        if (model.getStatus() == 1) {
                            otpSent = model.getPayload().getOtp();
                            otpEt.setText(otpSent);
                            startTimer();
                        } else {
                            showToast(OTP.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showToast(OTP.this, error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(OTP.this);
                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "sendOtp: ", e);
                showToast(this, R.string.something_went_wrong);
            }

        } else {
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void startTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                resendTv.setText(String.format(Locale.getDefault(), "%s ... %s", getString(R.string.please_wait), convertSecondsToHMmSs(millisUntilFinished)));
            }

            @Override
            public void onFinish() {
                didGetOtpTv.setVisibility(View.VISIBLE);
                resendTv.setText(getString(R.string.resend_otp));
            }
        };
        timer.start();
    }
}