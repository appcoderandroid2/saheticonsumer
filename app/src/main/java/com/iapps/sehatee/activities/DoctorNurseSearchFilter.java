package com.iapps.sehatee.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.InsuranceProvidersResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getAllInsuranceProvidersURL;
import static com.iapps.sehatee.utils.Constants.ANY_DAY;
import static com.iapps.sehatee.utils.Constants.CLINIC;
import static com.iapps.sehatee.utils.Constants.CONSULTANT;
import static com.iapps.sehatee.utils.Constants.FEMALE;
import static com.iapps.sehatee.utils.Constants.HOSPITAL;
import static com.iapps.sehatee.utils.Constants.INDIVIDUAL;
import static com.iapps.sehatee.utils.Constants.INSTITUTE;
import static com.iapps.sehatee.utils.Constants.MALE;
import static com.iapps.sehatee.utils.Constants.SEARCH_DOCTOR;
import static com.iapps.sehatee.utils.Constants.SEARCH_NURSE;
import static com.iapps.sehatee.utils.Constants.SPECIALIST;
import static com.iapps.sehatee.utils.Constants.TODAY;
import static com.iapps.sehatee.utils.Constants.TOMORROW;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DoctorNurseSearchFilter extends AppCompatActivity {

    private static final String TAG = "DoctorNurseSearchFilter";
    private CrystalRangeSeekbar feesSb;
    private TextView maxPriceTv, minPriceTv, selectedPriceRangeTv;
    private CheckBox hospitalCb, clinicCb, individualCb, instituteCb, km2Cb, km4Cb, km6Cb, consultantCb, specialistCb;
    private RadioButton anyDayRb, todayRb, tomorrowRb;
    private CardView distanceCard, insuranceCard, titleCard, availabilityCard;
    private Spinner insuranceProviderSp;
    private ArrayAdapter<String> insuranceAdapter;
    private List<String> insuranceProviders = new ArrayList<>();
    private List<InsuranceProvidersResponseModel.Payload.Insurance> insurancesList = new ArrayList<>();
    private RadioGroup genderRg, availabilityRg;
    private Button applyBtn, resetBtn;
    private String from = "";
    private String entity = "", selectedMinFee = "10", minFee = "10", selectedMaxFee = "1000", maxFee = "1000", availability = "", distance = "", insuranceProvider = "", gender = "", title = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_nurse_search_filter);

        from = getIntent().getStringExtra("from");
        entity = getIntent().getStringExtra("entity");
        maxFee = getIntent().getStringExtra("maxFee");
        minFee = getIntent().getStringExtra("minFee");
        selectedMaxFee = getIntent().getStringExtra("selectedMaxFee");
        selectedMinFee = getIntent().getStringExtra("selectedMinFee");
        availability = getIntent().getStringExtra("availability");
        distance = getIntent().getStringExtra("distance");
        insuranceProvider = getIntent().getStringExtra("insuranceProvider");
        distance = getIntent().getStringExtra("distance");
        gender = getIntent().getStringExtra("gender");
        title = getIntent().getStringExtra("title");

        String filterValues = "\nEntity = " + entity + ",\n" +
                "maxFee = " + maxFee + ",\n" +
                "minFee = " + minFee + ",\n" +
                "selectedMaxFee = " + selectedMaxFee + ",\n" +
                "selectedMinFee = " + selectedMinFee + ",\n" +
                "availability = " + availability + ",\n" +
                "distance = " + distance + ",\n" +
                "insuranceProvider = " + insuranceProvider + ",\n" +
                "gender = " + gender + ",\n" +
                "title = " + title + ",\n";
        Log.e(TAG, "onCreate: " + filterValues);

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());

        initViews();

        insuranceAdapter = new ArrayAdapter<>(this, R.layout.row_spinner_item_simple_text, insuranceProviders);
        insuranceAdapter.setDropDownViewResource(R.layout.row_spinner_item_simple_text);
        insuranceProviderSp.setAdapter(insuranceAdapter);

        getInsuranceProviders();

        resetBtn.setOnClickListener(v -> resetFilters());
        applyBtn.setOnClickListener(v -> applyFilters());

        updateUI();

        feesSb.setOnRangeSeekbarChangeListener((minValue, maxValue) -> selectedPriceRangeTv.setText(String.format(Locale.getDefault(), "( %s%d - %s%d )", getPrefs().getCurrencySymbol(), Integer.parseInt(minValue + ""), getPrefs().getCurrencySymbol(), Integer.parseInt(maxValue + ""))));

        feesSb.setOnRangeSeekbarFinalValueListener((minValue, maxValue) -> selectedPriceRangeTv.setText(String.format(Locale.getDefault(), "( %s%d - %s%d )", getPrefs().getCurrencySymbol(), Integer.parseInt(minValue + ""), getPrefs().getCurrencySymbol(), Integer.parseInt(maxValue + ""))));

    }

    private void initViews() {
        insuranceProviderSp = findViewById(R.id.insuranceSp);
        distanceCard = findViewById(R.id.distanceCard);
        insuranceCard = findViewById(R.id.insuranceCard);
        availabilityCard = findViewById(R.id.availabilityCard);
        titleCard = findViewById(R.id.titleCard);
        applyBtn = findViewById(R.id.applyFilterBtn);
        resetBtn = findViewById(R.id.resetFilterBtn);
        hospitalCb = findViewById(R.id.hospitalCb);
        clinicCb = findViewById(R.id.clinicCb);
        individualCb = findViewById(R.id.individualCb);
        instituteCb = findViewById(R.id.instituteCb);
        feesSb = findViewById(R.id.feesSb);
        minPriceTv = findViewById(R.id.minPriceTv);
        maxPriceTv = findViewById(R.id.maxPriceTv);
        selectedPriceRangeTv = findViewById(R.id.selectedPriceRangeTv);
        availabilityRg = findViewById(R.id.availabilityRg);
        anyDayRb = findViewById(R.id.anyDayRb);
        todayRb = findViewById(R.id.todayRb);
        tomorrowRb = findViewById(R.id.tomorrowRb);
        km2Cb = findViewById(R.id.km2Cb);
        km4Cb = findViewById(R.id.km4Cb);
        km6Cb = findViewById(R.id.km6Cb);
        genderRg = findViewById(R.id.genderRg);
        consultantCb = findViewById(R.id.consultantCb);
        specialistCb = findViewById(R.id.specialistCb);
    }

    private void getInsuranceProviders() {
        if (isOnline(this)) {

            VolleyHelper.getInstance(this).addRequest(getAllInsuranceProvidersURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    InsuranceProvidersResponseModel model = new Gson().fromJson(response, InsuranceProvidersResponseModel.class);
                    if (model.getStatus() == 1) {

                        if (model.getPayload().getInsurances().size() > 0) {
                            insurancesList.clear();
                            InsuranceProvidersResponseModel.Payload.Insurance insurance = new InsuranceProvidersResponseModel.Payload.Insurance();
                            insurance.setName(getString(R.string.select_insurance_provider));
                            insurancesList.add(insurance);
                            insurancesList.addAll(model.getPayload().getInsurances());

                            insuranceProviders.clear();
                            for (int i = 0; i < insurancesList.size(); i++) {
                                insuranceProviders.add(insurancesList.get(i).getName());
                            }

                            insuranceAdapter.notifyDataSetChanged();
                        }

                    } else {
                        showToast(DoctorNurseSearchFilter.this, model.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    showToast(DoctorNurseSearchFilter.this, error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    showUserRestrictedDialog(DoctorNurseSearchFilter.this);
                }
            });

        } else {
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        hospitalCb.setChecked(entity.contains(HOSPITAL));
        clinicCb.setChecked(entity.contains(CLINIC));
        individualCb.setChecked(entity.contains(INDIVIDUAL));
        instituteCb.setChecked(entity.contains(INSTITUTE));
        availabilityRg.check(availability.contains(TODAY) ? R.id.todayRb : availability.contains(TOMORROW) ? R.id.tomorrowRb : R.id.anyDayRb);
        km2Cb.setChecked(distance.toLowerCase().contains("2"));
        km4Cb.setChecked(distance.toLowerCase().contains("4"));
        km6Cb.setChecked(distance.toLowerCase().contains("6"));
        genderRg.check(gender.isEmpty() ? R.id.anyRb : gender.equals(FEMALE) ? R.id.femaleRb : R.id.maleRb);
        consultantCb.setChecked(title.contains(CONSULTANT));
        specialistCb.setChecked(title.contains(SPECIALIST));

        minPriceTv.setText(String.format(Locale.getDefault(), "%s %s", getPrefs().getCurrencySymbol(), minFee));
        maxPriceTv.setText(String.format(Locale.getDefault(), "%s %s", getPrefs().getCurrencySymbol(), maxFee));

        if (from.equals(SEARCH_DOCTOR)) {
            hospitalCb.setVisibility(View.VISIBLE);
            clinicCb.setVisibility(View.VISIBLE);
            availabilityCard.setVisibility(View.VISIBLE);
            titleCard.setVisibility(View.VISIBLE);
            insuranceCard.setVisibility(View.VISIBLE);

            individualCb.setVisibility(View.GONE);
            instituteCb.setVisibility(View.GONE);
            distanceCard.setVisibility(View.GONE);

        } else {
            hospitalCb.setVisibility(View.GONE);
            clinicCb.setVisibility(View.GONE);
            availabilityCard.setVisibility(View.GONE);
            distanceCard.setVisibility(View.GONE);
            titleCard.setVisibility(View.GONE);

            individualCb.setVisibility(View.VISIBLE);
            instituteCb.setVisibility(View.VISIBLE);
            insuranceCard.setVisibility(View.VISIBLE);

            int index = insuranceProviders.indexOf(insuranceProvider);
            insuranceProviderSp.setSelection(index);
        }

        feesSb.setMinStartValue(Float.parseFloat(selectedMinFee)).apply();
        feesSb.setMaxStartValue(Float.parseFloat(selectedMaxFee)).apply();

        feesSb.setMinValue(Integer.parseInt(minFee)).apply();
        feesSb.setMaxValue(Integer.parseInt(maxFee)).apply();

    }

    private void applyFilters() {
        String entity = hospitalCb.isChecked() ? HOSPITAL : "";
        entity = clinicCb.isChecked() ? (entity.isEmpty() ? CLINIC : entity + "," + CLINIC) : entity;
        entity = individualCb.isChecked() ? (entity.isEmpty() ? INDIVIDUAL : entity + "," + INDIVIDUAL) : entity;
        entity = instituteCb.isChecked() ? (entity.isEmpty() ? INSTITUTE : entity + "," + INSTITUTE) : entity;

        String availability = availabilityRg.getCheckedRadioButtonId() == R.id.todayRb ? TODAY : availabilityRg.getCheckedRadioButtonId() == R.id.tomorrowRb ? TOMORROW : ANY_DAY;

        String distance = km2Cb.isChecked() ? "2" : "";
        distance = km4Cb.isChecked() ? (distance.isEmpty() ? "4" : distance + ",4") : distance;
        distance = km6Cb.isChecked() ? (distance.isEmpty() ? "6" : distance + ",6") : distance;

        String title = consultantCb.isChecked() ? CONSULTANT : "";
        title = specialistCb.isChecked() ? (title.isEmpty() ? SPECIALIST : title + "," + SPECIALIST) : title;

        String gender = genderRg.getCheckedRadioButtonId() == R.id.anyRb ? "" : genderRg.getCheckedRadioButtonId() == R.id.maleRb ? MALE : FEMALE;

        String insuranceProvider = insuranceProviderSp.getSelectedItemPosition() == 0 ? "" : insuranceProviderSp.getSelectedItemPosition() == -1 ? "" : insurancesList.get(insuranceProviderSp.getSelectedItemPosition()).getId();

        selectedMinFee = feesSb.getSelectedMinValue() + "";
        selectedMaxFee = feesSb.getSelectedMaxValue() + "";

        setResult(RESULT_OK, new Intent()
                .putExtra("entity", entity)
                .putExtra("availability", availability)
                .putExtra("distance", distance)
                .putExtra("insuranceProvider", insuranceProvider)
                .putExtra("title", title)
                .putExtra("gender", gender)
                .putExtra("minFee", minFee)
                .putExtra("maxFee", maxFee)
                .putExtra("selectedMinFee", selectedMinFee)
                .putExtra("selectedMaxFee", selectedMaxFee)
        );
        finish();
    }

    private void resetFilters() {

        hospitalCb.setChecked(from.equals(SEARCH_DOCTOR));
        clinicCb.setChecked(from.equals(SEARCH_DOCTOR));

        individualCb.setChecked(from.equals(SEARCH_NURSE));
        instituteCb.setChecked(from.equals(SEARCH_NURSE));

        km2Cb.setChecked(false);
        km4Cb.setChecked(false);
        km6Cb.setChecked(false);
        consultantCb.setChecked(from.equals(SEARCH_DOCTOR));
        specialistCb.setChecked(from.equals(SEARCH_DOCTOR));
        genderRg.check(R.id.maleRb);
        availabilityRg.check(R.id.anyDayRb);
        insuranceProviderSp.setSelection(0);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}