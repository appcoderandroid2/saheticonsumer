package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.CityAreaListRvAdapter;
import com.iapps.sehatee.models.CityAreaModel;
import com.iapps.sehatee.models.CityModel;
import com.iapps.sehatee.models.ResponseModels.CityAreaResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getCityAreaListURL;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class SelectArea extends AppCompatActivity implements CityAreaListRvAdapter.OnItemClickedHelper {

    private static final String TAG = "SelectArea";
    private TextView cityNameTv;
    private RecyclerView rv;
    private CityAreaListRvAdapter adapter;
    private List<CityAreaModel> list = new ArrayList<>();
    private CityModel model = new CityModel();

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_area);

        model = (CityModel) getIntent().getSerializableExtra("model");

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());

        rv = findViewById(R.id.rv);
        cityNameTv = findViewById(R.id.cityNameTv);

        rv.setAdapter(adapter = new CityAreaListRvAdapter(list, this));
        rv.setLayoutManager(new LinearLayoutManager(this));

        cityNameTv.setText(model.getCityName());

        getAreas();

    }

    private void getAreas() {
        if (isOnline(this)) {

            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                dialog.show();
            }
            VolleyHelper.getInstance(this).addRequest(getCityAreaListURL(model.getCityId()), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    CityAreaResponseModel model = new Gson().fromJson(response, CityAreaResponseModel.class);
                    if (model.getStatus() == 1) {
                        if (model.getPayload().getArea().size() > 0) {
                            list.clear();
                            CityAreaModel cityAreaModel = new CityAreaModel();
                            cityAreaModel.setChecked(false);
                            cityAreaModel.setAreaName(getString(R.string.all_areas));
                            cityAreaModel.setAreaId("0");
                            list.add(cityAreaModel);
                            list.addAll(model.getPayload().getArea());
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        showToast(SelectArea.this, model.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onRestrictedByAdmin() {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(SelectArea.this);
                }
            });
        } else {
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void proceed() {
        int index = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isChecked()) {
                index = i;
                break;
            }
        }
        getPrefs().setCityArea(list.get(index).getAreaName());
        if (index == 0) {
            setResult(RESULT_OK, new Intent()
                    .putExtra("cityName", model.getCityName())
                    .putExtra("areaName", getString(R.string.all_areas))
            );
            finish();
            Log.e(TAG, "proceed: " + "CityName = " + model.getCityName() + ", Area = All Areas");
        } else {
            setResult(RESULT_OK, new Intent()
                    .putExtra("cityName", model.getCityName())
                    .putExtra("areaName", list.get(index).getAreaName())
            );
            finish();
            Log.e(TAG, "proceed: " + "CityName = " + model.getCityName() + ", Area = " + list.get(index).getAreaName());
        }

    }

    @Override
    public void onItemChecked(int pos) {
        if (pos > -1) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setChecked(false);
            }
            list.get(pos).setChecked(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (rv.isAnimating()) {
                        new Handler().postDelayed(this, 100);
                    } else {
                        adapter.notifyDataSetChanged();
                        proceed();
                    }
                }
            }, 300);

        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}