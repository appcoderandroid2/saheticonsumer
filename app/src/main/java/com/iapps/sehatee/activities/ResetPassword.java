package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import static com.iapps.sehatee.utils.APIs.getResetPasswordURL;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class ResetPassword extends AppCompatActivity {

    private static final String TAG = "ResetPassword";
    private EditText newPassEt, conPassEt;
    private Button resetPassBtn;
    private String userId = "", otp = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_image);
        setContentView(R.layout.activity_reset_password);

        if (getIntent() != null) {
            userId = getIntent().getStringExtra("userId");
            otp = getIntent().getStringExtra("otp");
        }

        newPassEt = findViewById(R.id.passEt);
        conPassEt = findViewById(R.id.conPassEt);
        resetPassBtn = findViewById(R.id.resetPassBtn);

        resetPassBtn.setOnClickListener((v) -> {
            v.setEnabled(false);
            String pass = newPassEt.getText().toString().trim();
            String conPass = conPassEt.getText().toString().trim();

            if (validate(pass, conPass)) {
                resetPassword(v, pass);
            } else {
                v.setEnabled(true);
            }
        });

        conPassEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                resetPassBtn.performClick();
                return true;
            }
            return false;
        });


    }

    private boolean validate(String pass, String conPass) {

        if (pass.isEmpty()) {
            showToast(this, R.string.error_empty_password);
            newPassEt.requestFocus();
            return false;
        }
        if (conPass.isEmpty()) {
            showToast(this, R.string.error_empty_password);
            conPassEt.requestFocus();
            return false;
        }
        if (pass.length() < 6) {
            showToast(this, R.string.error_password_too_short);
            newPassEt.requestFocus();
            newPassEt.setSelection(pass.length());
            return false;
        }
        if (conPass.length() < 6) {
            showToast(this, R.string.error_password_too_short);
            conPassEt.requestFocus();
            conPassEt.setSelection(pass.length());
            return false;
        }
        if (!pass.equals(conPass)) {
            showToast(this, R.string.error_passwords_didnt_match);
            conPassEt.requestFocus();
            return false;
        }
        return true;
    }

    private void resetPassword(View v, String pass) {
        if (isOnline(this)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userId", userId);
                jsonObject.put("password", pass);

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(this).addRequest(getResetPasswordURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            showToast(ResetPassword.this, R.string.password_reset_successful);
                            finishAffinity();
                            startActivity(new Intent(ResetPassword.this, Login.class));
                        } else {
                            showToast(ResetPassword.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(ResetPassword.this);
                    }
                });

            } catch (JSONException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "resetPassword: ", e);
                showToast(this, R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            showToast(this, R.string.error_internet_unavilable);
        }
    }
}