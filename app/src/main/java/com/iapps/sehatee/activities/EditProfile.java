package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.LogRegResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

import static com.iapps.sehatee.utils.APIs.getUpdateProfileURL;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.Constants.updateUserPrefData;

public class EditProfile extends AppCompatActivity {

    private static final String TAG = "EditProfile";
    private EditText nameEt, phEt, addressEt, cityEt, areaEt, postalEt, aboutMeEt;
    private Button submitBtn;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        findViewById(R.id.backIv).setOnClickListener((v) -> onBackPressed());

        nameEt = findViewById(R.id.nameEt);
        phEt = findViewById(R.id.phEt);
        addressEt = findViewById(R.id.addressEt);
        cityEt = findViewById(R.id.cityEt);
        areaEt = findViewById(R.id.areaEt);
        postalEt = findViewById(R.id.postalCodeEt);
        aboutMeEt = findViewById(R.id.aboutMeEt);
        submitBtn = findViewById(R.id.submitBtn);

        nameEt.setText(getPrefs().getFullName());
        phEt.setText(getPrefs().getPhone().equals(NA) ? "" : getPrefs().getPhone());
        addressEt.setText(getPrefs().getAddress());
        cityEt.setText(getPrefs().getCity());
        areaEt.setText(getPrefs().getCityArea());
        postalEt.setText(getPrefs().getPostalCode());
        aboutMeEt.setText(getPrefs().getAboutMe());

        submitBtn.setOnClickListener(v -> {
            v.setEnabled(false);
            String name = nameEt.getText().toString().trim();
            if (name.isEmpty()) {
                showToast(EditProfile.this, R.string.error_name_is_empty);
                v.setEnabled(true);
                nameEt.requestFocus();
                return;
            }
            submit(v);
        });

    }

    private void submit(View v) {
        if (isOnline(this)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", URLEncoder.encode(nameEt.getText().toString().trim().isEmpty() ? NA : nameEt.getText().toString().trim()));
                jsonObject.put("phone", URLEncoder.encode(phEt.getText().toString().trim().isEmpty() ? NA : phEt.getText().toString().trim()));
                jsonObject.put("address", URLEncoder.encode(addressEt.getText().toString().trim().isEmpty() ? NA : addressEt.getText().toString().trim()));
                jsonObject.put("cityName", URLEncoder.encode(cityEt.getText().toString().trim().isEmpty() ? NA : cityEt.getText().toString().trim()));
                jsonObject.put("areaName", URLEncoder.encode(areaEt.getText().toString().trim().isEmpty() ? NA : areaEt.getText().toString().trim()));
                jsonObject.put("postalCode", URLEncoder.encode(postalEt.getText().toString().trim().isEmpty() ? NA : postalEt.getText().toString().trim()));
                jsonObject.put("aboutMe", URLEncoder.encode(aboutMeEt.getText().toString().trim().isEmpty() ? NA : aboutMeEt.getText().toString().trim()));

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }
                VolleyHelper.getInstance(this).addRequest(getUpdateProfileURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);
                        if (model.getStatus() == 1) {
                            updateUserPrefData(model.getPayload().getUser());
                            showToast(EditProfile.this, R.string.profile_updated_successfully);
                            Log.e(TAG, "onSuccess: UserData => " + model.getPayload().getUser().toString());

                            setResult(RESULT_OK);
                            finish();
                        } else {
                            showToast(EditProfile.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showToast(EditProfile.this, error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(EditProfile.this);
                    }
                });

            } catch (JSONException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "submit: ", e);
                showToast(this, R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            showToast(this, R.string.error_internet_unavilable);
        }

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}