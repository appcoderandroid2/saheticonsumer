package com.iapps.sehatee.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.LogRegResponseModel;
import com.iapps.sehatee.utils.Constants;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Objects;

import static com.iapps.sehatee.utils.APIs.getRegisterURL;
import static com.iapps.sehatee.utils.Constants.FEMALE;
import static com.iapps.sehatee.utils.Constants.LOGIN;
import static com.iapps.sehatee.utils.Constants.MALE;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_ddMMMyyyy;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.helperClasses.VolleyHelper.POST;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SignUp";
    private RadioGroup genderRg;
    private EditText nameEt, phEt, emailEt, passwordEt;
    private TextView dobTv, loginTv;
    private Button signUpBtn;
    private String selectedDob = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_image);
        setContentView(R.layout.activity_sign_up);

        initViews();
        initClicks();

        passwordEt.setTextDirection(getPrefs().getAppLanguageCode().equals("ar") ? View.TEXT_DIRECTION_RTL : View.TEXT_DIRECTION_LTR);

    }

    private void initViews() {
        nameEt = findViewById(R.id.nameEt);
        phEt = findViewById(R.id.phEt);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passEt);
        genderRg = findViewById(R.id.genderRg);
        dobTv = findViewById(R.id.dobTv);
        loginTv = findViewById(R.id.loginTv);
        signUpBtn = findViewById(R.id.signUpBtn);
    }

    private void initClicks() {
        loginTv.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
        dobTv.setOnClickListener(this);
    }

    private void showDatePicker() {
        Calendar cal = Calendar.getInstance();
        if (!dobTv.getText().toString().isEmpty()) {
            try {
                cal.setTime(Objects.requireNonNull(sdf_ddMMMyyyy.parse(dobTv.getText().toString())));
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "showDatePicker: ", e);
            }
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            selectedDob = sdf_yyyyMMdd.format(calendar.getTime());
            dobTv.setText(sdf_ddMMMyyyy.format(calendar.getTime()));
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        if (datePickerDialog.getWindow() != null) {
            datePickerDialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }
        datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        if (!isFinishing()) {
            datePickerDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpBtn:
                v.setEnabled(false);
                String fullName = nameEt.getText().toString().trim();
                String email = emailEt.getText().toString().trim();
                String phone = phEt.getText().toString().trim();
                String pass = passwordEt.getText().toString().trim();
                String gender = genderRg.getCheckedRadioButtonId() == -1 ? "" : genderRg.getCheckedRadioButtonId() == R.id.maleRb ? MALE : FEMALE;

                if (validate(fullName, email, phone, pass, gender, selectedDob)) {
                    signUp(v, fullName, email, phone, pass, gender, selectedDob);
                } else {
                    v.setEnabled(true);
                }
                break;
            case R.id.loginTv:
                finish();
                break;
            case R.id.dobTv:
                showDatePicker();
                break;
        }
    }

    private boolean validate(String fullName, String email, String phone, String pass, String gender, String dob) {
        if (fullName.isEmpty()) {
            showToast(this, R.string.error_name_is_empty);
            nameEt.requestFocus();
            return false;
        }
        if (gender.isEmpty()) {
            showToast(this, R.string.error_select_gender);
            return false;
        }
        if (email.isEmpty()) {
            showToast(this, R.string.error_email_is_empty_or_invalid);
            emailEt.requestFocus();
            return false;
        }
        if (phone.isEmpty()) {
            showToast(this, R.string.error_phone_number_is_empty_or_invalid);
            phEt.requestFocus();
            return false;
        }
        if (pass.isEmpty()) {
            showToast(this, R.string.error_empty_password);
            passwordEt.requestFocus();
            return false;
        }
        if (dob.isEmpty()) {
            showToast(this, R.string.error_dob_not_set);
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showToast(this, R.string.error_email_is_empty_or_invalid);
            emailEt.requestFocus();
            emailEt.setSelection(email.length());
            return false;
        }
        if (!Patterns.PHONE.matcher(phone).matches()) {
            showToast(this, R.string.error_phone_number_is_empty_or_invalid);
            phEt.requestFocus();
            phEt.setSelection(email.length());
            return false;
        }
        if (pass.length() < 6) {
            showToast(this, R.string.error_password_too_short);
            passwordEt.requestFocus();
            passwordEt.setSelection(pass.length());
            return false;
        }
        return true;
    }

    private void signUp(View v, String name, String email, String phone, String pass, String gender, String dob) {
        if (isOnline(this)) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", URLEncoder.encode(name, "utf-8"));
                jsonObject.put("email", email);
                jsonObject.put("phone", phone);
                jsonObject.put("password", URLEncoder.encode(pass, "utf-8"));
                jsonObject.put("gender", gender);
                jsonObject.put("dateOfBirth", dob);

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(this).addRequest(getRegisterURL(), POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);
                        if (model.getStatus() == 1) {
                            getPrefs().setAuthToken(model.getPayload().getTokenType() + " " + model.getPayload().getToken());
                            Constants.updateUserPrefData(model.getPayload().getUser());
                            getPrefs().setLoggedIn(true);

                            if (!getPrefs().getSelectedCityId().equals(NA)) {
                                startActivity(new Intent(SignUp.this, Home.class));
                            } else {
                                startActivity(new Intent(SignUp.this, SelectCity.class).putExtra("from", LOGIN).putExtra("cityName", "").putExtra("areaName", ""));
                            }
                            finishAffinity();
                        } else {
                            showToast(SignUp.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showToast(SignUp.this, error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(SignUp.this);
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "signUp: ", e);
                showToast(this, R.string.something_went_wrong);
            }

        } else {
            v.setEnabled(true);
            showToast(this, R.string.error_internet_unavilable);
        }
    }
}