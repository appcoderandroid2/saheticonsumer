package com.iapps.sehatee.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.fragments.introFrags.IntroOneFrag;
import com.iapps.sehatee.fragments.introFrags.IntroThreeFrag;
import com.iapps.sehatee.fragments.introFrags.IntroTwoFrag;

import static com.iapps.sehatee.utils.Constants.LOGIN;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.setLocale;

public class IntroductionActivity extends AppCompatActivity {

    public static final String ANIMATE_SECOND_SLIDE = "ANIMATE_SECOND_SLIDE";
    public static final String ANIMATE_THIRD_SLIDE = "ANIMATE_THIRD_SLIDE";
    private View jockey;
    private ImageButton nextBtn;
    private Button skipBtn;
    private ViewPager vp;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        vp = findViewById(R.id.vp);
        jockey = findViewById(R.id.jockey);
        nextBtn = findViewById(R.id.nextBtn);
        skipBtn = findViewById(R.id.skipBtn);

        VpAdapter adapter = new VpAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFrag(new IntroOneFrag(), "");
        adapter.addFrag(new IntroTwoFrag(), "");
        adapter.addFrag(new IntroThreeFrag(), "");
        vp.setAdapter(adapter);

        nextBtn.setOnClickListener(v -> {
            int index = vp.getCurrentItem() + 1;

            if (index > 2) {
                moveForward();
            } else {
                vp.setCurrentItem(index, true);
                if (index == 2) {
                    nextBtn.setImageResource(R.drawable.ic_round_check_32);
                } else {
                    nextBtn.setImageResource(R.drawable.ic_round_arrow_forward_32);
                }
            }

        });

        skipBtn.setOnClickListener(v -> moveForward());

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                TransitionManager.beginDelayedTransition((ViewGroup) jockey.getParent());
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) jockey.getLayoutParams();
                layoutParams.gravity = position == 1 ? Gravity.CENTER : position == 2 ? Gravity.END : Gravity.START;
                jockey.setLayoutParams(layoutParams);

                LocalBroadcastManager.getInstance(IntroductionActivity.this).sendBroadcast(new Intent(position == 1 ? ANIMATE_SECOND_SLIDE : ANIMATE_THIRD_SLIDE));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void moveForward() {
        getPrefs().setIntroShown(true);

        if (getPrefs().isLoggedIn()) {
            if (!getPrefs().getSelectedCityId().equals(NA)) {
                startActivity(new Intent(IntroductionActivity.this, Home.class));
            } else {
                startActivity(new Intent(IntroductionActivity.this, SelectCity.class).putExtra("from", LOGIN).putExtra("cityName", "").putExtra("areaName", ""));
            }
        } else {
            startActivity(new Intent(IntroductionActivity.this, Login.class));
        }
        finish();
    }
}