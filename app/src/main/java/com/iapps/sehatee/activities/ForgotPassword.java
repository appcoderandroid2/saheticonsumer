package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.ForgotPasswordResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import static com.iapps.sehatee.utils.APIs.getForgotPasswordURL;
import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class ForgotPassword extends AppCompatActivity {

    private static final String TAG = "ForgotPassword";
    private EditText userIdEt;
    private Button submitBtn;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_image);
        setContentView(R.layout.activity_forgot_password);

        userIdEt = findViewById(R.id.emailOrPhEt);
        submitBtn = findViewById(R.id.submitBtn);

        submitBtn.setOnClickListener((v) -> {
            v.setEnabled(false);
            String userId = userIdEt.getText().toString();
            if (validate(userId)) {
                sendOtp(v, userId);
            }

        });

        userIdEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                submitBtn.performClick();
                return true;
            }
            return false;
        });

    }

    private boolean validate(String userId) {

        if (userId.isEmpty()) {
            showToast(this, R.string.error_invalid_user_id);
            userIdEt.requestFocus();
            userIdEt.setSelection(userId.length());
            return false;
        }

        if (!Patterns.PHONE.matcher(userId).matches() && !Patterns.EMAIL_ADDRESS.matcher(userId).matches()) {
            showToast(this, R.string.error_invalid_user_id);
            userIdEt.requestFocus();
            userIdEt.setSelection(userId.length());
            return false;
        }

        return true;
    }

    private void sendOtp(View v, String userId) {

        if (isOnline(this)) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userType", CUSTOMER);
                jsonObject.put("email", userId);

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(this).addRequest(getForgotPasswordURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        ForgotPasswordResponseModel model = new Gson().fromJson(response, ForgotPasswordResponseModel.class);
                        if (model.getStatus() == 1) {

                            startActivity(new Intent(ForgotPassword.this, ResetPassword.class)
//                            startActivity(new Intent(ForgotPassword.this, OTP.class)
                                            .putExtra("email", userId)
                                            .putExtra("userId", model.getPayload().getUserId())
                                            .putExtra("otp", model.getPayload().getOtp())
                            );
                        } else {
                            showToast(ForgotPassword.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showToast(ForgotPassword.this, error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(ForgotPassword.this);
                    }
                });

            } catch (JSONException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "sendOtp: ", e);
                showToast(this, R.string.something_went_wrong);
            }

        } else {
            v.setEnabled(true);
            showToast(this, R.string.error_internet_unavilable);
        }
    }
}