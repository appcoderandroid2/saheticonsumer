package com.iapps.sehatee.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.LogRegResponseModel;
import com.iapps.sehatee.utils.Constants;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Objects;

import static com.iapps.sehatee.utils.APIs.getSocialLoginURL;
import static com.iapps.sehatee.utils.Constants.FEMALE;
import static com.iapps.sehatee.utils.Constants.LOGIN;
import static com.iapps.sehatee.utils.Constants.MALE;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_ddMMMyyyy;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.helperClasses.VolleyHelper.POST;

public class FewMoreInformation extends AppCompatActivity {

    private static final String TAG = "FewMoreInformation";
    private RadioGroup genderRg;
    private View emailDivider;
    private EditText emailEt;
    private TextView dobTv;
    private Button proceedBtn;
    private String selectedDate = "";
    private String email = "", socialId = "", profilePic = "", name = "", provider = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_few_more_information);

        email = getIntent().getStringExtra("email");
        socialId = getIntent().getStringExtra("socialId");
        profilePic = getIntent().getStringExtra("profilePic");
        name = getIntent().getStringExtra("name");
        provider = getIntent().getStringExtra("provider");

        emailEt = findViewById(R.id.emailEt);
        emailDivider = findViewById(R.id.emailDivider);
        dobTv = findViewById(R.id.dobTv);
        genderRg = findViewById(R.id.genderRg);
        proceedBtn = findViewById(R.id.proceedBtn);

        try {
            emailEt.setVisibility(email.isEmpty() ? View.VISIBLE : View.GONE);
            emailDivider.setVisibility(email.isEmpty() ? View.VISIBLE : View.GONE);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e(TAG, "onCreate: ", e);
            emailEt.setVisibility(View.VISIBLE);
            emailDivider.setVisibility(View.VISIBLE);
        }

        emailEt.setText(emailEt.getVisibility() == View.VISIBLE ? "" : email);

        dobTv.setOnClickListener((v) -> showDatePicker());

        proceedBtn.setOnClickListener(v -> {
            v.setEnabled(false);
            String em = emailEt.getText().toString().trim();
            String gender = genderRg.getCheckedRadioButtonId() == -1 ? "" : genderRg.getCheckedRadioButtonId() == R.id.maleRb ? MALE : FEMALE;
            if (validate(em, gender)) {
                signUp(v, em, gender);
            } else {
                v.setEnabled(true);
            }
        });

    }

    private void showDatePicker() {
        Calendar cal = Calendar.getInstance();
        if (!dobTv.getText().toString().isEmpty()) {
            try {
                cal.setTime(Objects.requireNonNull(sdf_ddMMMyyyy.parse(dobTv.getText().toString())));
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "showDatePicker: ", e);
            }
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            selectedDate = sdf_yyyyMMdd.format(calendar.getTime());
            dobTv.setText(sdf_ddMMMyyyy.format(calendar.getTime()));
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        if (datePickerDialog.getWindow() != null) {
            datePickerDialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }
        datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        if (!isFinishing()) {
            datePickerDialog.show();
        }
    }

    private boolean validate(String em, String gender) {

        if (em.isEmpty()) {
            showToast(this, R.string.error_email_is_empty_or_invalid);
            emailEt.requestFocus();
            return false;
        }
        if (gender.isEmpty()) {
            showToast(this, R.string.error_select_gender);
            return false;
        }
        if (selectedDate.isEmpty()) {
            showToast(this, R.string.error_dob_not_set);
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showToast(this, R.string.error_email_is_empty_or_invalid);
            emailEt.requestFocus();
            emailEt.setSelection(email.length());
            return false;
        }
        return true;
    }

    private void signUp(View v, String em, String gender) {
        if (isOnline(this)) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("provider", provider);
                jsonObject.put("socialId", socialId);
                jsonObject.put("name", URLEncoder.encode(name, "utf-8"));
                jsonObject.put("email", em);
                jsonObject.put("gender", gender);
                jsonObject.put("dateOfBirth", selectedDate);
                jsonObject.put("profilePic", profilePic);
                jsonObject.put("isEmailVerified", email.isEmpty() ? 0 : 1);

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(this).addRequest(getSocialLoginURL(), POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);
                        if (model.getStatus() == 1) {
                            getPrefs().setAuthToken(model.getPayload().getTokenType() + " " + model.getPayload().getToken());
                            Constants.updateUserPrefData(model.getPayload().getUser());
                            getPrefs().setLoggedIn(true);

                            showToast(FewMoreInformation.this, R.string.welcome_to_sahety);

                            if (!getPrefs().getSelectedCityId().equals(NA)) {
                                startActivity(new Intent(FewMoreInformation.this, Home.class));
                            } else {
                                startActivity(new Intent(FewMoreInformation.this, SelectCity.class).putExtra("from", LOGIN).putExtra("cityName", "").putExtra("areaName", ""));
                            }
                            finishAffinity();
                        } else {
                            showToast(FewMoreInformation.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showToast(FewMoreInformation.this, error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(FewMoreInformation.this);
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "signUp: ", e);
                showToast(this, R.string.something_went_wrong);
            }

        } else {
            v.setEnabled(true);
            showToast(this, R.string.error_internet_unavilable);
        }
    }
}