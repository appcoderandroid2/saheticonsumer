package com.iapps.sehatee.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.iapps.sehatee.BuildConfig;
import com.iapps.sehatee.R;

import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.LOGIN;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.setLocale;

public class Splash extends AppCompatActivity {

    private static final String TAG = "Splash";
    private Handler handler = new Handler();
    private Runnable runnable = () -> {

        Log.e(TAG, "Selected City Id = > " + getPrefs().getSelectedCityId());

        if (getPrefs().isLoggedIn()) {
            if (!getPrefs().getSelectedCityId().equals(NA)) {
                startActivity(new Intent(Splash.this, Home.class));
            } else {
                startActivity(new Intent(Splash.this, SelectCity.class).putExtra("from", LOGIN).putExtra("cityName", "").putExtra("areaName", ""));
            }
        } else {

            if (getPrefs().getIntroShown()) {
                startActivity(new Intent(Splash.this, Login.class));
            } else {
                startActivity(new Intent(this, IntroductionActivity.class));
            }

        }
        finish();
    };

    @Override
    protected void onResume() {
        super.onResume();
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 500);
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_image);
        setContentView(R.layout.activity_splash);

        TextView versionTv = findViewById(R.id.versionTv);
        versionTv.setText(String.format(Locale.getDefault(), getString(R.string.version) + " - %s", BuildConfig.VERSION_NAME));

    }
}