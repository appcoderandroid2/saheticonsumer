package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.fragments.PhotoViewFrag;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.models.DocumentModel;
import com.iapps.sehatee.utils.helperClasses.DownloadHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getDoubleBtnDialog;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;

public class PhotoViewerActivity extends AppCompatActivity {

    private static final String TAG = "PhotoViewerActivity";
    private TextView countTv;
    private ViewPager vp;
    private VpAdapter adapter;
    private List<DocumentModel> list = new ArrayList<>();
    private boolean canShareOrDownload = true;
    private DownloadHelper mDm;
    private int RC_OPEN_APP_INFO_SETTINGS = 2002;
    private int SHARE = 0, DOWNLOAD = 1;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);

        list = (List<DocumentModel>) getIntent().getSerializableExtra("list");

        vp = findViewById(R.id.vp);
        countTv = findViewById(R.id.countTv);

        adapter = new VpAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        for (int i = 0; i < list.size(); i++) {
            PhotoViewFrag frag = new PhotoViewFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(i));
            frag.setArguments(bundle);
            adapter.addFrag(frag, "image" + (i + 1));
        }

        vp.setAdapter(adapter);

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                countTv.setText(String.format(Locale.getDefault(), "%d / %d", position + 1, list.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                canShareOrDownload = state == ViewPager.SCROLL_STATE_IDLE;
            }
        });

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.shareIv).setOnClickListener(v -> shareFile());
        findViewById(R.id.downloadIv).setOnClickListener(v -> downloadFile());

        countTv.setText(String.format(Locale.getDefault(), "%d / %d", 1, list.size()));

        vp.setCurrentItem(getIntent().getIntExtra("pos", 0));

    }

    private void shareFile() {
        if (canShareOrDownload) {
            mDm = new DownloadHelper.Builder(this)
                    .setFileName(list.get(vp.getCurrentItem()).getFileName())
                    .setFileUrl(list.get(vp.getCurrentItem()).getUrl())
                    .setFileExtension(list.get(vp.getCurrentItem()).getFileName())
                    .setShowAsNotification(false)
                    .build(new DownloadHelper.OnDownloadProgressHelper() {
                        @Override
                        public void onDownloadStarted() {
                            //DO nothing here while sharing
                        }

                        @Override
                        public void onDownloadComplete(DownloadManager dm, DownloadHelper.DownloadedFile file) {
                            Intent shareIntent = ShareCompat.IntentBuilder.from(PhotoViewerActivity.this)
                                    .setType("application/pdf")
                                    .setStream(file.getDownloadedFileUri())
                                    .createChooserIntent()
                                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                    .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            if (shareIntent.resolveActivity(getPackageManager()) != null) {
                                startActivity(shareIntent);
                            } else {
                                runOnUiThread(() -> showToast(PhotoViewerActivity.this, R.string.something_went_wrong));
                            }
                        }

                        @Override
                        public void onDownloadProgress(int percent) {
                            Log.e(TAG, "onDownloadProgress: Progress => " + percent);
                        }

                        @Override
                        public void onDownloadFailed() {
                            runOnUiThread(() -> showToast(PhotoViewerActivity.this, R.string.share_failed));
                        }

                        @Override
                        public void onPermissionDenied() {
                            if (!isFinishing()) {
                                String msg = getString(R.string.error_note_sharing_write_storage_permission_denied_earlier);
                                getDoubleBtnDialog(PhotoViewerActivity.this, getString(R.string.alert), msg, getString(R.string.not_now), getString(R.string.grant_permission), new OnDialogActionHelper() {
                                    @Override
                                    public void onPositiveBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                        mDm.start();
                                    }

                                    @Override
                                    public void onNegativeBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                    }
                                }).show();
                            }
                        }

                        @Override
                        public void onNeverAskPermissionChecked() {
                            if (!isFinishing()) {
                                String msg = getString(R.string.error_note_sharing_write_storage_permission_denied_and_checked_never_ask);
                                getDoubleBtnDialog(PhotoViewerActivity.this, getString(R.string.alert), msg, getString(R.string.not_now), getString(R.string.app_info), new OnDialogActionHelper() {
                                    @Override
                                    public void onPositiveBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                        openAppInfoSettings(SHARE);
                                    }

                                    @Override
                                    public void onNegativeBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                    }
                                }).show();
                            }
                        }
                    });

            mDm.start();
        }
    }

    private void downloadFile() {
        if (canShareOrDownload) {
            mDm = new DownloadHelper.Builder(this)
                    .setFileName(list.get(vp.getCurrentItem()).getFileName())
                    .setFileUrl(list.get(vp.getCurrentItem()).getUrl())
                    .setFileExtension(list.get(vp.getCurrentItem()).getFileName())
                    .build(new DownloadHelper.OnDownloadProgressHelper() {
                        @Override
                        public void onDownloadStarted() {
                            runOnUiThread(() -> showToast(PhotoViewerActivity.this, R.string.download_started));
                        }

                        @Override
                        public void onDownloadComplete(DownloadManager dm, DownloadHelper.DownloadedFile file) {
                            runOnUiThread(() -> showToast(PhotoViewerActivity.this, R.string.download_finished));
                        }

                        @Override
                        public void onDownloadProgress(int percent) {
                            Log.e(TAG, "onDownloadProgress: Progress => " + percent);
                        }

                        @Override
                        public void onDownloadFailed() {
                            runOnUiThread(() -> showToast(PhotoViewerActivity.this, R.string.download_failed));
                        }

                        @Override
                        public void onPermissionDenied() {
                            if (!isFinishing()) {
                                String msg = getString(R.string.error_note_downloading_write_storage_permission_denied_earlier);
                                getDoubleBtnDialog(PhotoViewerActivity.this, getString(R.string.alert), msg, getString(R.string.not_now), getString(R.string.grant_permission), new OnDialogActionHelper() {
                                    @Override
                                    public void onPositiveBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                        mDm.start();
                                    }

                                    @Override
                                    public void onNegativeBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                    }
                                }).show();
                            }
                        }

                        @Override
                        public void onNeverAskPermissionChecked() {
                            if (!isFinishing()) {
                                String msg = getString(R.string.error_note_downloading_write_storage_permission_denied_and_checked_never_ask);
                                getDoubleBtnDialog(PhotoViewerActivity.this, getString(R.string.alert), msg, getString(R.string.not_now), getString(R.string.app_info), new OnDialogActionHelper() {
                                    @Override
                                    public void onPositiveBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                        openAppInfoSettings(DOWNLOAD);
                                    }

                                    @Override
                                    public void onNegativeBtnClicked(Dialog dialog) {
                                        dialog.dismiss();
                                    }
                                }).show();
                            }
                        }
                    });
            mDm.start();
        }
    }

    public void openAppInfoSettings(int requestType) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        this.startActivityForResult(intent, RC_OPEN_APP_INFO_SETTINGS = requestType);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mDm.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_OPEN_APP_INFO_SETTINGS) {
            if (resultCode == RESULT_OK) {
                if (RC_OPEN_APP_INFO_SETTINGS == SHARE) {
                    shareFile();
                }
                if (RC_OPEN_APP_INFO_SETTINGS == DOWNLOAD) {
                    downloadFile();
                }
            }
        }
    }

}