package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.DoctorNurseSpecialtyRvAdapter;
import com.iapps.sehatee.models.DoctorNurseSpecialtyModel;
import com.iapps.sehatee.models.ResponseModels.DocNurseSpecialtyResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getSpecialityURL;
import static com.iapps.sehatee.utils.Constants.CLINIC;
import static com.iapps.sehatee.utils.Constants.INDIVIDUAL;
import static com.iapps.sehatee.utils.Constants.INSTITUTE;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.NURSE;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class NurseCityAndSpecialtySelect extends AppCompatActivity implements DoctorNurseSpecialtyRvAdapter.OnItemClickedHelper {

    private static final String TAG = "NurseCityAndSpecialtySe";
    private TextView cityNameTv;
    private EditText searchEt;
    private RecyclerView rv;
    private DoctorNurseSpecialtyRvAdapter adapter;
    private List<DoctorNurseSpecialtyModel> list = new ArrayList<>();
    private LinearLayout noDataLL;
    private FloatingActionButton nextFab;
    private CheckBox individualCb, instituteCb;
    private String cityName = "", areaName = "", specialty = "", entity = INDIVIDUAL + "," + INSTITUTE;
    private int RC_CITY_AREA = 1110;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nurse_city_and_specialty_select);

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());

        cityNameTv = findViewById(R.id.cityNameTv);
        searchEt = findViewById(R.id.searchEt);
        individualCb = findViewById(R.id.individualCb);
        instituteCb = findViewById(R.id.instituteCb);
        rv = findViewById(R.id.rv);
        noDataLL = findViewById(R.id.noDataLL);
        nextFab = findViewById(R.id.nextFab);

        if (!getPrefs().getSelectedCityName().equals(NA) && !getPrefs().getSelectedCityAreaName().equals(NA)) {
            String cityAndArea = String.format(Locale.getDefault(), "%s, %s",
                    cityName = getPrefs().getSelectedCityName().equals("All Cities") ? getString(R.string.all_cities) : getPrefs().getSelectedCityName(),
                    areaName = getPrefs().getSelectedCityAreaName().equals("All Areas") ? getString(R.string.all_areas) : getPrefs().getSelectedCityAreaName()
            );
            cityNameTv.setText(cityAndArea.contains(NA) ? getString(R.string.select_city) : cityAndArea);
        }

        rv.setAdapter(adapter = new DoctorNurseSpecialtyRvAdapter(list, this));
        rv.setLayoutManager(new LinearLayoutManager(this));

        nextFab.setOnClickListener(v -> {

            if ((cityName.isEmpty() || cityName.equals(NA)) && (areaName.isEmpty() || areaName.equals(NA))) {
                showToast(NurseCityAndSpecialtySelect.this, R.string.error_please_select_city);
                return;
            }
            if (specialty.isEmpty()) {
                showToast(NurseCityAndSpecialtySelect.this, R.string.error_select_a_spcialty);
                return;
            }

            entity = individualCb.isChecked() ? INDIVIDUAL : "";
            entity = entity.isEmpty() ? instituteCb.isChecked() ? CLINIC : INDIVIDUAL + "," + INSTITUTE : ", " + INSTITUTE;

            setResult(RESULT_OK, new Intent()
                    .putExtra("cityName", cityName)
                    .putExtra("areaName", areaName)
                    .putExtra("specialty", specialty)
                    .putExtra("entity", entity)
            );
            finish();
        });

        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cityNameTv.setOnClickListener(v ->
                startActivityForResult(new Intent(NurseCityAndSpecialtySelect.this, SelectCity.class)
                                .putExtra("from", NURSE)
                                .putExtra("cityName", getPrefs().getSelectedCityName())
                                .putExtra("areaName", getPrefs().getSelectedCityAreaName())
                        , RC_CITY_AREA));

        getSpecialties();
    }

    private void getSpecialties() {
        if (isOnline(this)) {

            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                dialog.show();
            }
            VolleyHelper.getInstance(this).addRequest(getSpecialityURL(NURSE), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    DocNurseSpecialtyResponseModel model = new Gson().fromJson(response, DocNurseSpecialtyResponseModel.class);
                    if (model.getStatus() == 1) {
                        if (model.getPayload().getData().size() > 0) {
                            list.clear();
                            DoctorNurseSpecialtyModel doctorNurseSpecialtyModel = new DoctorNurseSpecialtyModel();
                            doctorNurseSpecialtyModel.setSelected(false);
                            doctorNurseSpecialtyModel.setSpecialty(getString(R.string.all_specialty));
                            doctorNurseSpecialtyModel.setId("0");
                            list.add(doctorNurseSpecialtyModel);
                            list.addAll(model.getPayload().getData());
                        }
                    } else {
                        showToast(NurseCityAndSpecialtySelect.this, model.getMsg());
                    }
                    adapter.notifyDataSetChanged();
                    afterFilterUpdateUI(list.size());
                }

                @Override
                public void onFailure(String error) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    afterFilterUpdateUI(list.size());
                    showToast(NurseCityAndSpecialtySelect.this, error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    afterFilterUpdateUI(list.size());
                    showUserRestrictedDialog(NurseCityAndSpecialtySelect.this);
                }
            });

        } else {
            afterFilterUpdateUI(list.size());
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onItemSelected(int pos) {
        if (pos > -1) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setSelected(false);
            }
            list.get(pos).setSelected(true);
            adapter.notifyDataSetChanged();
            specialty = list.get(pos).getId();
        }
    }

    @Override
    public void afterFilterUpdateUI(int size) {
        noDataLL.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_CITY_AREA && resultCode == RESULT_OK && data != null) {

            cityName = data.getStringExtra("cityName");
            areaName = data.getStringExtra("areaName");

            cityNameTv.setText(String.format(Locale.getDefault(), "%s, %s", cityName, areaName));

            Log.e(TAG, "onActivityResult: " + "City = " + cityName + ", Area = " + areaName);
        }
    }
}