package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.LogRegResponseModel;
import com.iapps.sehatee.utils.Constants;
import com.iapps.sehatee.utils.Prefs;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;

import static com.iapps.sehatee.utils.APIs.getCheckIfSocialAccountExistsURL;
import static com.iapps.sehatee.utils.APIs.getLoginURL;
import static com.iapps.sehatee.utils.Constants.LOGIN;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.Prefs.FACEBOOK;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Login";
    private EditText userIdEt, passEt;
    private LinearLayout fbLL, googleLL;
    private Button loginBtn;
    private TextView signUpTv, forgotPassTv;
    private FacebookGraphResponseModel fbAccount = new FacebookGraphResponseModel();
    private GoogleSignInAccount gAccount;
    private GoogleSignInOptions gso;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1010;

    private CallbackManager callbackManager;
    private LoginManager loginManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_image);
        setContentView(R.layout.activity_login);

        initViews();
        initClicks();

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Insert your code here

                                Log.e(TAG, "onCompleted: " + object.toString());

                                fbAccount = new Gson().fromJson(object.toString(), FacebookGraphResponseModel.class);

                                fbAccount.getPicture().getData().setUrl("https://graph.facebook.com/" + fbAccount.getId() + "/picture?type=large&redirect=true&width=256&height=256");

                                Log.e(TAG, "onCompleted: FB_ACCOUNT => " + fbAccount.toString());

                                checkIfUserExists(fbAccount.getId(), FACEBOOK);

                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture.type(large),birthday,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "onCancel: User canceled fb login.");
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
                Log.e(TAG, "onError: ", error);
                showToast(Login.this, error.getLocalizedMessage());
            }
        });


        passEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                loginBtn.performClick();
                return true;
            }
            return false;
        });

    }

    private void initViews() {
        userIdEt = findViewById(R.id.emailOrPhEt);
        passEt = findViewById(R.id.passEt);
        forgotPassTv = findViewById(R.id.forgotPasswordTv);
        loginBtn = findViewById(R.id.loginBtn);
        fbLL = findViewById(R.id.fbLL);
        googleLL = findViewById(R.id.googleLL);
        signUpTv = findViewById(R.id.signUpTv);
    }

    private void initClicks() {
        forgotPassTv.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        fbLL.setOnClickListener(this);
        googleLL.setOnClickListener(this);
        signUpTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgotPasswordTv:
                startActivity(new Intent(this, ForgotPassword.class));
                break;
            case R.id.loginBtn:
                v.setEnabled(false);
                String userId = userIdEt.getText().toString().trim();
                String pass = passEt.getText().toString().trim();
                if (validate(userId, pass)) {
                    login(v, userId, pass);
                } else {
                    v.setEnabled(true);
                }
                break;
            case R.id.signUpTv:
                startActivity(new Intent(this, SignUp.class));
                break;
            case R.id.fbLL:
                startFbLoginFlow();
                break;
            case R.id.googleLL:
                startGoogleLoginFlow();
                break;
        }
    }

    private void startFbLoginFlow() {
        loginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void startGoogleLoginFlow() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private boolean validate(String userId, String pass) {
        if (userId.isEmpty()) {
            showToast(this, R.string.error_invalid_user_id);
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(userId).matches() && !Patterns.PHONE.matcher(userId).matches()) {
            showToast(this, R.string.error_invalid_user_id);
            return false;
        }

        if (pass.isEmpty()) {
            showToast(this, R.string.error_empty_password);
            return false;
        }

        if (pass.length() < 6) {
            showToast(this, R.string.error_password_too_short);
            return false;
        }
        return true;
    }

    private void login(View v, String userId, String pass) {
        if (isOnline(this)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userType", Constants.CUSTOMER);
                jsonObject.put("email", userId);
                jsonObject.put("password", URLEncoder.encode(pass, "utf-8"));

                final Dialog dialog = getProgressDialog(this, R.string.please_wait);
                if (!isFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(this).addRequest(getLoginURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);
                        if (model.getStatus() == 1) {
                            getPrefs().setAuthToken(model.getPayload().getTokenType() + " " + model.getPayload().getToken());
                            Constants.updateUserPrefData(model.getPayload().getUser());
                            getPrefs().setLoggedIn(true);

                            showToast(Login.this, R.string.welcome_to_sahety);

                            if (!getPrefs().getSelectedCityId().equals(NA)) {
                                startActivity(new Intent(Login.this, Home.class));
                            } else {
                                startActivity(new Intent(Login.this, SelectCity.class).putExtra("from", LOGIN).putExtra("cityName", "").putExtra("areaName", ""));
                            }
                            finishAffinity();
                        } else {
                            showToast(Login.this, model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showToast(Login.this, error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(Login.this);
                    }
                });

            } catch (JSONException | UnsupportedEncodingException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "login: ", e);
                showToast(Login.this, R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            showToast(this, R.string.error_internet_unavilable);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK && data != null) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    gAccount = task.getResult(ApiException.class);

                    checkIfUserExists(gAccount.getId(), Prefs.GOOGLE);

                } catch (ApiException e) {
                    e.printStackTrace();
                    Log.e(TAG, "onActivityResult: ", e);
                    showToast(this, R.string.google_sign_in_failed);
                }
            } else {
                Log.e(TAG, "onActivityResult: google sign in error");
            }
        }
    }

    private void checkIfUserExists(String id, final String provider) {

        String str = "{\"provider\": \"" + provider + "\",\"socialId\": \"" + id + "\"}";

        final Dialog dialog = getProgressDialog(this, R.string.please_wait);
        if (!isFinishing()) {
            dialog.show();
        }

        VolleyHelper.getInstance(this).addRequest(getCheckIfSocialAccountExistsURL(), VolleyHelper.POST, str, new VolleyHelper.OnVolleyResponseListener() {
            @Override
            public void onSuccess(String response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);

                if (model.getStatus() == 1) {

                    getPrefs().setAuthToken(model.getPayload().getTokenType() + " " + model.getPayload().getToken());
                    getPrefs().setLoginProvider(provider);
                    getPrefs().setCurrency(model.getPayload().getCurrency().getCurrency());
                    getPrefs().setCurrencySymbol(model.getPayload().getCurrency().getCurrencySymbol());
                    Constants.updateUserPrefData(model.getPayload().getUser());
                    getPrefs().setLoggedIn(true);

                    showToast(Login.this, R.string.welcome_to_sahety);

                    if (!getPrefs().getSelectedCityId().equals(NA)) {
                        startActivity(new Intent(Login.this, Home.class));
                    } else {
                        startActivity(new Intent(Login.this, SelectCity.class).putExtra("from", LOGIN).putExtra("cityName", "").putExtra("areaName", ""));
                    }
                    finishAffinity();

                } else {
                    if (provider.equals(Prefs.GOOGLE)) {

                        startActivity(new Intent(Login.this, FewMoreInformation.class)
                                .putExtra("email", gAccount.getEmail())
                                .putExtra("socialId", gAccount.getId())
                                .putExtra("profilePic", gAccount.getPhotoUrl() == null ? "NA" : gAccount.getPhotoUrl().toString())
                                .putExtra("name", gAccount.getDisplayName())
                                .putExtra("provider", provider)
                        );
                    } else {
                        //TODO Facebook data

                        startActivity(new Intent(Login.this, FewMoreInformation.class)
                                .putExtra("email", fbAccount.getEmail())
                                .putExtra("socialId", fbAccount.getId())
                                .putExtra("profilePic", fbAccount.getPicture().getData().getUrl())
                                .putExtra("name", fbAccount.getName())
                                .putExtra("provider", provider)
                        );
                    }
                }
            }

            @Override
            public void onFailure(String error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                showToast(Login.this, error);
            }

            @Override
            public void onRestrictedByAdmin() {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                showUserRestrictedDialog(Login.this);
            }
        });
    }

    private static class FacebookGraphResponseModel implements Serializable {
        private String id;
        private String name = "";
        private String email = "";
        private Picture picture = new Picture();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Picture getPicture() {
            return picture;
        }

        public void setPicture(Picture picture) {
            this.picture = picture;
        }

        @NonNull
        @Override
        public String toString() {
            return "FacebookGraphResponseModel{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", email='" + email + '\'' +
                    ", picture=" + picture.toString() +
                    '}';
        }

        public static class Picture implements Serializable {
            private Data data = new Data();

            public Data getData() {
                return data;
            }

            public void setData(Data data) {
                this.data = data;
            }

            @NonNull
            @Override
            public String toString() {
                return "Picture{" +
                        "data=" + data.toString() +
                        '}';
            }

            public static class Data implements Serializable {
                private int width = 0;
                private int height = 0;
                private boolean is_silhouette = false;
                private String url = "";

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public boolean isIs_silhouette() {
                    return is_silhouette;
                }

                public void setIs_silhouette(boolean is_silhouette) {
                    this.is_silhouette = is_silhouette;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                @NonNull
                @Override
                public String toString() {
                    return "Data{" +
                            "width=" + width +
                            ", height=" + height +
                            ", is_silhouette=" + is_silhouette +
                            ", url='" + url + '\'' +
                            '}';
                }
            }
        }
    }

}