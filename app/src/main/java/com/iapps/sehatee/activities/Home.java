package com.iapps.sehatee.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.fragments.AboutUsFrag;
import com.iapps.sehatee.fragments.AppointmentsFrag;
import com.iapps.sehatee.fragments.ContactUsFrag;
import com.iapps.sehatee.fragments.DcHomeFrag;
import com.iapps.sehatee.fragments.FavouritesFrag;
import com.iapps.sehatee.fragments.FeedbackFrag;
import com.iapps.sehatee.fragments.HelpFrag;
import com.iapps.sehatee.fragments.HomeFrag;
import com.iapps.sehatee.fragments.MyProfileFrag;
import com.iapps.sehatee.fragments.NotificationFrag;
import com.iapps.sehatee.fragments.OffersFrag;
import com.iapps.sehatee.fragments.PrescriptionsFrag;
import com.iapps.sehatee.fragments.PrivacyPolicyFrag;
import com.iapps.sehatee.fragments.ReportsFrag;
import com.iapps.sehatee.fragments.SettingsFrag;
import com.iapps.sehatee.fragments.TermsFrag;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.models.ResponseModels.LogRegResponseModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.Constants;
import com.iapps.sehatee.utils.Prefs;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.TimeZone;

import static com.iapps.sehatee.utils.APIs.getLogoutURL;
import static com.iapps.sehatee.utils.APIs.getNotificationCountURL;
import static com.iapps.sehatee.utils.APIs.getProfileDataURL;
import static com.iapps.sehatee.utils.APIs.getUpdateDeviceTokenURL;
import static com.iapps.sehatee.utils.Constants.ABOUT_US;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI_NO_BOTTOM_NAV;
import static com.iapps.sehatee.utils.Constants.ANDROID;
import static com.iapps.sehatee.utils.Constants.APPOINTMENTS;
import static com.iapps.sehatee.utils.Constants.CONTACT_US;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.FEEDBACK;
import static com.iapps.sehatee.utils.Constants.HELP;
import static com.iapps.sehatee.utils.Constants.HOME;
import static com.iapps.sehatee.utils.Constants.HOME_DC;
import static com.iapps.sehatee.utils.Constants.MY_FAVOURITES;
import static com.iapps.sehatee.utils.Constants.MY_PROFILE;
import static com.iapps.sehatee.utils.Constants.MY_TESTS;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.NOTIFICATIONS;
import static com.iapps.sehatee.utils.Constants.OFFERS;
import static com.iapps.sehatee.utils.Constants.PRESCRIPTIONS;
import static com.iapps.sehatee.utils.Constants.PRIVACY_POLICY;
import static com.iapps.sehatee.utils.Constants.PROFILE_UPDATED;
import static com.iapps.sehatee.utils.Constants.REPORTS;
import static com.iapps.sehatee.utils.Constants.SETTINGS;
import static com.iapps.sehatee.utils.Constants.TERMS_N_CONDITIONS;
import static com.iapps.sehatee.utils.Constants.UPDATE_NOTIFICATION_COUNT;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.Constants.updateUserPrefData;

public class Home extends AppCompatActivity implements Communicator, View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private static final String TAG = "Home";
    private ImageView menuIv, backIv, userIvD, navHomeIv;
    private TextView pageTitleTv, userNameD, notificationBadgeTv;
    private RadioGroup drawerRg, navRg;
    private FrameLayout notificationFL, navBarFL;
    private DrawerLayout drawerLayout;
    private LinearLayout drawer;
    private GoogleSignInOptions gso;
    private GoogleSignInClient googleSignInClient;
    private boolean shouldExit = false;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(PROFILE_UPDATED)) {
                        updateProfileData();
                    }
                    if (intent.getAction().equals(UPDATE_NOTIFICATION_COUNT)) {
                        getNotificationCount();
                    }
                }
            }
        }
    };

    private void getNotificationCount() {
        if (isOnline(this)) {
            VolleyHelper.getInstance(this).addRequest(getNotificationCountURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    SimpleResponseModel responseModel = new Gson().fromJson(response, SimpleResponseModel.class);
                    if (responseModel.getStatus() == 1) {
                        updateNotificationBadge(responseModel.getPayload().getCount());
                    }
                }

                @Override
                public void onFailure(String error) {

                }

                @Override
                public void onRestrictedByAdmin() {

                }
            });
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfileData();
        getNotificationCount();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();


        Log.e(TAG, "onCreate: Timezone  => " + TimeZone.getDefault().getID());
        Log.e(TAG, "onCreate: Country => " + countryCodeValue);

        Log.e(TAG, " \n\nUSER_DATA =>\n\n" + getPrefs().toString() + "\n ");
        Log.e(TAG, " \n\nLANGUAGE_CODE => " + Locale.getDefault().getLanguage() + "\n ");
        Log.e(TAG, " \n\nLANGUAGE_CODE from prefs => " + Locale.getDefault().getLanguage() + "\n ");
        Log.e(TAG, " \n\nAUTH_TOKEN =>\n\n" + getPrefs().getAuthToken() + "\n ");
        Log.e(TAG, " \n\nFCM_TOKEN =>\n\n" + getPrefs().getFcmToken() + "\n ");

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        initViews();
        initClicks();

        updateProfileData();

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String token = task.getResult().getToken();
                        getPrefs().setFcmToken(token);
                    }
                    updateFcmToken();
                });

        openFragment(new HomeFrag(), HOME);

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(UPDATE_NOTIFICATION_COUNT));
    }

    private void initViews() {
        menuIv = findViewById(R.id.menuIv);
        backIv = findViewById(R.id.backIv);
        pageTitleTv = findViewById(R.id.pageTitleTv);
        notificationBadgeTv = findViewById(R.id.notificationBadgeTv);
        notificationFL = findViewById(R.id.notificationFL);
        drawerLayout = findViewById(R.id.drawerLayout);
        drawer = findViewById(R.id.drawer);
        userIvD = findViewById(R.id.userIvD);
        navHomeIv = findViewById(R.id.navHomeIv);
        pageTitleTv = findViewById(R.id.pageTitleTv);
        userNameD = findViewById(R.id.userNameD);
        drawerRg = findViewById(R.id.drawerRg);
        navBarFL = findViewById(R.id.navBarFL);
        navRg = findViewById(R.id.navRg);
    }

    private void initClicks() {
        menuIv.setOnClickListener(this);
        backIv.setOnClickListener(this);
        notificationFL.setOnClickListener(this);
        userIvD.setOnClickListener(this);
        // Radio Group check change
        drawerRg.setOnCheckedChangeListener(this);
        navRg.setOnCheckedChangeListener(this);
    }

    private void getProfileData() {
        VolleyHelper.getInstance(this).addRequest(getProfileDataURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
            @Override
            public void onSuccess(String response) {
                LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);
                if (model.getStatus() == 1) {

                    getPrefs().setCurrency(model.getPayload().getCurrency().getCurrency());
                    getPrefs().setCurrencySymbol(model.getPayload().getCurrency().getCurrencySymbol());

                    updateUserPrefData(model.getPayload().getUser());
                    updateProfileData();
                }
            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public void onRestrictedByAdmin() {

            }
        });
    }

    private void updateFcmToken() {
        if (isOnline(this)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("deviceType", ANDROID);
                jsonObject.put("deviceToken", getPrefs().getFcmToken());

                VolleyHelper.getInstance(this).addRequest(getUpdateDeviceTokenURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFailure(String error) {

                    }

                    @Override
                    public void onRestrictedByAdmin() {

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "updateFcmToken: ", e);
            }
        }
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    public void goToHome() {
        if (getSupportFragmentManager().findFragmentByTag(HOME) != null) {
            getSupportFragmentManager().popBackStack(HOME, 0);
        }
    }

    @Override
    public void goToAppointments() {
        goToHome();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openFragment(new AppointmentsFrag(), APPOINTMENTS);
            }
        }, 240);
    }

    @Override
    public void goToDcHome() {
        if (getSupportFragmentManager().findFragmentByTag(HOME) != null) {
            getSupportFragmentManager().popBackStack(HOME_DC, 0);
        } else {
            openFragment(new DcHomeFrag(), HOME_DC);
        }
    }

    @Override
    public void goToReports() {
        goToHome();
        openFragment(new ReportsFrag(), REPORTS);
    }

    @Override
    public void recreateActivity() {
        Intent intent = new Intent(this, Home.class);
        finishAffinity();
        startActivity(intent);
//        recreate();
    }

    @Override
    public void openFragment(Fragment fragment, String pageTitle) {

        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragContainer, fragment, pageTitle)
                .addToBackStack(pageTitle)
                .commit();
    }

    @Override
    public void setMenuSelection(String fragName) {
        drawerRg.setOnCheckedChangeListener(null);
        navRg.setOnCheckedChangeListener(null);
        navHomeIv.setImageTintList(ResourcesCompat.getColorStateList(getResources(), R.color.metaTextColor, getTheme()));
        navHomeIv.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.dividerColor, getTheme()));
        switch (fragName) {
            case HOME:
                drawerRg.check(R.id.homeD);
                navRg.check(R.id.homeRb);
                navHomeIv.setImageTintList(ResourcesCompat.getColorStateList(getResources(), R.color.white, getTheme()));
                navHomeIv.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, getTheme()));
                break;
            case MY_PROFILE:
                drawerRg.check(R.id.profileD);
                break;
            case OFFERS:
                drawerRg.check(R.id.offersD);
                navRg.check(R.id.offersRb);
                break;
            case APPOINTMENTS:
                drawerRg.check(R.id.appointmentD);
                navRg.check(R.id.appointmentsRb);
                break;
            case PRESCRIPTIONS:
                drawerRg.check(R.id.prescriptionsD);
                break;
            case REPORTS:
                drawerRg.check(R.id.reportsD);
                navRg.check(R.id.reportsRb);
                break;
            case MY_FAVOURITES:
                drawerRg.check(R.id.favD);
                break;
            case FEEDBACK:
                drawerRg.check(R.id.feedbackD);
                break;
            case TERMS_N_CONDITIONS:
                drawerRg.check(R.id.tncD);
                break;
            case PRIVACY_POLICY:
                drawerRg.check(R.id.ppD);
                break;
            case ABOUT_US:
                drawerRg.check(R.id.aboutUsD);
                break;
            case CONTACT_US:
                drawerRg.check(R.id.contactUsD);
                break;
            case HELP:
                drawerRg.check(R.id.helpD);
                break;
            case MY_TESTS:
                navRg.check(R.id.reportsRb);
                break;
            default:
                navRg.clearCheck();
                break;
        }
        drawerRg.setOnCheckedChangeListener(this);
        navRg.setOnCheckedChangeListener(this);
    }

    @Override
    public Dialog getSingleBtnDialog(String title, String msg, String posBtnText, OnDialogActionHelper helper) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_single_action_btn, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView msgTv = view.findViewById(R.id.msgTv);
        Button posBtn = view.findViewById(R.id.posBtn);

        titleTv.setText(title);
        msgTv.setText(msg);
        posBtn.setText(posBtnText);

        posBtn.setText(posBtnText);

        posBtn.setOnClickListener((v) -> helper.onPositiveBtnClicked(dialog));

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        return dialog;
    }

    @Override
    public Dialog getDoubleBtnDialog(String title, String msg, String negBtnText, String posBtnText, OnDialogActionHelper helper) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_double_action_btn, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView msgTv = view.findViewById(R.id.msgTv);
        Button posBtn = view.findViewById(R.id.posBtn);
        Button negBtn = view.findViewById(R.id.negBtn);

        titleTv.setText(title);
        msgTv.setText(msg);
        negBtn.setText(negBtnText);
        posBtn.setText(posBtnText);

        posBtn.setOnClickListener((v) -> helper.onPositiveBtnClicked(dialog));
        negBtn.setOnClickListener((v) -> helper.onNegativeBtnClicked(dialog));

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        return dialog;
    }

    @Override
    public void showSnackBar(int resId) {

    }

    @Override
    public void showSnackBar(String msg) {

    }

    @Override
    public void showToast(int resId) {
        Constants.showToast(this, resId);
    }

    @Override
    public void showToast(String msg) {
        Constants.showToast(this, msg);
    }

    @Override
    public void setActionBar(int type) {
        switch (type) {
            case AB_BACK_WITH_NOTI:
                menuIv.setVisibility(View.GONE);
                backIv.setVisibility(View.VISIBLE);
                notificationFL.setVisibility(View.VISIBLE);
                navBarFL.setVisibility(View.VISIBLE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            case AB_BACK_WITHOUT_NOTI:
                menuIv.setVisibility(View.GONE);
                backIv.setVisibility(View.VISIBLE);
                notificationFL.setVisibility(View.GONE);
                navBarFL.setVisibility(View.GONE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            case AB_BACK_WITH_NOTI_NO_BOTTOM_NAV:
                menuIv.setVisibility(View.GONE);
                backIv.setVisibility(View.VISIBLE);
                notificationFL.setVisibility(View.VISIBLE);
                navBarFL.setVisibility(View.GONE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            default:
                menuIv.setVisibility(View.VISIBLE);
                backIv.setVisibility(View.GONE);
                notificationFL.setVisibility(View.VISIBLE);
                navBarFL.setVisibility(View.VISIBLE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;
        }
    }

    @Override
    public void setPageTitle(int resId) {
        pageTitleTv.setText(resId);
    }

    @Override
    public void updateProfileData() {
        Picasso.get()
                .load(getPrefs().getProfileImage().isEmpty() || getPrefs().getProfileImage() == null ? NA : getPrefs().getProfileImage())
                .error(R.drawable.ic_default_user_image)
                .into(userIvD);
        userNameD.setText(getPrefs().getFullName());
    }

    @Override
    public void updateNotificationBadge(int count) {
        notificationBadgeTv.setVisibility(count == 0 ? View.GONE : View.VISIBLE);
        notificationBadgeTv.setText(String.valueOf(count));
    }

    @Override
    public boolean isActivityFinishing() {
        return isFinishing();
    }

    @Override
    public void logout() {
        logoutUser();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuIv:
                drawerLayout.openDrawer(drawer);
                break;
            case R.id.backIv:
                onBackPressed();
                break;
            case R.id.notificationFL:
                if (!CURRENT_FRAG.equals(NOTIFICATIONS)) {
                    openFragment(new NotificationFrag(), NOTIFICATIONS);
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (drawerLayout.isDrawerOpen(drawer)) {
            drawerLayout.closeDrawer(drawer);
        }
        navHomeIv.setImageTintList(ResourcesCompat.getColorStateList(getResources(), R.color.metaTextColor, getTheme()));
        navHomeIv.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.dividerColor, getTheme()));

        switch (checkedId) {
            case R.id.homeRb:
            case R.id.homeD:
                if (!CURRENT_FRAG.equals(HOME)) {
                    goToHome();
                }
                break;
            case R.id.reportsD:
            case R.id.reportsRb:
                if (!CURRENT_FRAG.equals(REPORTS)) {
                    openFragment(new ReportsFrag(), REPORTS);
                }
                break;
            case R.id.settingsD:
                if (!CURRENT_FRAG.equals(SETTINGS)) {
                    openFragment(new SettingsFrag(), SETTINGS);
                }
                break;
            case R.id.profileD:
                if (!CURRENT_FRAG.equals(MY_PROFILE)) {
                    openFragment(new MyProfileFrag(), MY_PROFILE);
                }
                break;
            case R.id.offersRb:
            case R.id.offersD:
                if (!CURRENT_FRAG.equals(OFFERS)) {
                    openFragment(new OffersFrag(), OFFERS);
                }
                break;
            case R.id.appointmentsRb:
            case R.id.appointmentD:
                if (!CURRENT_FRAG.equals(APPOINTMENTS)) {
                    openFragment(new AppointmentsFrag(), APPOINTMENTS);
                }
                break;
            case R.id.prescriptionsD:
                if (!CURRENT_FRAG.equals(PRESCRIPTIONS)) {
                    openFragment(new PrescriptionsFrag(), PRESCRIPTIONS);
                }
                break;
            case R.id.favD:
                if (!CURRENT_FRAG.equals(MY_FAVOURITES)) {
                    openFragment(new FavouritesFrag(), MY_FAVOURITES);
                }
                break;
            case R.id.feedbackD:
                if (!CURRENT_FRAG.equals(FEEDBACK)) {
                    openFragment(new FeedbackFrag(), FEEDBACK);
                }
                break;
            case R.id.tncD:
                if (!CURRENT_FRAG.equals(TERMS_N_CONDITIONS)) {
                    openFragment(new TermsFrag(), TERMS_N_CONDITIONS);
                }
                break;
            case R.id.ppD:
                if (!CURRENT_FRAG.equals(PRIVACY_POLICY)) {
                    openFragment(new PrivacyPolicyFrag(), PRIVACY_POLICY);
                }
                break;
            case R.id.aboutUsD:
                if (!CURRENT_FRAG.equals(ABOUT_US)) {
                    openFragment(new AboutUsFrag(), ABOUT_US);
                }
                break;
            case R.id.contactUsD:
                if (!CURRENT_FRAG.equals(CONTACT_US)) {
                    openFragment(new ContactUsFrag(), CONTACT_US);
                }
                break;
            case R.id.helpD:
            case R.id.helpRb:
                if (!CURRENT_FRAG.equals(HELP)) {
                    openFragment(new HelpFrag(), HELP);
                }
                break;
            case R.id.logoutD:
                logout();
                drawerRg.clearCheck();
                break;
        }
    }

    private void logoutUser() {

//        getPrefs().clearSp();
//        finishAffinity();

        if (isOnline(this)) {
            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                dialog.show();
            }

            VolleyHelper.getInstance(this).addRequest(getLogoutURL(), VolleyHelper.POST, "", new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                    if (model.getStatus() == 1) {
                        if (getPrefs().getLoginProvider().equals(Prefs.GOOGLE)) {
                            googleSignInClient.signOut();
                        }
                        if (getPrefs().getLoginProvider().equals(Prefs.FACEBOOK)) {
                            LoginManager.getInstance().logOut();
                        }
                        getPrefs().clearSp();
                        finishAffinity();
                        startActivity(new Intent(Home.this, Login.class));
                    } else {
                        showToast(model.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(Home.this);
                }
            });
        } else {
            showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            if (shouldExit) {
                finishAffinity();
            } else {
                shouldExit = true;
                showToast(R.string.press_again_to_exit);
                new Handler().postDelayed(() -> shouldExit = false, 2000);
            }
        } else {
            super.onBackPressed();
        }
    }
}