package com.iapps.sehatee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.SelectAppointmentRvAdapter;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.ResponseModels.AppoHistoryResponseModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getAddAppointmentToShareReportsURL;
import static com.iapps.sehatee.utils.APIs.getAppointmentsForReportVisibilityURL;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.setLocale;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class SelectAppointmentToShareReport extends AppCompatActivity implements SelectAppointmentRvAdapter.OnItemSelectedHelper {

    private int pageNum = 1, lastPage = 1;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private SelectAppointmentRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<AppointmentsModel> list = new ArrayList<>();
    private Button addBtn;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_appointment_to_share_report);

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());

        rv = findViewById(R.id.rv);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        noDataLL = findViewById(R.id.noDataLL);
        addBtn = findViewById(R.id.addBtn);

        rv.setAdapter(adapter = new SelectAppointmentRvAdapter(list, this));
        rv.setLayoutManager(lm = new LinearLayoutManager(this));

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                if (lasVisibleItemIndex == list.size() - 1) {
                    if (pageNum <= lastPage) {
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            getAppointments();
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {
            pageNum = 1;
            lastPage = 1;
            getAppointments();
        });

        getAppointments();

        addBtn.setOnClickListener(v -> {

            int index = -1;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isSelected()) {
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                if (getIntent().getStringExtra("reportId").equals("0")) {
                    Intent intent = new Intent();
                    intent.putExtra("model", list.get(index));
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    shareAppointment(getIntent().getStringExtra("reportId"), list.get(index));
                }
            } else {
                showToast(SelectAppointmentToShareReport.this, R.string.error_appointment_not_selected);
            }

        });
    }

    private void shareAppointment(String reportId, AppointmentsModel appointmentsModel) {
        if (isOnline(this)) {
            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                if (!swipeRefreshLayout.isRefreshing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(this).addRequest(getAddAppointmentToShareReportsURL(), VolleyHelper.POST, "{\"reportAppointmentId\":\"" + reportId + "\", \"appointmentId\":\"" + appointmentsModel.getAppointmentId() + "\"}", new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);

                    if (model.getStatus() == 1) {
                        setResult(RESULT_OK, new Intent().putExtra("model", appointmentsModel));
                        finish();
                    } else {
                        showToast(SelectAppointmentToShareReport.this, model.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showToast(SelectAppointmentToShareReport.this, error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(SelectAppointmentToShareReport.this);
                }
            });

        } else {
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void getAppointments() {
        if (isOnline(this)) {
            final Dialog dialog = getProgressDialog(this, R.string.please_wait);
            if (!isFinishing()) {
                if (!swipeRefreshLayout.isRefreshing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(this).addRequest(getAppointmentsForReportVisibilityURL(getIntent().getStringExtra("reportId"), getIntent().getStringExtra("dcId"), pageNum), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    AppoHistoryResponseModel model = new Gson().fromJson(response, AppoHistoryResponseModel.class);

                    pageNum = model.getPayload().getAppointmentList().getCurrentPage();
                    lastPage = model.getPayload().getAppointmentList().getLastPage();

                    if (model.getStatus() == 1) {
                        if (pageNum == 1) {
                            list.clear();
                        }
                        if (model.getPayload().getAppointmentList().getData().size() > 0) {
                            list.addAll(model.getPayload().getAppointmentList().getData());
                        }
                        adapter.notifyDataSetChanged();
                        pageNum++;

                    } else {
                        showToast(SelectAppointmentToShareReport.this, model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showToast(SelectAppointmentToShareReport.this, error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(SelectAppointmentToShareReport.this);
                }
            });

        } else {
            updateUI();
            showToast(this, R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        addBtn.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemSelected(int pos) {
        if (pos > -1) {
            for (int i = 0; i < list.size(); i++) {
                if (i != pos) {
                    list.get(i).setSelected(false);
                }
            }
            list.get(pos).setSelected(!list.get(pos).isSelected());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }
}