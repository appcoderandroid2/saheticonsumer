package com.iapps.sehatee.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.SearchRvAdapter;
import com.iapps.sehatee.models.SearchModel;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.setLocale;

public class Search extends AppCompatActivity implements SearchRvAdapter.OnItemClickedHelper {

    private EditText searchEt;
    private ImageView searchIv;
    private RecyclerView rv;
    private SearchRvAdapter adapter;
    private List<SearchModel> list = new ArrayList<>();
    private boolean isWriting = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        setLocale(newBase, getPrefs().getAppLanguageCode());
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        findViewById(R.id.backIv).setOnClickListener(v -> onBackPressed());

        searchEt = findViewById(R.id.searchEt);
        searchIv = findViewById(R.id.searchIv);

        rv = findViewById(R.id.rv);
        rv.setAdapter(adapter = new SearchRvAdapter(list, this));
        rv.setLayoutManager(new LinearLayoutManager(this));


        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchIv.setImageResource(s.toString().length() == 0 ? R.drawable.ic_search_18 : R.drawable.ic_round_close_18);
                isWriting = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isWriting = false;
                    }
                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isWriting && s.toString().length() > 2) {
                    updateList(s.toString());
                }
            }
        });

        searchIv.setOnClickListener(v -> {
            if (searchEt.getText().toString().length() > 0) {
                searchEt.setText(null);
            }
        });

    }

    private void updateList(String query) {
        list.clear();
        for (int i = 0; i < 15; i++) {
            SearchModel model = new SearchModel();
            model.setName("Complete Care Total" + " ( " + query + " " + (i + 1) + ")");
            model.setOfferedBy("Some Diagnostic Center");
            list.add(model);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {
            setResult(RESULT_OK, getIntent().putExtra("id", list.get(pos).getId()));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }


}