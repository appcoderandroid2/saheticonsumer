package com.iapps.sehatee.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ResponseModels.HomeBannerResponseModel;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.NA;

public class BannerFrag extends Fragment {

    private HomeBannerResponseModel.BannerModel model = new HomeBannerResponseModel.BannerModel();

    public BannerFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (HomeBannerResponseModel.BannerModel) getArguments().getSerializable("model");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_banner, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView iv = view.findViewById(R.id.iv);
        TextView title = view.findViewById(R.id.titleTv);
        TextView desc = view.findViewById(R.id.descTv);

        Picasso.get().load(model.getImageLink().isEmpty() ? NA : model.getImageLink())
                .noPlaceholder()
                .into(iv);

        title.setText(String.format(Locale.getDefault(), "%s", model.getTitle()));
        desc.setText(String.format(Locale.getDefault(), "%s", model.getDesc()));
    }
}