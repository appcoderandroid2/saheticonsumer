package com.iapps.sehatee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.BuildConfig;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CHANGE_PASSWORD;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.SETTINGS;
import static com.iapps.sehatee.utils.Constants.getPrefs;

public class SettingsFrag extends Fragment {

    private static final String TAG = "MoreFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private RadioGroup langRg;
    private TextView versionTv, changePassTv;

    public SettingsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_settings, container, false);

            versionTv = view.findViewById(R.id.versionTv);
            changePassTv = view.findViewById(R.id.changePasswordTv);
            langRg = view.findViewById(R.id.langRg);

            updateUI();
        }
        CURRENT_FRAG = SETTINGS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.settings);
        comm.setMenuSelection(SETTINGS);
        return view;
    }

    private void updateUI() {
        versionTv.setText(ctx.getString(R.string.current_version, BuildConfig.VERSION_NAME));
        langRg.check(getPrefs().getAppLanguageCode().equals("en") ? R.id.englishRb : R.id.arabicRb);

        langRg.setOnCheckedChangeListener((group, checkedId) -> {
            getPrefs().setAppLanguageCode(checkedId == R.id.englishRb ? "en" : "ar");
            comm.recreateActivity();
        });

        changePassTv.setOnClickListener(v -> comm.openFragment(new ChangePasswordFrag(), CHANGE_PASSWORD));
    }
}