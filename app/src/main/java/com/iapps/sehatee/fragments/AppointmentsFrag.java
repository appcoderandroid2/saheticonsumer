package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.AppointmentsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.ResponseModels.AppoHistoryResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getAppointmentListURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.APPOINTMENTS;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DOCTOR;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class AppointmentsFrag extends Fragment implements AppointmentsRvAdapter.OnItemSelectedHelper {
    private static final String TAG = "AppointmentHistoryFrag";
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private AppointmentsRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<AppointmentsModel> list = new ArrayList<>();
    private View view;
    private Context ctx;
    private Communicator comm;
    private int pageNum = 1, lastPage = 1;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(APPOINTMENT_DETAILS_UPDATED)) {
                        int pos = intent.getIntExtra("pos", -1);
                        AppointmentsModel model = (AppointmentsModel) intent.getSerializableExtra("model");
                        if (rv != null) {
                            rv.post(() -> {
                                if (pos == -1) {
                                    pageNum = 1;
                                    lastPage = 1;
                                    getAppointments();
                                } else {
                                    list.set(pos, model);
                                    adapter.notifyItemChanged(pos);
                                }
                            });
                        }
                    }
                }
            }
        }
    };

    public AppointmentsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_appointment, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new AppointmentsRvAdapter(list, this));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(() -> {
                pageNum = 1;
                lastPage = 1;
                getAppointments();
            });

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getAppointments();
                            }
                        }
                    }
                }
            });

            getAppointments();

            LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(APPOINTMENT_DETAILS_UPDATED));

        }

        CURRENT_FRAG = APPOINTMENTS;
        comm.setActionBar(AB_BACK_WITH_NOTI);
        comm.setPageTitle(R.string.appointments);
        comm.setMenuSelection(APPOINTMENTS);
        return view;
    }

    private void getAppointments() {
        if (isOnline(ctx)) {
            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getAppointmentListURL(pageNum, "0"), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    AppoHistoryResponseModel model = new Gson().fromJson(response, AppoHistoryResponseModel.class);

                    pageNum = model.getPayload().getAppointmentList().getCurrentPage();
                    lastPage = model.getPayload().getAppointmentList().getLastPage();

                    if (model.getStatus() == 1) {
                        if (pageNum == 1) {
                            list.clear();
                        }
                        list.addAll(model.getPayload().getAppointmentList().getData());

                        adapter.notifyDataSetChanged();
                        pageNum++;
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI();

                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(AppointmentsFrag.this);
                }
            });
        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemSelected(int pos) {
        if (pos > -1) {
            if (list.get(pos).getDoctorNurseDetails().getUserType().equals(DOCTOR)) {
                AppointmentDetails frag = new AppointmentDetails();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", list.get(pos));
                bundle.putString("from", APPOINTMENTS);
                bundle.putInt("pos", pos);
                frag.setArguments(bundle);
                comm.openFragment(frag, APPOINTMENT_DETAILS);
            } else {
                AppointmentDetailsNurseFrag frag = new AppointmentDetailsNurseFrag();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", list.get(pos));
                bundle.putString("from", APPOINTMENTS);
                bundle.putInt("pos", pos);
                frag.setArguments(bundle);
                comm.openFragment(frag, APPOINTMENT_DETAILS);
            }
        }
    }
}