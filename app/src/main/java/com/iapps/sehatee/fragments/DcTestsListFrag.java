package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.SearchTestsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnCompleteHelper;
import com.iapps.sehatee.models.ResponseModels.TestResponseModel;
import com.iapps.sehatee.models.TestsModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getTestPackageListUrl;
import static com.iapps.sehatee.utils.Constants.DC_QUERY_UPDATED;
import static com.iapps.sehatee.utils.Constants.TEST;
import static com.iapps.sehatee.utils.Constants.TEST_DETAILS;
import static com.iapps.sehatee.utils.Constants.addToFavourite;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DcTestsListFrag extends Fragment implements SearchTestsRvAdapter.OnItemClickedHelper {

    private static final String TAG = "SearchTestsListFrag";
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLayout;
    private RecyclerView rv;
    private SearchTestsRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<TestsModel> list = new ArrayList<>();
    private String searchKey = "", cityName = "All", areaName = "All", category = "";
    private int pageNum = 1, lastPage = 1;
    private View view;
    private Context ctx;
    private Communicator comm;
    private DcSearchFrag parentFrag;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(DC_QUERY_UPDATED)) {
                        if (intent.getStringExtra("tabIndex") != null) {
                            if (intent.getStringExtra("tabIndex").equals(TEST)) {
                                if (rv != null) {
                                    rv.post(() -> {
                                        searchKey = intent.getStringExtra("searchKey");
                                        cityName = intent.getStringExtra("cityName");
                                        areaName = intent.getStringExtra("areaName");
                                        category = intent.getStringExtra("categories");
                                        pageNum = 1;
                                        lastPage = 1;
                                        getTestsList();
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    public DcTestsListFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
        parentFrag = (DcSearchFrag) getParentFragment();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_search_tests_list, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLayout = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new SearchTestsRvAdapter(list, this));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(() -> {
                pageNum = 1;
                lastPage = 1;
                getTestsList();
            });

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getTestsList();
                            }
                        }
                    }
                }
            });

            getTestsList();

            LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(DC_QUERY_UPDATED));
        }
        return view;
    }

    private void getTestsList() {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("countryId", 1);
                jsonObject.put("cityName", URLEncoder.encode(cityName.equals(ctx.getString(R.string.all_cities)) ? "All" : cityName, "utf-8"));
                jsonObject.put("areaName", URLEncoder.encode(areaName.equals(ctx.getString(R.string.all_areas)) ? "All" : areaName, "utf-8"));
                jsonObject.put("categoryId", category.equals("") ? "" : new JSONArray(category.split(",")));
                jsonObject.put("searchKey", URLEncoder.encode(searchKey, "utf-8"));
                jsonObject.put("type", TEST);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (parentFrag.getTabIndex() == 1) {
                    if (!comm.isActivityFinishing()) {
                        if (!swipeRefreshLayout.isRefreshing()) {
                            dialog.show();
                        }
                    }
                }

                VolleyHelper.getInstance(ctx).addRequest(getTestPackageListUrl(pageNum), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        TestResponseModel model = new Gson().fromJson(response, TestResponseModel.class);

                        pageNum = model.getPayload().getTestPackageList().getCurrentPage();
                        lastPage = model.getPayload().getTestPackageList().getLastPage();

                        if (model.getStatus() == 1) {

                            if (pageNum == 1) {
                                list.clear();
                            }

                            list.addAll(model.getPayload().getTestPackageList().getData());
                            adapter.notifyDataSetChanged();
                            pageNum++;
                        } else {
                            comm.showToast(model.getMsg());
                        }

                        updateUI();
                    }

                    @Override
                    public void onFailure(String error) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        updateUI();
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        swipeRefreshLayout.setRefreshing(false);
                        updateUI();
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(DcTestsListFrag.this);
                    }
                });


            } catch (JSONException | UnsupportedEncodingException/* | UnsupportedEncodingException*/ e) {
                swipeRefreshLayout.setRefreshing(false);
                updateUI();
                e.printStackTrace();
                Log.e(TAG, "getPackagesList: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
            updateUI();
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {
            DcTestDetailsFrag frag = new DcTestDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(pos));
            frag.setArguments(bundle);
            comm.openFragment(frag, TEST_DETAILS);
        }
    }

    @Override
    public void onFavClicked(int pos) {
        if (pos > -1) {
            addToFavourite(DcTestsListFrag.this, comm.isActivityFinishing(), list.get(pos).getId(), TEST, new OnCompleteHelper() {
                @Override
                public void onSuccess() {
                    list.get(pos).setFav(list.get(pos).isFav() ? 0 : 1);
                    adapter.notifyItemChanged(pos);
                }

                @Override
                public void onFailure(String error) {
                    comm.showToast(error);
                }
            });
        }
    }

    private void updateUI() {
        noDataLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }
}