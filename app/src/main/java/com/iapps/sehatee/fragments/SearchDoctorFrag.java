package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.DoctorNurseSearchFilter;
import com.iapps.sehatee.adapters.DoctorNurseSearchListRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.DoctorNurseListModel;
import com.iapps.sehatee.models.ResponseModels.DoctorNurseListResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.iapps.sehatee.utils.APIs.getDoctorListURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ANY_DAY;
import static com.iapps.sehatee.utils.Constants.CLINIC;
import static com.iapps.sehatee.utils.Constants.CONSULTANT;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DOCTOR_DETAILS;
import static com.iapps.sehatee.utils.Constants.HOSPITAL;
import static com.iapps.sehatee.utils.Constants.SEARCH_DOCTOR;
import static com.iapps.sehatee.utils.Constants.SPECIALIST;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class SearchDoctorFrag extends Fragment implements DoctorNurseSearchListRvAdapter.OnItemClickedHelper, View.OnClickListener {

    private static final String TAG = "SearchDoctorFrag";
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private DoctorNurseSearchListRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<DoctorNurseListModel> list = new ArrayList<>();
    private EditText searchEt;
    private TextView sortTv, filterTv;
    private int RC_FILTER = 3333;
    private String cityName = "All", areaName = "All", specialty = "0", entity = HOSPITAL + "," + CLINIC, selectedMinFee = "0", minFee = "0", selectedMaxFee = "2000", maxFee = "2000", availability = ANY_DAY, distance = "2 Km", insuranceProvider = "", gender = "", title = SPECIALIST + "," + CONSULTANT, sortValue = "2";
    private int pageNum = 1, lastPage = 1;

    private View view;
    private Context ctx;
    private Communicator comm;

    public SearchDoctorFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityName = getArguments().getString("cityName");
            areaName = getArguments().getString("areaName");
            specialty = getArguments().getString("specialty");
            entity = getArguments().getString("entity");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_search_doctor, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            sortTv = view.findViewById(R.id.sortTv);
            filterTv = view.findViewById(R.id.filterTv);
            searchEt = view.findViewById(R.id.searchEt);

            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new DoctorNurseSearchListRvAdapter(list, this));
            rv.setLayoutManager(lm = new GridLayoutManager(ctx, 2));

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        if (pageNum <= lastPage) {
                            if (lasVisibleItemIndex == list.size() - 1) {
                                getDoctorsList();
                            }
                        }
                    }
                }
            });

            swipeRefreshLayout.setOnRefreshListener(() -> {
                pageNum = 1;
                lastPage = 1;
                getDoctorsList();
            });

            searchEt.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNum = 1;
                    lastPage = 1;
                    getDoctorsList();
                    return true;
                }
                return false;
            });

            sortTv.setOnClickListener(this);
            filterTv.setOnClickListener(this);

            getDoctorsList();
        }

        CURRENT_FRAG = SEARCH_DOCTOR;
        comm.setPageTitle(R.string.find_a_doctor);
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        return view;
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    private void getDoctorsList() {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("countryId", "1");
                jsonObject.put("cityName", URLEncoder.encode(cityName.equals(ctx.getString(R.string.all_cities)) ? "All" : cityName, "utf-8"));
                jsonObject.put("areaName", URLEncoder.encode(areaName.equals(ctx.getString(R.string.all_areas)) ? "All" : areaName, "utf-8"));
                jsonObject.put("speciality", specialty.equals("0") ? new JSONArray() : new JSONArray(specialty.split(",")));
                jsonObject.put("minFees", selectedMinFee);
                jsonObject.put("maxFees", selectedMaxFee);
                jsonObject.put("gender", gender);
                jsonObject.put("entity", new JSONArray(entity.split(",")));
                jsonObject.put("title", new JSONArray(title.split(",")));
                jsonObject.put("availability", new JSONArray(availability.split(",")));
                jsonObject.put("insuranceId", insuranceProvider);
//                jsonObject.put("entity", HOSPITAL);
//                jsonObject.put("title", SPECIALIST);
                jsonObject.put("searchKey", URLEncoder.encode(searchEt.getText().toString().trim(), "utf-8"));
                jsonObject.put("isPriceLowToHigh", sortValue);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!swipeRefreshLayout.isRefreshing()) {
                    if (!comm.isActivityFinishing()) {
                        dialog.show();
                    }
                }

                VolleyHelper.getInstance(ctx).addRequest(getDoctorListURL(pageNum), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        DoctorNurseListResponseModel model = new Gson().fromJson(response, DoctorNurseListResponseModel.class);

                        pageNum = model.getPayload().getDoctorList().getCurrent_page();
                        lastPage = model.getPayload().getDoctorList().getLast_page();

                        if (model.getStatus() == 1) {
                            if (pageNum == 1) {
                                list.clear();
                            }
                            if (model.getPayload().getDoctorList().getData().size() > 0) {
                                list.addAll(model.getPayload().getDoctorList().getData());
                            }

                            for (int i = 0; i < list.size(); i++) {

                                for (int j = 0; j < list.get(i).getClinic().size(); j++) {
                                    for (int k = 0; k < list.get(i).getClinic().get(j).getSchedule().size(); k++) {
                                        if (list.get(i).getClinic().get(j).getSchedule().get(k).getTimeSlot().size() == 0) {
                                            list.get(i).getClinic().get(j).getSchedule().remove(k);
                                        }
                                    }
                                }
                            }

                            adapter.notifyDataSetChanged();

                            int min = 0;
                            int max = 2000;

                            for (int i = 0; i < list.size(); i++) {
                                int fee = Integer.parseInt(list.get(i).getFees());
//                                if (fee <= min) {
//                                    min = fee;
//                                }
                                if (fee >= max) {
                                    max = fee;
                                }
                            }

                            if (list.size() > 0) {
                                minFee = min + "";
                                maxFee = max + "";
                            }

                            Log.e(TAG, "onSuccess: min => " + min + ", max => " + max);

                            pageNum++;
                        } else {
                            comm.showToast(model.getMsg());
                        }
                        updateUI();
                    }

                    @Override
                    public void onFailure(String error) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        updateUI();
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        updateUI();
                        showUserRestrictedDialog(SearchDoctorFrag.this);
                    }
                });

            } catch (JSONException | UnsupportedEncodingException e) {
                updateUI();
                swipeRefreshLayout.setRefreshing(false);
                e.printStackTrace();
                Log.e(TAG, "getDoctorsList: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onViewClicked(int pos) {
        if (pos > -1) {
            DoctorDetailsFrag frag = new DoctorDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(pos));
            frag.setArguments(bundle);
            comm.openFragment(frag, DOCTOR_DETAILS);
        }
    }

    @Override
    public void onCallClicked(int pos) {
        if (pos > -1) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            Intent chooser = Intent.createChooser(callIntent, ctx.getString(R.string.call_using));
            callIntent.setData(Uri.parse("tel:" + list.get(pos).getPhone()));
            if (callIntent.resolveActivity(ctx.getPackageManager()) != null) {
                startActivity(chooser);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sortTv:
                showSortDialog();
                break;
            case R.id.filterTv:
                startActivityForResult(new Intent(ctx, DoctorNurseSearchFilter.class)
                                .putExtra("from", SEARCH_DOCTOR)
                                .putExtra("entity", entity)
                                .putExtra("availability", availability)
                                .putExtra("distance", distance)
                                .putExtra("insuranceProvider", insuranceProvider)
                                .putExtra("title", title)
                                .putExtra("gender", gender)
                                .putExtra("minFee", minFee)
                                .putExtra("maxFee", maxFee)
                                .putExtra("selectedMinFee", selectedMinFee)
                                .putExtra("selectedMaxFee", selectedMaxFee)
                        , RC_FILTER);
                break;
        }
    }


    private void showSortDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_sort_search_list, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        RadioGroup rg = view.findViewById(R.id.sortRg);

        Button resetBtn = view.findViewById(R.id.resetBtn);
        Button cancelBtn = view.findViewById(R.id.negBtn);
        Button applyBtn = view.findViewById(R.id.posBtn);

        rg.check(sortValue.equals("0") ? R.id.highToLowRb : sortValue.equals("1") ? R.id.lowToHighRb : R.id.mostRecommendedRb);

        applyBtn.setOnClickListener(v -> {
            dialog.dismiss();
            sortValue = rg.getCheckedRadioButtonId() == R.id.mostRecommendedRb ? "2" : rg.getCheckedRadioButtonId() == R.id.lowToHighRb ? "1" : "0";
            pageNum = 1;
            lastPage = 1;
            getDoctorsList();
        });

        cancelBtn.setOnClickListener(v -> dialog.dismiss());

        resetBtn.setOnClickListener(v -> {
            sortValue = "2";
            rg.check(R.id.mostRecommendedRb);
        });

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_FILTER && resultCode == RESULT_OK && data != null) {

            entity = data.getStringExtra("entity");
            maxFee = data.getStringExtra("maxFee");
            minFee = data.getStringExtra("minFee");
            selectedMaxFee = data.getStringExtra("selectedMaxFee");
            selectedMinFee = data.getStringExtra("selectedMinFee");
            availability = data.getStringExtra("availability");
            distance = data.getStringExtra("distance");
            insuranceProvider = data.getStringExtra("insuranceProvider");
            gender = data.getStringExtra("gender");
            title = data.getStringExtra("title");

            String filterValues = "\nEntity = " + entity + ",\n" +
                    "maxFee = " + maxFee + ",\n" +
                    "minFee = " + minFee + ",\n" +
                    "selectedMaxFee = " + selectedMaxFee + ",\n" +
                    "selectedMinFee = " + selectedMinFee + ",\n" +
                    "availability = " + availability + ",\n" +
                    "distance = " + distance + ",\n" +
                    "insuranceProvider = " + insuranceProvider + ",\n" +
                    "gender = " + gender + ",\n" +
                    "title = " + title + ",\n";

            Log.e(TAG, "Filter Values =>  " + filterValues);

            pageNum = 1;
            lastPage = 1;
            getDoctorsList();
        }

    }
}