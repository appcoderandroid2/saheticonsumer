package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.PhotoViewerActivity;
import com.iapps.sehatee.adapters.AppointmentPrescriptionRvAdapter;
import com.iapps.sehatee.adapters.AppointmentReportsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.interfaces.OnReviewHelper;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.DocumentModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.models.ResponseModels.SingleAppointmentDetailsResponseModel;
import com.iapps.sehatee.utils.helperClasses.PhotoPickOrTakeHelper;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getAddReviewURL;
import static com.iapps.sehatee.utils.APIs.getCancelAppointmentURL;
import static com.iapps.sehatee.utils.APIs.getDeletePrescriptionReportURL;
import static com.iapps.sehatee.utils.APIs.getSingleAppointmentDetailsURL;
import static com.iapps.sehatee.utils.APIs.getUploadPrescriptionReportImageURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ACCEPTED;
import static com.iapps.sehatee.utils.Constants.APPOINTMENTS;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_HISTORY;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_HISTORY_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.CANCELED;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DOCTOR;
import static com.iapps.sehatee.utils.Constants.FINISHED;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.PRESCRIPTION;
import static com.iapps.sehatee.utils.Constants.REJECTED;
import static com.iapps.sehatee.utils.Constants.REPORT;
import static com.iapps.sehatee.utils.Constants.REQUESTED;
import static com.iapps.sehatee.utils.Constants.getFormattedDateNoYear;
import static com.iapps.sehatee.utils.Constants.getFormattedTime;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd_hhmmss;
import static com.iapps.sehatee.utils.Constants.showAddReviewDialog;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class AppointmentDetails extends Fragment implements AppointmentPrescriptionRvAdapter.OnItemClickedHelper, AppointmentReportsRvAdapter.OnItemClickedHelper, PhotoPickOrTakeHelper.OnResultHelper {

    private static final String TAG = "AppointmentDetails";
    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout presNoDataLL, reportNoDataLL, canceledLL, rejectedLL;
    private ImageView iv;
    boolean isUploading = false;
    private Button addPrescriptionBtn, addReportBtn, cancelBtn, addReviewBtn;
    private TextView nameTv, specialtyTv, addressTv, dateTimeTv, phTv, statusTv, appointmentNumberTv, feesTv, patientNameTv, patientPhoneTv, appointmentHistoryTv, cancelReasonTv, rejectedReasonTv, logTv;
    private AppointmentsModel model = new AppointmentsModel();
    private int pos = 0;
    private String from = APPOINTMENTS;
    private RecyclerView prescriptionRv;
    private AppointmentPrescriptionRvAdapter appointmentPrescriptionRvAdapter;
    private List<DocumentModel> prescriptionList = new ArrayList<>();
    private RecyclerView reportsRv;
    private AppointmentReportsRvAdapter reportsAdapter;
    private List<DocumentModel> reportsList = new ArrayList<>();
    private PhotoPickOrTakeHelper photoPickOrTakeHelper;
    private boolean cameraOpenedForReport = false;
    private int RC_OPEN_APP_INFO_SETTINGS = 1212;

    public AppointmentDetails() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (AppointmentsModel) getArguments().getSerializable("model");
            from = getArguments().getString("from");
            pos = getArguments().getInt("pos");
        }
    }

    private CardView appointmentHistoryCard;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_appointment_details, container, false);

            initViews(view);

            appointmentHistoryCard.setVisibility(from.equals(APPOINTMENTS) ? View.VISIBLE : View.GONE);

            prescriptionList.clear();
            prescriptionList.addAll(model.getPrescriptionImages());

            prescriptionRv.setAdapter(appointmentPrescriptionRvAdapter = new AppointmentPrescriptionRvAdapter(prescriptionList, this, model.getStatus()));
            prescriptionRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            reportsList.clear();
            reportsList.addAll(model.getReportImages());

            reportsRv.setAdapter(reportsAdapter = new AppointmentReportsRvAdapter(reportsList, this, model.getStatus()));
            reportsRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            swipeRefreshLayout.setOnRefreshListener(() -> getAppointmentDetails(false));

            appointmentHistoryTv.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putString("appointmentId", model.getAppointmentId());
                AppointmentHistoryFrag frag = new AppointmentHistoryFrag();
                frag.setArguments(bundle);
                comm.openFragment(frag, APPOINTMENT_HISTORY);
            });

            addPrescriptionBtn.setOnClickListener(v -> {
                cameraOpenedForReport = false;
                openChooserDialog();
            });

            addReportBtn.setOnClickListener(v -> {
                cameraOpenedForReport = true;
                openChooserDialog();
            });

            cancelBtn.setOnClickListener(v -> {
                if (!comm.isActivityFinishing()) {
                    showCancelDialog();
                }
            });

            addReviewBtn.setOnClickListener(v -> {
                if (!comm.isActivityFinishing()) {
                    showAddReviewDialog(ctx, new OnReviewHelper() {
                        @Override
                        public void onSubmitClicked(Dialog d, float rate, String review) {
                            addReview(d, rate, URLEncoder.encode(review));
                        }

                        @Override
                        public void onNoRating(Dialog d) {
                            comm.showToast(R.string.error_no_rating);
                        }

                        @Override
                        public void onReviewEmpty(Dialog d) {
                            comm.showToast(R.string.error_empty_review);
                        }
                    });
                }
            });
            updateUI();
        }

        CURRENT_FRAG = APPOINTMENT_DETAILS;
        comm.setPageTitle(R.string.appointment_details);
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        return view;
    }

    private void addReview(Dialog d, float rate, String review) {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("itemId", model.getDoctorNurseDetails().getId());
                jsonObject.put("type", DOCTOR);
                jsonObject.put("rating", rate);
                jsonObject.put("review", review);
                jsonObject.put("reviewDateTime", sdf_yyyyMMdd_hhmmss.format(Calendar.getInstance().getTime()));

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
                VolleyHelper.getInstance(ctx).addRequest(getAddReviewURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            comm.showToast("Review submitted successfully");
                            d.dismiss();
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(AppointmentDetails.this);
                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "addReview: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void showCancelDialog() {
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_cancel_doctor_appointment, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        RadioGroup rg = v.findViewById(R.id.rg);
        Button posBtn = v.findViewById(R.id.posBtn);
        Button negBtn = v.findViewById(R.id.negBtn);

        posBtn.setOnClickListener(new View.OnClickListener() {
            String reason = ctx.getString(R.string.do_not_wish_to_specify);

            @Override
            public void onClick(View v) {
                if (rg.getCheckedRadioButtonId() == R.id.tooLongRb) {
                    reason = ctx.getString(R.string.doctor_is_taking_too_long_to_reply);
                } else if (rg.getCheckedRadioButtonId() == R.id.foundAnotherRb) {
                    reason = ctx.getString(R.string.another_doctor_appointment_is_fixed);
                } else if (rg.getCheckedRadioButtonId() == R.id.changeDateRb) {
                    reason = ctx.getString(R.string.doctor_is_taking_too_long_to_reply);
                } else {
                    reason = ctx.getString(R.string.do_not_wish_to_specify);
                }

                dialog.dismiss();
                cancelAppointment(reason);

            }
        });

        negBtn.setOnClickListener(v1 -> dialog.dismiss());

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void cancelAppointment(String reason) {
        if (isOnline(ctx)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appointmentId", model.getAppointmentId());
                jsonObject.put("cancelReason", URLEncoder.encode(reason, "utf-8"));

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getCancelAppointmentURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            getAppointmentDetails(true);
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(AppointmentDetails.this);
                    }
                });


            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.e(TAG, "cancelAppointment: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void getAppointmentDetails(boolean shouldShowDialog) {
        if (isOnline(ctx)) {
            swipeRefreshLayout.setRefreshing(false);

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (shouldShowDialog) {
                if (!comm.isActivityFinishing()) {
                    if (!swipeRefreshLayout.isRefreshing()) {
                        dialog.show();
                    }
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getSingleAppointmentDetailsURL(model.getAppointmentId()), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    SingleAppointmentDetailsResponseModel singleAppointmentDetailsResponseModel = new Gson().fromJson(response, SingleAppointmentDetailsResponseModel.class);
                    if (singleAppointmentDetailsResponseModel.getStatus() == 1) {
                        model = singleAppointmentDetailsResponseModel.getPayload().getAppointmentDetails();

//                        logTv.append("Prescriptions array from response length = " + model.getPrescriptionImages().size());
//                        logTv.append("\n");
//                        logTv.append("Reports array from response length = " + model.getReportImages().size());
//                        logTv.append("\n");

                        LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(from.equals(APPOINTMENTS)
                                ? APPOINTMENT_DETAILS_UPDATED
                                : APPOINTMENT_HISTORY_DETAILS_UPDATED)
                                .putExtra("pos", pos)
                                .putExtra("model", model)
                        );
                    } else {
                        comm.showToast(singleAppointmentDetailsResponseModel.getMsg());
                    }
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(AppointmentDetails.this);
                }
            });

        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void initViews(View view) {
        photoPickOrTakeHelper = new PhotoPickOrTakeHelper.Builder(this)
                .setEnableCropper(false)
                .build(this);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        presNoDataLL = view.findViewById(R.id.presFL).findViewById(R.id.noDataLL);
        reportNoDataLL = view.findViewById(R.id.reportFL).findViewById(R.id.noDataLL);
        iv = view.findViewById(R.id.iv);
        nameTv = view.findViewById(R.id.nameTv);
        specialtyTv = view.findViewById(R.id.specialtyTv);
        addressTv = view.findViewById(R.id.addressTv);
        dateTimeTv = view.findViewById(R.id.dateTimeTv);
        phTv = view.findViewById(R.id.phTv);
        statusTv = view.findViewById(R.id.statusTv);
        appointmentNumberTv = view.findViewById(R.id.appointmentNumberTv);
        feesTv = view.findViewById(R.id.feesTv);
        patientNameTv = view.findViewById(R.id.patientNameTv);
        patientPhoneTv = view.findViewById(R.id.patientPhoneTv);
        prescriptionRv = view.findViewById(R.id.prescriptionsRv);
        addPrescriptionBtn = view.findViewById(R.id.addPrescriptionBtn);
        addReportBtn = view.findViewById(R.id.addReportBtn);
        reportsRv = view.findViewById(R.id.reportsRv);
        appointmentHistoryTv = view.findViewById(R.id.appointmentHistoryTv);
        appointmentHistoryCard = view.findViewById(R.id.appointmentHistoryCard);
        canceledLL = view.findViewById(R.id.cancellationReasonLL);
        rejectedLL = view.findViewById(R.id.rejectionReasonLL);
        cancelReasonTv = view.findViewById(R.id.cancellationReasonTv);
        rejectedReasonTv = view.findViewById(R.id.rejectionReasonTv);
        cancelBtn = view.findViewById(R.id.cancelBtn);
        addReviewBtn = view.findViewById(R.id.addReviewBtn);

        logTv = view.findViewById(R.id.logTv);
    }

    private void updateUI() {
        Picasso.get()
                .load(model.getDoctorNurseDetails().getImageLink().isEmpty() ? NA : model.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(iv);

        nameTv.setText(model.getDoctorNurseDetails().getName());
        specialtyTv.setText(String.format(Locale.getDefault(), "(%s)", model.getDoctorNurseDetails().getSpecialty()));
        addressTv.setText(String.format(Locale.getDefault(), "%s, %s, %s, %s, %s, %s",
                model.getClinicDetails().getName(),
                model.getClinicDetails().getAddress(),
                model.getClinicDetails().getAreaName(),
                model.getClinicDetails().getCityName(),
                model.getClinicDetails().getCountryName(),
                model.getClinicDetails().getPostalCode()
        ));
        dateTimeTv.setText(String.format(Locale.getDefault(), "%s : %s, %s - %s",
                ctx.getString(R.string.appointment_date),
                getFormattedDateNoYear(model.getAppointmentDate()),
                getFormattedTime(model.getFromTime()),
                getFormattedTime(model.getToTime())
        ));

        phTv.setText(model.getClinicDetails().getPhone());
        appointmentNumberTv.setText(model.getAppointmentNo());
        feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), model.getClinicDetails().getCurrencySymbol(), model.getClinicDetails().getFees()));
        statusTv.setText(model.getStatus());

        patientNameTv.setText(model.getPatientName());
        patientPhoneTv.setText(model.getPatientPhone());

        if (model.getStatus().equals(REQUESTED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorAccent, ctx.getTheme()));
        } else if (model.getStatus().equals(ACCEPTED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.thumbsUpGreen, ctx.getTheme()));
        } else if (model.getStatus().equals(REJECTED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.notificationBadgeColor, ctx.getTheme()));
        } else if (model.getStatus().equals(CANCELED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimaryDark, ctx.getTheme()));
        } else {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimary, ctx.getTheme()));
        }

        if (model.getPrescriptionImages().size() > 0) {
            prescriptionList.clear();
            prescriptionList.addAll(model.getPrescriptionImages());
            appointmentPrescriptionRvAdapter.notifyDataSetChanged();
        }
        if (model.getReportImages().size() > 0) {
            reportsList.clear();
            reportsList.addAll(model.getReportImages());
            reportsAdapter.notifyDataSetChanged();
        }

        presNoDataLL.setVisibility(prescriptionList.size() > 0 ? View.GONE : View.VISIBLE);
        reportNoDataLL.setVisibility(reportsList.size() > 0 ? View.GONE : View.VISIBLE);

        addReportBtn.setVisibility(model.getStatus().equals(REJECTED) || model.getStatus().equals(CANCELED) || model.getStatus().equals(REQUESTED) ? View.GONE : View.VISIBLE);
        addPrescriptionBtn.setVisibility(model.getStatus().equals(REJECTED) || model.getStatus().equals(CANCELED) || model.getStatus().equals(REQUESTED) ? View.GONE : View.VISIBLE);

        canceledLL.setVisibility(model.getStatus().equals(CANCELED) ? View.VISIBLE : View.GONE);
        cancelReasonTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.cancellation_reason), model.getCancelReason()));
        rejectedLL.setVisibility(model.getStatus().equals(REJECTED) ? View.VISIBLE : View.GONE);
        rejectedReasonTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.rejection_reason), model.getCancelReason()));

        addReviewBtn.setText(R.string.add_rating);
        addReviewBtn.setVisibility(model.getStatus().equals(FINISHED) ? View.VISIBLE : View.GONE);

        cancelBtn.setVisibility(model.getStatus().equals(FINISHED) || model.getStatus().equals(CANCELED) || model.getStatus().equals(REJECTED) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onReportClicked(int pos) {
        if (pos > -1) {
            Intent intent = new Intent(ctx, PhotoViewerActivity.class);
            intent.putExtra("pos", pos);
            intent.putExtra("list", (Serializable) reportsList);
            startActivity(intent);
        }
    }

    @Override
    public void onReportDeleteClicked(int pos) {
        if (pos > -1) {
            if (!comm.isActivityFinishing()) {
                comm.getDoubleBtnDialog(ctx.getString(R.string.alert), ctx.getString(R.string.alert_delete), ctx.getString(R.string.no), ctx.getString(R.string.yes), new OnDialogActionHelper() {
                    @Override
                    public void onPositiveBtnClicked(Dialog d) {
                        d.dismiss();
                        deleteItem(REPORT, reportsList.get(pos).getUrl(), pos);
                    }

                    @Override
                    public void onNegativeBtnClicked(Dialog d) {
                        d.dismiss();
                    }
                }).show();
            }
        }
    }

    private void deleteItem(String imageType, String url, int pos) {

        if (isOnline(ctx)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appointmentId", model.getAppointmentId());
                jsonObject.put("imageType", imageType);
                jsonObject.put("image", url.substring(url.lastIndexOf("/")).replace("/", "").trim());

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getDeletePrescriptionReportURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            if (imageType.equals(PRESCRIPTION)) {
                                appointmentPrescriptionRvAdapter.notifyItemRemoved(pos);
                                prescriptionList.remove(pos);
                                presNoDataLL.setVisibility(prescriptionList.size() > 0 ? View.GONE : View.VISIBLE);
                            } else {
                                reportsAdapter.notifyItemRemoved(pos);
                                reportsList.remove(pos);
                                reportNoDataLL.setVisibility(reportsList.size() > 0 ? View.GONE : View.VISIBLE);
                            }
                            getAppointmentDetails(false);
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(AppointmentDetails.this);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "deleteItem: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onPrescriptionClicked(int pos) {
        if (pos > -1) {
            Intent intent = new Intent(ctx, PhotoViewerActivity.class);
            intent.putExtra("pos", pos);
            intent.putExtra("list", (Serializable) prescriptionList);
            startActivity(intent);
        }
    }

    @Override
    public void onPrescriptionDeleteClicked(int pos) {
        if (pos > -1) {
            if (!comm.isActivityFinishing()) {
                comm.getDoubleBtnDialog(ctx.getString(R.string.alert), ctx.getString(R.string.alert_delete), ctx.getString(R.string.no), ctx.getString(R.string.yes), new OnDialogActionHelper() {
                    @Override
                    public void onPositiveBtnClicked(Dialog d) {
                        d.dismiss();
                        deleteItem(PRESCRIPTION, prescriptionList.get(pos).getUrl(), pos);
                    }

                    @Override
                    public void onNegativeBtnClicked(Dialog d) {
                        d.dismiss();
                    }
                }).show();
            }
        }
    }

    private void openChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_photo_pick_or_take, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView cameraTv = view.findViewById(R.id.cameraTv);
        TextView galleryTv = view.findViewById(R.id.galleryTv);

        cameraTv.setOnClickListener((v) -> {
            dialog.dismiss();
            photoPickOrTakeHelper.openCamera();
        });
        galleryTv.setOnClickListener((v) -> {
            dialog.dismiss();
//            logTv.append("Pick a photo clicked");
//            logTv.append("\n");
            photoPickOrTakeHelper.openExplorer();
        });

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onFailure() {
//        logTv.append("onFailure");
//        logTv.append("\n");
        comm.showToast(R.string.something_went_wrong);
    }

    @Override
    public void onSuccess(PhotoPickOrTakeHelper.Result result) {
//        logTv.append("onSuccess");
//        logTv.append("\n");
//        logTv.append(result.getImageName());
//        logTv.append("\n");
        if (!isUploading) {
            uploadFile(result, cameraOpenedForReport);
        }
    }

    @Override
    public void onPermissionDenied(int requestType) {
        if (!comm.isActivityFinishing()) {
            String msg = requestType == PhotoPickOrTakeHelper.RC_OPEN_CAMERA ? getString(R.string.error_note_photo_take_permission_denied) : getString(R.string.error_note_pick_photo_permission_denied);
            comm.getDoubleBtnDialog(ctx.getString(R.string.alert), msg, "Not Now", "Grant Permission", new OnDialogActionHelper() {
                @Override
                public void onPositiveBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                    if (requestType == PhotoPickOrTakeHelper.RC_OPEN_CAMERA) {
                        photoPickOrTakeHelper.openCamera();
                    }
                    if (requestType == PhotoPickOrTakeHelper.RC_OPEN_EXPLORER) {
                        photoPickOrTakeHelper.openExplorer();
                    }
                }

                @Override
                public void onNegativeBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    public void openAppInfoSettings(int requestType) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", ctx.getPackageName(), null);
        intent.setData(uri);
        this.startActivityForResult(intent, RC_OPEN_APP_INFO_SETTINGS = requestType);
    }

    @Override
    public void onNeverAskPermissionChecked(int requestType) {
        if (!comm.isActivityFinishing()) {
            String msg = requestType == PhotoPickOrTakeHelper.RC_OPEN_CAMERA ? ctx.getString(R.string.error_note_take_photo_permission_denied_and_never_ask_checked) : ctx.getString(R.string.error_note_pick_photo_permission_denied_and_never_ask_checked);
            comm.getDoubleBtnDialog(ctx.getString(R.string.alert), msg, "Not Now", "APP INFO", new OnDialogActionHelper() {
                @Override
                public void onPositiveBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                    openAppInfoSettings(requestType);
                }

                @Override
                public void onNegativeBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        photoPickOrTakeHelper.onRequestPermissionsResult(requestCode, grantResults);
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        photoPickOrTakeHelper.onActivityResult(requestCode, resultCode, data, this);
//        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadFile(PhotoPickOrTakeHelper.Result result, boolean cameraOpenedForReport) {
        if (!isUploading) {
            if (isOnline(ctx)) {
                try {

                    isUploading = true;
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("appointmentId", model.getAppointmentId());
                    jsonObject.put("imageType", cameraOpenedForReport ? REPORT : PRESCRIPTION);
                    jsonObject.put("image", new JSONArray(new String[]{result.getEncodedString()}));

//                logTv.append("Image array size" + jsonObject.getJSONArray("image").length());
//                logTv.append("\n");

                    final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                    if (!comm.isActivityFinishing()) {
                        dialog.show();
                    }
                    VolleyHelper.getInstance(ctx).addRequest(getUploadPrescriptionReportImageURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                        @Override
                        public void onSuccess(String response) {
                            isUploading = false;
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }

                            SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                            if (model.getStatus() == 1) {

                                if (cameraOpenedForReport) {
                                    reportsList.add(new DocumentModel());
                                    reportsAdapter.notifyItemInserted(reportsList.size() - 1);
//                                logTv.append("Dummy Prescription image added");
//                                logTv.append("\n");
                                } else {
                                    prescriptionList.add(new DocumentModel());
//                                logTv.append("Dummy Report image added");
//                                logTv.append("\n");
                                    appointmentPrescriptionRvAdapter.notifyItemInserted(prescriptionList.size() - 1);
                                }

                                comm.showToast(R.string.upload_successful);
                                getAppointmentDetails(false);
                            } else {
                                comm.showToast(model.getMsg());
                            }
                        }

                        @Override
                        public void onFailure(String error) {
                            isUploading = false;
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            comm.showToast(error);
                        }

                        @Override
                        public void onRestrictedByAdmin() {
                            isUploading = false;
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            showUserRestrictedDialog(AppointmentDetails.this);
                        }
                    });
                } catch (JSONException e) {
                    isUploading = false;
                    e.printStackTrace();
                    Log.e(TAG, "uploadFile: ", e);
                    comm.showToast(R.string.something_went_wrong);
                }
            } else {
                isUploading = false;
                comm.showToast(R.string.error_internet_unavilable);
            }
        }
    }
}