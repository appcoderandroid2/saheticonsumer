package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.DcCategoriesFilter;
import com.iapps.sehatee.activities.SelectCity;
import com.iapps.sehatee.adapters.BannerVpAdapter;
import com.iapps.sehatee.adapters.DcCategoryHomeRvAdapter;
import com.iapps.sehatee.adapters.PopularPackagesRvAdapter;
import com.iapps.sehatee.adapters.PopularTestsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnCompleteHelper;
import com.iapps.sehatee.models.BannerModel;
import com.iapps.sehatee.models.CategoryModel;
import com.iapps.sehatee.models.PackageModel;
import com.iapps.sehatee.models.ResponseModels.BannerResponseModel;
import com.iapps.sehatee.models.ResponseModels.DcPackTestCategoriesResponseModel;
import com.iapps.sehatee.models.ResponseModels.PackagesResponseModel;
import com.iapps.sehatee.models.ResponseModels.TestResponseModel;
import com.iapps.sehatee.models.TestsModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getCategoriesURL;
import static com.iapps.sehatee.utils.APIs.getDcHomeBannerURL;
import static com.iapps.sehatee.utils.APIs.getTestPackageListUrl;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_CATEGORIES;
import static com.iapps.sehatee.utils.Constants.BANNER_HEIGHT;
import static com.iapps.sehatee.utils.Constants.BANNER_WIDTH;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.HOME_DC;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.PACKAGE_DETAILS;
import static com.iapps.sehatee.utils.Constants.SEARCH_DC;
import static com.iapps.sehatee.utils.Constants.TEST;
import static com.iapps.sehatee.utils.Constants.TEST_DETAILS;
import static com.iapps.sehatee.utils.Constants.addToFavourite;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DcHomeFrag extends Fragment implements PopularPackagesRvAdapter.OnItemClickedHelper, View.OnClickListener, PopularTestsRvAdapter.OnItemClickedHelper, DcCategoryHomeRvAdapter.OnItemClickedHelper, BannerVpAdapter.OnBannerClickedHelper {

    private static final String TAG = "DcHomeFrag";
    private static final int RC_CATEGORIES_FILTER = 1001;
    private SwipeRefreshLayout swipeRefreshLayout;
    private static final int RC_CHANGE_LOCATION = 2002;
    private LinearLayout noPackLL, noCatLL, noTestLL;
    private RecyclerView popPackRv, catRv, popTestsRv;
    private PopularPackagesRvAdapter popularPackagesRvAdapter;
    private PopularTestsRvAdapter popularTestsRvAdapter;
    private DcCategoryHomeRvAdapter dcCategoryOffersRvAdapter;
    private List<CategoryModel> categoryModelList = new ArrayList<>();
    private List<PackageModel> popPackList = new ArrayList<>();
    private List<TestsModel> testsList = new ArrayList<>();
    private TextView viewAllPopPackTv, viewAllLifestyleTv, viewAllPopTestTv;
    private ViewPager bannerVp;
    private BannerVpAdapter bannerVpAdapter;
    private List<BannerModel> bannerList = new ArrayList<>();
    private View view;
    private Context ctx;
    private Communicator comm;
    private String categories = "", cityName = "All", areaName = "All";
    private Handler bannerHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            int nextIndex = bannerVp.getCurrentItem() + 1;
            if (nextIndex == bannerList.size()) {
                nextIndex = 0;
            }
            bannerVp.setCurrentItem(nextIndex, nextIndex != 0);
            bannerHandler.postDelayed(this, 4000);
        }
    };

    public DcHomeFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        bannerHandler.removeCallbacks(runnable);
        bannerHandler.postDelayed(runnable, 4000);
    }

    @Override
    public void onPause() {
        bannerHandler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_dc_home, container, false);

            initViews(view);
            initClicks();

            popPackRv.setAdapter(popularPackagesRvAdapter = new PopularPackagesRvAdapter(popPackList, this));
            popPackRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            catRv.setAdapter(dcCategoryOffersRvAdapter = new DcCategoryHomeRvAdapter(categoryModelList, this));
            catRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            popTestsRv.setAdapter(popularTestsRvAdapter = new PopularTestsRvAdapter(testsList, this));
            popTestsRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            swipeRefreshLayout.setOnRefreshListener(() -> {
                getBanners();
                getPackagesList();
                getCategories();
                getTestsList();
            });

            getBanners();
            getPackagesList();
            getCategories();
            getTestsList();
        }
        CURRENT_FRAG = HOME_DC;
        comm.setActionBar(AB_BACK_WITH_NOTI);
        comm.setPageTitle(R.string.welcome_to_sahety);
        comm.setMenuSelection(HOME_DC);
        return view;
    }

    private void getBanners() {
        if (isOnline(ctx)) {

            VolleyHelper.getInstance(ctx).addRequest(getDcHomeBannerURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    BannerResponseModel model = new Gson().fromJson(response, BannerResponseModel.class);
                    if (model.getStatus() == 1) {
                        if (model.getPayload().getData().size() > 0) {
                            bannerList.clear();
                            bannerList.addAll(model.getPayload().getData());
                            bannerVpAdapter = new BannerVpAdapter(bannerList, DcHomeFrag.this);
                            bannerVp.setAdapter(bannerVpAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(String error) {

                }

                @Override
                public void onRestrictedByAdmin() {

                }
            });
        }
    }

    private void initViews(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        noPackLL = view.findViewById(R.id.packagesFL).findViewById(R.id.noDataLL);
        noCatLL = view.findViewById(R.id.catFL).findViewById(R.id.noDataLL);
        noTestLL = view.findViewById(R.id.testFL).findViewById(R.id.noDataLL);
        bannerVp = view.findViewById(R.id.bannerVp);
        popPackRv = view.findViewById(R.id.popPackRv);
        catRv = view.findViewById(R.id.catRv);
        popTestsRv = view.findViewById(R.id.popTestsRv);
        viewAllPopPackTv = view.findViewById(R.id.viewAllPopPackTv);
        viewAllLifestyleTv = view.findViewById(R.id.viewAllLifestyleTv);
        viewAllPopTestTv = view.findViewById(R.id.viewAllPopTestTv);

        // sizing banner vp to equivalent resolution to 640x296
        bannerVp.post(() -> bannerVp.getLayoutParams().height = (BANNER_HEIGHT * bannerVp.getWidth() / BANNER_WIDTH));
    }

    private void initClicks() {
        viewAllPopPackTv.setOnClickListener(this);
        viewAllLifestyleTv.setOnClickListener(this);
        viewAllPopTestTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.viewAllPopPackTv) {
            DcSearchFrag frag = new DcSearchFrag();
            Bundle bundle = new Bundle();
            bundle.putString("categories", categories);
            bundle.putString("cityName", cityName);
            bundle.putString("areaName", areaName);
            bundle.putInt("tabIndex", 0);
            frag.setArguments(bundle);
            comm.openFragment(frag, SEARCH_DC);
        } else if (id == R.id.viewAllLifestyleTv) {
            DcAllCategories frag = new DcAllCategories();
            Bundle bundle = new Bundle();
            bundle.putString("categories", categories);
            bundle.putString("cityName", cityName);
            bundle.putString("areaName", areaName);
            bundle.putString("from", HOME_DC);
            bundle.putInt("tabIndex", 0);
            frag.setArguments(bundle);
            comm.openFragment(frag, ALL_CATEGORIES);
        } else if (id == R.id.viewAllPopTestTv) {
            DcSearchFrag frag = new DcSearchFrag();
            Bundle bundle = new Bundle();
            bundle.putString("categories", categories);
            bundle.putString("cityName", cityName);
            bundle.putString("areaName", areaName);
            bundle.putInt("tabIndex", 1);
            frag.setArguments(bundle);
            comm.openFragment(frag, SEARCH_DC);
        } else if (id == R.id.filterTv) {
            startActivityForResult(new Intent(ctx, DcCategoriesFilter.class).putExtra("categories", categories), RC_CATEGORIES_FILTER);
        } else if (id == R.id.searchTv) {
            DcSearchFrag frag = new DcSearchFrag();
            Bundle bundle = new Bundle();
            bundle.putString("categories", categories);
            bundle.putString("cityName", cityName);
            bundle.putString("areaName", areaName);
            frag.setArguments(bundle);
            comm.openFragment(frag, SEARCH_DC);
        } else if (id == R.id.changeLocationTv) {
            startActivityForResult(new Intent(ctx, SelectCity.class).putExtra("from", HOME_DC).putExtra("cityName", cityName).putExtra("areaName", areaName), RC_CHANGE_LOCATION);
        }
    }

    private void updateUI(String type) {
        switch (type) {
            case PACKAGE:
                noPackLL.setVisibility(popPackList.size() > 0 ? View.GONE : View.VISIBLE);
                break;
            case TEST:
                noTestLL.setVisibility(testsList.size() > 0 ? View.GONE : View.VISIBLE);
                break;
            case ALL_CATEGORIES:
                noCatLL.setVisibility(categoryModelList.size() > 0 ? View.GONE : View.VISIBLE);
                break;
            default:
                noPackLL.setVisibility(popPackList.size() > 0 ? View.GONE : View.VISIBLE);
                noCatLL.setVisibility(categoryModelList.size() > 0 ? View.GONE : View.VISIBLE);
                noTestLL.setVisibility(testsList.size() > 0 ? View.GONE : View.VISIBLE);
                break;
        }

    }

    private void getPackagesList() {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("countryId", 1);
                jsonObject.put("cityName", cityName);
                jsonObject.put("areaName", areaName);
                jsonObject.put("categoryId", "");
                jsonObject.put("searchKey", "");
                jsonObject.put("type", PACKAGE);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!swipeRefreshLayout.isRefreshing()) {
                    if (!comm.isActivityFinishing()) {
                        dialog.show();
                    }
                }

                VolleyHelper.getInstance(ctx).addRequest(getTestPackageListUrl(1), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        PackagesResponseModel model = new Gson().fromJson(response, PackagesResponseModel.class);
                        if (model.getStatus() == 1) {

                            if (model.getPayload().getTestPackageList().getData().size() > 0) {
                                popPackList.clear();
                                popPackList.addAll(model.getPayload().getTestPackageList().getData());
                            }
                            popularPackagesRvAdapter.notifyDataSetChanged();

                        } else {
                            comm.showToast(model.getMsg());
                        }

                        updateUI(PACKAGE);
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        updateUI(PACKAGE);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        updateUI(PACKAGE);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(DcHomeFrag.this);
                    }
                });


            } catch (JSONException e) {
                updateUI(PACKAGE);
                e.printStackTrace();
                Log.e(TAG, "getPackagesList: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            updateUI(PACKAGE);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void getCategories() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getCategoriesURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    DcPackTestCategoriesResponseModel model = new Gson().fromJson(response, DcPackTestCategoriesResponseModel.class);
                    if (model.getStatus() == 1) {
                        categoryModelList.clear();
                        categoryModelList.addAll(model.getPayload().getCategories());

                        dcCategoryOffersRvAdapter.notifyDataSetChanged();
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI(ALL_CATEGORIES);
                }

                @Override
                public void onFailure(String error) {
                    updateUI(ALL_CATEGORIES);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    updateUI(ALL_CATEGORIES);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(DcHomeFrag.this);
                }
            });

        } else {
            updateUI(ALL_CATEGORIES);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void getTestsList() {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("countryId", 1);
                jsonObject.put("cityName", cityName);
                jsonObject.put("areaName", areaName);
                jsonObject.put("categoryId", "");
                jsonObject.put("searchKey", "");
                jsonObject.put("type", TEST);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!swipeRefreshLayout.isRefreshing()) {
                    if (!comm.isActivityFinishing()) {
                        dialog.show();
                    }
                }

                VolleyHelper.getInstance(ctx).addRequest(getTestPackageListUrl(1), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        TestResponseModel model = new Gson().fromJson(response, TestResponseModel.class);
                        if (model.getStatus() == 1) {

                            if (model.getPayload().getTestPackageList().getData().size() > 0) {
                                testsList.clear();
                                testsList.addAll(model.getPayload().getTestPackageList().getData());
                            }
                            popularTestsRvAdapter.notifyDataSetChanged();

                        } else {
                            comm.showToast(model.getMsg());
                        }

                        updateUI(TEST);
                    }

                    @Override
                    public void onFailure(String error) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        updateUI(TEST);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        swipeRefreshLayout.setRefreshing(false);
                        updateUI(TEST);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(DcHomeFrag.this);
                    }
                });


            } catch (JSONException e) {
                updateUI(TEST);
                e.printStackTrace();
                Log.e(TAG, "getPackagesList: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
            updateUI(TEST);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onPopPackItemClicked(int pos) {
        if (pos > -1) {
            DcPackageDetailsFrag frag = new DcPackageDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", popPackList.get(pos));
            bundle.putString("from", PACKAGE);
            frag.setArguments(bundle);
            comm.openFragment(frag, PACKAGE_DETAILS);
        }
    }

    @Override
    public void onPopPackFavClicked(int pos) {
        if (pos > -1) {
            addToFavourite(DcHomeFrag.this, comm.isActivityFinishing(), popPackList.get(pos).getId(), PACKAGE, new OnCompleteHelper() {
                @Override
                public void onSuccess() {
                    popPackList.get(pos).setFav(popPackList.get(pos).isFav() ? 0 : 1);
                    popularPackagesRvAdapter.notifyItemChanged(pos);
                }

                @Override
                public void onFailure(String error) {
                    comm.showToast(error);
                }
            });
        }
    }

    @Override
    public void onPopTestItemClicked(int pos) {
        if (pos > -1) {
            DcTestDetailsFrag frag = new DcTestDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", testsList.get(pos));
            bundle.putString("from", TEST);
            frag.setArguments(bundle);
            comm.openFragment(frag, TEST_DETAILS);
        }
    }

    @Override
    public void onPopTestFavClicked(int pos) {
        if (pos > -1) {
            addToFavourite(DcHomeFrag.this, comm.isActivityFinishing(), testsList.get(pos).getId(), TEST, new OnCompleteHelper() {
                @Override
                public void onSuccess() {
                    testsList.get(pos).setFav(testsList.get(pos).isFav() ? 0 : 1);
                    popularTestsRvAdapter.notifyItemChanged(pos);
                }

                @Override
                public void onFailure(String error) {
                    comm.showToast(error);
                }
            });
        }
    }

    @Override
    public void onCategoryItemClicked(int pos) {
        if (pos > -1) {
            DcSearchFrag frag = new DcSearchFrag();
            Bundle bundle = new Bundle();
            bundle.putString("categories", categoryModelList.get(pos).getId());
            bundle.putString("cityName", cityName);
            bundle.putString("areaName", areaName);
            bundle.putInt("tabIndex", 0);
            frag.setArguments(bundle);
            comm.openFragment(frag, SEARCH_DC);
        }
    }

    @Override
    public void onBannerClicked(int pos) {
        if (pos > -1) {
//            comm.showToast("Banner clicked, position = " + (pos + 1));
        }
    }
}