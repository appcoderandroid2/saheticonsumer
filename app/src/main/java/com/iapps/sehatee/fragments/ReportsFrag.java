package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.ReportsRvAdapter;
import com.iapps.sehatee.adapters.SelectReferralDoctorRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.models.ReportModel;
import com.iapps.sehatee.models.ResponseModels.RefereeDoctorResponseModel;
import com.iapps.sehatee.models.ResponseModels.ReportsResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.iapps.sehatee.utils.APIs.getRefereeDoctorListUrl;
import static com.iapps.sehatee.utils.APIs.getReportsURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.REPORTS;
import static com.iapps.sehatee.utils.Constants.REPORT_DETAILS;
import static com.iapps.sehatee.utils.Constants.REPORT_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_ddMMMyyyy;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class ReportsFrag extends Fragment implements ReportsRvAdapter.OnItemClickedHelper {

    private static final String TAG = "ReportsFrag";
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private ReportsRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<ReportModel> list = new ArrayList<>();
    private View view;
    private Context ctx;
    private Communicator comm;
    private String fromDate = "", toDate = "", docId = "0", docName = "";
    private CardView filterCard;
    private TextView dateTv, docTv, fromToTv, docRefTv;
    private ImageView clearIv;
    private int pageNum = 1, lastPage = 1;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(REPORT_DETAILS_UPDATED)) {
                        int pos = intent.getIntExtra("pos", -1);
                        ReportModel model = (ReportModel) intent.getSerializableExtra("model");
                        if (rv != null) {
                            rv.post(() -> {
                                if (pos == -1) {
                                    pageNum = 1;
                                    lastPage = 1;
                                    getReports();
                                } else {
                                    list.set(pos, model);
                                    adapter.notifyItemChanged(pos);
                                }
                            });
                        }
                    }
                }
            }
        }
    };
    private SelectReferralDoctorRvAdapter selectReferralDoctorRvAdapter;

    public ReportsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_reports, container, false);

            initViews();

            rv.setAdapter(adapter = new ReportsRvAdapter(list, this));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getReports();
                            }
                        }
                    }
                }
            });

            clearIv.setOnClickListener(v -> comm.getDoubleBtnDialog(ctx.getString(R.string.clearFilter),
                    ctx.getString(R.string.do_you_want_to_clear_filter),
                    ctx.getString(R.string.no),
                    ctx.getString(R.string.yes),
                    new OnDialogActionHelper() {
                        @Override
                        public void onPositiveBtnClicked(Dialog d) {
                            d.dismiss();
                            filterCard.setVisibility(View.GONE);
                            fromDate = "";
                            toDate = "";
                            docId = "0";
                            docName = "";
                            fromToTv.setText(null);
                            docRefTv.setText(null);
                            pageNum = 1;
                            lastPage = 1;
                            getReports();
                        }

                        @Override
                        public void onNegativeBtnClicked(Dialog d) {
                            d.dismiss();
                        }
                    }
            ).show());

            dateTv.setOnClickListener(v -> showDateSelectDialog());
            docTv.setOnClickListener(v -> getDoctorList());

            swipeRefreshLayout.setOnRefreshListener(() -> {
                pageNum = 1;
                lastPage = 1;
                getReports();
            });

            getReports();

            LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(REPORT_DETAILS_UPDATED));
        }

        CURRENT_FRAG = REPORTS;
        comm.setActionBar(AB_BACK_WITH_NOTI);
        comm.setPageTitle(R.string.reports);
        comm.setMenuSelection(REPORTS);
        return view;
    }

    private void getDoctorList() {

        final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
        if (!comm.isActivityFinishing()) {
            dialog.show();
        }

        VolleyHelper.getInstance(ctx).addRequest(getRefereeDoctorListUrl(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
            @Override
            public void onSuccess(String response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                RefereeDoctorResponseModel responseModel = new Gson().fromJson(response, RefereeDoctorResponseModel.class);
                if (responseModel.getStatus() == 1) {
                    showDocSelectDialog(responseModel.getPayload().getDoctorList());
                } else {
                    comm.showToast(responseModel.getMsg());
                }
            }

            @Override
            public void onFailure(String error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                comm.showToast(error);
            }

            @Override
            public void onRestrictedByAdmin() {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                showUserRestrictedDialog(ReportsFrag.this);
            }
        });
    }

    private void initViews() {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        noDataLL = view.findViewById(R.id.noDataLL);
        rv = view.findViewById(R.id.rv);
        filterCard = view.findViewById(R.id.filterCard);
        dateTv = view.findViewById(R.id.dateTv);
        docTv = view.findViewById(R.id.docTv);
        fromToTv = view.findViewById(R.id.fromToTv);
        docRefTv = view.findViewById(R.id.docRefTv);
        clearIv = view.findViewById(R.id.clearFilterIv);
    }

    private void showDateSelectDialog() {
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_report_date_filter, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView fTv = v.findViewById(R.id.fromTv);
        TextView tTv = v.findViewById(R.id.toTv);

        Button posBtn = v.findViewById(R.id.posBtn);
        Button negBtn = v.findViewById(R.id.negBtn);

        if (!fromDate.isEmpty()) {
            fTv.setText(getFormattedDate(fromDate));
        }
        if (!toDate.isEmpty()) {
            tTv.setText(getFormattedDate(toDate));
        }
        posBtn.setOnClickListener(v1 -> {
            if (fTv.getText().toString().equals(ctx.getString(R.string.select_a_date)) || tTv.getText().toString().equals(ctx.getString(R.string.select_a_date))) {
                comm.showToast(getString(R.string.report_filter_date_error));
                return;
            }
            updateFilterCard();
            dialog.dismiss();
            pageNum = 1;
            lastPage = 1;
            getReports();
        });

        negBtn.setOnClickListener(v12 -> dialog.dismiss());

        fTv.setOnClickListener(v13 -> showDatePickerDialog(fTv, true));

        tTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(tTv, false);
            }
        });

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void updateFilterCard() {
        filterCard.setVisibility((fromDate.isEmpty() || toDate.isEmpty()) && docId.equals("0") ? View.GONE : View.VISIBLE);
        fromToTv.setVisibility(fromDate.isEmpty() ? View.GONE : View.VISIBLE);
        docRefTv.setVisibility(docId.equals("0") ? View.GONE : View.VISIBLE);
        fromToTv.setText(String.format(Locale.getDefault(), "%s - %s", getFormattedDate(fromDate), getFormattedDate(toDate)));
        docRefTv.setText(String.format(Locale.getDefault(), "%s - %s", ctx.getString(R.string.doctor_reference), docName));
    }

    private void showDatePickerDialog(TextView v, boolean fromOrTo) {
        Calendar cal = Calendar.getInstance();
        try {
            if (fromOrTo) {
                if (!fromDate.isEmpty()) {
                    cal.setTime(Objects.requireNonNull(sdf_yyyyMMdd.parse(fromDate)));
                }
            } else {
                if (!toDate.isEmpty()) {
                    cal.setTime(Objects.requireNonNull(sdf_yyyyMMdd.parse(toDate)));
                }
            }
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }


        DatePickerDialog dialog = new DatePickerDialog(ctx, (view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (fromOrTo) {
                fromDate = sdf_yyyyMMdd.format(calendar.getTime());
            } else {
                toDate = sdf_yyyyMMdd.format(calendar.getTime());
            }

            v.setText(sdf_ddMMMyyyy.format(calendar.getTime()));

        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

//        if (!fromOrTo) {
////            dialog.getDatePicker().setMaxDate(cal.getTimeInMillis() - 5000);
////        } else {
//            String from = fromDate;
//            Calendar tCal = Calendar.getInstance();
//            try {
//                tCal.setTime(Objects.requireNonNull(sdf_yyyyMMdd.parse(from)));
//                dialog.getDatePicker().setMinDate(tCal.getTimeInMillis() + 5000);
//            } catch (ParseException | NullPointerException e) {
//                e.printStackTrace();
//            }
//        }
//
//        dialog.getDatePicker().setMaxDate(cal.getTimeInMillis() - 5000);
        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void showDocSelectDialog(List<RefereeDoctorResponseModel.RefereeDoctor> doctorList) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_report_doctor_filter, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView noResult = v.findViewById(R.id.noResultTv);
        noResult.setVisibility(doctorList.size() > 0 ? View.GONE : View.VISIBLE);

        RecyclerView rv = v.findViewById(R.id.rv);

        for (int i = 0; i < doctorList.size(); i++) {
            doctorList.get(i).setSelected(doctorList.get(i).getId().equals(docId));
        }

        Log.e(TAG, "showDocSelectDialog: list size => " + doctorList.size());
        selectReferralDoctorRvAdapter = new SelectReferralDoctorRvAdapter(doctorList, pos -> {
            if (pos > -1) {
                for (int i = 0; i < doctorList.size(); i++) {
                    doctorList.get(i).setSelected(false);
                }
                doctorList.get(pos).setSelected(true);
                if (selectReferralDoctorRvAdapter != null) {
                    selectReferralDoctorRvAdapter.notifyDataSetChanged();
                }
                docId = doctorList.get(pos).getId();
                docName = doctorList.get(pos).getName();
            }
        });
        rv.setAdapter(selectReferralDoctorRvAdapter);
        rv.setLayoutManager(new LinearLayoutManager(ctx));

        Button posBtn = v.findViewById(R.id.posBtn);
        Button negBtn = v.findViewById(R.id.negBtn);

        posBtn.setOnClickListener(v1 -> {
            dialog.dismiss();
            pageNum = 1;
            lastPage = 1;
            getReports();
            updateFilterCard();
        });
        negBtn.setOnClickListener(v12 -> dialog.dismiss());

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void getReports() {
        if (isOnline(ctx)) {
            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getReportsURL(fromDate, toDate, docId, pageNum), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    ReportsResponseModel model = new Gson().fromJson(response, ReportsResponseModel.class);
//
                    pageNum = model.getPayload().getReportAppointmentList().getCurrentPage();
                    lastPage = model.getPayload().getReportAppointmentList().getLastPage();

                    if (model.getStatus() == 1) {
                        if (pageNum == 1) {
                            list.clear();
                        }
                        list.addAll(model.getPayload().getReportAppointmentList().getData());

                        adapter.notifyDataSetChanged();
                        pageNum++;
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(ReportsFrag.this);
                }
            });
        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {
            ReportDetailsFrag frag = new ReportDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(pos));
            bundle.putInt("pos", pos);
            frag.setArguments(bundle);
            comm.openFragment(frag, REPORT_DETAILS);
        }
    }
}