package com.iapps.sehatee.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.chrisbanes.photoview.PhotoView;
import com.iapps.sehatee.R;
import com.iapps.sehatee.models.DocumentModel;
import com.squareup.picasso.Picasso;

import static com.iapps.sehatee.utils.Constants.NA;

public class PhotoViewFrag extends Fragment {

    private DocumentModel model = new DocumentModel();

    public PhotoViewFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (DocumentModel) getArguments().getSerializable("model");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PhotoView pv = view.findViewById(R.id.photoView);
        Picasso.get().load(model.getUrl().isEmpty() ? NA : model.getUrl()).error(R.drawable.no_image_found).into(pv);

    }
}