package com.iapps.sehatee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.interfaces.Communicator;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI_NO_BOTTOM_NAV;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.MY_FAVOURITES;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.TEST;

public class FavouritesFrag extends Fragment {
    private View view;
    private Context ctx;
    private Communicator comm;
    private ViewPager vp;
    private VpAdapter vpAdapter;
    private TabLayout tabLayout;

    public FavouritesFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_my_fav, container, false);

            vp = view.findViewById(R.id.vp);
            tabLayout = view.findViewById(R.id.tabLayout);

            vpAdapter = new VpAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            vp.setAdapter(vpAdapter);
            tabLayout.setupWithViewPager(vp);
        }

        vp.removeAllViews();
        vpAdapter.clearFrags();

        FavPackagesFrag offerPackagesFrag = new FavPackagesFrag();
        FavTestsFrag offerTestsFrag = new FavTestsFrag();
        Bundle bundle = new Bundle();
        bundle.putInt("tabIndex", 0);
        offerPackagesFrag.setArguments(bundle);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("tabIndex", 1);
        offerTestsFrag.setArguments(bundle1);

        vpAdapter.addFrag(offerPackagesFrag, PACKAGE);
        vpAdapter.addFrag(offerTestsFrag, TEST);
        vpAdapter.notifyDataSetChanged();

        CURRENT_FRAG = MY_FAVOURITES;
        comm.setActionBar(AB_BACK_WITH_NOTI_NO_BOTTOM_NAV);
        comm.setPageTitle(R.string.my_fav);
        comm.setMenuSelection(MY_FAVOURITES);
        return view;
    }

    public int getCurrentTabIndex() {
        return vp.getCurrentItem();
    }
}