package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.HelpRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.HelpModel;
import com.iapps.sehatee.models.ResponseModels.HelpResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getHelpURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.HELP;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.helperClasses.VolleyHelper.GET;

public class HelpFrag extends Fragment implements HelpRvAdapter.OnItemClickedHelper {

    private static final String TAG = "HelpFrag";
    private RecyclerView rv;
    private HelpRvAdapter adapter;
    private List<HelpModel> list = new ArrayList<>();
    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;

    public HelpFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_help, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);
            rv.setAdapter(adapter = new HelpRvAdapter(list, this));
            rv.setLayoutManager(new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(this::getHelp);

            getHelp();

        }

        CURRENT_FRAG = HELP;
        comm.setActionBar(AB_BACK_WITH_NOTI);
        comm.setPageTitle(R.string.help);
        comm.setMenuSelection(HELP);
        return view;
    }

    private void getHelp() {
        if (isOnline(ctx)) {
            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getHelpURL(), GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    HelpResponseModel model = new Gson().fromJson(response, HelpResponseModel.class);
                    if (model.getStatus() == 1) {
                        list.clear();
                        list.addAll(model.getPayload().getHelpList());

                        adapter.notifyDataSetChanged();
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(HelpFrag.this);
                }
            });

        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }


    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {
            list.get(pos).setExpanded(!list.get(pos).isExpanded());
            adapter.notifyItemChanged(pos);
        }
    }
}