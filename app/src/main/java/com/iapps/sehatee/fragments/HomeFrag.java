package com.iapps.sehatee.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.DoctorCityAndSpecialitySelect;
import com.iapps.sehatee.activities.NurseCityAndSpecialtySelect;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ResponseModels.HomeBannerResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.iapps.sehatee.utils.APIs.getHomeBannerURL;
import static com.iapps.sehatee.utils.Constants.AB_MENU_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.CUSTOMER_SERVICE;
import static com.iapps.sehatee.utils.Constants.HOME;
import static com.iapps.sehatee.utils.Constants.HOME_DC;
import static com.iapps.sehatee.utils.Constants.SEARCH_DOCTOR;
import static com.iapps.sehatee.utils.Constants.SEARCH_NURSE;
import static com.iapps.sehatee.utils.Constants.UPDATE_NOTIFICATION_COUNT;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;

public class HomeFrag extends Fragment implements View.OnClickListener {

    private static final String TAG = "HomeFrag";
    private TextView docTv, nurseTv, dcTv, cusServiceTv;
    private int RC_DOCTOR_FILTERS = 1111;
    private int RC_NURSE_FILTERS = 2222;
    private View view;
    private Context ctx;
    private Communicator comm;
    private ViewPager vp;
    private VpAdapter adapter;
    private List<HomeBannerResponseModel.BannerModel> bannerList = new ArrayList<>();
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            vp.post(() -> {
                int index = vp.getCurrentItem() + 1;
                if (index == bannerList.size()) {
                    index = 0;
                }
                vp.setCurrentItem(index, index != 0);
                handler.postDelayed(runnable, 4000);
            });
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals("DOCTOR")) {
                        if (docTv != null) {
                            docTv.performClick();
                        }
                    }
                    if (intent.getAction().equals("NURSE")) {
                        if (docTv != null) {
                            nurseTv.performClick();
                        }
                    }
                    if (intent.getAction().equals("DC")) {
                        if (docTv != null) {
                            dcTv.performClick();
                        }
                    }
                }
            }
        }
    };

    public HomeFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 4000);
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_home, container, false);

            initViews();
            initClicks();

            vp.setAdapter(adapter = new VpAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT));

            getHomeBanner();

            boolean isLTR = !getResources().getBoolean(R.bool.isRTL);

            // Note: The X and Y axis does not change with layout direction.
            // isLTR from resource is telling the actual language direction
            // So the animation will work properly if we consider the language direction with sliding direction

            slideCards(docTv, 100, isLTR /*for LTR here the value should be true*/);
            slideCards(dcTv, 150, isLTR /*for LTR here the value should be true*/);
            slideCards(nurseTv, 100, !isLTR /*for LTR here the value should be false*/);
            slideCards(cusServiceTv, 150, !isLTR /*for LTR here the value should be false*/);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("DOCTOR");
            intentFilter.addAction("NURSE");
            intentFilter.addAction("DC");
            LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, intentFilter);
            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(UPDATE_NOTIFICATION_COUNT));
        }

        CURRENT_FRAG = HOME;
        comm.setActionBar(AB_MENU_WITH_NOTI);
        comm.setPageTitle(R.string.welcome_to_sahety);
        comm.setMenuSelection(HOME);
        return view;
    }

    private void slideCards(View v, long delay, boolean isLeftToRight) {
        v.post(new Runnable() {
            @Override
            public void run() {
                v.setEnabled(false);
                float to = v.getX();
                float from = isLeftToRight ? to - 400 : to + v.getRight() + 400;
                Log.e(TAG, "run: Is RTL => " + getResources().getBoolean(R.bool.isRTL) + ", to => " + to);

                ValueAnimator animator = ValueAnimator.ofFloat(from, to);
                animator.setStartDelay(delay);
                animator.setDuration(500);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.addUpdateListener(animation -> {
                    float val = (float) animation.getAnimatedValue();
                    v.setX(val);
                    v.setAlpha(to / val);
                });
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setEnabled(true);
                    }
                });
                animator.start();
            }
        });

    }

    private void initViews() {
        vp = view.findViewById(R.id.vp);
        docTv = view.findViewById(R.id.docTv);
        nurseTv = view.findViewById(R.id.nursesTv);
        dcTv = view.findViewById(R.id.dcTv);
        cusServiceTv = view.findViewById(R.id.customerServiceTv);
    }

    private void initClicks() {
        docTv.setOnClickListener(this);
        nurseTv.setOnClickListener(this);
        dcTv.setOnClickListener(this);
        cusServiceTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        v.animate()
                .scaleX(0.95f)
                .scaleY(0.95f)
                .setInterpolator(new OvershootInterpolator())
                .setDuration(150)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.animate()
                                .scaleX(1f)
                                .scaleY(1f)
                                .setInterpolator(new OvershootInterpolator())
                                .setDuration(150)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        switch (v.getId()) {
                                            case R.id.docTv:
                                                startActivityForResult(new Intent(ctx, DoctorCityAndSpecialitySelect.class), RC_DOCTOR_FILTERS);
                                                break;
                                            case R.id.nursesTv:
                                                startActivityForResult(new Intent(ctx, NurseCityAndSpecialtySelect.class), RC_NURSE_FILTERS);
                                                break;
                                            case R.id.dcTv:
                                                comm.openFragment(new DcHomeFrag(), HOME_DC);
                                                break;
                                            case R.id.customerServiceTv:
                                                comm.openFragment(new CustomerServiceFrag(), CUSTOMER_SERVICE);
                                                break;
                                        }
                                    }
                                })
                                .start();
                    }
                })
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_DOCTOR_FILTERS && resultCode == RESULT_OK && data != null) {
            SearchDoctorFrag frag = new SearchDoctorFrag();
            Bundle bundle = new Bundle();
            bundle.putString("cityName", data.getStringExtra("cityName"));
            bundle.putString("areaName", data.getStringExtra("areaName"));
            bundle.putString("specialty", data.getStringExtra("specialty"));
            bundle.putString("entity", data.getStringExtra("entity"));
            Log.e(TAG, "onActivityResult: " + bundle.toString());
            frag.setArguments(bundle);
            comm.openFragment(frag, SEARCH_DOCTOR);
        }

        if (requestCode == RC_NURSE_FILTERS && resultCode == RESULT_OK && data != null) {
            SearchNurseFrag frag = new SearchNurseFrag();
            Bundle bundle = new Bundle();
            bundle.putString("cityName", data.getStringExtra("cityName"));
            bundle.putString("areaName", data.getStringExtra("areaName"));
            bundle.putString("specialty", data.getStringExtra("specialty"));
            bundle.putString("entity", data.getStringExtra("entity"));
            Log.e(TAG, "onActivityResult: " + bundle.toString());
            frag.setArguments(bundle);
            comm.openFragment(frag, SEARCH_NURSE);
        }


    }

    private void getHomeBanner() {

        final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
        if (!comm.isActivityFinishing()) {
            dialog.show();
        }

        VolleyHelper.getInstance(ctx)
                .addRequest(getHomeBannerURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        HomeBannerResponseModel model = new Gson().fromJson(response, HomeBannerResponseModel.class);
                        if (model.getStatus() == 1) {
                            bannerList.clear();
                            bannerList.addAll(model.getPayload().getData());
                            adapter = new VpAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
                            for (int i = 0; i < bannerList.size(); i++) {
                                BannerFrag frag = new BannerFrag();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("model", bannerList.get(i));
                                frag.setArguments(bundle);
                                adapter.addFrag(frag, "");
                            }
                            vp.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
//                        showUserRestrictedDialog(HomeFrag.this);
                    }
                });
    }


}