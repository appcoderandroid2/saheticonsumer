package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import static com.iapps.sehatee.utils.APIs.getChangePasswordURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CHANGE_PASSWORD;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class ChangePasswordFrag extends Fragment {

    private static final String TAG = "ChangePasswordFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private EditText passEt, newPassEt, conNewPassEt;
    private Button changePassBtn;

    public ChangePasswordFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_change_password, container, false);

            passEt = view.findViewById(R.id.passEt);
            newPassEt = view.findViewById(R.id.newPassEt);
            conNewPassEt = view.findViewById(R.id.conNewPassEt);
            changePassBtn = view.findViewById(R.id.changePassBtn);

            changePassBtn.setOnClickListener(v -> {
                v.setEnabled(false);
                String currPass = passEt.getText().toString().trim();
                String newPass = newPassEt.getText().toString().trim();
                String conNewPass = conNewPassEt.getText().toString().trim();
                if (validate(currPass, newPass, conNewPass)) {
                    changePassword(v, currPass, newPass);
                } else {
                    v.setEnabled(true);
                }
            });
        }

        CURRENT_FRAG = CHANGE_PASSWORD;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.change_password);
        comm.setMenuSelection(CHANGE_PASSWORD);
        return view;
    }

    private boolean validate(String currPass, String newPass, String conNewPass) {

        if (currPass.isEmpty()) {
            showToast(ctx, R.string.error_empty_password);
            passEt.requestFocus();
            return false;
        }
        if (newPass.isEmpty()) {
            showToast(ctx, R.string.error_empty_password);
            newPassEt.requestFocus();
            return false;
        }
        if (conNewPass.isEmpty()) {
            showToast(ctx, R.string.error_empty_password);
            conNewPassEt.requestFocus();
            return false;
        }
        if (currPass.length() < 6) {
            showToast(ctx, R.string.error_password_too_short);
            passEt.requestFocus();
            passEt.setSelection(currPass.length());
            return false;
        }
        if (newPass.length() < 6) {
            showToast(ctx, R.string.error_password_too_short);
            newPassEt.requestFocus();
            newPassEt.setSelection(currPass.length());
            return false;
        }
        if (conNewPass.length() < 6) {
            showToast(ctx, R.string.error_password_too_short);
            conNewPassEt.requestFocus();
            conNewPassEt.setSelection(currPass.length());
            return false;
        }
        if (!newPass.equals(conNewPass)) {
            showToast(ctx, R.string.error_passwords_didnt_match);
            newPassEt.setText(null);
            conNewPassEt.setText(null);
            newPassEt.requestFocus();
            return false;
        }

        return true;
    }

    private void changePassword(View v, String currPass, String newPass) {

        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("currentPassword", currPass);
                jsonObject.put("newPassword", newPass);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getChangePasswordURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            if (!comm.isActivityFinishing()) {
                                comm.getSingleBtnDialog(ctx.getString(R.string.alert), ctx.getString(R.string.password_change_note_for_dialog), ctx.getString(R.string.logim_again), d -> {
                                    d.dismiss();
                                    comm.logout();
                                }).show();
                            }
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(ChangePasswordFrag.this);
                    }
                });

            } catch (JSONException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "changePassword: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }
}