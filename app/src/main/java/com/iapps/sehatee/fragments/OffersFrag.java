package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.BannerVpAdapter;
import com.iapps.sehatee.adapters.DcCategoryHomeRvAdapter;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.BannerModel;
import com.iapps.sehatee.models.CategoryModel;
import com.iapps.sehatee.models.ResponseModels.BannerResponseModel;
import com.iapps.sehatee.models.ResponseModels.DcPackTestCategoriesResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getCategoriesURL;
import static com.iapps.sehatee.utils.APIs.getOfferBannerURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_CATEGORIES;
import static com.iapps.sehatee.utils.Constants.BANNER_HEIGHT;
import static com.iapps.sehatee.utils.Constants.BANNER_WIDTH;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.OFFERS;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.TEST;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class OffersFrag extends Fragment implements DcCategoryHomeRvAdapter.OnItemClickedHelper, BannerVpAdapter.OnBannerClickedHelper {

    private static final String TAG = "OffersFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private RecyclerView catRv;
    private DcCategoryHomeRvAdapter adapter;
    private List<CategoryModel> list = new ArrayList<>();
    private TextView viewAllCatTv;
    private ViewPager vp;
    private TabLayout tabLayout;
    private VpAdapter vpAdapter;
    private LinearLayout noDataLL;
    private ViewPager bannerVp;
    private BannerVpAdapter bannerVpAdapter;
    private List<BannerModel> bannerList = new ArrayList<>();
    private Handler bannerHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            int nextIndex = bannerVp.getCurrentItem() + 1;
            if (nextIndex == bannerList.size()) {
                nextIndex = 0;
            }
            bannerVp.setCurrentItem(nextIndex, nextIndex != 0);
            bannerHandler.postDelayed(this, 4000);
        }
    };

    public OffersFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        bannerHandler.removeCallbacks(runnable);
        bannerHandler.postDelayed(runnable, 4000);
    }

    @Override
    public void onPause() {
        bannerHandler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_offers, container, false);

            viewAllCatTv = view.findViewById(R.id.viewAllCatTv);
            vp = view.findViewById(R.id.vp);
            tabLayout = view.findViewById(R.id.tabLayout);
            noDataLL = view.findViewById(R.id.noDataLL);
            bannerVp = view.findViewById(R.id.bannerVp);

            // sizing banner vp to equivalent resolution to 640x296
            bannerVp.post(() -> bannerVp.getLayoutParams().height = (BANNER_HEIGHT * bannerVp.getWidth() / BANNER_WIDTH));

            bannerVp.setAdapter(bannerVpAdapter = new BannerVpAdapter(bannerList, this));

            vpAdapter = new VpAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            vp.setAdapter(vpAdapter);
            tabLayout.setupWithViewPager(vp);

            catRv = view.findViewById(R.id.catRv);

            catRv.setAdapter(adapter = new DcCategoryHomeRvAdapter(list, this));
            catRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            getBanners();
            getCategories();

            viewAllCatTv.setOnClickListener(v -> {
                DcAllCategories frag = new DcAllCategories();
                Bundle bundle = new Bundle();
                // these are required because the same fragment is being used from Dc Home Screen
                bundle.putString("categories", "");
                bundle.putString("cityName", getPrefs().getSelectedCityName());
                bundle.putString("areaName", getPrefs().getSelectedCityAreaName());
                bundle.putInt("tabIndex", 0);
                // ========================================================================
                bundle.putString("from", OFFERS);
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_CATEGORIES);
            });

        }

        vp.removeAllViews();
        vpAdapter.clearFrags();

        OfferPackagesFrag offerPackagesFrag = new OfferPackagesFrag();
        OfferTestsFrag offerTestsFrag = new OfferTestsFrag();
        Bundle bundle = new Bundle();
        bundle.putString("catId", "0");
        bundle.putString("from", OFFERS);

        offerPackagesFrag.setArguments(bundle);
        offerTestsFrag.setArguments(bundle);

        vpAdapter.addFrag(offerPackagesFrag, PACKAGE);
        vpAdapter.addFrag(offerTestsFrag, TEST);
        vpAdapter.notifyDataSetChanged();


        CURRENT_FRAG = OFFERS;
        comm.setActionBar(AB_BACK_WITH_NOTI);
        comm.setPageTitle(R.string.offers);
        comm.setMenuSelection(OFFERS);
        return view;
    }

    private void getBanners() {
        if (isOnline(ctx)) {

            VolleyHelper.getInstance(ctx).addRequest(getOfferBannerURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    BannerResponseModel model = new Gson().fromJson(response, BannerResponseModel.class);
                    if (model.getStatus() == 1) {
                        if (model.getPayload().getData().size() > 0) {
                            bannerList.clear();
                            bannerList.addAll(model.getPayload().getData());
                            bannerVpAdapter = new BannerVpAdapter(bannerList, OffersFrag.this);
                            bannerVp.setAdapter(bannerVpAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(String error) {

                }

                @Override
                public void onRestrictedByAdmin() {

                }
            });
        }
    }

    @Override
    public void onCategoryItemClicked(int pos) {
        if (pos > -1) {
            OfferPackagesTestFrag frag = new OfferPackagesTestFrag();
            Bundle bundle = new Bundle();
            bundle.putString("catId", list.get(pos).getId());
            bundle.putString("catName", list.get(pos).getCategoryName());
            frag.setArguments(bundle);
            comm.openFragment(frag, OFFERS);
        }
    }

    private void getCategories() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!comm.isActivityFinishing()) {
                dialog.show();
            }

            VolleyHelper.getInstance(ctx).addRequest(getCategoriesURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    DcPackTestCategoriesResponseModel model = new Gson().fromJson(response, DcPackTestCategoriesResponseModel.class);
                    if (model.getStatus() == 1) {
                        list.clear();
                        list.addAll(model.getPayload().getCategories());

                        adapter.notifyDataSetChanged();
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(OffersFrag.this);
                }
            });

        } else {
            updateUI();
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    public int getCurrentTabIndex() {
        return vp.getCurrentItem();
    }

    @Override
    public void onBannerClicked(int pos) {
        if (pos > -1) {
//            comm.showToast("Banner clicked, position = " + (pos + 1));
        }
    }
}