package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ResponseModels.AboutUsResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import static com.iapps.sehatee.utils.APIs.getAboutUsURL;
import static com.iapps.sehatee.utils.Constants.ABOUT_US;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class AboutUsFrag extends Fragment {

    private View view;
    private Context ctx;
    private Communicator comm;

    public AboutUsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        CURRENT_FRAG = ABOUT_US;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.about_us);
        comm.setMenuSelection(ABOUT_US);
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
        if (!comm.isActivityFinishing()) {
            dialog.show();
        }

        TextView tv = view.findViewById(R.id.tv);
        VolleyHelper.getInstance(ctx).addRequest(getAboutUsURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
            @Override
            public void onSuccess(String response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                AboutUsResponseModel model = new Gson().fromJson(response, AboutUsResponseModel.class);
                if (model.getStatus() == 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tv.setText(Html.fromHtml(model.getPayload().getData(), Html.FROM_HTML_MODE_LEGACY).toString());
                    } else {
                        tv.setText(Html.fromHtml(model.getPayload().getData()).toString());
                    }
                } else {
                    comm.showToast(model.getMsg());
                }
            }

            @Override
            public void onFailure(String error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                comm.showToast(error);
            }

            @Override
            public void onRestrictedByAdmin() {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                showUserRestrictedDialog(AboutUsFrag.this);
            }
        });
    }
}