package com.iapps.sehatee.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.DcCategoriesFilter;
import com.iapps.sehatee.activities.Search;
import com.iapps.sehatee.activities.SelectCity;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.interfaces.Communicator;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DC_QUERY_UPDATED;
import static com.iapps.sehatee.utils.Constants.HOME_DC;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.PACKAGE_DETAILS;
import static com.iapps.sehatee.utils.Constants.SEARCH_DC;
import static com.iapps.sehatee.utils.Constants.SEARCH_PACKAGES;
import static com.iapps.sehatee.utils.Constants.SEARCH_TESTS;
import static com.iapps.sehatee.utils.Constants.TEST;
import static com.iapps.sehatee.utils.Constants.TEST_DETAILS;
import static com.iapps.sehatee.utils.Constants.getPrefs;

public class DcSearchFrag extends Fragment implements View.OnClickListener {

    private static final String TAG = "SearchDcFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private ViewPager vp;
    private EditText searchEt;
    private TextView locationTv, searchTv, filterTv;
    private String categories = "";
    private int RC_CATEGORY_FILTER = 1001;
    private int RC_SEARCH = 2002;
    private String cityName = "All";
    private String areaName = "All";
    private String id = "";
    private int tabIndex = 0;
    private int RC_CHANGE_LOCATION = 3003;

    public DcSearchFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityName = getArguments().getString("cityName");
            areaName = getArguments().getString("areaName");
            categories = getArguments() != null ? getArguments().getString("categories") : "";
            tabIndex = getArguments() != null ? getArguments().getInt("tabIndex", 0) : 0;
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_search_dc, container, false);

            locationTv = view.findViewById(R.id.changeLocationTv);
            searchTv = view.findViewById(R.id.searchTv);
            filterTv = view.findViewById(R.id.filterTv);
            searchEt = view.findViewById(R.id.searchEt);

            vp = view.findViewById(R.id.vp);
            TabLayout tabLayout = view.findViewById(R.id.tabLayout);
            tabLayout.setupWithViewPager(vp);

            locationTv.setOnClickListener(this);
            searchTv.setOnClickListener(this);
            filterTv.setOnClickListener(this);

            cityName = getPrefs().getSelectedCityName();
            areaName = getPrefs().getSelectedCityAreaName();

            locationTv.setText(String.format(Locale.getDefault(), "%s, %s", cityName, areaName));

            vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    tabIndex = position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            searchEt.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    try {
                        LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(DC_QUERY_UPDATED)
                                .putExtra("categories", categories)
                                .putExtra("cityName", cityName)
                                .putExtra("areaName", areaName)
                                .putExtra("searchKey", URLEncoder.encode(searchEt.getText().toString().trim(), "utf-8"))
                                .putExtra("tabIndex", tabIndex == 0 ? PACKAGE : TEST));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        comm.showToast(R.string.something_went_wrong);
                    }
                    return true;
                }
                return false;
            });

            searchEt.setOnTouchListener((v, event) -> {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (searchEt.getRight() - searchEt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        try {
                            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(DC_QUERY_UPDATED)
                                    .putExtra("categories", categories)
                                    .putExtra("cityName", cityName)
                                    .putExtra("areaName", areaName)
                                    .putExtra("searchKey", URLEncoder.encode(searchEt.getText().toString().trim(), "utf-8"))
                                    .putExtra("tabIndex", tabIndex == 0 ? PACKAGE : TEST));

                            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            if (imm != null) {
                                imm.hideSoftInputFromWindow(searchEt.getWindowToken(), 0);
                            }

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            comm.showToast(R.string.something_went_wrong);
                        }
                        return true;
                    }
                }
                return false;
            });


        }

        VpAdapter adapter = new VpAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        Bundle bundle = new Bundle();
        bundle.putString("categories", categories);
        bundle.putString("cityName", cityName);
        bundle.putString("areaName", areaName);
        DcPackagesListFrag dcPackagesListFrag = new DcPackagesListFrag();
        dcPackagesListFrag.setArguments(bundle);
        DcTestsListFrag dcTestsListFrag = new DcTestsListFrag();
        dcTestsListFrag.setArguments(bundle);

        adapter.addFrag(dcPackagesListFrag, ctx.getString(R.string.packages));
        adapter.addFrag(dcTestsListFrag, ctx.getString(R.string.tests));

        vp.setAdapter(adapter);

        vp.setCurrentItem(tabIndex);

        CURRENT_FRAG = SEARCH_DC;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.find_packages_or_tests);
        return view;
    }

    public int getTabIndex() {
        return vp.getCurrentItem();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filterTv:
                startActivityForResult(new Intent(ctx, DcCategoriesFilter.class).putExtra("categories", categories), RC_CATEGORY_FILTER);
                break;
            case R.id.searchTv:
                startActivityForResult(new Intent(ctx, Search.class)
                                .putExtra("from", tabIndex == 0 ? SEARCH_PACKAGES : SEARCH_TESTS)
                        , RC_SEARCH);
                break;
            case R.id.changeLocationTv:
                startActivityForResult(new Intent(ctx, SelectCity.class).putExtra("from", HOME_DC).putExtra("cityName", cityName).putExtra("areaName", areaName), RC_CHANGE_LOCATION);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_CATEGORY_FILTER && resultCode == RESULT_OK && data != null) {
            categories = data.getStringExtra("categories");
            Log.e(TAG, "onActivityResult: Categories selected => " + categories);

            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(DC_QUERY_UPDATED)
                    .putExtra("categories", categories)
                    .putExtra("cityName", cityName)
                    .putExtra("areaName", areaName)
                    .putExtra("searchKey", searchEt.getText().toString().trim())
                    .putExtra("tabIndex", tabIndex == 0 ? PACKAGE : TEST)
            );
        }

        if (requestCode == RC_CHANGE_LOCATION && resultCode == RESULT_OK && data != null) {
            cityName = data.getStringExtra("cityName");
            areaName = data.getStringExtra("areaName");
            Log.e(TAG, "onActivityResult: " + "City = " + cityName + ", Area = " + areaName);

            locationTv.setText(String.format(Locale.getDefault(), "%s, %s", cityName, areaName));

            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(DC_QUERY_UPDATED)
                    .putExtra("categories", categories)
                    .putExtra("cityName", cityName)
                    .putExtra("areaName", areaName)
                    .putExtra("searchKey", searchEt.getText().toString().trim())
                    .putExtra("tabIndex", tabIndex == 0 ? PACKAGE : TEST)
            );
        }

        if (requestCode == RC_SEARCH && resultCode == RESULT_OK && data != null) {

            id = data.getStringExtra("id");
            if (data.getStringExtra("from").equals(SEARCH_PACKAGES)) {
                comm.openFragment(new DcPackageDetailsFrag(), PACKAGE_DETAILS);
            } else {
                comm.openFragment(new DcTestDetailsFrag(), TEST_DETAILS);
            }
        }
    }
}