package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.SearchTestsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnCompleteHelper;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.models.ResponseModels.TestResponseModel;
import com.iapps.sehatee.models.TestsModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getFavouriteListURL;
import static com.iapps.sehatee.utils.Constants.TEST;
import static com.iapps.sehatee.utils.Constants.TEST_DETAILS;
import static com.iapps.sehatee.utils.Constants.addToFavourite;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class FavTestsFrag extends Fragment implements SearchTestsRvAdapter.OnItemClickedHelper {

    private View view;
    private Context ctx;
    private Communicator comm;
    private int tabIndex = 0;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private SearchTestsRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<TestsModel> list = new ArrayList<>();
    private FavouritesFrag parentFrag = new FavouritesFrag();
    private int pageNum = 1, lastPage = 1;

    public FavTestsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
        parentFrag = (FavouritesFrag) getParentFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tabIndex = getArguments().getInt("tabIndex", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_fav_tests, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new SearchTestsRvAdapter(list, this));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(this::getFavTests);

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getFavTests();
                            }
                        }
                    }
                }
            });
            getFavTests();

        }
        return view;
    }

    private void getFavTests() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getFavouriteListURL(TEST), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    TestResponseModel model = new Gson().fromJson(response, TestResponseModel.class);

                    pageNum = model.getPayload().getFavouriteList().getCurrentPage();
                    lastPage = model.getPayload().getFavouriteList().getLastPage();

                    if (model.getStatus() == 1) {

                        if (pageNum == 1) {
                            list.clear();
                        }

                        list.addAll(model.getPayload().getFavouriteList().getData());
                        adapter.notifyDataSetChanged();
                        pageNum++;
                    } else {
                        showToast(model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(FavTestsFrag.this);
                }
            });
        } else {
            swipeRefreshLayout.setRefreshing(false);
            updateUI();
            showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    private void showToast(int msg) {
        if (parentFrag.getCurrentTabIndex() == tabIndex) {
            comm.showToast(msg);
        }
    }

    private void showToast(String msg) {
        if (parentFrag.getCurrentTabIndex() == tabIndex) {
            comm.showToast(msg);
        }
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {
            DcTestDetailsFrag frag = new DcTestDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(pos));
            bundle.putInt("pos", pos);
            frag.setArguments(bundle);
            comm.openFragment(frag, TEST_DETAILS);
        }
    }

    @Override
    public void onFavClicked(int pos) {
        if (pos > -1) {
            if (!comm.isActivityFinishing()) {
                comm.getDoubleBtnDialog(
                        ctx.getString(R.string.alert),
                        ctx.getString(R.string.make_it_not_fav),
                        ctx.getString(R.string.no),
                        ctx.getString(R.string.yes),
                        new OnDialogActionHelper() {
                            @Override
                            public void onPositiveBtnClicked(Dialog d) {
                                d.dismiss();
                                addToFavourite(FavTestsFrag.this, comm.isActivityFinishing(), list.get(pos).getId(), TEST, new OnCompleteHelper() {
                                    @Override
                                    public void onSuccess() {
                                        adapter.notifyItemRemoved(pos);
                                        list.remove(pos);
                                        updateUI();
                                        showToast(getString(R.string.successfully_removed));
                                    }

                                    @Override
                                    public void onFailure(String error) {
                                        showToast(error);
                                    }
                                });
                            }

                            @Override
                            public void onNegativeBtnClicked(Dialog d) {
                                d.dismiss();
                            }
                        }
                ).show();
            }
        }
    }
}