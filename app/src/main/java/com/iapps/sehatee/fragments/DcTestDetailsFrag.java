package com.iapps.sehatee.fragments;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnCompleteHelper;
import com.iapps.sehatee.models.TestsModel;

import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.SETUP_APPO_DETAILS;
import static com.iapps.sehatee.utils.Constants.TEST;
import static com.iapps.sehatee.utils.Constants.TEST_DETAILS;
import static com.iapps.sehatee.utils.Constants.addToFavourite;
import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;

public class DcTestDetailsFrag extends Fragment {

    private View view;
    private Context ctx;
    private Communicator comm;
    private Button proceedBtn;
    private TestsModel model = new TestsModel();
    private ImageView favIv;
    private TextView nameTv, originalPriceTv, offerPriceTv, productConstituentsTv, categoriesTv, reportAvailabilityTv, moreInfoTv, prerequisiteTv, dcNameTv, dcAddressTv, reviewsTv;
    private LinearLayout ratingLL;
    private RatingBar ratingBar;

    public DcTestDetailsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (TestsModel) getArguments().getSerializable("model");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_test_details, container, false);

            initViews(view);

            proceedBtn.setOnClickListener(v -> {
                DcAppointmentDetailsSetupFrag frag = new DcAppointmentDetailsSetupFrag();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", model);
                bundle.putString("from", TEST);
                frag.setArguments(bundle);
                comm.openFragment(frag, SETUP_APPO_DETAILS);
            });

            favIv.setOnClickListener((v) -> addToFavourite(DcTestDetailsFrag.this, comm.isActivityFinishing(), model.getId(), TEST, new OnCompleteHelper() {
                @Override
                public void onSuccess() {
                    model.setFav(model.isFav() ? 0 : 1);
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    comm.showToast(error);
                }
            }));

            updateUI();

            ratingLL.setOnClickListener(v -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", TEST);
                bundle.putString("itemId", model.getId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });
        }
        CURRENT_FRAG = TEST_DETAILS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.test_details);
        return view;
    }

    private void initViews(View view) {
        nameTv = view.findViewById(R.id.nameTv);
        originalPriceTv = view.findViewById(R.id.priceTv);
        offerPriceTv = view.findViewById(R.id.offerPriceTv);
        favIv = view.findViewById(R.id.favIv);
        proceedBtn = view.findViewById(R.id.proceedBtn);
        productConstituentsTv = view.findViewById(R.id.productConstituentsTv);
        prerequisiteTv = view.findViewById(R.id.prerequisiteTv);
        categoriesTv = view.findViewById(R.id.categoriesTv);
        reportAvailabilityTv = view.findViewById(R.id.reportAvailabilityTv);
        moreInfoTv = view.findViewById(R.id.moreInfoTv);
        dcNameTv = view.findViewById(R.id.dcNameTv);
        dcAddressTv = view.findViewById(R.id.dcAddressTv);
        ratingLL = view.findViewById(R.id.ratingLL);
        ratingBar = view.findViewById(R.id.ratingBar);
        reviewsTv = view.findViewById(R.id.reviewsTv);
    }

    private void updateUI() {

        nameTv.setText(model.getTestName());
        productConstituentsTv.setText(model.getProductConstituents());
        prerequisiteTv.setText(model.getPrerequisite());
        categoriesTv.setText(model.getCategory().isEmpty() ? NA : model.getCategory());
        reportAvailabilityTv.setText(model.getReportAvailability());
        moreInfoTv.setText(model.getMoreInfo());
        ratingBar.setRating(model.getRating());
        reviewsTv.setText(String.format(Locale.getDefault(), "%s %s", model.getTotalReview(), ctx.getString(R.string.reviews)));

        dcNameTv.setText(String.format(Locale.getDefault(), "%s - %s", ctx.getString(R.string.offered_by), model.getDcInfo().getName()));

        String address = model.getDcInfo().getAddress().equals(NA) ? "" : model.getDcInfo().getAddress();
        address += model.getDcInfo().getArea().equals(NA) ? "" : ", " + model.getDcInfo().getArea();
        address += model.getDcInfo().getCity().equals(NA) ? "" : ", " + model.getDcInfo().getCity();
        address += model.getDcInfo().getPostalCode().equals(NA) ? "" : ", " + model.getDcInfo().getPostalCode();
//        address += user.getCountryName().equals(NA) ? "" : ", " + user.getCountryName();
        address = address.isEmpty() ? NA : address;

        dcAddressTv.setText(address);

        originalPriceTv.setText(String.format(Locale.getDefault(), "%s%s", model.getCurrencySymbol(), model.getPrice()));

        offerPriceTv.setText(String.format(Locale.getDefault(), "%s%s", model.getCurrencySymbol(),
                (getDiscountedPrice(Integer.parseInt(model.getPrice()),
                        Integer.parseInt(model.getDiscount()),
                        model.getDiscountType()))));

        if (Integer.parseInt(model.getDiscount()) > 0) {
            offerPriceTv.setVisibility(View.VISIBLE);
            originalPriceTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.metaTextColor, null));
            originalPriceTv.setPaintFlags(originalPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {

            originalPriceTv.setPaintFlags(originalPriceTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            offerPriceTv.setVisibility(View.GONE);
            originalPriceTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.titleTextColor, null));
        }

        favIv.setImageResource(model.isFav() ? R.drawable.ic_round_favorite_24 : R.drawable.ic_round_favorite_border_24);
    }
}