package com.iapps.sehatee.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.SelectAppointmentToShareReport;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.PackageModel;
import com.iapps.sehatee.models.TestsModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.SCHEDULE_APPOINTMENT;
import static com.iapps.sehatee.utils.Constants.SETUP_APPO_DETAILS;
import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;

public class DcAppointmentDetailsSetupFrag extends Fragment {

    private static final String TAG = "SetupDcAppointmentDetai";
    private EditText nameEt, phEt;
    private RadioGroup rg;
    private Button addDocRefBtn, proceedBtn;
    private String from = "", selectedDate = "", age = "";
    private TextView dobTv, nameTv, priceTv;
    private LinearLayout docRefLL;
    private ImageView docIv, closeIv;
    private TextView docNameTv, dateTimeTv, appointmentIdTv;
    private View view;
    private Context ctx;
    private Communicator comm;
    private PackageModel packageModel = new PackageModel();
    private TestsModel testsModel = new TestsModel();
    private String appointmentId = "0";
    private int RC_ADD_DOC_REF = 100;

    public DcAppointmentDetailsSetupFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            from = getArguments().getString("from");
            if (from.equals(PACKAGE)) {
                packageModel = (PackageModel) getArguments().getSerializable("model");
            } else {
                testsModel = (TestsModel) getArguments().getSerializable("model");
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_setup_dc_appointment_details, container, false);

            initViews();

            dobTv.setOnClickListener(v -> showDatePicker((TextView) v));

            closeIv.setOnClickListener(v -> clearRef());

            if (from.equals(PACKAGE)) {
                nameTv.setText(packageModel.getPackageName());
                int original = Integer.parseInt(packageModel.getPrice());
                int discount = Integer.parseInt(packageModel.getDiscount());
                priceTv.setText(String.format(Locale.getDefault(), "%s%d", packageModel.getCurrencySymbol(), getDiscountedPrice(original, discount, packageModel.getDiscountType())));

            } else {
                nameTv.setText(testsModel.getTestName());
                int original = Integer.parseInt(testsModel.getPrice());
                int discount = Integer.parseInt(testsModel.getDiscount());
                priceTv.setText(String.format(Locale.getDefault(), "%s%d", testsModel.getCurrencySymbol(), getDiscountedPrice(original, discount, testsModel.getDiscountType())));
            }

            if (rg.getCheckedRadioButtonId() == R.id.someoneElseRb) {
                nameEt.setEnabled(true);
                phEt.setEnabled(true);
                dobTv.setEnabled(true);
                nameEt.setText(null);
                phEt.setText(null);
                dobTv.setText(null);
                nameEt.requestFocus();
            } else {
                nameEt.setEnabled(false);
                phEt.setEnabled(false);
                dobTv.setEnabled(getPrefs().getDOB().equals(NA));
                nameEt.setText(getPrefs().getFullName());
                phEt.setText(getPrefs().getPhone());
                if (!dobTv.isEnabled()) {
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(sdf_yyyyMMdd.parse(getPrefs().getDOB()));
                        selectedDate = sdf_yyyyMMdd.format(calendar.getTime());
                        dobTv.setText(getAgeString(calendar));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        Log.e(TAG, "onCreateView: ", e);
                        dobTv.setEnabled(true);
                    }
                }
            }

            rg.setOnCheckedChangeListener((group, checkedId) -> {
                if (checkedId == R.id.someoneElseRb) {
                    nameEt.setEnabled(true);
                    phEt.setEnabled(true);
                    dobTv.setEnabled(true);
                    nameEt.setText(null);
                    phEt.setText(null);
                    dobTv.setText(null);
                    nameEt.requestFocus();
                } else {
                    nameEt.setEnabled(false);
                    phEt.setEnabled(false);
                    dobTv.setEnabled(getPrefs().getDOB().equals(NA));
                    nameEt.setText(getPrefs().getFullName());
                    phEt.setText(getPrefs().getPhone());
                    if (!dobTv.isEnabled()) {
                        Calendar calendar = Calendar.getInstance();
                        try {
                            calendar.setTime(sdf_yyyyMMdd.parse(getPrefs().getDOB()));
                            selectedDate = sdf_yyyyMMdd.format(calendar.getTime());
                            dobTv.setText(getAgeString(calendar));
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.e(TAG, "onCreateView: ", e);
                            dobTv.setEnabled(true);
                        }
                    }
                }
            });

            addDocRefBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(ctx, SelectAppointmentToShareReport.class)
                                    .putExtra("from", SCHEDULE_APPOINTMENT)
                                    .putExtra("reportId", "0")
                                    .putExtra("dcId", from.equals(PACKAGE) ? packageModel.getDcInfo().getId() : testsModel.getDcInfo().getId())
                            , RC_ADD_DOC_REF);
                }
            });

            proceedBtn.setOnClickListener(v -> {

                if (rg.getCheckedRadioButtonId() == R.id.someoneElseRb) {

                    String name = nameEt.getText().toString().trim();
                    String ph = phEt.getText().toString().trim();
                    String age = dobTv.getText().toString().trim();
                    if (name.isEmpty()) {
                        comm.showToast(R.string.error_name_is_empty);
                        return;
                    }
                    if (ph.isEmpty()) {
                        comm.showToast(R.string.error_ph_is_empty);
                        return;
                    }
                    if (!Patterns.PHONE.matcher(ph).matches()) {
                        comm.showToast(R.string.error_phone_number_is_empty_or_invalid);
                        return;
                    }
                    if (age.isEmpty()) {
                        comm.showToast(R.string.error_dob_empty);
                        return;
                    }
                }
                proceed();
            });

        }

        CURRENT_FRAG = SETUP_APPO_DETAILS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.appointment_details);
        return view;
    }

    private void clearRef() {
        appointmentId = "0";
        docNameTv.setText(null);
        dateTimeTv.setText(null);
        appointmentIdTv.setText(null);
        docIv.setImageBitmap(null);
        docRefLL.setVisibility(View.GONE);
        addDocRefBtn.setVisibility(View.VISIBLE);
    }

    private void initViews() {
        nameTv = view.findViewById(R.id.nameTv);
        priceTv = view.findViewById(R.id.priceTv);
        nameEt = view.findViewById(R.id.nameEt);
        phEt = view.findViewById(R.id.phEt);
        dobTv = view.findViewById(R.id.dobTv);
        rg = view.findViewById(R.id.rg);
        addDocRefBtn = view.findViewById(R.id.addDocRefBtn);
        proceedBtn = view.findViewById(R.id.proceedBtn);
        docRefLL = view.findViewById(R.id.docRefLL);
        docIv = view.findViewById(R.id.iv);
        docNameTv = view.findViewById(R.id.docNameTv);
        dateTimeTv = view.findViewById(R.id.dateTimeTv);
        appointmentIdTv = view.findViewById(R.id.appointmentNumberTv);
        closeIv = view.findViewById(R.id.deleteIv);
    }

    private void showDatePicker(TextView v) {
        Calendar cal = Calendar.getInstance();

        DatePickerDialog dialog = new DatePickerDialog(ctx, (view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            selectedDate = sdf_yyyyMMdd.format(calendar.getTime());

            v.setText(getAgeString(calendar));

        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        dialog.getDatePicker().setMaxDate(cal.getTimeInMillis() - 5000);

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    public String getAgeString(Calendar calendar) {

        // Do not get whole year millis in single line. It will produce error in number due to number overflow.
        long dayInMillis = 1000 * 60 * 60 * 24;
        int yearDayCount = 365;
        long yearInMillis = dayInMillis * yearDayCount;
        long diffInMillis = Calendar.getInstance().getTimeInMillis() - calendar.getTimeInMillis();
        int age = (int) (diffInMillis / yearInMillis);
        return age <= 0 ? ctx.getString(R.string.less_than_one_year) : ctx.getString(R.string.age_years, age);
    }

    private void proceed() {

        DcScheduleAppointmentFrag frag = new DcScheduleAppointmentFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("packageModel", packageModel);
        bundle.putSerializable("testModel", testsModel);
        bundle.putString("from", from);
        bundle.putString("name", nameEt.getText().toString().trim());
        bundle.putString("phone", phEt.getText().toString().trim());
        bundle.putString("dob", selectedDate);
        bundle.putString("age", dobTv.getText().toString().trim());
        bundle.putString("for", rg.getCheckedRadioButtonId() == R.id.meRb ? CUSTOMER : "SOMEONE_ELSE");
        bundle.putString("appointmentId", appointmentId);
        frag.setArguments(bundle);
        comm.openFragment(frag, SCHEDULE_APPOINTMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_ADD_DOC_REF && resultCode == RESULT_OK && data != null) {
            AppointmentsModel appointmentsModel = (AppointmentsModel) data.getSerializableExtra("model");
            updateDocRef(appointmentsModel);
        }
    }

    private void updateDocRef(AppointmentsModel appointmentsModel) {
        docRefLL.setVisibility(View.VISIBLE);
        addDocRefBtn.setVisibility(View.GONE);
        Picasso.get().load(appointmentsModel.getDoctorNurseDetails().getImageLink().isEmpty() ? "NA" : appointmentsModel.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .noPlaceholder()
                .into(docIv);
        docNameTv.setText(appointmentsModel.getDoctorNurseDetails().getName());
        dateTimeTv.setText(getFormattedDate(appointmentsModel.getAppointmentDate()));
        appointmentIdTv.setText(appointmentId = appointmentsModel.getAppointmentId());
    }
}