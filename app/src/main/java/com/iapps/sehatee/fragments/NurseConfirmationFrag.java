package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.DoctorNurseListModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getSubmitNurseBookingURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CONFIRMATION;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DOCTOR;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_HHmmss;
import static com.iapps.sehatee.utils.Constants.sdf_ddMMMyyyy;
import static com.iapps.sehatee.utils.Constants.sdf_hhmm_a;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class NurseConfirmationFrag extends Fragment {

    private static final String TAG = "DoctorNurseConfirmation";
    private EditText nameEt, phEt;
    private RadioGroup rg;
    private Button proceedBtn;
    private String from = "";
    private int pos = 0;
    private ImageView iv;
    private TextView nameTv, specialtyTv, feesTv, visitorCountTv, appoDateTv, appoTimeTv;
    private LinearLayout ratingLL;
    private RatingBar ratingBar;
    private DoctorNurseListModel model = new DoctorNurseListModel();
    private View view;
    private Context ctx;
    private Communicator comm;

    public NurseConfirmationFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (DoctorNurseListModel) getArguments().getSerializable("model");
            from = getArguments().getString("from");
            pos = getArguments().getInt("pos", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_nurse_confirmation, container, false);

            initViews(view);

            Picasso.get()
                    .load(model.getImageLink())
                    .error(R.drawable.ic_default_user_image)
                    .into(iv);
            nameTv.setText(model.getName());
            specialtyTv.setText(String.format(Locale.getDefault(), "(%s)", model.getSpecialty()));
            ratingBar.setRating(model.getRating());
            visitorCountTv.setText(String.format(Locale.getDefault(), "%s %s", model.getVisitorCount(), ctx.getString(R.string.reviews)));
            feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), getPrefs().getCurrencySymbol(), model.getSpecialtyList().get(pos).getFees()));

            nameEt.setEnabled(false);
            nameEt.setText(getPrefs().getFullName()); // set user's name and ph here
            phEt.setEnabled(getPrefs().getPhone().equals(NA));
            phEt.setText(getPrefs().getPhone().equals(NA) ? "" : getPrefs().getPhone());

            rg.setOnCheckedChangeListener((group, checkedId) -> {
                if (checkedId == R.id.someoneElseRb) {
                    nameEt.setEnabled(true);
                    phEt.setEnabled(true);
                    nameEt.setText(null);
                    phEt.setText(null);
                    nameEt.requestFocus();
                } else {
                    nameEt.setEnabled(false);
                    nameEt.setText(getPrefs().getFullName()); // set user's name and ph here
                    phEt.setEnabled(getPrefs().getPhone().equals(NA));
                    phEt.setText(getPrefs().getPhone().equals(NA) ? "" : getPrefs().getPhone());
                    phEt.requestFocus();
                }
            });

            appoDateTv.setOnClickListener(v -> openDatePickerDialog(appoDateTv));
            appoTimeTv.setOnClickListener(v -> openTimePickerDialog(appoTimeTv));

            proceedBtn.setOnClickListener(v -> {

                v.setEnabled(false);
                String name = nameEt.getText().toString().trim();
                String ph = phEt.getText().toString().trim();
                String date = appoDateTv.getText().toString().trim();
                String time = appoTimeTv.getText().toString().trim();

                if (validate(name, ph, date)) {
                    proceed(v, name, ph, date, time);
                } else {
                    v.setEnabled(true);
                }
            });

            ratingLL.setOnClickListener(v -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", DOCTOR);
                bundle.putString("itemId", model.getId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });

        }

        CURRENT_FRAG = CONFIRMATION;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.confirmation);
        return view;
    }

    private void initViews(View view) {
        iv = view.findViewById(R.id.iv);
        nameTv = view.findViewById(R.id.nameTv);
        specialtyTv = view.findViewById(R.id.specialtyTv);
        ratingBar = view.findViewById(R.id.ratingBar);
        visitorCountTv = view.findViewById(R.id.visitorCountTv);
        feesTv = view.findViewById(R.id.feesTv);
        nameEt = view.findViewById(R.id.nameEt);
        phEt = view.findViewById(R.id.phEt);
        rg = view.findViewById(R.id.rg);
        proceedBtn = view.findViewById(R.id.proceedBtn);
        appoDateTv = view.findViewById(R.id.appointmentDateTv);
        appoTimeTv = view.findViewById(R.id.appointmentTimeTv);
        ratingLL = view.findViewById(R.id.ratingLL);
    }

    private void openDatePickerDialog(TextView appoDateTv) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(ctx, (view, year, month, dayOfMonth) -> {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            appoDateTv.setText(sdf_ddMMMyyyy.format(cal.getTime()));

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        calendar.add(Calendar.DATE, 1);
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void openTimePickerDialog(TextView appoTimeTv) {
        Calendar calendar = Calendar.getInstance();

        TimePickerDialog dialog = new TimePickerDialog(ctx, (view, hourOfDay, minute) -> {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            appoTimeTv.setText(sdf_hhmm_a.format(calendar.getTime()));
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private boolean validate(String name, String ph, String date) {
        if (date.isEmpty()) {
            comm.showToast(R.string.error_select_a_date);
            nameEt.requestFocus();
            return false;
        }
        if (name.isEmpty()) {
            comm.showToast(R.string.error_name_is_empty);
            nameEt.requestFocus();
            return false;
        }

        if (ph.isEmpty()) {
            comm.showToast(R.string.error_ph_is_empty);
            phEt.requestFocus();
            return false;
        }

        if (!Patterns.PHONE.matcher(ph).matches()) {
            comm.showToast(R.string.error_phone_number_is_empty_or_invalid);
            phEt.requestFocus();
            phEt.setSelection(ph.length());
            return false;
        }

        return true;
    }

    private void proceed(View v, String name, String ph, @NonNull String date, @NonNull String time) {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userId", model.getId());
                jsonObject.put("nurseServiceId", model.getSpecialtyList().get(pos).getNurseServiceId());
                jsonObject.put("specialityId", model.getSpecialtyList().get(pos).getSpecialtyId());
                jsonObject.put("appointmentDate", sdf_yyyyMMdd.format(sdf_ddMMMyyyy.parse(date)));
                jsonObject.put("appointmentTime", sdf_HHmmss.format(sdf_hhmm_a.parse(time)));
                jsonObject.put("fees", model.getSpecialtyList().get(pos).getFees());
                jsonObject.put("name", name);
                jsonObject.put("phone", ph);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getSubmitNurseBookingURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            showAppointmentSuccessFulDialog();
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(NurseConfirmationFrag.this);
                    }
                });


            } catch (JSONException | ParseException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "proceed: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    public void showAppointmentSuccessFulDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_doctor_nurse_booking_confirmation, null, false);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView nameTv = v.findViewById(R.id.nameTv);
        TextView phTv = v.findViewById(R.id.phTv);
        TextView addressTv = v.findViewById(R.id.addressTv);
        TextView feesTv = v.findViewById(R.id.feesTv);
        TextView dateTimeTv = v.findViewById(R.id.dateTimeTv);
        Button appointmentBtn = v.findViewById(R.id.appointmentsBtn);
        ImageButton closeBtn = v.findViewById(R.id.closeBtn);

        nameTv.setText(model.getName());
        phTv.setText(model.getPhone());
        feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), getPrefs().getCurrencySymbol(), model.getSpecialtyList().get(pos).getFees()));

        String address = model.getAddress().equals(NA) ? "" : model.getAddress();
        address += model.getAreaName().equals(NA) ? "" : ", " + model.getAreaName();
        address += model.getCityName().equals(NA) ? "" : ", " + model.getCityName();
        address += model.getPostalCode().equals(NA) ? "" : ", " + model.getPostalCode();
        address += model.getCountryName().equals(NA) ? "" : ", " + model.getCountryName();
        address = address.isEmpty() ? NA : address;

        addressTv.setText(address);

        dateTimeTv.setText(appoDateTv.getText().toString().trim());

        appointmentBtn.setOnClickListener(v1 -> {
            dialog.dismiss();
            comm.goToAppointments();
        });

        closeBtn.setOnClickListener(v12 -> {
            dialog.dismiss();
            comm.goToHome();
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }
}