package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnReviewHelper;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.models.ResponseModels.SingleAppointmentDetailsResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getAddReviewURL;
import static com.iapps.sehatee.utils.APIs.getCancelAppointmentURL;
import static com.iapps.sehatee.utils.APIs.getSingleAppointmentDetailsURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ACCEPTED;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.CANCELED;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.FINISHED;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.NURSE;
import static com.iapps.sehatee.utils.Constants.REJECTED;
import static com.iapps.sehatee.utils.Constants.REQUESTED;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd_hhmmss;
import static com.iapps.sehatee.utils.Constants.showAddReviewDialog;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class AppointmentDetailsNurseFrag extends Fragment {

    private static final String TAG = "AppointmentDetailsNurse";
    private View view;
    private Context ctx;
    private Communicator comm;
    private AppointmentsModel model = new AppointmentsModel();
    private int pos = 0;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView iv;
    private TextView nameTv, specialtyTv, addressTv, dateTimeTv, phTv, statusTv, appointmentNumberTv, feesTv, patientNameTv, patientPhoneTv, chosenExpertiseTv, insuranceProviderTv, cancelReasonTv;
    private LinearLayout canceledLL;
    private Button cancelBtn, addReviewBtn;


    public AppointmentDetailsNurseFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (AppointmentsModel) getArguments().getSerializable("model");
            pos = getArguments().getInt("pos");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_appointment_details_nurse, container, false);

            initViews(view);

            swipeRefreshLayout.setOnRefreshListener(() -> getAppointmentDetails(false));

            cancelBtn.setOnClickListener(v -> {
                if (!comm.isActivityFinishing()) {
                    showCancelDialog();
                }
            });

            updateUI();

            addReviewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!comm.isActivityFinishing()) {
                        showAddReviewDialog(ctx, new OnReviewHelper() {
                            @Override
                            public void onSubmitClicked(Dialog d, float rate, String review) {
                                addReview(d, rate, URLEncoder.encode(review));
                            }

                            @Override
                            public void onNoRating(Dialog d) {
                                comm.showToast(R.string.error_no_rating);
                            }

                            @Override
                            public void onReviewEmpty(Dialog d) {
                                comm.showToast(R.string.error_empty_review);
                            }
                        });
                    }
                }
            });
        }

        CURRENT_FRAG = APPOINTMENT_DETAILS;
        comm.setPageTitle(R.string.appointment_details);
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        return view;
    }

    private void initViews(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        iv = view.findViewById(R.id.iv);
        nameTv = view.findViewById(R.id.nameTv);
        specialtyTv = view.findViewById(R.id.specialtyTv);
        addressTv = view.findViewById(R.id.addressTv);
        dateTimeTv = view.findViewById(R.id.dateTimeTv);
        phTv = view.findViewById(R.id.phTv);
        statusTv = view.findViewById(R.id.statusTv);
        appointmentNumberTv = view.findViewById(R.id.appointmentNumberTv);
        feesTv = view.findViewById(R.id.feesTv);
        chosenExpertiseTv = view.findViewById(R.id.chosenExpertiseTv);
        patientNameTv = view.findViewById(R.id.patientNameTv);
        patientPhoneTv = view.findViewById(R.id.patientPhoneTv);
        insuranceProviderTv = view.findViewById(R.id.insuranceProviderTv);
        canceledLL = view.findViewById(R.id.cancellationReasonLL);
        cancelReasonTv = view.findViewById(R.id.cancellationReasonTv);
        cancelBtn = view.findViewById(R.id.cancelBtn);
        addReviewBtn = view.findViewById(R.id.addReviewBtn);
    }

    private void addReview(Dialog d, float rate, String review) {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("itemId", model.getDoctorNurseDetails().getId());
                jsonObject.put("type", NURSE);
                jsonObject.put("rating", rate);
                jsonObject.put("review", review);
                jsonObject.put("reviewDateTime", sdf_yyyyMMdd_hhmmss.format(Calendar.getInstance().getTime()));

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
                VolleyHelper.getInstance(ctx).addRequest(getAddReviewURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            comm.showToast("Review submitted successfully");
                            d.dismiss();
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(AppointmentDetailsNurseFrag.this);
                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "addReview: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void showCancelDialog() {
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_cancel_nurse_appointment, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        RadioGroup rg = v.findViewById(R.id.rg);
        Button posBtn = v.findViewById(R.id.posBtn);
        Button negBtn = v.findViewById(R.id.negBtn);

        posBtn.setOnClickListener(new View.OnClickListener() {
            String reason = ctx.getString(R.string.do_not_wish_to_specify);

            @Override
            public void onClick(View v) {
                if (rg.getCheckedRadioButtonId() == R.id.tooLongRb) {
                    reason = ctx.getString(R.string.nurse_is_taking_too_long_to_reply);
                } else if (rg.getCheckedRadioButtonId() == R.id.foundAnotherRb) {
                    reason = ctx.getString(R.string.another_nurse_appointment_is_fixed);
                } else if (rg.getCheckedRadioButtonId() == R.id.changeDateRb) {
                    reason = ctx.getString(R.string.doctor_is_taking_too_long_to_reply);
                } else {
                    reason = ctx.getString(R.string.do_not_wish_to_specify);
                }

                dialog.dismiss();
                cancelAppointment(reason);
            }
        });

        negBtn.setOnClickListener(v1 -> dialog.dismiss());

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void cancelAppointment(String reason) {
        if (isOnline(ctx)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appointmentId", model.getAppointmentId());
                jsonObject.put("cancelReason", URLEncoder.encode(reason, "utf-8"));

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getCancelAppointmentURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            getAppointmentDetails(true);
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(AppointmentDetailsNurseFrag.this);
                    }
                });


            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.e(TAG, "cancelAppointment: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        Picasso.get()
                .load(model.getDoctorNurseDetails().getImageLink().isEmpty() ? NA : model.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(iv);
        nameTv.setText(model.getDoctorNurseDetails().getName());

        String address = model.getDoctorNurseDetails().getAddress().equals(NA) ? "" : model.getClinicDetails().getAddress();
        address += model.getDoctorNurseDetails().getAreaName().equals(NA) ? "" : ", " + model.getClinicDetails().getAreaName();
        address += model.getDoctorNurseDetails().getCityName().equals(NA) ? "" : ", " + model.getClinicDetails().getCityName();
        address += model.getDoctorNurseDetails().getCountryName().equals(NA) ? "" : ", " + model.getClinicDetails().getCountryName();
        address += model.getDoctorNurseDetails().getPostalCode().equals(NA) ? "" : ", " + model.getClinicDetails().getPostalCode();

        address = address.isEmpty() ? NA : address;
        addressTv.setText(address);
        dateTimeTv.setText(String.format(Locale.getDefault(), "%s : %s",
                ctx.getString(R.string.appointment_date),
                getFormattedDate(model.getAppointmentDate() + (model.getAppointmentTime() == null ? "" : " " + model.getAppointmentTime()))
        ));

        phTv.setText(model.getDoctorNurseDetails().getPhone());
        specialtyTv.setText(String.format(Locale.getDefault(), "( %s )", model.getDoctorNurseDetails().getSpecialty()));
        dateTimeTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.appointment_date), getFormattedDate(model.getAppointmentDate())));

        appointmentNumberTv.setText(model.getAppointmentNo());
        feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), getPrefs().getCurrencySymbol(), model.getNurseFees()));
        chosenExpertiseTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.chosen_expertise), model.getChosenExpertise()));

        patientNameTv.setText(model.getPatientName());
        patientPhoneTv.setText(model.getPatientPhone());

        insuranceProviderTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.insurance_provider), model.getInsuranceProviderName()));

        statusTv.setText(model.getStatus());
        if (model.getStatus().equals(REQUESTED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorAccent, ctx.getTheme()));
        } else if (model.getStatus().equals(ACCEPTED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.thumbsUpGreen, ctx.getTheme()));
        } else if (model.getStatus().equals(REJECTED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.notificationBadgeColor, ctx.getTheme()));
        } else if (model.getStatus().equals(CANCELED)) {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimaryDark, ctx.getTheme()));
        } else {
            statusTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimary, ctx.getTheme()));
        }

        canceledLL.setVisibility(model.getStatus().equals(CANCELED) ? View.VISIBLE : View.GONE);
        cancelReasonTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.cancellation_reason), NA));

        addReviewBtn.setText(R.string.add_rating);
        addReviewBtn.setVisibility(model.getStatus().equals(FINISHED) ? View.VISIBLE : View.GONE);

        cancelBtn.setVisibility(model.getStatus().equals(FINISHED) || model.getStatus().equals(CANCELED) || model.getStatus().equals(REJECTED) ? View.GONE : View.VISIBLE);
    }

    private void getAppointmentDetails(boolean shouldShowDialog) {
        if (isOnline(ctx)) {
            swipeRefreshLayout.setRefreshing(false);

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (shouldShowDialog) {
                if (!comm.isActivityFinishing()) {
                    if (!swipeRefreshLayout.isRefreshing()) {
                        dialog.show();
                    }
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getSingleAppointmentDetailsURL(model.getAppointmentId()), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    SingleAppointmentDetailsResponseModel singleAppointmentDetailsResponseModel = new Gson().fromJson(response, SingleAppointmentDetailsResponseModel.class);
                    if (singleAppointmentDetailsResponseModel.getStatus() == 1) {
                        model = singleAppointmentDetailsResponseModel.getPayload().getAppointmentDetails();
                        LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(APPOINTMENT_DETAILS_UPDATED).putExtra("pos", pos).putExtra("model", model));
                    } else {
                        comm.showToast(singleAppointmentDetailsResponseModel.getMsg());
                    }
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(AppointmentDetailsNurseFrag.this);
                }
            });

        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }
}