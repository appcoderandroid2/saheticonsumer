package com.iapps.sehatee.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.DoctorClinicsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ClinicModel;
import com.iapps.sehatee.models.DoctorNurseListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CONFIRMATION;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DOCTOR;
import static com.iapps.sehatee.utils.Constants.DOCTOR_DETAILS;
import static com.iapps.sehatee.utils.Constants.DOCTOR_DETAILS_UPDATED;

public class DoctorDetailsFrag extends Fragment implements DoctorClinicsRvAdapter.OnItemClickedHelper {

    private RecyclerView rv;
    private DoctorClinicsRvAdapter adapter;
    private List<ClinicModel> list = new ArrayList<>();
    private TextView venueCountTv;
    private View view;
    private Context ctx;
    private Communicator comm;
    private ImageView iv;
    private RatingBar ratingBar;
    private TextView name, specialty, visitorCount;
    private DoctorNurseListModel model = new DoctorNurseListModel();
    private LinearLayout ratingLL;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(DOCTOR_DETAILS_UPDATED)) {
                        int clinicIndex = intent.getIntExtra("clinicIndex", -1);
                        int scheduleIndex = intent.getIntExtra("scheduleIndex", -1);
                        int slotIndex = intent.getIntExtra("slotIndex", -1);
                        if (clinicIndex != -1) {
                            if (scheduleIndex != -1) {
                                if (slotIndex != -1) {
                                    model.getClinic().get(clinicIndex).getSchedule().get(scheduleIndex).getTimeSlot().remove(slotIndex);
                                    if (model.getClinic().get(clinicIndex).getSchedule().get(scheduleIndex).getTimeSlot().size() == 0) {
                                        model.getClinic().get(clinicIndex).getSchedule().remove(scheduleIndex);
                                    }
                                    list.clear();
                                    list.addAll(model.getClinic());
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    public DoctorDetailsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (DoctorNurseListModel) getArguments().getSerializable("model");
        }
        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(DOCTOR_DETAILS_UPDATED));
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_doctor_details, container, false);

            iv = view.findViewById(R.id.iv);
            name = view.findViewById(R.id.nameTv);
            specialty = view.findViewById(R.id.specialtyTv);
            ratingBar = view.findViewById(R.id.ratingBar);
            visitorCount = view.findViewById(R.id.visitorCountTv);
            rv = view.findViewById(R.id.rv);
            venueCountTv = view.findViewById(R.id.venueCountTv);
            ratingLL = view.findViewById(R.id.ratingLL);


            Picasso.get()
                    .load(model.getImageLink().isEmpty() ? "NA" : model.getImageLink())
                    .error(R.drawable.ic_default_user_image)
                    .into(iv);

            name.setText(model.getName());
            specialty.setText(String.format(Locale.getDefault(), "(%s)", model.getSpecialty()));
            ratingBar.setRating(model.getRating());
            visitorCount.setText(String.format(Locale.getDefault(), "%s %s", model.getVisitorCount(), ctx.getString(R.string.visitors)));

            list.clear();
            list.addAll(model.getClinic());

            rv.setAdapter(adapter = new DoctorClinicsRvAdapter(list, this));
            rv.setLayoutManager(new LinearLayoutManager(ctx));

            ratingLL.setOnClickListener(v -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", DOCTOR);
                bundle.putString("itemId", model.getId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });

            venueCountTv.setText(ctx.getString(R.string.available_on_venues, list.size()));
        }
        CURRENT_FRAG = DOCTOR_DETAILS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.doctor_details);
        return view;
    }

    @Override
    public void onBookNowClicked(int pos) {
        if (pos > -1) {
            DoctorConfirmationFrag frag = new DoctorConfirmationFrag();
            Bundle bundle = new Bundle();
            bundle.putString("from", DOCTOR_DETAILS);
            bundle.putInt("pos", pos);
            bundle.putSerializable("model", model);
            frag.setArguments(bundle);
            comm.openFragment(frag, CONFIRMATION);
        }
    }

    @Override
    public void onCallClicked(int pos) {
        if (pos > -1) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            Intent chooser = Intent.createChooser(callIntent, ctx.getString(R.string.call_using));
            callIntent.setData(Uri.parse("tel:" + list.get(pos).getPhone()));
            if (callIntent.resolveActivity(ctx.getPackageManager()) != null) {
                startActivity(chooser);
            }
        }
    }

    @Override
    public void onLocationClicked(int pos) {
        if (pos > -1) {
            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + list.get(pos).getAddress() + ", " + list.get(pos).getAreaName() + list.get(pos).getCityName() + ", " + list.get(pos).getPostalCode());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(ctx.getPackageManager()) != null) {
                startActivity(mapIntent);
            } else {
                comm.showToast("Sorry! Google Map not found.");
            }
        }
    }
}