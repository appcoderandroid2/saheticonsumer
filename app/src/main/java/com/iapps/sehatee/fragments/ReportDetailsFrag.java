package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.PhotoViewerActivity;
import com.iapps.sehatee.activities.SelectAppointmentToShareReport;
import com.iapps.sehatee.adapters.ReportImagesRvAdapter;
import com.iapps.sehatee.adapters.ReportVisibilityRvAdapter;
import com.iapps.sehatee.adapters.TestListInDetailsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.interfaces.OnReviewHelper;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.DocumentModel;
import com.iapps.sehatee.models.ReportModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.models.ResponseModels.SingleReportDetailsResponseModel;
import com.iapps.sehatee.models.TestsModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.iapps.sehatee.utils.APIs.getAddReviewURL;
import static com.iapps.sehatee.utils.APIs.getCancelReportAppointmentURL;
import static com.iapps.sehatee.utils.APIs.getRemoveAppointmentToShareReportsURL;
import static com.iapps.sehatee.utils.APIs.getReportDetailsURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ACCEPTED;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.FINISHED;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.REPORT_DETAILS;
import static com.iapps.sehatee.utils.Constants.REPORT_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.REQUESTED;
import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;
import static com.iapps.sehatee.utils.Constants.getFormattedDateTime;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd_hhmmss;
import static com.iapps.sehatee.utils.Constants.showAddReviewDialog;
import static com.iapps.sehatee.utils.Constants.showToast;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class ReportDetailsFrag extends Fragment implements ReportImagesRvAdapter.OnItemClickedHelper, ReportVisibilityRvAdapter.OnItemClickedHelper {

    private static final String TAG = "ReportDetailsFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView reportRv, testRv;
    private ReportImagesRvAdapter reportsRvAdapter;
    private List<DocumentModel> reportsList = new ArrayList<>();
    private TestListInDetailsRvAdapter testListInDetailsRvAdapter;
    private List<TestsModel> testsList = new ArrayList<>();
    private RecyclerView reportVisibilityRv;
    private ReportVisibilityRvAdapter reportVisibilityRvAdapter;
    private List<AppointmentsModel> reportVisibilityList = new ArrayList<>();
    private ReportModel model = new ReportModel();
    private ImageView iv;
    private TextView dcNameTv, dcAddressTv, nameTv, descTv, dateTv, priceTv, statusTv;
    private LinearLayout testListLL, noDataLL;
    private Button addAppointmentBtn, cancelBtn, addReviewBtn;
    private int pos = 0;
    private int RC_SELECT_APPOINTMENT = 1001;

    public ReportDetailsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (ReportModel) getArguments().getSerializable("model");
            pos = getArguments().getInt("pos");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_report_details, container, false);

            initViews(view);

            reportsList.clear();
            testsList.clear();
            reportVisibilityList.clear();

            reportRv.setAdapter(reportsRvAdapter = new ReportImagesRvAdapter(reportsList, this, model.getStatus()));
            reportRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            testRv.setAdapter(testListInDetailsRvAdapter = new TestListInDetailsRvAdapter(testsList));
            testRv.setLayoutManager(new LinearLayoutManager(ctx));

            reportVisibilityRv.setAdapter(reportVisibilityRvAdapter = new ReportVisibilityRvAdapter(reportVisibilityList, this));
            reportVisibilityRv.setLayoutManager(new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(this::getReportDetails);

            updateUI();

            addAppointmentBtn.setOnClickListener(v -> startActivityForResult(new Intent(ctx, SelectAppointmentToShareReport.class)
                            .putExtra("reportId", model.getId())
                            .putExtra("dcId", "0")
                            .putExtra("from", REPORT_DETAILS)
                    , RC_SELECT_APPOINTMENT));

            addReviewBtn.setOnClickListener(v -> {
                if (!comm.isActivityFinishing()) {
                    showAddReviewDialog(ctx, new OnReviewHelper() {
                        @Override
                        public void onSubmitClicked(Dialog d, float rate, String review) {
                            addReview(d, rate, URLEncoder.encode(review));
                        }

                        @Override
                        public void onNoRating(Dialog d) {
                            comm.showToast(R.string.error_no_rating);
                        }

                        @Override
                        public void onReviewEmpty(Dialog d) {
                            comm.showToast(R.string.error_empty_review);
                        }
                    });
                }
            });

            cancelBtn.setOnClickListener(v -> {
                if (!comm.isActivityFinishing()) {
                    showCancelDialog();
                }
            });

        }
        CURRENT_FRAG = REPORT_DETAILS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.report_details);
        return view;
    }

    private void initViews(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        reportRv = view.findViewById(R.id.reportsRv);
        testRv = view.findViewById(R.id.testsRv);
        iv = view.findViewById(R.id.iv);
        dcNameTv = view.findViewById(R.id.dcNameTv);
        dcAddressTv = view.findViewById(R.id.dcAddressTv);
        nameTv = view.findViewById(R.id.nameTv);
        descTv = view.findViewById(R.id.descTv);
        dateTv = view.findViewById(R.id.dateTimeTv);
        priceTv = view.findViewById(R.id.priceTv);
        statusTv = view.findViewById(R.id.statusTv);
        testListLL = view.findViewById(R.id.testListLL);
        noDataLL = view.findViewById(R.id.noDataLL);
        reportVisibilityRv = view.findViewById(R.id.reportVisibilityRv);
        addAppointmentBtn = view.findViewById(R.id.addAppointmentBtn);
        addReviewBtn = view.findViewById(R.id.addReviewBtn);
        cancelBtn = view.findViewById(R.id.cancelBtn);
    }

    private void showCancelDialog() {
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_cancel_dc_appointment, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        RadioGroup rg = v.findViewById(R.id.rg);
        Button posBtn = v.findViewById(R.id.posBtn);
        Button negBtn = v.findViewById(R.id.negBtn);

        posBtn.setOnClickListener(new View.OnClickListener() {
            String reason = ctx.getString(R.string.do_not_wish_to_specify);

            @Override
            public void onClick(View v) {
                if (rg.getCheckedRadioButtonId() == R.id.tooLongRb) {
                    reason = ctx.getString(R.string.dc_taking_too_long_to_reply);
                } else if (rg.getCheckedRadioButtonId() == R.id.foundAnotherRb) {
                    reason = ctx.getString(R.string.got_a_different_appointment);
                } else if (rg.getCheckedRadioButtonId() == R.id.changeDateRb) {
                    reason = ctx.getString(R.string.need_to_change_appointment_details);
                } else {
                    reason = ctx.getString(R.string.do_not_wish_to_specify);
                }

                dialog.dismiss();
                cancelAppointment(reason);

            }
        });

        negBtn.setOnClickListener(v1 -> dialog.dismiss());

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void cancelAppointment(String reason) {
        if (isOnline(ctx)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appointmentId", model.getId());
                jsonObject.put("cancelReason", URLEncoder.encode(reason, "utf-8"));

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getCancelReportAppointmentURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            getReportDetails();
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(ReportDetailsFrag.this);
                    }
                });


            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.e(TAG, "cancelAppointment: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void addReview(Dialog d, float rate, String review) {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("itemId", model.getItemId());
                jsonObject.put("type", model.getType());
                jsonObject.put("rating", rate);
                jsonObject.put("review", review);
                jsonObject.put("reviewDateTime", sdf_yyyyMMdd_hhmmss.format(Calendar.getInstance().getTime()));

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
                VolleyHelper.getInstance(ctx).addRequest(getAddReviewURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            comm.showToast("Review submitted successfully");
                            d.dismiss();
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(ReportDetailsFrag.this);
                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "addReview: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {

        Picasso.get()
                .load(model.getImageLink().isEmpty() ? NA : model.getImageLink())
                .error(R.drawable.no_image_found)
                .into(iv);
        dcNameTv.setText(NA);
        dcAddressTv.setText(NA);
        nameTv.setText(model.getName());
        descTv.setText(model.getProductConstituents());
        dateTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getResources().getString(R.string.appointment_date), getFormattedDateTime(model.getDateTime())));
        priceTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.price), model.getCurrencySymbol(),
                (getDiscountedPrice(model.getPrice(), model.getDiscount(), model.getDiscountType()))));
        statusTv.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getResources().getString(R.string.report_status), model.getStatus().toUpperCase()));

        dcNameTv.setText(model.getDcInfo().getName());

        String address = model.getDcInfo().getAddress().equals(NA) ? "" : model.getDcInfo().getAddress();
        address += model.getDcInfo().getArea().equals(NA) ? "" : ", " + model.getDcInfo().getArea();
        address += model.getDcInfo().getCity().equals(NA) ? "" : ", " + model.getDcInfo().getCity();
        address += model.getDcInfo().getPostalCode().equals(NA) ? "" : ", " + model.getDcInfo().getPostalCode();
//        address += user.getCountryName().equals(NA) ? "" : ", " + user.getCountryName();
        address = address.isEmpty() ? NA : address;

        dcAddressTv.setText(address);

        testsList.clear();
        List<String> tests = Arrays.asList(model.getTestName().split(","));
        for (int i = 0; i < tests.size(); i++) {
            TestsModel model = new TestsModel();
            model.setTestName(tests.get(i));
            testsList.add(model);
        }
        testListInDetailsRvAdapter.notifyDataSetChanged();

        reportVisibilityList.clear();
        reportVisibilityList.addAll(model.getReportVisibility());
        reportVisibilityRvAdapter.notifyDataSetChanged();

        addReviewBtn.setText(R.string.add_rating);
        addReviewBtn.setVisibility(model.getStatus().equals(FINISHED) ? View.VISIBLE : View.GONE);

        cancelBtn.setVisibility(model.getStatus().equals(REQUESTED) || model.getStatus().equals(ACCEPTED) ? View.VISIBLE : View.GONE);
        noDataLL.setVisibility(reportsList.size() > 0 ? View.GONE : View.VISIBLE);
        testListLL.setVisibility(model.getType().equals(PACKAGE) ? View.VISIBLE : View.GONE);
    }

    private void getReportDetails() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }
            VolleyHelper.getInstance(ctx).addRequest(getReportDetailsURL(model.getId()), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    SingleReportDetailsResponseModel reportDetailsResponseModel = new Gson().fromJson(response, SingleReportDetailsResponseModel.class);

                    if (reportDetailsResponseModel.getStatus() == 1) {
                        model = reportDetailsResponseModel.getPayload().getReportDetails();
                        LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(REPORT_DETAILS_UPDATED)
                                .putExtra("model", model)
                                .putExtra("pos", pos)
                        );
                    } else {
                        comm.showToast(reportDetailsResponseModel.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(ReportDetailsFrag.this);
                }
            });

        } else {
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onReportClicked(int pos) {
        if (pos > -1) {
            Intent intent = new Intent(ctx, PhotoViewerActivity.class);
            intent.putExtra("pos", pos);
            intent.putExtra("list", (Serializable) reportsList);
            startActivity(intent);
        }
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {

        }
    }

    @Override
    public void onDeleteClicked(int pos) {
        if (pos > -1) {
            if (!comm.isActivityFinishing()) {
                comm.getDoubleBtnDialog(ctx.getString(R.string.alert), getString(R.string.alter_report_visibility_remove_appointment), ctx.getString(R.string.cancel), getString(R.string.remove), new OnDialogActionHelper() {
                    @Override
                    public void onPositiveBtnClicked(Dialog d) {
                        d.dismiss();
                        deleteAppointment(pos, model.getId(), reportVisibilityList.get(pos).getAppointmentId());
                    }

                    @Override
                    public void onNegativeBtnClicked(Dialog d) {
                        d.dismiss();
                    }
                }).show();
            }
        }
    }

    private void deleteAppointment(int pos, String id, String appointmentId) {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!comm.isActivityFinishing()) {
                dialog.show();
            }

            VolleyHelper.getInstance(ctx).addRequest(getRemoveAppointmentToShareReportsURL(), VolleyHelper.POST, "{\"reportAppointmentId\":\"" + id + "\", \"appointmentId\":\"" + appointmentId + "\"}", new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    SimpleResponseModel simpleResponseModel = new Gson().fromJson(response, SimpleResponseModel.class);

                    if (simpleResponseModel.getStatus() == 1) {
                        int index = -1;
                        for (int i = 0; i < model.getReportVisibility().size(); i++) {
                            if (model.getReportVisibility().get(i).getAppointmentId().equals(appointmentId)) {
                                index = i;
                                break;
                            }
                        }
                        if (index != -1) {
                            model.getReportVisibility().remove(index);
                            reportVisibilityRvAdapter.notifyItemRemoved(index);
                            reportVisibilityList.remove(index);
                            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(REPORT_DETAILS_UPDATED)
                                    .putExtra("pos", pos)
                                    .putExtra("model", model)
                            );
                        }
                    } else {
                        showToast(ctx, simpleResponseModel.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showToast(ctx, error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(ReportDetailsFrag.this);
                }
            });
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SELECT_APPOINTMENT && resultCode == RESULT_OK && data != null) {
            AppointmentsModel appointmentsModel = (AppointmentsModel) data.getSerializableExtra("model");

            model.getReportVisibility().add(appointmentsModel);
            LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(REPORT_DETAILS_UPDATED)
                    .putExtra("pos", pos)
                    .putExtra("model", model)
            );
            reportVisibilityList.add(appointmentsModel);
            reportVisibilityRvAdapter.notifyItemInserted(reportVisibilityList.size() - 1);
        }
    }
}