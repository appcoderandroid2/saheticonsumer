package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.NotificationRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.NotificationModel;
import com.iapps.sehatee.models.ResponseModels.NotificationResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getNotificationsURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.NOTIFICATIONS;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class NotificationFrag extends Fragment implements NotificationRvAdapter.OnItemClickedHelper {

    private static final String TAG = "NotificationFrag";
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private NotificationRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<NotificationModel> list = new ArrayList<>();
    private View view;
    private Context ctx;
    private Communicator comm;
    private int pageNum = 1, lastPage = 1;

    public NotificationFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_notification, container, false);
//
//            for (int i = 0; i < 15; i++) {
//                list.add(new NotificationModel());
//            }

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);
            rv.setAdapter(adapter = new NotificationRvAdapter(list, this));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(() -> {
                pageNum = 1;
                lastPage = 1;
                getNotifications();
            });

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getNotifications();
                            }
                        }
                    }
                }
            });

            getNotifications();

        }

        CURRENT_FRAG = NOTIFICATIONS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.notifications);
        comm.setMenuSelection(NOTIFICATIONS);
        return view;
    }

    private void getNotifications() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getNotificationsURL(pageNum), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    NotificationResponseModel model = new Gson().fromJson(response, NotificationResponseModel.class);

                    pageNum = model.getPayload().getNotificationList().getCurrentPage();
                    lastPage = model.getPayload().getNotificationList().getLastPage();

                    if (model.getStatus() == 1) {
                        if (pageNum == 1) {
                            list.clear();
                        }
                        list.addAll(model.getPayload().getNotificationList().getData());

                        adapter.notifyDataSetChanged();
                        comm.updateNotificationBadge(0);
                        pageNum++;

                    } else {
                        comm.showToast(model.getMsg());
                    }
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(NotificationFrag.this);
                }
            });

        } else {
            swipeRefreshLayout.setRefreshing(false);
            updateUI();
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {

        }
    }
}