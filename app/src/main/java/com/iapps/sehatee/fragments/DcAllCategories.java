package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.DcCategoryGridRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.CategoryModel;
import com.iapps.sehatee.models.ResponseModels.DcPackTestCategoriesResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getCategoriesURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI_NO_BOTTOM_NAV;
import static com.iapps.sehatee.utils.Constants.ALL_CATEGORIES;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.OFFERS;
import static com.iapps.sehatee.utils.Constants.SEARCH_DC;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DcAllCategories extends Fragment implements DcCategoryGridRvAdapter.OnItemClickedHelper {

    private static final String TAG = "DcAllCategories";
    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private DcCategoryGridRvAdapter adapter;
    private List<CategoryModel> list = new ArrayList<>();
    private String cityName = "", areaName = "", from;

    public DcAllCategories() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityName = getArguments().getString("cityName");
            areaName = getArguments().getString("areaName");
            from = getArguments().getString("from");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_dc_all_categories, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new DcCategoryGridRvAdapter(list, this));
            rv.setLayoutManager(new GridLayoutManager(ctx, 2));

            swipeRefreshLayout.setOnRefreshListener(this::getCategories);

            getCategories();

        }
        CURRENT_FRAG = ALL_CATEGORIES;
        comm.setActionBar(AB_BACK_WITH_NOTI_NO_BOTTOM_NAV);
        comm.setPageTitle(R.string.all_categories);
        return view;
    }

    private void getCategories() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getCategoriesURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    DcPackTestCategoriesResponseModel model = new Gson().fromJson(response, DcPackTestCategoriesResponseModel.class);
                    if (model.getStatus() == 1) {
                        list.clear();
                        list.addAll(model.getPayload().getCategories());

                        adapter.notifyDataSetChanged();
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    updateUI();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(DcAllCategories.this);
                }
            });

        } else {
            swipeRefreshLayout.setRefreshing(false);
            updateUI();
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCategoryItemClicked(int pos) {
        if (pos > -1) {
            if (from.equals(OFFERS)) {
                OfferPackagesTestFrag frag = new OfferPackagesTestFrag();
                Bundle bundle = new Bundle();
                bundle.putString("catId", list.get(pos).getId());
                bundle.putString("catName", list.get(pos).getCategoryName());
                frag.setArguments(bundle);
                comm.openFragment(frag, OFFERS);
            } else {
                DcSearchFrag frag = new DcSearchFrag();
                Bundle bundle = new Bundle();
                bundle.putString("categories", list.get(pos).getId());
                bundle.putString("cityName", cityName);
                bundle.putString("areaName", areaName);
                bundle.putInt("tabIndex", 0);
                frag.setArguments(bundle);
                comm.openFragment(frag, SEARCH_DC);
            }
        }
    }
}