package com.iapps.sehatee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;

import static com.iapps.sehatee.utils.Constants.AB_MENU_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.PRESCRIPTIONS;

public class PrescriptionsFrag extends Fragment {

    private View view;
    private Context ctx;
    private Communicator comm;

    public PrescriptionsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        CURRENT_FRAG = PRESCRIPTIONS;
        comm.setActionBar(AB_MENU_WITH_NOTI);
        comm.setPageTitle(R.string.prescriptions);
        comm.setMenuSelection(PRESCRIPTIONS);
        return inflater.inflate(R.layout.fragment_prescriptions, container, false);
    }
}