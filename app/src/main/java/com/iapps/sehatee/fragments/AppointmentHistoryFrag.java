package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.AppointmentHistoryRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.AppointmentsModel;
import com.iapps.sehatee.models.ResponseModels.AppoHistoryResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getAppointmentListURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_DETAILS;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_HISTORY;
import static com.iapps.sehatee.utils.Constants.APPOINTMENT_HISTORY_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class AppointmentHistoryFrag extends Fragment implements AppointmentHistoryRvAdapter.OnItemSelectedHelper {

    private static final String TAG = "AppointmentHistoryFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private AppointmentHistoryRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<AppointmentsModel> list = new ArrayList<>();
    private int pageNum = 1, lastPage = 1;
    private String appointmentId = "0";
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(APPOINTMENT_HISTORY_DETAILS_UPDATED)) {
                        int pos = intent.getIntExtra("pos", -1);
                        AppointmentsModel model = (AppointmentsModel) intent.getSerializableExtra("model");
                        if (rv != null) {
                            rv.post(() -> {
                                if (pos == -1) {
                                    pageNum = 1;
                                    lastPage = 1;
                                    getDoctorHistory();
                                } else {
                                    list.set(pos, model);
                                    adapter.notifyItemChanged(pos);
                                }
                            });
                        }
                    }
                }
            }
        }
    };

    public AppointmentHistoryFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            appointmentId = getArguments().getString("appointmentId");
        }
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_appointment_history, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new AppointmentHistoryRvAdapter(list, this));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    Log.e(TAG, "onScrollStateChanged: lastIndex = " + lasVisibleItemIndex + ", list size = " + list.size());
                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getDoctorHistory();
                            }
                        }
                    }
                }
            });

            swipeRefreshLayout.setOnRefreshListener(this::getDoctorHistory);

            getDoctorHistory();

            LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(APPOINTMENT_HISTORY_DETAILS_UPDATED));

        }

        CURRENT_FRAG = APPOINTMENT_HISTORY;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.appointment_history);
        return view;
    }


    private void getDoctorHistory() {
        if (isOnline(ctx)) {
            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getAppointmentListURL(pageNum, appointmentId), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    AppoHistoryResponseModel model = new Gson().fromJson(response, AppoHistoryResponseModel.class);

                    pageNum = model.getPayload().getAppointmentList().getCurrentPage();
                    lastPage = model.getPayload().getAppointmentList().getLastPage();

                    if (model.getStatus() == 1) {
                        if (pageNum == 1) {
                            list.clear();
                        }
                        list.addAll(model.getPayload().getAppointmentList().getData());

                        adapter.notifyDataSetChanged();
                        pageNum++;
                    } else {
                        comm.showToast(model.getMsg());
                    }

                    updateUI();

                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(AppointmentHistoryFrag.this);
                }
            });
        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemSelected(int pos) {
        if (pos > -1) {
            AppointmentDetails frag = new AppointmentDetails();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(pos));
            bundle.putString("from", APPOINTMENT_HISTORY);
            bundle.putInt("pos", pos);
            frag.setArguments(bundle);
            comm.openFragment(frag, APPOINTMENT_DETAILS);
        }
    }
}