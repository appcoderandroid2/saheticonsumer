package com.iapps.sehatee.fragments.introFrags;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.iapps.sehatee.R;

import static com.iapps.sehatee.activities.IntroductionActivity.ANIMATE_SECOND_SLIDE;

public class IntroTwoFrag extends Fragment {

    private Context ctx;
    private CardView card1, card2, card3;
    private LinearLayout LL;
    private TextView titleTv, descTv;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(ANIMATE_SECOND_SLIDE)) {
                        LL.post(() -> startAnimationProcess());
                    }
                }
            }
        }
    };

    public IntroTwoFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(ANIMATE_SECOND_SLIDE));
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro_two, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LL = view.findViewById(R.id.LL);
        card1 = view.findViewById(R.id.card1);
        card2 = view.findViewById(R.id.card2);
        card3 = view.findViewById(R.id.card3);
        titleTv = view.findViewById(R.id.titleTv);
        descTv = view.findViewById(R.id.descTv);

    }


    private void startAnimationProcess() {

        titleTv.setAlpha(0);
        descTv.setAlpha(0);
        card1.setAlpha(0);
        card2.setAlpha(0);
        card3.setAlpha(0);

        float titleX = titleTv.getX();
        float descX = descTv.getX();
        float card1Y = card1.getY();
        float card2Y = card2.getY();
        float card3Y = card3.getY();


        animateCards(card1, card1Y + 250, card1Y, 200);
        animateCards(card2, card2Y + 200, card2Y, 300);
        animateCards(card3, card3Y + 150, card3Y, 400);

        animateCardRotationY(card1, -4, 2, 200);

        animateCardRotation(card2, 6, -5, 300);
        animateCardRotationX(card2, 5, -3, 300);
        animateCardRotationY(card2, 5, -3, 300);

        animateCardRotation(card3, -5, 3, 400);
        animateCardRotationY(card3, -3, 1, 400);

        animateTexts(titleTv, titleX + 200, titleX, 100);
        animateTexts(descTv, descX + 150, descX, 200);
    }


    private void animateCards(View v, float from, float to, long delay) {

        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setY((Float) animation.getAnimatedValue()));
        axisAnimator.start();

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(0, 1);
        alphaAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        alphaAnimator.setDuration(1000);
        alphaAnimator.setStartDelay(delay);
        alphaAnimator.addUpdateListener(animation ->
                v.setAlpha((float) animation.getAnimatedValue())
        );
        alphaAnimator.start();
    }

    private void animateCardRotation(View v, float from, float to, long delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setRotation((Float) animation.getAnimatedValue()));
        axisAnimator.start();
    }

    private void animateCardRotationX(View v, float from, float to, long delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setRotationX((Float) animation.getAnimatedValue()));
        axisAnimator.start();
    }

    private void animateCardRotationY(View v, float from, float to, long delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setRotationY((Float) animation.getAnimatedValue()));
        axisAnimator.start();
    }

    private void animateTexts(View v, float from, float to, long delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setX((Float) animation.getAnimatedValue()));
        axisAnimator.start();

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(0, 1);
        alphaAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        alphaAnimator.setDuration(1000);
        alphaAnimator.setStartDelay(delay);
        alphaAnimator.addUpdateListener(animation ->
                v.setAlpha((float) animation.getAnimatedValue())
        );
        alphaAnimator.start();
    }
}