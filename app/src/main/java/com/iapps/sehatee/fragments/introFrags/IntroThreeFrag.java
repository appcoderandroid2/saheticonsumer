package com.iapps.sehatee.fragments.introFrags;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.iapps.sehatee.R;

import static com.iapps.sehatee.activities.IntroductionActivity.ANIMATE_THIRD_SLIDE;

public class IntroThreeFrag extends Fragment {

    private Context ctx;
    private LinearLayout LL;
    private TextView titleTv, descTv;
    private CardView card1;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals(ANIMATE_THIRD_SLIDE)) {
                        LL.post(() -> startAnimationProcess());
                    }
                }
            }
        }
    };

    public IntroThreeFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(ANIMATE_THIRD_SLIDE));
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro_three, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LL = view.findViewById(R.id.LL);
        titleTv = view.findViewById(R.id.titleTv);
        descTv = view.findViewById(R.id.descTv);
        card1 = view.findViewById(R.id.card1);

    }

    private void startAnimationProcess() {

        float titleX = titleTv.getX();
        float descX = descTv.getX();

        animateCard(card1, 0.5f, 1f, 200);

        animateTexts(titleTv, titleX + 200, titleX, 100);
        animateTexts(descTv, descX + 200, descX, 200);
    }

    private void animateCard(View v, float from, float to, int delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> {
            v.setScaleX((Float) animation.getAnimatedValue());
            v.setScaleY((Float) animation.getAnimatedValue());
        });
        axisAnimator.start();

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(0, 1);
        alphaAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        alphaAnimator.setDuration(1000);
        alphaAnimator.setStartDelay(delay);
        alphaAnimator.addUpdateListener(animation ->
                v.setAlpha((float) animation.getAnimatedValue())
        );
        alphaAnimator.start();
    }

    private void animateTexts(View v, float from, float to, long delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setX((Float) animation.getAnimatedValue()));
        axisAnimator.start();

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(0, 1);
        alphaAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        alphaAnimator.setDuration(1000);
        alphaAnimator.setStartDelay(delay);
        alphaAnimator.addUpdateListener(animation ->
                v.setAlpha((float) animation.getAnimatedValue())
        );
        alphaAnimator.start();
    }
}