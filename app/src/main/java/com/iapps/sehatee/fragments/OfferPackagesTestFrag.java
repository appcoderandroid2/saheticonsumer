package com.iapps.sehatee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.VpAdapter;
import com.iapps.sehatee.interfaces.Communicator;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI_NO_BOTTOM_NAV;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.OFFERS;
import static com.iapps.sehatee.utils.Constants.OTHER;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.TEST;

public class OfferPackagesTestFrag extends Fragment {

    private View view;
    private Context ctx;
    private Communicator comm;
    private ViewPager vp;
    private TabLayout tabLayout;
    private VpAdapter vpAdapter;
    private TextView catNameTv;
    private String catId = "0", catName = "";

    public OfferPackagesTestFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            catId = getArguments().getString("catId");
            catName = getArguments().getString("catName");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_offer_packages_test, container, false);

            vp = view.findViewById(R.id.vp);
            tabLayout = view.findViewById(R.id.tabLayout);
            catNameTv = view.findViewById(R.id.categoryNameTv);

            catNameTv.setText(catName);

            vpAdapter = new VpAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            vp.setAdapter(vpAdapter);
            tabLayout.setupWithViewPager(vp);
        }

        vp.removeAllViews();
        vpAdapter.clearFrags();

        OfferPackagesFrag offerPackagesFrag = new OfferPackagesFrag();
        OfferTestsFrag offerTestsFrag = new OfferTestsFrag();
        Bundle bundle = new Bundle();
        bundle.putString("catId", catId);
        bundle.putString("from", OTHER);

        offerPackagesFrag.setArguments(bundle);
        offerTestsFrag.setArguments(bundle);

        vpAdapter.addFrag(offerPackagesFrag, PACKAGE);
        vpAdapter.addFrag(offerTestsFrag, TEST);
        vpAdapter.notifyDataSetChanged();

        CURRENT_FRAG = OFFERS;
        comm.setActionBar(AB_BACK_WITH_NOTI_NO_BOTTOM_NAV);
        comm.setPageTitle(R.string.offers);
        comm.setMenuSelection(OFFERS);
        return view;
    }

    public int getCurrentTabIndex() {
        return vp.getCurrentItem();
    }
}