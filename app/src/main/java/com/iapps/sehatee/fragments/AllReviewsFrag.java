package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.ReviewRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ResponseModels.ReviewResponseModel;
import com.iapps.sehatee.models.ReviewModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getReviewsURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class AllReviewsFrag extends Fragment implements ReviewRvAdapter.OnItemClickedHelper {

    private static final String TAG = "AllReviewsFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private ReviewRvAdapter adapter;
    private LinearLayoutManager lm;
    private List<ReviewModel> list = new ArrayList<>();
    private int pageNum = 1, lastPage = 1;
    private String userType = CUSTOMER, itemId = "";

    public AllReviewsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userType = getArguments().getString("userType");
            itemId = getArguments().getString("itemId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_all_reviews, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv = view.findViewById(R.id.rv);

            rv.setAdapter(adapter = new ReviewRvAdapter(list, this, userType));
            rv.setLayoutManager(lm = new LinearLayoutManager(ctx));

            rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lasVisibleItemIndex = lm.findLastCompletelyVisibleItemPosition();

                    if (lasVisibleItemIndex == list.size() - 1) {
                        if (pageNum <= lastPage) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                getReviews();
                            }
                        }
                    }
                }
            });

            swipeRefreshLayout.setOnRefreshListener(() -> {
                pageNum = 1;
                lastPage = 1;
                getReviews();
            });

            getReviews();
        }
        CURRENT_FRAG = ALL_REVIEWS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.all_reviews);
        return view;
    }

    private void getReviews() {
        if (isOnline(ctx)) {

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getReviewsURL(itemId, userType, pageNum), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    ReviewResponseModel reviewResponseModel = new Gson().fromJson(response, ReviewResponseModel.class);
                    pageNum = reviewResponseModel.getPayload().getReviewList().getCurrentPage();
                    lastPage = reviewResponseModel.getPayload().getReviewList().getLastPage();

                    if (reviewResponseModel.getStatus() == 1) {
                        if (pageNum == 1) {
                            list.clear();
                        }
                        list.addAll(reviewResponseModel.getPayload().getReviewList().getData());
                        adapter.notifyDataSetChanged();
                        pageNum++;
                    } else {
                        comm.showToast(reviewResponseModel.getMsg());
                    }
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(AllReviewsFrag.this);
                }
            });
        } else {
            updateUI();
            swipeRefreshLayout.setRefreshing(false);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {

        }
    }
}