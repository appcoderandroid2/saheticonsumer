package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.AvailableDateRvAdapter;
import com.iapps.sehatee.adapters.AvailableTimeRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ClinicModel;
import com.iapps.sehatee.models.DoctorNurseListModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getSubmitDoctorAppointmentURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CONFIRMATION;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.DOCTOR;
import static com.iapps.sehatee.utils.Constants.DOCTOR_DETAILS;
import static com.iapps.sehatee.utils.Constants.DOCTOR_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.NURSE_DETAILS;
import static com.iapps.sehatee.utils.Constants.NURSE_DETAILS_UPDATED;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;
import static com.iapps.sehatee.utils.Constants.getFormattedTime;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd_hhmm_a;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DoctorConfirmationFrag extends Fragment implements AvailableDateRvAdapter.OnItemSelectedHelper, AvailableTimeRvAdapter.OnItemSelectedHelper {

    private static final String TAG = "DoctorNurseConfirmation";
    private RecyclerView dateRv, timeRv;
    private AvailableDateRvAdapter dateAdapter;
    private AvailableTimeRvAdapter timeAdapter;
    private List<ClinicModel.Schedule> dateList = new ArrayList<>();
    private List<ClinicModel.Schedule.TimeSlot> timeList = new ArrayList<>();
    private EditText nameEt, phEt;
    private RadioGroup rg;
    private Button proceedBtn;
    private CardView timeSlotCard;
    private String from = "";
    private int pos = 0, selectedTimeIndex = -1, selectedDateIndex = -1;
    private LinearLayout ratingLL;
    private ImageView iv;
    private TextView nameTv, specialtyTv, feesTv, visitorCountTv;
    private RatingBar ratingBar;
    private DoctorNurseListModel model = new DoctorNurseListModel();
    private View view;
    private Context ctx;
    private Communicator comm;

    public DoctorConfirmationFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (DoctorNurseListModel) getArguments().getSerializable("model");
            from = getArguments().getString("from");
            pos = getArguments().getInt("pos", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_doctor_confirmation, container, false);

            iv = view.findViewById(R.id.iv);
            nameTv = view.findViewById(R.id.nameTv);
            specialtyTv = view.findViewById(R.id.specialtyTv);
            ratingBar = view.findViewById(R.id.ratingBar);
            visitorCountTv = view.findViewById(R.id.visitorCountTv);
            feesTv = view.findViewById(R.id.feesTv);
            dateRv = view.findViewById(R.id.dateRv);
            timeRv = view.findViewById(R.id.timeRv);
            timeSlotCard = view.findViewById(R.id.timeSlotCard);
            nameEt = view.findViewById(R.id.nameEt);
            phEt = view.findViewById(R.id.phEt);
            rg = view.findViewById(R.id.rg);
            proceedBtn = view.findViewById(R.id.proceedBtn);
            ratingLL = view.findViewById(R.id.ratingLL);

            timeSlotCard.setVisibility(from.equals(DOCTOR_DETAILS) ? View.VISIBLE : View.GONE);

            Picasso.get()
                    .load(model.getImageLink())
                    .error(R.drawable.ic_default_user_image)
                    .into(iv);
            nameTv.setText(model.getName());
            specialtyTv.setText(String.format(Locale.getDefault(), "(%s)", model.getSpecialty()));
            ratingBar.setRating(model.getRating());
            visitorCountTv.setText(String.format(Locale.getDefault(), "%s %s", model.getVisitorCount(), ctx.getString(R.string.visitors)));
            feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), getPrefs().getCurrencySymbol(), model.getFees()));

            dateList.clear();
            dateList.addAll(model.getClinic().get(pos).getSchedule());
            if (dateList.size() > 0) {
                dateList.get(0).setSelected(true);
                selectedDateIndex = 0;
            }
            timeList.clear();
            timeList.addAll(dateList.size() > 0 ? dateList.get(0).getTimeSlot() : new ArrayList<>());
            if (timeList.size() > 0) {
                timeList.get(0).setSelected(true);
                selectedTimeIndex = 0;
            }

            dateRv.setAdapter(dateAdapter = new AvailableDateRvAdapter(dateList, this, true));
            dateRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            timeRv.setAdapter(timeAdapter = new AvailableTimeRvAdapter(timeList, this, true));
            timeRv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            nameEt.setEnabled(false);
            nameEt.setText(getPrefs().getFullName());
            phEt.setEnabled(getPrefs().getPhone().equals(NA));
            phEt.setText(getPrefs().getPhone().equals(NA) ? "" : getPrefs().getPhone());

            rg.setOnCheckedChangeListener((group, checkedId) -> {
                if (checkedId == R.id.someoneElseRb) {
                    nameEt.setEnabled(true);
                    phEt.setEnabled(true);
                    nameEt.setText(null);
                    phEt.setText(null);
                    nameEt.requestFocus();
                } else {
                    nameEt.setEnabled(false);
                    nameEt.setText(getPrefs().getFullName()); // set user's name and ph here
                    phEt.setEnabled(getPrefs().getPhone().equals(NA));
                    phEt.setText(getPrefs().getPhone().equals(NA) ? "" : getPrefs().getPhone());
                    phEt.requestFocus();
                }
            });

            proceedBtn.setOnClickListener(v -> {

                if (from.equals(NURSE_DETAILS)) {
                    return;
                }

                v.setEnabled(false);
                String name = nameEt.getText().toString().trim();
                String ph = phEt.getText().toString().trim();

                if (validate(name, ph)) {
                    proceed(v, name, ph);
                } else {
                    v.setEnabled(true);
                }
            });

            ratingLL.setOnClickListener(v -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", DOCTOR);
                bundle.putString("itemId", model.getId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });


        }

        CURRENT_FRAG = CONFIRMATION;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.confirmation);
        return view;
    }

    private boolean validate(String name, String ph) {
        if (selectedDateIndex == -1) {
            comm.showToast(R.string.error_select_a_date);
            return false;
        }

        if (selectedTimeIndex == -1) {
            comm.showToast(R.string.error_select_a_time);
            return false;
        }

        if (name.isEmpty()) {
            comm.showToast(R.string.error_name_is_empty);
            nameEt.requestFocus();
            return false;
        }

        if (ph.isEmpty()) {
            comm.showToast(R.string.error_ph_is_empty);
            phEt.requestFocus();
            return false;
        }

        if (!Patterns.PHONE.matcher(ph).matches()) {
            comm.showToast(R.string.error_phone_number_is_empty_or_invalid);
            phEt.requestFocus();
            phEt.setSelection(ph.length());
            return false;
        }

        return true;
    }

    private void proceed(View v, String name, String ph) {
        if (isOnline(ctx)) {
            try {

                Date currentTime = Calendar.getInstance().getTime();
                String fromTime = model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getFromTime();
                fromTime = getFormattedTime(fromTime);
                String fromDate = model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getDate();

                Date startTime = sdf_yyyyMMdd_hhmm_a.parse(fromDate + " " + fromTime);

                Log.e(TAG, "proceed: startTime => " + startTime + ", currentTime => " + currentTime);

                if (sdf_yyyyMMdd.format(currentTime).equals(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getDate())) {
                    if (currentTime.after(startTime)) {
                        comm.getSingleBtnDialog(ctx.getString(R.string.alert), getString(R.string.error_note_time_slot_crossed), ctx.getString(R.string.ok), d -> {
                            d.dismiss();
                            comm.goBack();
                            new Handler().postDelayed(() -> LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(from.equals(DOCTOR_DETAILS) ? DOCTOR_DETAILS_UPDATED : NURSE_DETAILS_UPDATED)), 300);
                        }).show();
                        return;
                    }
                }

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userId", model.getId());
                jsonObject.put("clinicId", model.getClinic().get(pos).getClinicId());
                jsonObject.put("appointmentDate", model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getDate());
                jsonObject.put("scheduleId", model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getScheduleId());
                jsonObject.put("fromTime", model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getFromTime());
                jsonObject.put("toTime", model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getToTime());
                jsonObject.put("fees", model.getFees());
                jsonObject.put("name", name);
                jsonObject.put("phone", ph);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }

                VolleyHelper.getInstance(ctx).addRequest(getSubmitDoctorAppointmentURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            showAppointmentSuccessFulDialog();
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(DoctorConfirmationFrag.this);
                    }
                });


            } catch (JSONException | ParseException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "proceed: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private boolean isInBetween(Date currentTime, Date startTime, Date endTime) {
        return currentTime.after(startTime) && currentTime.before(endTime);
    }

    public void showAppointmentSuccessFulDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_doctor_nurse_booking_confirmation, null, false);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView nameTv = v.findViewById(R.id.nameTv);
        TextView phTv = v.findViewById(R.id.phTv);
        TextView addressTv = v.findViewById(R.id.addressTv);
        TextView feesTv = v.findViewById(R.id.feesTv);
        TextView dateTimeTv = v.findViewById(R.id.dateTimeTv);
        Button appointmentBtn = v.findViewById(R.id.appointmentsBtn);
        ImageButton closeBtn = v.findViewById(R.id.closeBtn);

        nameTv.setText(model.getName());
        phTv.setText(model.getClinic().get(pos).getPhone());
        feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), getPrefs().getCurrencySymbol(), model.getFees()));
        addressTv.setText(String.format(Locale.getDefault(), "%s, %s, %s, %s", model.getClinic().get(pos).getAddress(),
                model.getClinic().get(pos).getAreaName(),
                model.getClinic().get(pos).getCityName(),
                model.getClinic().get(pos).getPostalCode()));
        dateTimeTv.setText(String.format(Locale.getDefault(), "%s, %s - %s", getFormattedDate(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getDate()),
                getFormattedTime(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getFromTime()),
                getFormattedTime(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getToTime())));

        appointmentBtn.setOnClickListener(v1 -> {
            dialog.dismiss();
            comm.goToAppointments();
        });

        closeBtn.setOnClickListener(v12 -> {
            dialog.dismiss();
            comm.goToHome();
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onDateSelected(int pos) {
        if (pos > -1) {
            for (int i = 0; i < dateList.size(); i++) {
                dateList.get(i).setSelected(false);
                for (int j = 0; j < dateList.get(i).getTimeSlot().size(); j++) {
                    dateList.get(i).getTimeSlot().get(j).setSelected(false);
                }
            }
            dateList.get(pos).setSelected(true);
            selectedDateIndex = pos;
            selectedTimeIndex = -1;
            timeList.clear();
            timeList.addAll(dateList.get(pos).getTimeSlot());
            dateAdapter.notifyDataSetChanged();
            timeAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onTimeSelected(int pos) {
        if (pos > -1) {
            for (int i = 0; i < timeList.size(); i++) {
                timeList.get(i).setSelected(false);
            }
            timeList.get(pos).setSelected(true);
            selectedTimeIndex = pos;
            timeAdapter.notifyDataSetChanged();
        }
    }
}