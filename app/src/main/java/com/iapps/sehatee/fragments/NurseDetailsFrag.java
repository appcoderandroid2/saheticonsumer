package com.iapps.sehatee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.NurseExpertiseRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.DoctorNurseListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CONFIRMATION;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.NURSE;
import static com.iapps.sehatee.utils.Constants.NURSE_DETAILS;

public class NurseDetailsFrag extends Fragment implements NurseExpertiseRvAdapter.OnItemClickedHelper {

    Button bookNowBtn;
    private View view;
    private Context ctx;
    private Communicator comm;
    private DoctorNurseListModel model = new DoctorNurseListModel();
    private ImageView iv;
    private RatingBar ratingBar;
    private TextView nameTv, specialtyTv, visitorCountTv, aboutMeTv, addressTv, phoneTv;
    private RecyclerView rv;
    private NurseExpertiseRvAdapter adapter;
    private List<DoctorNurseListModel.Specialty> list = new ArrayList<>();
    private LinearLayout ratingLL;

    public NurseDetailsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (DoctorNurseListModel) getArguments().getSerializable("model");
            list.addAll(model.getSpecialtyList());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_nurse_details, container, false);

            initViews(view);

            rv.setAdapter(adapter = new NurseExpertiseRvAdapter(list, this));
            rv.setLayoutManager(new LinearLayoutManager(ctx));

            updateUI();

            ratingLL.setOnClickListener(v -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", NURSE);
                bundle.putString("itemId", model.getId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });
        }

        CURRENT_FRAG = NURSE_DETAILS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.nurse_details);
        return view;
    }

    private void initViews(View view) {
        iv = view.findViewById(R.id.iv);
        nameTv = view.findViewById(R.id.nameTv);
        specialtyTv = view.findViewById(R.id.specialtyTv);
        ratingBar = view.findViewById(R.id.ratingBar);
        visitorCountTv = view.findViewById(R.id.visitorCountTv);
        aboutMeTv = view.findViewById(R.id.aboutMeTv);
        addressTv = view.findViewById(R.id.addressTv);
        phoneTv = view.findViewById(R.id.phTv);
        rv = view.findViewById(R.id.rv);
        ratingLL = view.findViewById(R.id.ratingLL);
    }

    private void updateUI() {
        Picasso.get()
                .load(model.getImageLink().isEmpty() ? "NA" : model.getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(iv);

        nameTv.setText(model.getName());
        specialtyTv.setText(String.format(Locale.getDefault(), "(%s)", model.getSpecialty()));
        ratingBar.setRating(model.getRating());
        visitorCountTv.setText(String.format(Locale.getDefault(), "%s %s", model.getVisitorCount(), ctx.getString(R.string.reviews)));
        aboutMeTv.setText(model.getAboutMe());

        String address = model.getAddress().equals(NA) ? "" : model.getAddress();
        address += model.getAreaName().equals(NA) ? "" : ", " + model.getAreaName();
        address += model.getCityName().equals(NA) ? "" : ", " + model.getCityName();
        address += model.getPostalCode().equals(NA) ? "" : ", " + model.getPostalCode();
        address += model.getCountryName().equals(NA) ? "" : ", " + model.getCountryName();
        address = address.isEmpty() ? NA : address;
        addressTv.setText(address);
        phoneTv.setText(model.getPhone());

    }

    @Override
    public void onBookNowClicked(int pos) {
        if (pos > -1) {
            NurseConfirmationFrag frag = new NurseConfirmationFrag();
            Bundle bundle = new Bundle();
            bundle.putString("from", NURSE_DETAILS);
            bundle.putSerializable("model", model);
            bundle.putInt("pos", pos);
            frag.setArguments(bundle);
            comm.openFragment(frag, CONFIRMATION);
        }
    }
}