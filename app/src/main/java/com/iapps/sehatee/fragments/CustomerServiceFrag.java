package com.iapps.sehatee.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.CUSTOMER_SERVICE;

public class CustomerServiceFrag extends Fragment {

    public CustomerServiceFrag() {
        // Required empty public constructor
    }

    private View view;
    private Context ctx;
    private Communicator comm;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_customer_service, container, false);

        }
        CURRENT_FRAG = CUSTOMER_SERVICE;
        comm.setActionBar(AB_BACK_WITH_NOTI);
        comm.setMenuSelection(CUSTOMER_SERVICE);
        comm.setPageTitle(R.string.customer_service);
        return view;
    }
}