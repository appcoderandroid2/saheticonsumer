package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.activities.EditProfile;
import com.iapps.sehatee.adapters.ReviewRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.models.ResponseModels.LogRegResponseModel;
import com.iapps.sehatee.models.ResponseModels.ReviewResponseModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.models.ReviewModel;
import com.iapps.sehatee.utils.helperClasses.PhotoPickOrTakeHelper;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.iapps.sehatee.utils.APIs.getProfileDataURL;
import static com.iapps.sehatee.utils.APIs.getReviewsURL;
import static com.iapps.sehatee.utils.APIs.getUploadProfilePicURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITH_NOTI_NO_BOTTOM_NAV;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.MY_PROFILE;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;
import static com.iapps.sehatee.utils.Constants.updateUserPrefData;

public class MyProfileFrag extends Fragment implements ReviewRvAdapter.OnItemClickedHelper, PhotoPickOrTakeHelper.OnResultHelper {

    private static final String TAG = "MyProfileFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private ReviewRvAdapter adapter;
    private List<ReviewModel> list = new ArrayList<>();
    private ImageView userIv, editIv, cameraIv;
    private TextView nameTv, reviewCountTv, emailTv, phoneTv, genderTv, dobTv, addressTv, aboutMeTv, viewAllReviewTv, noDataTv;
    private RatingBar ratingBar;

    private PhotoPickOrTakeHelper photoPickOrTakeHelper;
    private int RC_OPEN_APP_INFO_SETTINGS = 3030;
    private int RC_EDIT_PROFILE = 4040;

    public MyProfileFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_my_profile, container, false);

            initViews(view);

            rv.setAdapter(adapter = new ReviewRvAdapter(list, this, CUSTOMER));
            rv.setLayoutManager(new LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false));

            updateUI();

            noDataTv.setText(R.string.no_results);

            cameraIv.setOnClickListener(v -> openChooserDialog());

            viewAllReviewTv.setOnClickListener((v) -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", CUSTOMER);
                bundle.putString("itemId", getPrefs().getUserId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });

            editIv.setOnClickListener((v) -> MyProfileFrag.this.startActivityForResult(new Intent(ctx, EditProfile.class), RC_EDIT_PROFILE));

            getReviews();

        }

        CURRENT_FRAG = MY_PROFILE;
        comm.setActionBar(AB_BACK_WITH_NOTI_NO_BOTTOM_NAV);
        comm.setPageTitle(R.string.my_profile);
        comm.setMenuSelection(MY_PROFILE);
        return view;
    }

    private void getReviews() {
        if (isOnline(ctx)) {

            VolleyHelper.getInstance(ctx).addRequest(getReviewsURL(getPrefs().getUserId(), CUSTOMER, 1), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {

                    ReviewResponseModel reviewResponseModel = new Gson().fromJson(response, ReviewResponseModel.class);
                    if (reviewResponseModel.getStatus() == 1) {
                        list.clear();
                        list.addAll(reviewResponseModel.getPayload().getReviewList().getData());
                        adapter.notifyDataSetChanged();
                    }

                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    updateUI();
                }

                @Override
                public void onRestrictedByAdmin() {
                    updateUI();
                }
            });
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    private void initViews(View view) {
        userIv = view.findViewById(R.id.iv);
        nameTv = view.findViewById(R.id.nameTv);
        reviewCountTv = view.findViewById(R.id.reviewCountTv);
        emailTv = view.findViewById(R.id.emailTv);
        phoneTv = view.findViewById(R.id.phTv);
        genderTv = view.findViewById(R.id.genderTv);
        dobTv = view.findViewById(R.id.dobTv);
        addressTv = view.findViewById(R.id.addressTv);
        aboutMeTv = view.findViewById(R.id.aboutMeTv);
        viewAllReviewTv = view.findViewById(R.id.viewAllReviewTv);
        editIv = view.findViewById(R.id.editIv);
        cameraIv = view.findViewById(R.id.cameraIv);
        noDataLL = view.findViewById(R.id.noDataLL);
        noDataTv = view.findViewById(R.id.noDataTv);
        ratingBar = view.findViewById(R.id.ratingBar);

        photoPickOrTakeHelper = new PhotoPickOrTakeHelper.Builder(this)
                .setEnableCropper(true)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setCropperAspectRatio(1, 1)
                .setPortraitWidth(320)
                .setLandscapeWidth(320)
                .build(this);

        rv = view.findViewById(R.id.rv);
    }

    private void updateUI() {
        Picasso.get()
                .load(getPrefs().getProfileImage().isEmpty() ? NA : getPrefs().getProfileImage())
                .error(R.drawable.ic_default_user_image)
                .into(userIv);
        nameTv.setText(getPrefs().getFullName());
        //TODO visitor count tv here
        emailTv.setText(getPrefs().getEmail());
        phoneTv.setText(getPrefs().getPhone());
        genderTv.setText(getPrefs().getGender());
        dobTv.setText(getFormattedDate(getPrefs().getDOB()));
        addressTv.setText(getPrefs().getFullAddress());
        aboutMeTv.setText(getPrefs().getAboutMe());
        ratingBar.setRating(getPrefs().getRating());
        reviewCountTv.setText(String.format(Locale.getDefault(), "%s %s", getPrefs().getTotalReviews(), ctx.getString(R.string.reviews)));

        viewAllReviewTv.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemClicked(int pos) {
        if (pos > -1) {

        }
    }

    private void openChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_photo_pick_or_take, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView cameraTv = view.findViewById(R.id.cameraTv);
        TextView galleryTv = view.findViewById(R.id.galleryTv);

        cameraTv.setOnClickListener((v) -> {
            dialog.dismiss();
            photoPickOrTakeHelper.openCamera();
        });
        galleryTv.setOnClickListener((v) -> {
            dialog.dismiss();
            photoPickOrTakeHelper.openExplorer();
        });

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    private void updateProfilePic(String encodedString) {
        if (isOnline(ctx)) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("profilePic", encodedString);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
                VolleyHelper.getInstance(ctx).addRequest(getUploadProfilePicURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            getProfileData();
                        }
                        comm.showToast(model.getMsg());
                    }

                    @Override
                    public void onFailure(String error) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(MyProfileFrag.this);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "uploadProfilePic: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    @Override
    public void onSuccess(PhotoPickOrTakeHelper.Result result) {
        userIv.setImageURI(result.getUri());
        updateProfilePic(result.getEncodedString());
    }

    @Override
    public void onFailure() {
        comm.showToast(R.string.something_went_wrong);
    }

    @Override
    public void onPermissionDenied(int requestType) {
        if (!comm.isActivityFinishing()) {
            String msg = requestType == PhotoPickOrTakeHelper.RC_OPEN_CAMERA ? getString(R.string.error_note_photo_take_permission_denied) : getString(R.string.error_note_pick_photo_permission_denied);
            comm.getDoubleBtnDialog(ctx.getString(R.string.alert), msg, ctx.getString(R.string.not_now), ctx.getString(R.string.grant_permission), new OnDialogActionHelper() {
                @Override
                public void onPositiveBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                    if (requestType == PhotoPickOrTakeHelper.RC_OPEN_CAMERA) {
                        photoPickOrTakeHelper.openCamera();
                    }
                    if (requestType == PhotoPickOrTakeHelper.RC_OPEN_EXPLORER) {
                        photoPickOrTakeHelper.openExplorer();
                    }
                }

                @Override
                public void onNegativeBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    @Override
    public void onNeverAskPermissionChecked(int requestType) {
        if (!comm.isActivityFinishing()) {
            String msg = requestType == PhotoPickOrTakeHelper.RC_OPEN_CAMERA ? ctx.getString(R.string.error_note_take_photo_permission_denied_and_never_ask_checked) : ctx.getString(R.string.error_note_pick_photo_permission_denied_and_never_ask_checked);
            comm.getDoubleBtnDialog(ctx.getString(R.string.alert), msg, ctx.getString(R.string.not_now), ctx.getString(R.string.app_info), new OnDialogActionHelper() {
                @Override
                public void onPositiveBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                    openAppInfoSettings(requestType);
                }

                @Override
                public void onNegativeBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    public void openAppInfoSettings(int requestType) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", ctx.getPackageName(), null);
        intent.setData(uri);
        this.startActivityForResult(intent, RC_OPEN_APP_INFO_SETTINGS = requestType);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        photoPickOrTakeHelper.onRequestPermissionsResult(requestCode, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        photoPickOrTakeHelper.onActivityResult(requestCode, resultCode, data, this);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_OPEN_APP_INFO_SETTINGS) {
            if (resultCode == RESULT_OK) {
                if (RC_OPEN_APP_INFO_SETTINGS == PhotoPickOrTakeHelper.RC_OPEN_CAMERA) {
                    photoPickOrTakeHelper.openCamera();
                }
                if (RC_OPEN_APP_INFO_SETTINGS == PhotoPickOrTakeHelper.RC_OPEN_EXPLORER) {
                    photoPickOrTakeHelper.openExplorer();
                }
            }
        }

        if (requestCode == RC_EDIT_PROFILE && resultCode == RESULT_OK) {
            updateUI();
            comm.updateProfileData();
        }

    }

    private void getProfileData() {
        VolleyHelper.getInstance(ctx).addRequest(getProfileDataURL(), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
            @Override
            public void onSuccess(String response) {
                LogRegResponseModel model = new Gson().fromJson(response, LogRegResponseModel.class);
                if (model.getStatus() == 1) {

                    getPrefs().setCurrency(model.getPayload().getCurrency().getCurrency());
                    getPrefs().setCurrencySymbol(model.getPayload().getCurrency().getCurrencySymbol());

                    updateUserPrefData(model.getPayload().getUser());
                    comm.updateProfileData();
                    updateUI();
                }
            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public void onRestrictedByAdmin() {

            }
        });
    }

}