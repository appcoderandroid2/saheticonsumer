package com.iapps.sehatee.fragments;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.TestListInDetailsRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnCompleteHelper;
import com.iapps.sehatee.models.PackageModel;
import com.iapps.sehatee.models.TestsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.ALL_REVIEWS;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.PACKAGE_DETAILS;
import static com.iapps.sehatee.utils.Constants.SETUP_APPO_DETAILS;
import static com.iapps.sehatee.utils.Constants.addToFavourite;
import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;

public class DcPackageDetailsFrag extends Fragment {
    private static final String TAG = "DcPackageDetailsFrag";
    private View view;
    private Context ctx;
    private Communicator comm;
    private Button proceedBtn;
    private PackageModel model = new PackageModel();
    private ImageView iv, favIv;
    private TextView nameTv, originalPriceTv, offerPriceTv, productConstituentsTv, categoriesTv, reportAvailabilityTv, moreInfoTv, dcNameTv, dcAddressTv, reviewsTv;
    private LinearLayout ratingLL;
    private RatingBar ratingBar;
    private RecyclerView testsRv;
    private TestListInDetailsRvAdapter adapter;
    private List<TestsModel> testList = new ArrayList<>();

    public DcPackageDetailsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (PackageModel) getArguments().getSerializable("model");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {

            view = inflater.inflate(R.layout.fragment_package_details, container, false);

            initViews(view);

            proceedBtn.setOnClickListener(v -> {
                DcAppointmentDetailsSetupFrag frag = new DcAppointmentDetailsSetupFrag();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", model);
                bundle.putString("from", PACKAGE);
                frag.setArguments(bundle);
                comm.openFragment(frag, SETUP_APPO_DETAILS);
            });

            List<String> tests = Arrays.asList(model.getTestName().split(","));
            testList.clear();
            for (int i = 0; i < tests.size(); i++) {
                TestsModel model = new TestsModel();
                model.setTestName(tests.get(i));
                testList.add(model);
            }
            testsRv.setAdapter(adapter = new TestListInDetailsRvAdapter(testList));
            testsRv.setLayoutManager(new LinearLayoutManager(ctx));

            favIv.setOnClickListener((v) -> addToFavourite(DcPackageDetailsFrag.this, comm.isActivityFinishing(), model.getId(), PACKAGE, new OnCompleteHelper() {
                @Override
                public void onSuccess() {
                    model.setFav(model.isFav() ? 0 : 1);
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    comm.showToast(error);
                }
            }));
            updateUI();
            ratingLL.setOnClickListener(v -> {
                AllReviewsFrag frag = new AllReviewsFrag();
                Bundle bundle = new Bundle();
                bundle.putString("userType", PACKAGE);
                bundle.putString("itemId", model.getId());
                frag.setArguments(bundle);
                comm.openFragment(frag, ALL_REVIEWS);
            });
        }

        CURRENT_FRAG = PACKAGE_DETAILS;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.package_details);
        return view;
    }

    private void initViews(View view) {
        nameTv = view.findViewById(R.id.nameTv);
        originalPriceTv = view.findViewById(R.id.priceTv);
        offerPriceTv = view.findViewById(R.id.offerPriceTv);
        iv = view.findViewById(R.id.iv);
        favIv = view.findViewById(R.id.favIv);
        proceedBtn = view.findViewById(R.id.proceedBtn);
        productConstituentsTv = view.findViewById(R.id.productConstituentsTv);
        categoriesTv = view.findViewById(R.id.categoriesTv);
        reportAvailabilityTv = view.findViewById(R.id.reportAvailabilityTv);
        moreInfoTv = view.findViewById(R.id.moreInfoTv);
        testsRv = view.findViewById(R.id.testsRv);
        dcNameTv = view.findViewById(R.id.dcNameTv);
        dcAddressTv = view.findViewById(R.id.dcAddressTv);
        ratingLL = view.findViewById(R.id.ratingLL);
        ratingBar = view.findViewById(R.id.ratingBar);
        reviewsTv = view.findViewById(R.id.reviewsTv);
    }

    private void updateUI() {
        Picasso.get().
                load(model.getImageLink().isEmpty() ? NA : model.getImageLink())
                .into(iv);

        nameTv.setText(model.getPackageName());
        productConstituentsTv.setText(model.getProductConstituents());
        categoriesTv.setText(model.getCategory().isEmpty() ? NA : model.getCategory());
        reportAvailabilityTv.setText(model.getReportAvailability());
        moreInfoTv.setText(model.getMoreInfo());
        ratingBar.setRating(model.getRating());
        reviewsTv.setText(String.format(Locale.getDefault(), "%s %s", model.getTotalReview(), ctx.getString(R.string.reviews)));

        dcNameTv.setText(String.format(Locale.getDefault(), "%s - %s", ctx.getString(R.string.offered_by), model.getDcInfo().getName()));

        String address = model.getDcInfo().getAddress().equals(NA) ? "" : model.getDcInfo().getAddress();
        address += model.getDcInfo().getArea().equals(NA) ? "" : ", " + model.getDcInfo().getArea();
        address += model.getDcInfo().getCity().equals(NA) ? "" : ", " + model.getDcInfo().getCity();
        address += model.getDcInfo().getPostalCode().equals(NA) ? "" : ", " + model.getDcInfo().getPostalCode();
//        address += user.getCountryName().equals(NA) ? "" : ", " + user.getCountryName();
        address = address.isEmpty() ? NA : address;

        dcAddressTv.setText(address);

        originalPriceTv.setText(String.format(Locale.getDefault(), "%s%s", model.getCurrencySymbol(), model.getPrice()));

        offerPriceTv.setText(String.format(Locale.getDefault(), "%s%s", model.getCurrencySymbol(),
                (getDiscountedPrice(Integer.parseInt(model.getPrice()),
                        Integer.parseInt(model.getDiscount()),
                        model.getDiscountType()))));

        if (Integer.parseInt(model.getDiscount()) > 0) {
            offerPriceTv.setVisibility(View.VISIBLE);
            originalPriceTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.metaTextColor, null));
            originalPriceTv.setPaintFlags(originalPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {

            originalPriceTv.setPaintFlags(originalPriceTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            offerPriceTv.setVisibility(View.GONE);
            originalPriceTv.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.titleTextColor, null));
        }

        favIv.setImageResource(model.isFav() ? R.drawable.ic_round_favorite_24 : R.drawable.ic_round_favorite_border_24);
    }
}