package com.iapps.sehatee.fragments.introFrags;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.iapps.sehatee.R;

public class IntroOneFrag extends Fragment {

    private TextView titleTv, descTv;
    private CardView card1, card2;
    private LinearLayout LL;
    private Context ctx;

    public IntroOneFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro_one, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        titleTv = view.findViewById(R.id.titleTv);
        descTv = view.findViewById(R.id.descTv);
        card1 = view.findViewById(R.id.card1);
        card2 = view.findViewById(R.id.card2);
        LL = view.findViewById(R.id.LL);

        LL.post(() -> {

            float card1X = card1.getX();
            float card2X = card2.getX();
            float card1Y = card1.getY();
            float card2Y = card2.getY();
            float titleY = titleTv.getY();
            float descY = descTv.getY();

            boolean isRTL = ctx.getResources().getBoolean(R.bool.isRTL);

            animateTexts(titleTv, titleY + 160, titleY, 100);
            animateTexts(descTv, descY + 160, descY, 200);

            animateCards(card1, isRTL ? card1X - 100 : card1X + 100, card1X, true, 100);
            animateCards(card1, card1Y + 300, card1Y, false, 200);
            animateCards(card2, isRTL ? card2X - 100 : card2X + 100, card2X, true, 200);
            animateCards(card2, card2Y + 200, card2Y, false, 300);

        });

    }

    private void animateCards(View v, float from, float to, boolean isX, long delay) {

        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> {
            float f = (float) animation.getAnimatedValue();
            if (isX) {
                v.setX(f);
            } else {
                v.setY(f);
            }
        });
        axisAnimator.start();

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(0, 1);
        alphaAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        alphaAnimator.setDuration(1000);
        alphaAnimator.setStartDelay(delay);
        alphaAnimator.addUpdateListener(animation ->
                v.setAlpha((float) animation.getAnimatedValue())
        );
        alphaAnimator.start();
    }

    private void animateTexts(View v, float from, float to, long delay) {
        ValueAnimator axisAnimator = ValueAnimator.ofFloat(from, to);
        axisAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        axisAnimator.setDuration(1000);
        axisAnimator.setStartDelay(delay);
        axisAnimator.addUpdateListener(animation -> v.setY((Float) animation.getAnimatedValue()));
        axisAnimator.start();

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(0, 1);
        alphaAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        alphaAnimator.setDuration(1000);
        alphaAnimator.setStartDelay(delay);
        alphaAnimator.addUpdateListener(animation ->
                v.setAlpha((float) animation.getAnimatedValue())
        );
        alphaAnimator.start();
    }
}