package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.net.URLEncoder;

import static com.iapps.sehatee.utils.APIs.getContactUsURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CONTACT_US;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class ContactUsFrag extends Fragment {

    private View view;
    private Context ctx;
    private Communicator comm;
    private EditText msgEt;
    private Button sendBtn;

    public ContactUsFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_contact_us, container, false);

            msgEt = view.findViewById(R.id.msgEt);
            sendBtn = view.findViewById(R.id.sendBtn);

            sendBtn.setOnClickListener(v -> {
                v.setEnabled(false);
                String msg = msgEt.getText().toString().trim();
                if (validate(msg)) {
                    send(v, msg);
                } else {
                    v.setEnabled(true);
                }
            });
        }
        CURRENT_FRAG = CONTACT_US;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.contact_us);
        comm.setMenuSelection(CONTACT_US);
        return view;
    }

    private boolean validate(String msg) {
        if (msg.isEmpty()) {
            comm.showToast(R.string.error_empty_message);
            msgEt.requestFocus();
            return false;
        }
        return true;
    }

    private void send(View v, String msg) {
        if (isOnline(ctx)) {

            String jsonString = "{\"message\":\"" + URLEncoder.encode(msg) + "\"}";

            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!comm.isActivityFinishing()) {
                dialog.show();
            }

            VolleyHelper.getInstance(ctx).addRequest(getContactUsURL(), VolleyHelper.POST, jsonString, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    v.setEnabled(true);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                    if (model.getStatus() == 1) {
                        comm.showToast(R.string.message_sent_successfully);
                        msgEt.setText(null);
                    } else {
                        comm.showToast(model.getMsg());
                    }
                }

                @Override
                public void onFailure(String error) {
                    v.setEnabled(true);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    comm.showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    v.setEnabled(true);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    showUserRestrictedDialog(ContactUsFrag.this);
                }
            });
        } else {
            v.setEnabled(true);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }
}