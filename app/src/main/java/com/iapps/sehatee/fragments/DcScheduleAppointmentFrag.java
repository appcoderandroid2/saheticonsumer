package com.iapps.sehatee.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.interfaces.OnDialogActionHelper;
import com.iapps.sehatee.models.PackageModel;
import com.iapps.sehatee.models.ResponseModels.SimpleResponseModel;
import com.iapps.sehatee.models.TestsModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Locale;

import static com.iapps.sehatee.utils.APIs.getBookDcAppointmentURL;
import static com.iapps.sehatee.utils.Constants.AB_BACK_WITHOUT_NOTI;
import static com.iapps.sehatee.utils.Constants.CURRENT_FRAG;
import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.MY_PROFILE;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.SCHEDULE_APPOINTMENT;
import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;
import static com.iapps.sehatee.utils.Constants.getFormattedDateTime;
import static com.iapps.sehatee.utils.Constants.getPrefs;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.sdf_HHmmss;
import static com.iapps.sehatee.utils.Constants.sdf_ddMMMyyyy;
import static com.iapps.sehatee.utils.Constants.sdf_hhmm_a;
import static com.iapps.sehatee.utils.Constants.sdf_yyyyMMdd;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class DcScheduleAppointmentFrag extends Fragment {

    private static final String TAG = "DcScheduleAppointmentFr";
    private View view;
    private Context ctx;
    private Communicator comm;
    private Button proceedBtn;
    private TextView nameTv, dateTv, timeTv;
    private EditText addressEt;
    private String from = PACKAGE, name = "", phone = "", dob = "", age = "", patientType = CUSTOMER, selectedDate = "", selectedTime = "", address = "", appointmentId = "0";
    private RadioGroup sampleCollectionRg;
    private PackageModel packageModel = new PackageModel();
    private TestsModel testsModel = new TestsModel();

    public DcScheduleAppointmentFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            from = getArguments().getString("from");
            name = getArguments().getString("name");
            phone = getArguments().getString("phone");
            dob = getArguments().getString("dob");
            age = getArguments().getString("age");
            appointmentId = getArguments().getString("appointmentId", "0");
            patientType = getArguments().getString("for");
            if (from.equals(PACKAGE)) {
                packageModel = (PackageModel) getArguments().getSerializable("packageModel");
            } else {
                testsModel = (TestsModel) getArguments().getSerializable("testModel");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_dc_schedule_appointment, container, false);

            dateTv = view.findViewById(R.id.dateTv);
            timeTv = view.findViewById(R.id.timeTv);
            nameTv = view.findViewById(R.id.nameTv);
            addressEt = view.findViewById(R.id.addressEt);
            sampleCollectionRg = view.findViewById(R.id.sampleCollectionRg);
            proceedBtn = view.findViewById(R.id.proceedBtn);

            nameTv.setText(name);

            dateTv.setOnClickListener(v -> showDatePicker(dateTv));
            timeTv.setOnClickListener(v -> showTimePicker(timeTv));

            proceedBtn.setOnClickListener(v -> {
                v.setEnabled(false);
                String address = addressEt.getText().toString().trim();
                if (validate(selectedDate, selectedTime, address)) {
                    proceed(v, address);
                } else {
                    v.setEnabled(true);
                }
            });
        }
        addressEt.setText(patientType.equals(CUSTOMER) ? getPrefs().getFullAddress() : NA);
        if (patientType.equals(CUSTOMER)) {
            addressEt.setEnabled(false);
            if (addressEt.getText().toString().equals(NA)) {
                if (!comm.isActivityFinishing()) {
                    comm.getDoubleBtnDialog(getString(R.string.sorry), getString(R.string.error_note_no_address),
                            ctx.getString(R.string.cancel), ctx.getString(R.string.my_profile), new OnDialogActionHelper() {
                                @Override
                                public void onPositiveBtnClicked(Dialog d) {
                                    d.dismiss();
                                    comm.openFragment(new MyProfileFrag(), MY_PROFILE);
                                }

                                @Override
                                public void onNegativeBtnClicked(Dialog d) {
                                    d.dismiss();
                                }
                            }).show();
                }
            }
        } else {
            addressEt.setEnabled(true);
        }

        CURRENT_FRAG = SCHEDULE_APPOINTMENT;
        comm.setActionBar(AB_BACK_WITHOUT_NOTI);
        comm.setPageTitle(R.string.schedule_appointmaent);
        return view;
    }

    private void showDatePicker(TextView v) {

        Calendar cal = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(ctx, (view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            selectedDate = sdf_yyyyMMdd.format(calendar.getTime());
            v.setText(sdf_ddMMMyyyy.format(calendar.getTime()));

        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        if (datePickerDialog.getWindow() != null) {
            datePickerDialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        cal.add(Calendar.DATE, 1);
        datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        if (!comm.isActivityFinishing()) {
            datePickerDialog.show();
        }
    }

    private void showTimePicker(TextView v) {
        Calendar cal = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(ctx, (view, hourOfDay, minute) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);

            selectedTime = sdf_HHmmss.format(calendar.getTime());
            v.setText(sdf_hhmm_a.format(calendar.getTime()));

        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false);
        if (timePickerDialog.getWindow() != null) {
            timePickerDialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }
        if (!comm.isActivityFinishing()) {
            timePickerDialog.show();
        }
    }

    private boolean validate(String selectedDate, String selectedTime, String address) {
        if (patientType.equals(CUSTOMER)) {
            if (getPrefs().getFullAddress().equals(NA)) {
                if (!comm.isActivityFinishing()) {
                    comm.getDoubleBtnDialog(getString(R.string.sorry), getString(R.string.error_note_no_address),
                            ctx.getString(R.string.cancel), ctx.getString(R.string.my_profile), new OnDialogActionHelper() {
                                @Override
                                public void onPositiveBtnClicked(Dialog d) {
                                    d.dismiss();
                                    comm.openFragment(new MyProfileFrag(), MY_PROFILE);
                                }

                                @Override
                                public void onNegativeBtnClicked(Dialog d) {
                                    d.dismiss();
                                }
                            }).show();

                    return false;
                }
            }
        }

        if (selectedDate.isEmpty()) {
            comm.showToast(R.string.error_select_a_date);
            return false;
        }
        if (selectedTime.isEmpty()) {
            comm.showToast(R.string.error_select_a_time);
            return false;
        }
        if (address.isEmpty() || address.equals(NA)) {
            comm.showToast(R.string.error_please_enter_address);
            return false;
        }

        return true;
    }

    private void proceed(View v, String address) {
        if (isOnline(ctx)) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", from);
                jsonObject.put("itemId", from.equals(PACKAGE) ? packageModel.getId() : testsModel.getId());
                jsonObject.put("price", from.equals(PACKAGE) ? packageModel.getPrice() : testsModel.getPrice());
                jsonObject.put("discount", from.equals(PACKAGE) ? packageModel.getDiscount() : testsModel.getDiscount());
                jsonObject.put("discountType", from.equals(PACKAGE) ? packageModel.getDiscountType() : testsModel.getDiscountType());
                jsonObject.put("currency", from.equals(PACKAGE) ? packageModel.getCurrency() : testsModel.getCurrency());
                jsonObject.put("currencySymbol", from.equals(PACKAGE) ? packageModel.getCurrencySymbol() : testsModel.getCurrencySymbol());
                jsonObject.put("appointmentFor", patientType.equals(CUSTOMER) ? "Me" : "Someone Else");
                jsonObject.put("appointmentId", appointmentId);
                jsonObject.put("patientName", name);
                jsonObject.put("patientPhone", phone);
                jsonObject.put("dateOfBirth", dob);
                jsonObject.put("age", age);
                jsonObject.put("sampleCollectionType", sampleCollectionRg.getCheckedRadioButtonId() == R.id.visitUsRb ? "Visit Us" : "Home Visit");
                jsonObject.put("address", URLEncoder.encode(address, "utf-8"));
                jsonObject.put("appointmentDate", selectedDate + " " + selectedTime);

                final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
                VolleyHelper.getInstance(ctx).addRequest(getBookDcAppointmentURL(), VolleyHelper.POST, jsonObject.toString(), new VolleyHelper.OnVolleyResponseListener() {
                    @Override
                    public void onSuccess(String response) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        SimpleResponseModel model = new Gson().fromJson(response, SimpleResponseModel.class);
                        if (model.getStatus() == 1) {
                            if (from.equals(PACKAGE)) {
                                showPackageSuccessDialog(packageModel);
                            } else {
                                showTestSuccessDialog(testsModel);
                            }
                        } else {
                            comm.showToast(model.getMsg());
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        comm.showToast(error);
                    }

                    @Override
                    public void onRestrictedByAdmin() {
                        v.setEnabled(true);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        showUserRestrictedDialog(DcScheduleAppointmentFrag.this);
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                v.setEnabled(true);
                e.printStackTrace();
                Log.e(TAG, "proceed: ", e);
                comm.showToast(R.string.something_went_wrong);
            }
        } else {
            v.setEnabled(true);
            comm.showToast(R.string.error_internet_unavilable);
        }
    }

    public void showPackageSuccessDialog(PackageModel model) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_doctor_nurse_booking_confirmation, null, false);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView nameTv = v.findViewById(R.id.nameTv);
        TextView phTv = v.findViewById(R.id.phTv);
        TextView addressTv = v.findViewById(R.id.addressTv);
        TextView feesTv = v.findViewById(R.id.feesTv);
        TextView dateTimeTv = v.findViewById(R.id.dateTimeTv);
        LinearLayout phLL = v.findViewById(R.id.phoneLL);
        Button appointmentBtn = v.findViewById(R.id.appointmentsBtn);
        ImageButton closeBtn = v.findViewById(R.id.closeBtn);

        phLL.setVisibility(View.GONE);

        nameTv.setText(model.getDcInfo().getName());
        phTv.setText(phone);

        feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.price), getPrefs().getCurrencySymbol(), getDiscountedPrice(Integer.parseInt(model.getPrice()), Integer.parseInt(model.getDiscount()), model.getDiscountType())));
        String address = model.getDcInfo().getAddress().equals(NA) ? "" : model.getDcInfo().getAddress();
        address += model.getDcInfo().getArea().equals(NA) ? "" : ", " + model.getDcInfo().getArea();
        address += model.getDcInfo().getCity().equals(NA) ? "" : ", " + model.getDcInfo().getCity();
        address += model.getDcInfo().getPostalCode().equals(NA) ? "" : ", " + model.getDcInfo().getPostalCode();
//        address += user.getCountryName().equals(NA) ? "" : ", " + user.getCountryName();
        address = address.isEmpty() ? NA : address;

        addressTv.setText(address);
        dateTimeTv.setText(String.format(Locale.getDefault(), "%s", getFormattedDateTime(selectedDate + " " + selectedTime)));

        appointmentBtn.setOnClickListener(v1 -> {
            dialog.dismiss();
            comm.goToReports();
        });

        closeBtn.setOnClickListener(v12 -> {
            dialog.dismiss();
            comm.goToHome();
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }

    public void showTestSuccessDialog(TestsModel model) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_doctor_nurse_booking_confirmation, null, false);
        builder.setView(v);
        AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        TextView nameTv = v.findViewById(R.id.nameTv);
        TextView phTv = v.findViewById(R.id.phTv);
        TextView addressTv = v.findViewById(R.id.addressTv);
        TextView feesTv = v.findViewById(R.id.feesTv);
        TextView dateTimeTv = v.findViewById(R.id.dateTimeTv);
        Button appointmentBtn = v.findViewById(R.id.appointmentsBtn);
        ImageButton closeBtn = v.findViewById(R.id.closeBtn);

        nameTv.setText(name);
        phTv.setText(phone);
        feesTv.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.price), getPrefs().getCurrencySymbol(), getDiscountedPrice(Integer.parseInt(model.getPrice()), Integer.parseInt(model.getDiscount()), model.getDiscountType())));
//        addressTv.setText(String.format(Locale.getDefault(), "%s, %s, %s, %s",
//                model.getClinic().get(pos).getAddress(),
//                model.getClinic().get(pos).getAreaName(),
//                model.getClinic().get(pos).getCityName(),
//                model.getClinic().get(pos).getPostalCode()));
//        dateTimeTv.setText(String.format(Locale.getDefault(), "%s, %s - %s", getFormattedDate(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getDate()),
//                getFormattedTime(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getFromTime()),
//                getFormattedTime(model.getClinic().get(pos).getSchedule().get(selectedDateIndex).getTimeSlot().get(selectedTimeIndex).getToTime())));

        appointmentBtn.setOnClickListener(v1 -> {
            dialog.dismiss();
            comm.goToReports();
        });

        closeBtn.setOnClickListener(v12 -> {
            dialog.dismiss();
            comm.goToHome();
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        if (!comm.isActivityFinishing()) {
            dialog.show();
        }
    }
}