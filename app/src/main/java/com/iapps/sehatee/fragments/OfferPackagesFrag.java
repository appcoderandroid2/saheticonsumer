package com.iapps.sehatee.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.iapps.sehatee.R;
import com.iapps.sehatee.adapters.OfferPackagesRvAdapter;
import com.iapps.sehatee.interfaces.Communicator;
import com.iapps.sehatee.models.PackageModel;
import com.iapps.sehatee.models.ResponseModels.OfferPackageResponseModel;
import com.iapps.sehatee.utils.helperClasses.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.APIs.getOfferPackagesTestsListURL;
import static com.iapps.sehatee.utils.Constants.OFFERS;
import static com.iapps.sehatee.utils.Constants.PACKAGE;
import static com.iapps.sehatee.utils.Constants.PACKAGE_DETAILS;
import static com.iapps.sehatee.utils.Constants.getProgressDialog;
import static com.iapps.sehatee.utils.Constants.isOnline;
import static com.iapps.sehatee.utils.Constants.showUserRestrictedDialog;

public class OfferPackagesFrag extends Fragment implements OfferPackagesRvAdapter.OnOfferItemClickedHelper {

    private View view;
    private Context ctx;
    private Communicator comm;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout noDataLL;
    private RecyclerView rv;
    private OfferPackagesRvAdapter adapter;
    private List<PackageModel> list = new ArrayList<>();
    private String catId = "0", from = "";

    public OfferPackagesFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ctx = context;
        comm = (Communicator) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            catId = getArguments().getString("catId");
            from = getArguments().getString("from");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_offer_packages, container, false);

            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            rv = view.findViewById(R.id.rv);
            noDataLL = view.findViewById(R.id.noDataLL);
            rv.setAdapter(adapter = new OfferPackagesRvAdapter(list, this));
            rv.setLayoutManager(new LinearLayoutManager(ctx));

            swipeRefreshLayout.setOnRefreshListener(this::getOfferPackages);

            getOfferPackages();
        }

        return view;
    }

    private void getOfferPackages() {
        if (isOnline(ctx)) {
            final Dialog dialog = getProgressDialog(ctx, R.string.please_wait);
            if (!swipeRefreshLayout.isRefreshing()) {
                if (!comm.isActivityFinishing()) {
                    dialog.show();
                }
            }

            VolleyHelper.getInstance(ctx).addRequest(getOfferPackagesTestsListURL(catId, PACKAGE), VolleyHelper.GET, null, new VolleyHelper.OnVolleyResponseListener() {
                @Override
                public void onSuccess(String response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    OfferPackageResponseModel model = new Gson().fromJson(response, OfferPackageResponseModel.class);
                    if (model.getStatus() == 1) {
                        list.clear();
                        list.addAll(model.getPayload().getOfferList().getData());
                        adapter.notifyDataSetChanged();
                    } else {
                        showToast(model.getMsg());
                    }
                    updateUI();
                }

                @Override
                public void onFailure(String error) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showToast(error);
                }

                @Override
                public void onRestrictedByAdmin() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    updateUI();
                    showUserRestrictedDialog(OfferPackagesFrag.this);
                }
            });

        } else {
            swipeRefreshLayout.setRefreshing(false);
            updateUI();
            showToast(R.string.error_internet_unavilable);
        }
    }

    private void updateUI() {
        noDataLL.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onOfferItemClicked(int pos) {
        if (pos > -1) {
            DcPackageDetailsFrag frag = new DcPackageDetailsFrag();
            Bundle bundle = new Bundle();
            bundle.putSerializable("model", list.get(pos));
            frag.setArguments(bundle);
            comm.openFragment(frag, PACKAGE_DETAILS);
        }
    }

    private void showToast(int msg) {
        if (from.equals(OFFERS)) {
            if (getParentFragment() != null && ((OffersFrag) getParentFragment()).getCurrentTabIndex() == 1) {
                comm.showToast(msg);
            }
        } else {
            if (getParentFragment() != null && ((OfferPackagesTestFrag) getParentFragment()).getCurrentTabIndex() == 1) {
                comm.showToast(msg);
            }
        }
    }

    private void showToast(String msg) {
        if (from.equals(OFFERS)) {
            if (getParentFragment() != null && ((OffersFrag) getParentFragment()).getCurrentTabIndex() == 1) {
                comm.showToast(msg);
            }
        } else {
            if (getParentFragment() != null && ((OfferPackagesTestFrag) getParentFragment()).getCurrentTabIndex() == 1) {
                comm.showToast(msg);
            }
        }
    }

}