package com.iapps.sehatee.models.ResponseModels;

import com.iapps.sehatee.models.AppointmentsModel;

import java.io.Serializable;

/**
 * Created by jpdbiet on 06,July,2020
 */
public class SingleAppointmentDetailsResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {

        private AppointmentsModel appointmentDetails = new AppointmentsModel();

        public AppointmentsModel getAppointmentDetails() {
            return appointmentDetails;
        }

        public void setAppointmentDetails(AppointmentsModel appointmentDetails) {
            this.appointmentDetails = appointmentDetails;
        }
    }
}
