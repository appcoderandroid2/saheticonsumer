package com.iapps.sehatee.models;

import java.io.Serializable;

/**
 * Created by jpdbiet on 16,June,2020
 */
public class CityModel implements Serializable {
    private String cityId = "";
    private String cityName = "";
    private boolean isChecked = false;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

}
