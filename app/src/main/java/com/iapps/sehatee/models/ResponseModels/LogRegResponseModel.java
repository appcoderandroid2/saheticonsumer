package com.iapps.sehatee.models.ResponseModels;

import androidx.annotation.NonNull;

import com.iapps.sehatee.models.UserModel;

import java.io.Serializable;

/**
 * Created by jpdbiet on 23,June,2020
 */
public class LogRegResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private UserModel user = new UserModel();
        private Currency currency = new Currency();
        private String tokenType = "";
        private String token = "";

        public UserModel getUser() {
            return user;
        }

        public void setUser(UserModel user) {
            this.user = user;
        }

        public Currency getCurrency() {
            return currency;
        }

        public void setCurrency(Currency currency) {
            this.currency = currency;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public static class Currency implements Serializable {
            private String currency = "";
            private String currencySymbol = "";

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCurrencySymbol() {
                return currencySymbol;
            }

            public void setCurrencySymbol(String currencySymbol) {
                this.currencySymbol = currencySymbol;
            }

            @NonNull
            @Override
            public String toString() {
                return "Currency{" +
                        "currency='" + currency + '\'' +
                        ", currencySymbol='" + currencySymbol + '\'' +
                        '}';
            }
        }
    }
}
