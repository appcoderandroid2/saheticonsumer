package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;
import com.iapps.sehatee.models.AppointmentsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 30,June,2020
 */
public class AppoHistoryResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private AppointmentList appointmentList = new AppointmentList();

        public AppointmentList getAppointmentList() {
            return appointmentList;
        }

        public void setAppointmentList(AppointmentList appointmentList) {
            this.appointmentList = appointmentList;
        }

        public static class AppointmentList implements Serializable {
            @SerializedName("current_page")
            private int currentPage = 1;
            @SerializedName("last_page")
            private int lastPage = 1;
            private int total = 0;
            private List<AppointmentsModel> data = new ArrayList<>();

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<AppointmentsModel> getData() {
                return data;
            }

            public void setData(List<AppointmentsModel> data) {
                this.data = data;
            }
        }
    }
}
