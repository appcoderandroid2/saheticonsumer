package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;
import com.iapps.sehatee.models.ReviewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 28,July,2020
 */
public class ReviewResponseModel implements Serializable {

    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private ReviewList reviewList = new ReviewList();

        public ReviewList getReviewList() {
            return reviewList;
        }

        public void setReviewList(ReviewList reviewList) {
            this.reviewList = reviewList;
        }

        public static class ReviewList implements Serializable {
            @SerializedName("current_page")
            private int currentPage = 1;
            @SerializedName("last_page")
            private int lastPage = 1;
            private int total = 0;
            private List<ReviewModel> data = new ArrayList<>();

            public List<ReviewModel> getData() {
                return data;
            }

            public void setData(List<ReviewModel> data) {
                this.data = data;
            }

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }
        }
    }
}
