package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jpdbiet on 27,July,2020
 */
public class BannerModel implements Serializable {
    private String id = "";
    @SerializedName("bannerImage")
    private String imageLink = "NA";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
