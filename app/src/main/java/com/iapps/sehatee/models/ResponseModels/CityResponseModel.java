package com.iapps.sehatee.models.ResponseModels;

import com.iapps.sehatee.models.CityModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 24,June,2020
 */
public class CityResponseModel implements Serializable {

    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private List<CityModel> city = new ArrayList<>();

        public List<CityModel> getCity() {
            return city;
        }

        public void setCity(List<CityModel> city) {
            this.city = city;
        }
    }

}
