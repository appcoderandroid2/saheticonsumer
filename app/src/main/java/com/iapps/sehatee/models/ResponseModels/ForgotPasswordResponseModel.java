package com.iapps.sehatee.models.ResponseModels;

import java.io.Serializable;

/**
 * Created by jpdbiet on 24,June,2020
 */
public class ForgotPasswordResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private String userId = "";
        private String otp = "";

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }
    }
}
