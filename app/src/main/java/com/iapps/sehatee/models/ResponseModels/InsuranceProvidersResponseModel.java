package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 03,July,2020
 */
public class InsuranceProvidersResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        @SerializedName("insurance")
        private List<Insurance> insurances = new ArrayList<>();

        public List<Insurance> getInsurances() {
            return insurances;
        }

        public void setInsurances(List<Insurance> insurances) {
            this.insurances = insurances;
        }

        public static class Insurance implements Serializable {
            @SerializedName("insuranceId")
            private String id = "";
            @SerializedName("insuranceName")
            private String name = "";

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
