package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;
import com.iapps.sehatee.models.PackageModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 13,July,2020
 */
public class PackagesResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {

        private TestPackageList testPackageList = new TestPackageList();
        private FavouriteList favouriteList = new FavouriteList();


        public TestPackageList getTestPackageList() {
            return testPackageList;
        }

        public void setTestPackageList(TestPackageList testPackageList) {
            this.testPackageList = testPackageList;
        }

        public FavouriteList getFavouriteList() {
            return favouriteList;
        }

        public void setFavouriteList(FavouriteList favouriteList) {
            this.favouriteList = favouriteList;
        }

        public static class TestPackageList implements Serializable {
            @SerializedName("current_page")
            private int currentPage = 1;
            @SerializedName("last_page")
            private int lastPage = 1;
            private int total = 1;
            private List<PackageModel> data = new ArrayList<>();

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<PackageModel> getData() {
                return data;
            }

            public void setData(List<PackageModel> data) {
                this.data = data;
            }
        }

        public static class FavouriteList implements Serializable {
            @SerializedName("current_page")
            private int currentPage = 1;
            @SerializedName("last_page")
            private int lastPage = 1;
            private int total = 1;
            private List<PackageModel> data = new ArrayList<>();

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<PackageModel> getData() {
                return data;
            }

            public void setData(List<PackageModel> data) {
                this.data = data;
            }
        }
    }
}
