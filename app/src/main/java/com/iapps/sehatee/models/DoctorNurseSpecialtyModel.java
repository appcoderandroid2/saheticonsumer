package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class DoctorNurseSpecialtyModel implements Serializable {
    @SerializedName("specialityId")
    private String id = "";
    @SerializedName("image")
    private String imageLink = "";
    @SerializedName("speciality")
    private String specialty = "";
    private boolean isSelected = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
