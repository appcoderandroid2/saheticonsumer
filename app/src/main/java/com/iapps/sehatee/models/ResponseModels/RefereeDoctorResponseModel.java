package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RefereeDoctorResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private List<RefereeDoctor> doctorList = new ArrayList<>();

        public List<RefereeDoctor> getDoctorList() {
            return doctorList;
        }

        public void setDoctorList(List<RefereeDoctor> doctorList) {
            this.doctorList = doctorList;
        }
    }

    public static class RefereeDoctor implements Serializable {
        @SerializedName("userId")
        private String id = "0";
        private String name = "";
        @SerializedName("profilePic")
        private String imageLink = "NA";
        private boolean isSelected = false;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
