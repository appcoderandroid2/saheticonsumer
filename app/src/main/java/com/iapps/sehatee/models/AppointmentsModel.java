package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class AppointmentsModel implements Serializable {
    @SerializedName("user")
    private DoctorNurseDetails doctorNurseDetails = new DoctorNurseDetails();
    @SerializedName("clinic")
    private ClinicDetails clinicDetails = new ClinicDetails();
    private String fromTime = "";
    private String toTime = "";
    @SerializedName("insuranceName")
    private String insuranceProviderName = "";
    @SerializedName("speciality")
    private String chosenExpertise = "";
    @SerializedName("fees")
    private String nurseFees = "";
    private String currency = "";
    private String currencySymbol = "";
    private String appointmentId = "";
    private String appointmentNo = "";
    private String appointmentDate = "";
    private String appointmentTime = "";
    private String patientName = "";
    private String patientPhone = "";
    private String status = "";
    private String cancelReason = NA;
    @SerializedName("prescriptionImage")
    private List<DocumentModel> prescriptionImages = new ArrayList<>();
    @SerializedName("reportImage")
    private List<DocumentModel> reportImages = new ArrayList<>();
    private boolean isSelected = false;

    public ClinicDetails getClinicDetails() {
        return clinicDetails;
    }

    public void setClinicDetails(ClinicDetails clinicDetails) {
        this.clinicDetails = clinicDetails;
    }

    public DoctorNurseDetails getDoctorNurseDetails() {
        return doctorNurseDetails;
    }

    public void setDoctorNurseDetails(DoctorNurseDetails doctorNurseDetails) {
        this.doctorNurseDetails = doctorNurseDetails;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNurseFees() {
        return nurseFees;
    }

    public void setNurseFees(String nurseFees) {
        this.nurseFees = nurseFees;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(String appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public String getInsuranceProviderName() {
        return insuranceProviderName;
    }

    public void setInsuranceProviderName(String insuranceProviderName) {
        this.insuranceProviderName = insuranceProviderName;
    }

    public String getChosenExpertise() {
        return chosenExpertise;
    }

    public void setChosenExpertise(String chosenExpertise) {
        this.chosenExpertise = chosenExpertise;
    }

    public List<DocumentModel> getPrescriptionImages() {
        return prescriptionImages;
    }

    public void setPrescriptionImages(List<DocumentModel> prescriptionImages) {
        this.prescriptionImages = prescriptionImages;
    }

    public List<DocumentModel> getReportImages() {
        return reportImages;
    }

    public void setReportImages(List<DocumentModel> reportImages) {
        this.reportImages = reportImages;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public static class DoctorNurseDetails implements Serializable {
        @SerializedName("userId")
        private String id = "";
        private String name = "";
        @SerializedName("speciality")
        private String specialty = "";
        private String phone = "";
        @SerializedName("visitors")
        private String visitorCount = "";
        private float rating = 0f;
        private String degree = "";
        @SerializedName("profilePic")
        private String imageLink = "";
        private String userType = "";
        private String address = "";
        private String cityName = "";
        private String areaName = "";
        private String postalCode = "";
        private String countryName = "";


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpecialty() {
            return specialty;
        }

        public void setSpecialty(String specialty) {
            this.specialty = specialty;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getVisitorCount() {
            return visitorCount;
        }

        public void setVisitorCount(String visitorCount) {
            this.visitorCount = visitorCount;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public String getDegree() {
            return degree;
        }

        public void setDegree(String degree) {
            this.degree = degree;
        }

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }
    }

    public static class ClinicDetails implements Serializable {
        @SerializedName("clinicId")
        private String id = "";
        @SerializedName("clinicName")
        private String name = "";
        private String countryName = "";
        private String cityName = "";
        private String areaName = "";
        private String address = "";
        private String postalCode = "";
        private String phone = "";
        private String fees = "";
        private String currency = "";
        private String currencySymbol = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFees() {
            return fees;
        }

        public void setFees(String fees) {
            this.fees = fees;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCurrencySymbol() {
            return currencySymbol;
        }

        public void setCurrencySymbol(String currencySymbol) {
            this.currencySymbol = currencySymbol;
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
