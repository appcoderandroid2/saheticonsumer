package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 21,June,2020
 */
public class ReviewModel implements Serializable {

    private float rating = 0f;
    private String review = "";
    private String date = "";
    private UserModel user = new UserModel();

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public static class UserModel implements Serializable {
        @SerializedName("userId")
        private String id = "";
        private String name = "";
        private String userType = "";
        @SerializedName("speciality")
        private String specialty = "";
        private String totalReview = "";
        private float rating = 0f;
        @SerializedName("profilePic")
        private String imageLink = NA;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getSpecialty() {
            return specialty;
        }

        public void setSpecialty(String specialty) {
            this.specialty = specialty;
        }

        public String getTotalReview() {
            return totalReview;
        }

        public void setTotalReview(String totalReview) {
            this.totalReview = totalReview;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }
    }
}
