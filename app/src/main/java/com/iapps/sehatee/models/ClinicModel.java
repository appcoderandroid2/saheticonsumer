package com.iapps.sehatee.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 26,June,2020
 */
public class ClinicModel implements Serializable {

    private String clinicId = "";
    private String clinicName = "";
    private String fees = "";
    private String phone = "";
    private String cityName = "";
    private String areaName = "";
    private String address = "";
    private String postalCode = "";
    private List<Schedule> schedule = new ArrayList<>();

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }

    public static class Schedule implements Serializable {
        private String date = "";
        private List<TimeSlot> timeSlot = new ArrayList<>();
        private boolean isSelected = false;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<TimeSlot> getTimeSlot() {
            return timeSlot;
        }

        public void setTimeSlot(List<TimeSlot> timeSlot) {
            this.timeSlot = timeSlot;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public static class TimeSlot implements Serializable {
            private String scheduleId = "";
            private String dayName = "";
            private String fromTime = "";
            private String toTime = "";
            private int patientSlot = 0;
            private boolean isSelected = false;

            public String getScheduleId() {
                return scheduleId;
            }

            public void setScheduleId(String scheduleId) {
                this.scheduleId = scheduleId;
            }

            public String getDayName() {
                return dayName;
            }

            public void setDayName(String dayName) {
                this.dayName = dayName;
            }

            public String getFromTime() {
                return fromTime;
            }

            public void setFromTime(String fromTime) {
                this.fromTime = fromTime;
            }

            public String getToTime() {
                return toTime;
            }

            public void setToTime(String toTime) {
                this.toTime = toTime;
            }

            public int getPatientSlot() {
                return patientSlot;
            }

            public void setPatientSlot(int patientSlot) {
                this.patientSlot = patientSlot;
            }

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean selected) {
                isSelected = selected;
            }
        }
    }
}
