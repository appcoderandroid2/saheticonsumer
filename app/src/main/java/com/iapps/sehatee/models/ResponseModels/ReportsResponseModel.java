package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;
import com.iapps.sehatee.models.ReportModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 22,July,2020
 */
public class ReportsResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private ReportAppointmentList reportAppointmentList = new ReportAppointmentList();

        public ReportAppointmentList getReportAppointmentList() {
            return reportAppointmentList;
        }

        public void setReportAppointmentList(ReportAppointmentList reportAppointmentList) {
            this.reportAppointmentList = reportAppointmentList;
        }

        public static class ReportAppointmentList implements Serializable {
            @SerializedName("current_page")
            private int currentPage = 1;
            @SerializedName("last_page")
            private int lastPage = 1;
            private List<ReportModel> data = new ArrayList<>();

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public List<ReportModel> getData() {
                return data;
            }

            public void setData(List<ReportModel> data) {
                this.data = data;
            }
        }
    }
}
