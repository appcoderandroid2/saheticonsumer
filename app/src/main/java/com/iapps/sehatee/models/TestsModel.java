package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class TestsModel implements Serializable {
    @SerializedName("testId")
    private String id = "";
    private String packageName = "";
    private String testName = "";
    private String price = "0";
    private String discountType = "";
    private String discount = "0";
    private String currency = "";
    private float rating = 0f;
    private String totalReview = "0";
    private String currencySymbol = "";
    private String productConstituents = "";
    private String reportAvailability = "";
    private String prerequisite = "";
    //    private String
    private String moreInfo = "";
    private String category = "";
    @SerializedName("image")
    private String imageLink = "";
    @SerializedName("isFavourite")
    private int isFav = 0;
    private DcInfoModel dcInfo = new DcInfoModel();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public String getProductConstituents() {
        return productConstituents;
    }

    public void setProductConstituents(String productConstituents) {
        this.productConstituents = productConstituents;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReportAvailability() {
        return reportAvailability;
    }

    public void setReportAvailability(String reportAvailability) {
        this.reportAvailability = reportAvailability;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public boolean isFav() {
        return isFav == 1;
    }

    public void setFav(int fav) {
        isFav = fav;
    }

    public DcInfoModel getDcInfo() {
        return dcInfo;
    }

    public void setDcInfo(DcInfoModel dcInfo) {
        this.dcInfo = dcInfo;
    }
}
