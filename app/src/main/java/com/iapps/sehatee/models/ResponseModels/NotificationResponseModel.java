package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;
import com.iapps.sehatee.models.NotificationModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 24,July,2020
 */
public class NotificationResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private NotificationList notificationList = new NotificationList();

        public NotificationList getNotificationList() {
            return notificationList;
        }

        public void setNotificationList(NotificationList notificationList) {
            this.notificationList = notificationList;
        }

        public static class NotificationList implements Serializable {
            @SerializedName("current_page")
            private int currentPage = 1;
            @SerializedName("last_page")
            private int lastPage = 1;
            private List<NotificationModel> data = new ArrayList<>();

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public List<NotificationModel> getData() {
                return data;
            }

            public void setData(List<NotificationModel> data) {
                this.data = data;
            }
        }
    }
}
