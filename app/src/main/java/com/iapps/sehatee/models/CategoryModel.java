package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jpdbiet on 21,June,2020
 */
public class CategoryModel implements Serializable {

    @SerializedName("categoryId")
    private String id = "";
    private String categoryName = "";
    @SerializedName("image")
    private String imageLink = "";
    private boolean isChecked = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
