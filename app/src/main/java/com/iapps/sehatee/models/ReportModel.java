package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.Constants.NO_DISCOUNT;

/**
 * Created by jpdbiet on 20,June,2020
 */
public class ReportModel implements Serializable {
    @SerializedName("appointmentId")
    private String id = "";
    @SerializedName("appointmentNo")
    private String reportNumber = "";
    @SerializedName("appointmentDate")
    private String dateTime = "";
    private String type = "";
    @SerializedName("itemName")
    private String name = "";
    private String itemId = "";
    private int price = 0;
    private int discount = 0;
    private String discountType = NO_DISCOUNT;
    private String currency = "";
    private String currencySymbol = "";
    private String patientName = "";
    private String patientPhone = "";
    private String dateOfBirth = "";
    private String age = "";
    private String sampleCollectionType = "";
    private String address = "";
    private String productConstituents = "";
    private String categoryName = "";
    private String testName = "";
    private String status = "";
    @SerializedName("image")
    private String imageLink = "";
    private List<DocumentModel> reportImage = new ArrayList<>();
    private List<AppointmentsModel> reportVisibility = new ArrayList<>();
    private DcInfoModel dcInfo = new DcInfoModel();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSampleCollectionType() {
        return sampleCollectionType;
    }

    public void setSampleCollectionType(String sampleCollectionType) {
        this.sampleCollectionType = sampleCollectionType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProductConstituents() {
        return productConstituents;
    }

    public void setProductConstituents(String productConstituents) {
        this.productConstituents = productConstituents;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public List<DocumentModel> getReportImage() {
        return reportImage;
    }

    public void setReportImage(List<DocumentModel> reportImage) {
        this.reportImage = reportImage;
    }

    public DcInfoModel getDcInfo() {
        return dcInfo;
    }

    public void setDcInfo(DcInfoModel dcInfo) {
        this.dcInfo = dcInfo;
    }

    public List<AppointmentsModel> getReportVisibility() {
        return reportVisibility;
    }

    public void setReportVisibility(List<AppointmentsModel> reportVisibility) {
        this.reportVisibility = reportVisibility;
    }
}
