package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class DoctorNurseListModel implements Serializable {
    @SerializedName("userId")
    private String id = "";
    private String name = "";
    @SerializedName("speciality")
    private String specialty = "";
    private String degree = "";
    private String phone = "";
    private String aboutMe = "";
    private String countryName = "";
    private String address = "";
    private String cityName = "";
    private String areaName = "";
    private String postalCode = "";
    @SerializedName("totalReview")
    private String visitorCount = "";
    private String fees = "";
    private float rating = 0f;
    @SerializedName("availability")
    private String available = "";
    @SerializedName("profilePic")
    private String imageLink = "";
    @SerializedName("specialityList")
    private List<Specialty> specialtyList = new ArrayList<>();

    private List<ClinicModel> clinic = new ArrayList<>();

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getVisitorCount() {
        return visitorCount;
    }

    public void setVisitorCount(String visitorCount) {
        this.visitorCount = visitorCount;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<Specialty> getSpecialtyList() {
        return specialtyList;
    }

    public void setSpecialtyList(List<Specialty> specialtyList) {
        this.specialtyList = specialtyList;
    }

    public List<ClinicModel> getClinic() {
        return clinic;
    }

    public void setClinic(List<ClinicModel> clinic) {
        this.clinic = clinic;
    }

    public static class Specialty implements Serializable {
        private String nurseServiceId = "";
        @SerializedName("specialityId")
        private String specialtyId = "";
        @SerializedName("speciality")
        private String specialty = "";
        private String fees = "";

        public String getNurseServiceId() {
            return nurseServiceId;
        }

        public void setNurseServiceId(String nurseServiceId) {
            this.nurseServiceId = nurseServiceId;
        }

        public String getSpecialtyId() {
            return specialtyId;
        }

        public void setSpecialtyId(String specialtyId) {
            this.specialtyId = specialtyId;
        }

        public String getSpecialty() {
            return specialty;
        }

        public void setSpecialty(String specialty) {
            this.specialty = specialty;
        }

        public String getFees() {
            return fees;
        }

        public void setFees(String fees) {
            this.fees = fees;
        }
    }

}
