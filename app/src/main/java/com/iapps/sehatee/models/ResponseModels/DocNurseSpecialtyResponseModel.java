package com.iapps.sehatee.models.ResponseModels;

import com.google.gson.annotations.SerializedName;
import com.iapps.sehatee.models.DoctorNurseSpecialtyModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 26,June,2020
 */
public class DocNurseSpecialtyResponseModel implements Serializable {
    public int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        @SerializedName("speciality")
        private List<DoctorNurseSpecialtyModel> data = new ArrayList<>();

        public List<DoctorNurseSpecialtyModel> getData() {
            return data;
        }

        public void setData(List<DoctorNurseSpecialtyModel> data) {
            this.data = data;
        }
    }
}
