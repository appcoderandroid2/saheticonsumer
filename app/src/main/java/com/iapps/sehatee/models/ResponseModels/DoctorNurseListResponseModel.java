package com.iapps.sehatee.models.ResponseModels;

import com.iapps.sehatee.models.DoctorNurseListModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 26,June,2020
 */
public class DoctorNurseListResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private DoctorList doctorList = new DoctorList();
        private NurseList nurseList = new NurseList();

        public DoctorList getDoctorList() {
            return doctorList;
        }

        public void setDoctorList(DoctorList doctorList) {
            this.doctorList = doctorList;
        }

        public NurseList getNurseList() {
            return nurseList;
        }

        public void setNurseList(NurseList nurseList) {
            this.nurseList = nurseList;
        }

        public static class DoctorList implements Serializable {
            private int current_page = 1;
            private int last_page = 1;
            private int total = 1;
            private List<DoctorNurseListModel> data = new ArrayList<>();

            public int getCurrent_page() {
                return current_page;
            }

            public void setCurrent_page(int current_page) {
                this.current_page = current_page;
            }

            public int getLast_page() {
                return last_page;
            }

            public void setLast_page(int last_page) {
                this.last_page = last_page;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DoctorNurseListModel> getData() {
                return data;
            }

            public void setData(List<DoctorNurseListModel> data) {
                this.data = data;
            }
        }

        public static class NurseList implements Serializable {
            private int current_page = 1;
            private int last_page = 1;
            private int total = 1;
            private List<DoctorNurseListModel> data = new ArrayList<>();

            public int getCurrent_page() {
                return current_page;
            }

            public void setCurrent_page(int current_page) {
                this.current_page = current_page;
            }

            public int getLast_page() {
                return last_page;
            }

            public void setLast_page(int last_page) {
                this.last_page = last_page;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DoctorNurseListModel> getData() {
                return data;
            }

            public void setData(List<DoctorNurseListModel> data) {
                this.data = data;
            }
        }
    }
}
