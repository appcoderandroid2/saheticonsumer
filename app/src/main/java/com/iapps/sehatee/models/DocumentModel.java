package com.iapps.sehatee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 01,July,2020
 */
public class DocumentModel implements Serializable {
    private int id = 0;
    private String fileName = "";
    private String fileExtension = "";
    @SerializedName("image")
    private String url = NA;
    private int isRemovable = 1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        if (url.isEmpty() || url.equals(NA)) {
            setFileName(NA);
            setFileExtension(NA);
        } else {
            setFileName(url.substring(url.lastIndexOf("/"), url.lastIndexOf(".")).replace("/", "").trim());
            setFileExtension(url.substring(url.lastIndexOf(".")));
        }
    }

    public boolean isRemovable() {
        return isRemovable == 1;
    }

    public void setRemovable(int removable) {
        isRemovable = removable;
    }
}
