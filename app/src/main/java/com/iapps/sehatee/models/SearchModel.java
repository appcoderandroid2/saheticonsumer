package com.iapps.sehatee.models;

import java.io.Serializable;

/**
 * Created by jpdbiet on 19,June,2020
 */
public class SearchModel implements Serializable {
    private String id = "";
    private String name = "";
    private String offeredBy = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfferedBy() {
        return offeredBy;
    }

    public void setOfferedBy(String offeredBy) {
        this.offeredBy = offeredBy;
    }
}
