package com.iapps.sehatee.models.ResponseModels;

import com.iapps.sehatee.models.PackageModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 25,July,2020
 */
public class OfferPackageResponseModel implements Serializable {
    private int status = 0;
    private String msg = "";
    private Payload payload = new Payload();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public static class Payload implements Serializable {
        private OfferList offerList = new OfferList();

        public OfferList getOfferList() {
            return offerList;
        }

        public void setOfferList(OfferList offerList) {
            this.offerList = offerList;
        }

        public static class OfferList implements Serializable {
            private List<PackageModel> data = new ArrayList<>();

            public List<PackageModel> getData() {
                return data;
            }

            public void setData(List<PackageModel> data) {
                this.data = data;
            }
        }
    }
}
