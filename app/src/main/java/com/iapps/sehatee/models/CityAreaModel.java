package com.iapps.sehatee.models;

import java.io.Serializable;

/**
 * Created by jpdbiet on 16,June,2020
 */
public class CityAreaModel implements Serializable {
    private String areaId = "";
    private String areaName = "";
    private boolean isChecked = false;

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

}
