package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.SearchModel;

import java.util.List;
import java.util.Locale;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class SearchRvAdapter extends RecyclerView.Adapter<SearchRvAdapter.VH> {
    private List<SearchModel> list;
    private OnItemClickedHelper helper;

    public SearchRvAdapter(List<SearchModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        holder.nameTv.setText(list.get(position).getName());
        holder.offeredByTv.setText(String.format(Locale.getDefault(), holder.itemView.getResources().getString(R.string.offered_by) + " : %s", list.get(position).getOfferedBy()));
        holder.itemView.setOnClickListener(v -> helper.onItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private TextView nameTv, offeredByTv;

        public VH(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.nameTv);
            offeredByTv = itemView.findViewById(R.id.offeredByTv);
        }
    }
}
