package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.CategoryModel;

import java.util.List;

/**
 * Created by jpdbiet on 19,June,2020
 */
public class DcCategoriesFilterRvAdapter extends RecyclerView.Adapter<DcCategoriesFilterRvAdapter.VH> {

    private List<CategoryModel> list;
    private OnItemClickedHelper helper;

    public DcCategoriesFilterRvAdapter(List<CategoryModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dc_condition_filters, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        holder.cb.setText(list.get(position).getCategoryName());

        holder.cb.setOnCheckedChangeListener(null); // not to trigger the OnCheckedChangeListener unnecessarily
        holder.cb.setChecked(list.get(position).isChecked());

        holder.cb.setOnCheckedChangeListener((buttonView, isChecked) -> helper.onFilterChecked(holder.getAdapterPosition(), isChecked));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.cb.setOnCheckedChangeListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onFilterChecked(int pos, boolean isChecked);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private CheckBox cb;

        public VH(@NonNull View itemView) {
            super(itemView);
            cb = itemView.findViewById(R.id.cb);
        }
    }
}
