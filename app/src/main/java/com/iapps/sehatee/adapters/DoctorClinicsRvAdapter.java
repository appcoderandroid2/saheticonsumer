package com.iapps.sehatee.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ClinicModel;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getPrefs;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class DoctorClinicsRvAdapter extends RecyclerView.Adapter<DoctorClinicsRvAdapter.VH> implements AvailableDateRvAdapter.OnItemSelectedHelper {
    private List<ClinicModel> list;
    private OnItemClickedHelper helper;

    public DoctorClinicsRvAdapter(List<ClinicModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_doctor_venues, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {


        holder.rv.setAdapter(new AvailableDateRvAdapter(list.get(position).getSchedule(), this, false));
        holder.rv.setLayoutManager(new LinearLayoutManager(holder.itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));

        Context ctx = holder.itemView.getContext();

        holder.noDataTv.setVisibility(list.get(position).getSchedule().size() > 0 ? View.GONE : View.VISIBLE);

        holder.address.setText(String.format(Locale.getDefault(), "%s\n%s, %s, %s, %s",
                list.get(position).getClinicName(),
                list.get(position).getAddress(),
                list.get(position).getAreaName(),
                list.get(position).getCityName(),
                list.get(position).getPostalCode()));

        holder.fees.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.consultation_fees), getPrefs().getCurrencySymbol(), list.get(position).getFees()));

        holder.phone.setText(list.get(position).getPhone());

        holder.bookNowBtn.setOnClickListener((v) -> {
            helper.onBookNowClicked(holder.getAdapterPosition());
            holder.itemView.performClick();
        });

        holder.bookNowBtn.setEnabled(list.get(position).getSchedule().size() > 0);

        holder.locationBtn.setOnClickListener(v -> helper.onLocationClicked(holder.getAdapterPosition()));
        holder.callBtn.setOnClickListener(v -> helper.onCallClicked(holder.getAdapterPosition()));

    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.bookNowBtn.setOnClickListener(null);
        holder.callBtn.setOnClickListener(null);
        holder.locationBtn.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onDateSelected(int pos) {
        //TODO Do nothing here
    }

    public interface OnItemClickedHelper {
        void onBookNowClicked(int pos);

        void onCallClicked(int pos);

        void onLocationClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private Button bookNowBtn;
        private ImageButton callBtn, locationBtn;
        private RecyclerView rv;
        private TextView noDataTv, address, fees, phone;

        public VH(@NonNull View itemView) {
            super(itemView);
            bookNowBtn = itemView.findViewById(R.id.bookNowBtn);
            rv = itemView.findViewById(R.id.rv);
            noDataTv = itemView.findViewById(R.id.noDataTv);
            address = itemView.findViewById(R.id.addressTv);
            fees = itemView.findViewById(R.id.feesTv);
            phone = itemView.findViewById(R.id.phTv);
            callBtn = itemView.findViewById(R.id.callBtn);
            locationBtn = itemView.findViewById(R.id.locationBtn);


        }
    }
}
