package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ClinicModel;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getFormattedTime;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class AvailableTimeRvAdapter extends RecyclerView.Adapter<AvailableTimeRvAdapter.VH> {

    private List<ClinicModel.Schedule.TimeSlot> list;
    private OnItemSelectedHelper helper;
    private boolean callBackRequired = false;

    public AvailableTimeRvAdapter(List<ClinicModel.Schedule.TimeSlot> list, OnItemSelectedHelper helper, boolean callBackRequired) {
        this.list = list;
        this.helper = helper;
        this.callBackRequired = callBackRequired;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_availability_date, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {


        holder.tv.setText(String.format(Locale.getDefault(), "%s - %s", getFormattedTime(list.get(position).getFromTime()), getFormattedTime(list.get(position).getToTime())));


        if (callBackRequired) {

            holder.tv.setBackgroundResource(list.get(position).isSelected() ?
                    R.drawable.bg_white_with_simple_accent_border : R.drawable.bg_white_with_simple_border_with_ripple);

            holder.tv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(),
                    list.get(position).isSelected() ? R.color.colorAccent : R.color.descTextColor, null));

            holder.tv.setOnClickListener(v -> helper.onTimeSelected(holder.getAdapterPosition()));
        }
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.tv.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemSelectedHelper {
        void onTimeSelected(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private TextView tv;

        public VH(@NonNull View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.tv);
        }
    }
}