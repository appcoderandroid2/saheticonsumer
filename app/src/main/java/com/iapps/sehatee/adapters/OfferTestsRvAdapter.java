package com.iapps.sehatee.adapters;

import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.TestsModel;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.FLAT;
import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;

/**
 * Created by jpdbiet on 21,June,2020
 */
public class OfferTestsRvAdapter extends RecyclerView.Adapter<OfferTestsRvAdapter.VH> {

    private static final String TAG = "OfferTestsRvAdapter";
    private List<TestsModel> list;
    private OnOfferItemClickedHelper helper;

    public OfferTestsRvAdapter(List<TestsModel> list, OnOfferItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offers_most_popular_tests, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        TestsModel model = list.get(position);

        holder.nameTv.setText(model.getTestName());
        holder.dcNameTv.setText(String.format(Locale.getDefault(), "%s - %s", holder.itemView.getResources().getString(R.string.offered_by), model.getDcInfo().getName()));
        holder.originalPriceTv.setText(String.format(Locale.getDefault(), "%s %s", model.getCurrencySymbol(), model.getPrice()));
        holder.offerPriceTv.setText(String.format(Locale.getDefault(), "%s%s", list.get(position).getCurrencySymbol(),
                (getDiscountedPrice(Integer.parseInt(list.get(position).getPrice()),
                        Integer.parseInt(list.get(position).getDiscount()),
                        list.get(position).getDiscountType()))));
        holder.ratingBar.setRating(model.getRating());
        holder.reviewsTv.setText(String.format(Locale.getDefault(), "%s %s", model.getTotalReview(), holder.itemView.getResources().getString(R.string.reviews)));

        if (Integer.parseInt(list.get(position).getDiscount()) > 0) {
            holder.offerPriceTv.setVisibility(View.VISIBLE);
            holder.originalPriceTv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(), R.color.metaTextColor, null));
            holder.originalPriceTv.setPaintFlags(holder.originalPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.originalPriceTv.setPaintFlags(holder.originalPriceTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.offerPriceTv.setVisibility(View.GONE);
            holder.originalPriceTv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(), R.color.titleTextColor, null));
        }

        holder.discTv.setText(String.format(Locale.getDefault(), "%s%%", model.getDiscountType().equals(FLAT) ? getDiscountPercent(model) : model.getDiscount()));

        holder.itemView.setOnClickListener(v -> helper.onOfferItemClicked(holder.getAdapterPosition()));
    }

    private String getDiscountPercent(TestsModel model) {
        int percent = 0;
        try {

            int price = Integer.parseInt(model.getPrice());
            int discount = Integer.parseInt(model.getDiscount());

            percent = (discount * 100) / price;

        } catch (NumberFormatException nfe) {
            Log.e(TAG, "getDiscountPercent: ", nfe);
        }
        return String.valueOf(percent);
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnOfferItemClickedHelper {
        void onOfferItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private RatingBar ratingBar;
        private TextView nameTv, dcNameTv, reviewsTv, originalPriceTv, offerPriceTv, discTv;

        public VH(@NonNull View itemView) {
            super(itemView);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            nameTv = itemView.findViewById(R.id.nameTv);
            dcNameTv = itemView.findViewById(R.id.dcNameTv);
            reviewsTv = itemView.findViewById(R.id.reviewsTv);
            originalPriceTv = itemView.findViewById(R.id.priceTv);
            offerPriceTv = itemView.findViewById(R.id.offerPriceTv);
            discTv = itemView.findViewById(R.id.discountTv);
        }
    }
}
