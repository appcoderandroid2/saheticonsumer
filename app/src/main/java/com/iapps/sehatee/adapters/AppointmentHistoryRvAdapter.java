package com.iapps.sehatee.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.AppointmentsModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.ACCEPTED;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.REJECTED;
import static com.iapps.sehatee.utils.Constants.REQUESTED;
import static com.iapps.sehatee.utils.Constants.getFormattedDateNoYear;
import static com.iapps.sehatee.utils.Constants.getFormattedTime;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class AppointmentHistoryRvAdapter extends RecyclerView.Adapter<AppointmentHistoryRvAdapter.VH> {

    private List<AppointmentsModel> list;
    private OnItemSelectedHelper helper;

    public AppointmentHistoryRvAdapter(List<AppointmentsModel> list, OnItemSelectedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_appointment_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        AppointmentsModel model = list.get(position);
        Context ctx = holder.itemView.getContext();

        Picasso.get()
                .load(model.getDoctorNurseDetails().getImageLink().isEmpty() ? NA : model.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(holder.iv);

        holder.nameTv.setText(model.getDoctorNurseDetails().getName());
        holder.specialty.setText(String.format(Locale.getDefault(), "(%s)", model.getDoctorNurseDetails().getSpecialty()));
        holder.appointmentNumber.setText(model.getAppointmentNo());
        holder.address.setText(String.format(Locale.getDefault(), "%s, %s, %s, %s, %s, %s",
                model.getClinicDetails().getName(),
                model.getClinicDetails().getAddress(),
                model.getClinicDetails().getAreaName(),
                model.getClinicDetails().getCityName(),
                model.getClinicDetails().getCountryName(),
                model.getClinicDetails().getPostalCode()
        ));
        holder.dateTime.setText(String.format(Locale.getDefault(), "%s : %s, %s - %s",
                holder.itemView.getResources().getString(R.string.appointment_date),
                getFormattedDateNoYear(model.getAppointmentDate()),
                getFormattedTime(model.getFromTime()),
                getFormattedTime(model.getToTime())
        ));

        holder.status.setText(model.getStatus());
        if (model.getStatus().equals(REQUESTED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorAccent, ctx.getTheme()));
        } else if (model.getStatus().equals(ACCEPTED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.thumbsUpGreen, ctx.getTheme()));
        } else if (model.getStatus().equals(REJECTED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.notificationBadgeColor, ctx.getTheme()));
        } else {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimary, ctx.getTheme()));
        }


        holder.itemView.setOnClickListener(v -> helper.onItemSelected(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemSelectedHelper {
        void onItemSelected(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView iv;
        private TextView nameTv, specialty, address, dateTime, status, appointmentNumber;

        public VH(@NonNull View itemView) {
            super(itemView);

            iv = itemView.findViewById(R.id.iv);
            nameTv = itemView.findViewById(R.id.nameTv);
            specialty = itemView.findViewById(R.id.specialtyTv);
            address = itemView.findViewById(R.id.addressTv);
            dateTime = itemView.findViewById(R.id.dateTimeTv);
            status = itemView.findViewById(R.id.statusTv);
            appointmentNumber = itemView.findViewById(R.id.appointmentNumberTv);

        }
    }
}