package com.iapps.sehatee.adapters;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.TestsModel;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getDiscountedPrice;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class SearchTestsRvAdapter extends RecyclerView.Adapter<SearchTestsRvAdapter.VH> {
    private List<TestsModel> list;
    private OnItemClickedHelper helper;

    public SearchTestsRvAdapter(List<TestsModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tests_horizontal, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.nameTv.setText(list.get(position).getTestName());

        holder.originalPriceTv.setText(String.format(Locale.getDefault(), "%s%s", list.get(position).getCurrencySymbol(), list.get(position).getPrice()));

        holder.offerPriceTv.setText(String.format(Locale.getDefault(), "%s%s", list.get(position).getCurrencySymbol(),
                (getDiscountedPrice(Integer.parseInt(list.get(position).getPrice()),
                        Integer.parseInt(list.get(position).getDiscount()),
                        list.get(position).getDiscountType()))));

        holder.ratingBar.setRating(list.get(position).getRating());
        holder.reviewsTv.setText(String.format(Locale.getDefault(), "%s %s", list.get(position).getTotalReview(), holder.itemView.getResources().getString(R.string.reviews)));

        if (Integer.parseInt(list.get(position).getDiscount()) > 0) {
            holder.offerPriceTv.setVisibility(View.VISIBLE);
            holder.originalPriceTv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(), R.color.metaTextColor, null));
            holder.originalPriceTv.setPaintFlags(holder.originalPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {

            holder.originalPriceTv.setPaintFlags(holder.originalPriceTv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.offerPriceTv.setVisibility(View.GONE);
            holder.originalPriceTv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(), R.color.titleTextColor, null));
        }

        holder.favIv.setImageResource(list.get(position).isFav() ? R.drawable.ic_round_favorite_24 : R.drawable.ic_round_favorite_border_24);

        holder.itemView.setOnClickListener((v) -> helper.onItemClicked(holder.getAdapterPosition()));
        holder.favIv.setOnClickListener((v) -> helper.onFavClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.favIv.setOnClickListener(null);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);

        void onFavClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView favIv;
        private RatingBar ratingBar;
        private TextView nameTv, originalPriceTv, offerPriceTv, reviewsTv;

        public VH(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.nameTv);
            originalPriceTv = itemView.findViewById(R.id.priceTv);
            offerPriceTv = itemView.findViewById(R.id.offerPriceTv);
            favIv = itemView.findViewById(R.id.favIv);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            reviewsTv = itemView.findViewById(R.id.reviewsTv);
        }
    }
}
