package com.iapps.sehatee.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.DoctorNurseListModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getFormattedDateNoYear;
import static com.iapps.sehatee.utils.Constants.getPrefs;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class DoctorNurseSearchListRvAdapter extends RecyclerView.Adapter<DoctorNurseSearchListRvAdapter.VH> {
    private List<DoctorNurseListModel> list;
    private OnItemClickedHelper helper;

    public DoctorNurseSearchListRvAdapter(List<DoctorNurseListModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_doc_nurse_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        Context ctx = holder.itemView.getContext();

        Picasso.get()
                .load(list.get(position).getImageLink().isEmpty() ? "NA" : list.get(position).getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(holder.iv);

        holder.name.setText(list.get(position).getName());
        holder.specialty.setText(String.format(Locale.getDefault(), "%s", list.get(position).getSpecialty()));
        holder.ratingBar.setRating(list.get(position).getRating());
        holder.visitorCount.setText(String.format(Locale.getDefault(), "%s %s", list.get(position).getVisitorCount(), ctx.getString(R.string.visitors)));
        holder.fee.setText(String.format(Locale.getDefault(), "%s : %s%s", ctx.getString(R.string.fees), getPrefs().getCurrencySymbol(), list.get(position).getFees()));
        holder.availability.setText(String.format(Locale.getDefault(), "%s : %s", ctx.getString(R.string.availability), getFormattedDateNoYear(list.get(position).getAvailable())));

        holder.view.setOnClickListener(v -> helper.onViewClicked(holder.getAdapterPosition()));
        holder.call.setOnClickListener(v -> helper.onCallClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.view.setOnClickListener(null);
        holder.call.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onViewClicked(int pos);

        void onCallClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private ImageView iv;
        private RatingBar ratingBar;
        private TextView name, specialty, visitorCount, fee, availability, call, view;

        public VH(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            name = itemView.findViewById(R.id.nameTv);
            specialty = itemView.findViewById(R.id.specialtyTv);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            visitorCount = itemView.findViewById(R.id.visitorCountTv);
            fee = itemView.findViewById(R.id.feesTv);
            availability = itemView.findViewById(R.id.availableTv);
            call = itemView.findViewById(R.id.callTv);
            view = itemView.findViewById(R.id.viewProfileTv);
        }
    }
}
