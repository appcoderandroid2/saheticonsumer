package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.DoctorNurseListModel;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.getPrefs;

/**
 * Created by jpdbiet on 03,July,2020
 */
public class NurseExpertiseRvAdapter extends RecyclerView.Adapter<NurseExpertiseRvAdapter.VH> {
    private List<DoctorNurseListModel.Specialty> list;
    private OnItemClickedHelper helper;

    public NurseExpertiseRvAdapter(List<DoctorNurseListModel.Specialty> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_nurse_expertise, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        holder.name.setText(list.get(position).getSpecialty());
        holder.fees.setText(String.format(Locale.getDefault(), "%s : %s%s",
                holder.itemView.getResources().getString(R.string.fees),
                getPrefs().getCurrencySymbol(),
                list.get(position).getFees()
        ));

        holder.bookNowBtn.setOnClickListener(v -> helper.onBookNowClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.bookNowBtn.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onBookNowClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private TextView name, fees;
        private Button bookNowBtn;

        public VH(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameTv);
            fees = itemView.findViewById(R.id.feesTv);
            bookNowBtn = itemView.findViewById(R.id.bookNowBtn);
        }
    }
}
