package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.CityModel;

import java.util.List;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class CityListRvAdapter extends RecyclerView.Adapter<CityListRvAdapter.VH> {
    private List<CityModel> list;
    private OnItemClickedHelper helper;

    public CityListRvAdapter(List<CityModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_city_area, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        holder.nameRb.setText(list.get(position).getCityName());
        holder.nameRb.setChecked(list.get(position).isChecked());

        holder.nameRb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                helper.onItemChecked(holder.getAdapterPosition());
                holder.itemView.performClick();
            }
        });
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.nameRb.setOnCheckedChangeListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemChecked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private RadioButton nameRb;

        public VH(@NonNull View itemView) {
            super(itemView);
            nameRb = itemView.findViewById(R.id.nameRb);
        }
    }
}
