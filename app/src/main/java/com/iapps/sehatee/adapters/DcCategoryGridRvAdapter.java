package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 21,June,2020
 */
public class DcCategoryGridRvAdapter extends RecyclerView.Adapter<DcCategoryGridRvAdapter.VH> {

    private List<CategoryModel> list;
    private OnItemClickedHelper helper;

    public DcCategoryGridRvAdapter(List<CategoryModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dc_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        Picasso.get()
                .load(list.get(position).getImageLink().isEmpty() ? NA : list.get(position).getImageLink())
                .error(R.drawable.bg_transparent)
                .into(holder.iv);
        holder.tv.setText(list.get(position).getCategoryName());
        holder.itemView.setOnClickListener(v -> helper.onCategoryItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onCategoryItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView iv;
        private TextView tv;

        public VH(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            tv = itemView.findViewById(R.id.tv);
        }
    }
}
