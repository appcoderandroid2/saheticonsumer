package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.DocumentModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.iapps.sehatee.utils.Constants.CANCELED;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.REJECTED;

/**
 * Created by jpdbiet on 01,July,2020
 */
public class AppointmentReportsRvAdapter extends RecyclerView.Adapter<AppointmentReportsRvAdapter.VH> {
    private List<DocumentModel> list;
    private OnItemClickedHelper helper;
    private String status;

    public AppointmentReportsRvAdapter(List<DocumentModel> list, OnItemClickedHelper helper, String status) {
        this.list = list;
        this.helper = helper;
        this.status = status;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_images, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        Picasso.get()
                .load(list.get(position).getUrl().isEmpty() ? NA : list.get(position).getUrl())
                .error(R.drawable.no_image_found)
                .into(holder.iv);

        holder.itemView.setOnClickListener((v) -> helper.onReportClicked(holder.getAdapterPosition()));
        if (status.equals(CANCELED) || status.equals(REJECTED)) {
            holder.deleteIv.setOnClickListener(null);
            holder.deleteIv.setVisibility(View.GONE);
        } else {
            holder.deleteIv.setVisibility(list.get(position).isRemovable() ? View.VISIBLE : View.GONE);
            holder.deleteIv.setOnClickListener((v) -> helper.onReportDeleteClicked(holder.getAdapterPosition()));
        }

        holder.deleteIv.setVisibility(list.get(position).isRemovable() ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onViewRecycled(@NonNull AppointmentReportsRvAdapter.VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
        holder.deleteIv.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onReportClicked(int pos);

        void onReportDeleteClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private ImageView iv, deleteIv;

        public VH(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            deleteIv = itemView.findViewById(R.id.deleteIv);
        }
    }
}
