package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.DoctorNurseSpecialtyModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 15,June,2020
 */
public class DoctorNurseSpecialtyRvAdapter extends RecyclerView.Adapter<DoctorNurseSpecialtyRvAdapter.VH> {
    private List<DoctorNurseSpecialtyModel> list;
    private List<DoctorNurseSpecialtyModel> filteredList;
    private OnItemClickedHelper helper;
    private MyFilter myFilter = new MyFilter();

    public DoctorNurseSpecialtyRvAdapter(List<DoctorNurseSpecialtyModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.filteredList = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_doctor_nurse_specialty, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        if (list.get(position).isSelected()) {
            holder.tv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(), R.color.colorAccent, null));
        } else {
            holder.tv.setTextColor(ResourcesCompat.getColor(holder.itemView.getResources(), R.color.descTextColor, null));
        }

        Picasso.get()
                .load(filteredList.get(position).getImageLink().isEmpty() ? NA : list.get(position).getImageLink())
                .error(R.drawable.logo_128)
                .into(holder.iv);
        holder.tv.setText(filteredList.get(position).getSpecialty());

        holder.itemView.setOnClickListener(v -> helper.onItemSelected(list.indexOf(filteredList.get(holder.getAdapterPosition()))));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public MyFilter getFilter() {
        return myFilter;
    }

    public interface OnItemClickedHelper {
        void onItemSelected(int pos);

        void afterFilterUpdateUI(int size);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private TextView tv;
        private ImageView iv;

        public VH(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            tv = itemView.findViewById(R.id.tv);
        }
    }

    public class MyFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence key) {

            FilterResults result = new FilterResults();
            String query = key.toString().toLowerCase();

            List<DoctorNurseSpecialtyModel> tmpList = new ArrayList<>();

            if (query.isEmpty()) {
                tmpList.addAll(list);
            } else {
                for (DoctorNurseSpecialtyModel model : list) {
                    String str = model.getSpecialty().toLowerCase();
                    if (str.contains(query) || str.startsWith(query) || str.endsWith(query)) {
                        tmpList.add(model);
                    }
                }
            }

            result.values = tmpList;
            result.count = tmpList.size();

            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredList = (List<DoctorNurseSpecialtyModel>) results.values;
            notifyDataSetChanged();
            helper.afterFilterUpdateUI(results.count);
        }
    }
}
