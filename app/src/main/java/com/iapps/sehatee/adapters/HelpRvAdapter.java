package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.HelpModel;

import java.util.List;

/**
 * Created by jpdbiet on 20,June,2020
 */
public class HelpRvAdapter extends RecyclerView.Adapter<HelpRvAdapter.VH> {
    private List<HelpModel> list;
    private OnItemClickedHelper helper;

    public HelpRvAdapter(List<HelpModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_help, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.ansCard.setVisibility(list.get(position).isExpanded() ? View.VISIBLE : View.GONE);
        holder.iv.setImageResource(list.get(position).isExpanded() ? R.drawable.ic_round_remove_circle_gray_24 : R.drawable.ic_round_add_circle_accent_24);

        holder.question.setText(list.get(position).getQuestion());
        holder.answer.setText(list.get(position).getAnswer());

        holder.questCard.setOnClickListener(v -> helper.onItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.questCard.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private ImageView iv;
        private TextView question, answer;
        private CardView questCard, ansCard;

        public VH(@NonNull View itemView) {
            super(itemView);

            iv = itemView.findViewById(R.id.iv);
            questCard = itemView.findViewById(R.id.quesCard);
            ansCard = itemView.findViewById(R.id.ansCard);
            question = itemView.findViewById(R.id.questionTv);
            answer = itemView.findViewById(R.id.ansTv);

        }
    }
}
