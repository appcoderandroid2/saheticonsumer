package com.iapps.sehatee.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.AppointmentsModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.ACCEPTED;
import static com.iapps.sehatee.utils.Constants.CANCELED;
import static com.iapps.sehatee.utils.Constants.DOCTOR;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.REJECTED;
import static com.iapps.sehatee.utils.Constants.REQUESTED;
import static com.iapps.sehatee.utils.Constants.getFormattedDateNoYear;
import static com.iapps.sehatee.utils.Constants.getFormattedDateTime;
import static com.iapps.sehatee.utils.Constants.getFormattedTime;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class AppointmentsRvAdapter extends RecyclerView.Adapter<AppointmentsRvAdapter.VH> {

    private List<AppointmentsModel> list;
    private OnItemSelectedHelper helper;

    public AppointmentsRvAdapter(List<AppointmentsModel> list, OnItemSelectedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_appointments, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        AppointmentsModel model = list.get(position);
        Context ctx = holder.itemView.getContext();

        Picasso.get()
                .load(model.getDoctorNurseDetails().getImageLink().isEmpty() ? NA : model.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(holder.iv);

        holder.nameTv.setText(model.getDoctorNurseDetails().getName());
        holder.specialty.setText(String.format(Locale.getDefault(), "(%s)", model.getDoctorNurseDetails().getSpecialty()));
        holder.appointmentNumber.setText(model.getAppointmentNo());

        if (model.getDoctorNurseDetails().getUserType().equals(DOCTOR)) {

            String address = model.getClinicDetails().getAddress().equals(NA) ? "" : model.getClinicDetails().getAddress();
            address += model.getClinicDetails().getAreaName().equals(NA) ? "" : ", " + model.getClinicDetails().getAreaName();
            address += model.getClinicDetails().getCityName().equals(NA) ? "" : ", " + model.getClinicDetails().getCityName();
            address += model.getClinicDetails().getPostalCode().equals(NA) ? "" : ", " + model.getClinicDetails().getPostalCode();
            address += model.getClinicDetails().getCountryName().equals(NA) ? "" : ", " + model.getClinicDetails().getCountryName();

            address = address.isEmpty() ? NA : address;

            holder.address.setText(address);
            holder.dateTime.setText(String.format(Locale.getDefault(), "%s : %s, %s - %s",
                    holder.itemView.getResources().getString(R.string.appointment_date),
                    getFormattedDateNoYear(model.getAppointmentDate()),
                    getFormattedTime(model.getFromTime()),
                    getFormattedTime(model.getToTime())
            ));

            holder.ph.setText(model.getClinicDetails().getPhone());
        } else {
            String address = model.getDoctorNurseDetails().getAddress().equals(NA) ? "" : model.getClinicDetails().getAddress();
            address += model.getDoctorNurseDetails().getAreaName().equals(NA) ? "" : ", " + model.getClinicDetails().getAreaName();
            address += model.getDoctorNurseDetails().getCityName().equals(NA) ? "" : ", " + model.getClinicDetails().getCityName();
            address += model.getDoctorNurseDetails().getCountryName().equals(NA) ? "" : ", " + model.getClinicDetails().getCountryName();
            address += model.getDoctorNurseDetails().getPostalCode().equals(NA) ? "" : ", " + model.getClinicDetails().getPostalCode();

            address = address.isEmpty() ? NA : address;

            holder.address.setText(address);

            holder.dateTime.setText(String.format(Locale.getDefault(), "%s : %s",
                    holder.itemView.getResources().getString(R.string.appointment_date),
                    getFormattedDateTime(model.getAppointmentDate() + (model.getAppointmentTime() == null ? "" : " " + model.getAppointmentTime()))
            ));

            holder.ph.setText(model.getDoctorNurseDetails().getPhone());

        }

        holder.status.setText(model.getStatus());

        if (model.getStatus().equals(REQUESTED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorAccent, ctx.getTheme()));
        } else if (model.getStatus().equals(ACCEPTED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.thumbsUpGreen, ctx.getTheme()));
        } else if (model.getStatus().equals(REJECTED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.notificationBadgeColor, ctx.getTheme()));
        } else if (model.getStatus().equals(CANCELED)) {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimaryDark, ctx.getTheme()));
        } else {
            holder.status.setTextColor(ResourcesCompat.getColor(ctx.getResources(), R.color.colorPrimary, ctx.getTheme()));
        }

        holder.itemView.setOnClickListener(v -> helper.onItemSelected(holder.getAdapterPosition()));

    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemSelectedHelper {
        void onItemSelected(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView iv;
        private TextView nameTv, specialty, address, dateTime, ph, status, appointmentNumber;
        private CardView cardView;

        public VH(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card);
            iv = itemView.findViewById(R.id.iv);
            nameTv = itemView.findViewById(R.id.nameTv);
            specialty = itemView.findViewById(R.id.specialtyTv);
            address = itemView.findViewById(R.id.addressTv);
            dateTime = itemView.findViewById(R.id.dateTimeTv);
            ph = itemView.findViewById(R.id.phTv);
            status = itemView.findViewById(R.id.statusTv);
            appointmentNumber = itemView.findViewById(R.id.appointmentNumberTv);

        }
    }
}