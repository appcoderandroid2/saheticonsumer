package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.AppointmentsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;

/**
 * Created by jpdbiet on 27,July,2020
 */
public class ReportVisibilityRvAdapter extends RecyclerView.Adapter<ReportVisibilityRvAdapter.VH> {

    private List<AppointmentsModel> list;
    private OnItemClickedHelper helper;

    public ReportVisibilityRvAdapter(List<AppointmentsModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_share_report, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        AppointmentsModel model = list.get(position);

        Picasso.get()
                .load(model.getDoctorNurseDetails().getImageLink().isEmpty() ? NA : model.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(holder.iv);
        holder.nameTv.setText(model.getDoctorNurseDetails().getName());
        holder.appointmentNumberTv.setText(model.getAppointmentNo());
        holder.dateTv.setText(getFormattedDate(model.getAppointmentDate()));

        holder.deleteIv.setOnClickListener(v -> helper.onDeleteClicked(holder.getAdapterPosition()));

        holder.itemView.setOnClickListener(v -> helper.onItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.deleteIv.setOnClickListener(null);
        holder.itemView.setOnClickListener(null);
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);

        void onDeleteClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView iv, deleteIv;
        private TextView nameTv, dateTv, appointmentNumberTv;

        public VH(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            deleteIv = itemView.findViewById(R.id.deleteIv);
            nameTv = itemView.findViewById(R.id.nameTv);
            dateTv = itemView.findViewById(R.id.appointmentDateTv);
            appointmentNumberTv = itemView.findViewById(R.id.appointmentNumberTv);
        }
    }
}
