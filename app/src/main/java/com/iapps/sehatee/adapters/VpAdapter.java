package com.iapps.sehatee.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpdbiet on 22,April,2020
 */
public class VpAdapter extends FragmentPagerAdapter {

    public static final String adapterPosition = "position";

    private List<Pair<Fragment, String>> frags = new ArrayList<>();

    public VpAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    public void addFrag(Fragment frag, String pageTitle) {
        frags.add(Pair.create(frag, pageTitle));
    }

    public void clearFrags() {
        frags.clear();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Bundle bundle;
        Fragment frag = frags.get(position).first;
        if (frag.getArguments() != null) {
            bundle = frag.getArguments();
        } else {
            bundle = new Bundle();
        }
        bundle.putInt(adapterPosition, position);
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public int getCount() {
        return frags.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return frags.get(position).second;
    }

}
