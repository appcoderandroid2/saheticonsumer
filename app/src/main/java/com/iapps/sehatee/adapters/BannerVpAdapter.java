package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.BannerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.iapps.sehatee.utils.Constants.NA;

/**
 * Created by jpdbiet on 27,July,2020
 */
public class BannerVpAdapter extends PagerAdapter {

    private List<BannerModel> list = new ArrayList<>();
    private OnBannerClickedHelper helper;

    public BannerVpAdapter(List<BannerModel> list, OnBannerClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        final int pos = position;
        View card = LayoutInflater.from(container.getContext()).inflate(R.layout.row_banner, container, false);

        ImageView iv = card.findViewById(R.id.iv);
        Picasso.get()
                .load(list.get(pos).getImageLink().isEmpty() ? NA : list.get(pos).getImageLink())
                .noPlaceholder()
                .into(iv);

        card.setOnClickListener(v -> helper.onBannerClicked(pos));
        container.addView(card);

        return card;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ((View) object).setOnClickListener(null);
        container.removeView((View) object);
    }

    public interface OnBannerClickedHelper {
        void onBannerClicked(int pos);
    }
}
