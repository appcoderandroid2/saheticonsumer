package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ReportModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getFormattedDateTime;

/**
 * Created by jpdbiet on 20,June,2020
 */
public class ReportsRvAdapter extends RecyclerView.Adapter<ReportsRvAdapter.VH> {
    private List<ReportModel> list;
    private OnItemClickedHelper helper;

    public ReportsRvAdapter(List<ReportModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reports, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        ReportModel model = list.get(position);

        Picasso.get()
                .load(model.getImageLink().isEmpty() ? NA : model.getImageLink())
                .error(R.drawable.no_image_found)
                .into(holder.iv);

        holder.nameTv.setText(model.getName());
        holder.dcNameTv.setText(String.format(Locale.getDefault(), "%s - %s", holder.itemView.getResources().getString(R.string.offered_by), model.getDcInfo().getName()));
        holder.dateTimeTv.setText(String.format(Locale.getDefault(), "%s : %s", holder.itemView.getResources().getString(R.string.appointment_date), getFormattedDateTime(model.getDateTime())));
        holder.statusTv.setText(String.format(Locale.getDefault(), "%s : %s", holder.itemView.getResources().getString(R.string.report_status), model.getStatus().toUpperCase()));

        holder.itemView.setOnClickListener(v -> helper.onItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private ImageView iv;
        private TextView nameTv, dcNameTv, dateTimeTv, statusTv;

        public VH(@NonNull View itemView) {
            super(itemView);

            iv = itemView.findViewById(R.id.iv);
            nameTv = itemView.findViewById(R.id.nameTv);
            dcNameTv = itemView.findViewById(R.id.dcNameTv);
            dateTimeTv = itemView.findViewById(R.id.dateTimeTv);
            statusTv = itemView.findViewById(R.id.statusTv);

        }
    }
}
