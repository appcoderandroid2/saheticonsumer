package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.NotificationModel;

import java.util.List;

import static com.iapps.sehatee.utils.Constants.getFormattedDateTime;

/**
 * Created by jpdbiet on 20,June,2020
 */
public class NotificationRvAdapter extends RecyclerView.Adapter<NotificationRvAdapter.VH> {
    private List<NotificationModel> list;
    private OnItemClickedHelper helper;

    public NotificationRvAdapter(List<NotificationModel> list, OnItemClickedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        holder.title.setText(list.get(position).getTitle());
        holder.date.setText(getFormattedDateTime(list.get(position).getDate()));
        holder.msg.setText(list.get(position).getMsg());
        holder.itemView.setOnClickListener(v -> helper.onItemClicked(holder.getAdapterPosition()));
    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {

        private TextView title, date, msg;

        public VH(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.titleTv);
            date = itemView.findViewById(R.id.dateTv);
            msg = itemView.findViewById(R.id.msgTv);

        }
    }
}
