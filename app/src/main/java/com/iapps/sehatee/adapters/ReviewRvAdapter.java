package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.ReviewModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import static com.iapps.sehatee.utils.Constants.CUSTOMER;
import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getFormattedDateTime;

/**
 * Created by jpdbiet on 21,June,2020
 */
public class ReviewRvAdapter extends RecyclerView.Adapter<ReviewRvAdapter.VH> {

    private List<ReviewModel> list;
    private OnItemClickedHelper helper;
    private String userType;

    public ReviewRvAdapter(List<ReviewModel> list, OnItemClickedHelper helper, String userType) {
        this.list = list;
        this.helper = helper;
        this.userType = userType;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_review, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        ReviewModel model = list.get(position);

        Picasso.get()
                .load(model.getUser().getImageLink().isEmpty() ? NA : model.getUser().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(holder.iv);

        holder.nameTv.setText(model.getUser().getName());
        holder.subtitleTv.setText(userType.equals(CUSTOMER) ? model.getUser().getSpecialty() : getFormattedDateTime(model.getDate()));
        holder.ratingBar.setRating(model.getRating());
        holder.ratingTv.setText(String.format(Locale.getDefault(), "%.1f", model.getRating()));
        holder.reviewTv.setText(model.getReview());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickedHelper {
        void onItemClicked(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView iv;
        private TextView nameTv, subtitleTv, ratingTv, reviewTv;
        //        private EmojiAppCompatTextView reviewTv;
        private RatingBar ratingBar;

        public VH(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            nameTv = itemView.findViewById(R.id.nameTv);
            subtitleTv = itemView.findViewById(R.id.subtitleTv);
            ratingTv = itemView.findViewById(R.id.ratingTv);
            reviewTv = itemView.findViewById(R.id.reviewTv);
        }
    }
}
