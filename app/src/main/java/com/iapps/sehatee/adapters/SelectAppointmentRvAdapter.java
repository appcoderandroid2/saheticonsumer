package com.iapps.sehatee.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iapps.sehatee.R;
import com.iapps.sehatee.models.AppointmentsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.iapps.sehatee.utils.Constants.NA;
import static com.iapps.sehatee.utils.Constants.getFormattedDate;

/**
 * Created by jpdbiet on 17,June,2020
 */
public class SelectAppointmentRvAdapter extends RecyclerView.Adapter<SelectAppointmentRvAdapter.VH> {

    private List<AppointmentsModel> list;
    private OnItemSelectedHelper helper;

    public SelectAppointmentRvAdapter(List<AppointmentsModel> list, OnItemSelectedHelper helper) {
        this.list = list;
        this.helper = helper;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_share_report, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {

        AppointmentsModel model = list.get(position);

        Picasso.get()
                .load(model.getDoctorNurseDetails().getImageLink().isEmpty() ? NA : model.getDoctorNurseDetails().getImageLink())
                .error(R.drawable.ic_default_user_image)
                .into(holder.iv);
        holder.nameTv.setText(model.getDoctorNurseDetails().getName());
        holder.appointmentNumberTv.setText(model.getAppointmentNo());
        holder.dateTv.setText(getFormattedDate(model.getAppointmentDate()));

        holder.deleteIv.setVisibility(View.GONE);

        holder.container.setBackgroundResource(model.isSelected() ? R.drawable.bg_white_with_simple_accent_border : R.drawable.bg_white_with_simple_border_with_ripple);

        holder.itemView.setOnClickListener(v -> helper.onItemSelected(holder.getAdapterPosition()));

    }

    @Override
    public void onViewRecycled(@NonNull VH holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemSelectedHelper {
        void onItemSelected(int pos);
    }

    public static class VH extends RecyclerView.ViewHolder {
        private ImageView iv, deleteIv;
        private TextView nameTv, dateTv, appointmentNumberTv;
        private LinearLayout container;

        public VH(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            iv = itemView.findViewById(R.id.iv);
            deleteIv = itemView.findViewById(R.id.deleteIv);
            nameTv = itemView.findViewById(R.id.nameTv);
            dateTv = itemView.findViewById(R.id.appointmentDateTv);
            appointmentNumberTv = itemView.findViewById(R.id.appointmentNumberTv);
        }
    }
}